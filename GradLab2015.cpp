﻿//------------------------------------------------------------------------------
// <copyright file="KinectFusionBasics.cpp" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

// System includes
#define NOMINMAX
#include "stdafx.h"
#include <Windows.h>
#include <Kinect.h>
#include <pcl/visualization/cloud_viewer.h>
#include "GradLab2015.h"
#include "ICP.h"
#include <iostream>
#include "string.h"
#include <conio.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>

using namespace std;

/// Main Function
int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE, _In_ LPWSTR, _In_ int nCmdShow)
{
	int status;

	//char opt = printMenu();
	char opt = '3';

	switch(opt) {
		case '1':
			status = initializeSensor();
			status = livePointCloud(0);
			break;
		case '2':
			status = initializeSensor();
			status = livePointCloud(1);
			break;
		case '3':
			status = mergeClouds();
			break;
		case '4':
			status = shutDownSensor();
			break;

	}

	return 0;
}

int mergeClouds() {
	
	OutputDebugString(L"Merging Point Clouds...\n");

	pcl::PointCloud <pcl::PointXYZRGB> ::Ptr cloudFront(new  pcl::PointCloud <pcl::PointXYZRGB>());
	pcl::PointCloud <pcl::PointXYZRGB> ::Ptr cloudBack(new  pcl::PointCloud <pcl::PointXYZRGB>());
	pcl::PointCloud <pcl::PointXYZRGB> ::Ptr cloudLeft(new  pcl::PointCloud <pcl::PointXYZRGB>());
	pcl::PointCloud <pcl::PointXYZRGB> ::Ptr cloudRight(new  pcl::PointCloud <pcl::PointXYZRGB>());
	
	pcl::PointCloud <pcl::PointXYZRGB> ::Ptr final(new  pcl::PointCloud <pcl::PointXYZRGB>());


	pcl::io::loadPLYFile("data/Organized Data/eon0.ply", *cloudFront);
	pcl::io::loadPLYFile("data/Organized Data/eon45.ply", *cloudLeft);

	//*final += *cloudFront;
	//zFilter(cloudFront, final);
	icpMain(final, cloudFront, cloudLeft);

	//*final += *transform;
	
	pcl::io::savePLYFile("data/temp.ply", *final, true);
	return 1;
}


int initializeSensor() {
	
	OutputDebugString(L"\nInitializng camera...\n");
	hResult = GetDefaultKinectSensor(&pSensor);
	if (FAILED(hResult)) {
		std::cerr << "Error: GetDefaultKinectSensor" << std::endl;
		return -1;
	}

	// Open Sensor
	hResult = pSensor->Open();
	if (FAILED(hResult)) {
		std::cerr << "Error: IKinectSensor :: Open ()" << std::endl;
		return -1;
	}

	// Retrieved Coordinate Mapper
	hResult = pSensor->get_CoordinateMapper(&pCoordinateMapper);
	if (FAILED(hResult)) {
		std::cerr << "Error: IKinectSensor :: Get_CoordinateMapper ()" << std::endl;
		return -1;
	}

	// Retrieved Color Frame Source
	hResult = pSensor->get_ColorFrameSource(&pColorSource);
	if (FAILED(hResult)) {
		std::cerr << "Error: IKinectSensor :: Get_ColorFrameSource ()" << std::endl;
		return -1;
	}

	// Retrieved Depth Frame Source
	hResult = pSensor->get_DepthFrameSource(&pDepthSource);
	if (FAILED(hResult)) {
		std::cerr << "Error: IKinectSensor :: Get_DepthFrameSource ()" << std::endl;
		return -1;
	}

	// Open Color Frame Reader
	hResult = pColorSource->OpenReader(&pColorReader);
	if (FAILED(hResult)) {
		std::cerr << "Error: IColorFrameSource :: OpenReader ()" << std::endl;
		return -1;
	}

	// Open Depth Frame Reader
	hResult = pDepthSource->OpenReader(&pDepthReader);
	if (FAILED(hResult)) {
		std::cerr << "Error: IDepthFrameSource :: OpenReader ()" << std::endl;
		return -1;
	}

	// Retrieved Color Frame Size
	hResult = pColorSource->get_FrameDescription(&pColorDescription);
	if (FAILED(hResult)) {
		std::cerr << "Error: IColorFrameSource :: Get_FrameDescription ()" << std::endl;
		return -1;
	}

	pColorDescription->get_Width(&colorWidth); // 1920
	pColorDescription->get_Height(&colorHeight); // 1080

	// Retrieved Depth Frame Size
	hResult = pDepthSource->get_FrameDescription(&pDepthDescription);
	if (FAILED(hResult)) {
		std::cerr << "Error: IDepthFrameSource :: Get_FrameDescription ()" << std::endl;
		return -1;
	}

	pDepthDescription->get_Width(&depthWidth); // 512
	pDepthDescription->get_Height(&depthHeight); // 424

	OutputDebugString(L"Camera Initialized.\n");
	return 1;
}

int shutDownSensor() {
	// End Processing
	safeRelease(pColorSource);
	safeRelease(pDepthSource);
	safeRelease(pColorReader);
	safeRelease(pDepthReader);
	safeRelease(pColorDescription);
	safeRelease(pDepthDescription);
	safeRelease(pCoordinateMapper);
	if (pSensor) {
		pSensor->Close();
	}
	safeRelease(pSensor);
	return 1;
}

int livePointCloud(int storeFile) {

	std::vector <RGBQUAD> colorBuffer(colorWidth * colorHeight); // To Reserve Color Frame Buffer
	std::vector <UINT16> depthBuffer(depthWidth * depthHeight); // To Reserve Depth Frame Buffer

	while (!viewer.wasStopped()) {

		// Create Point Cloud
		pointCloud->clear();

		// Acquire Latest Color Frame
		IColorFrame * pColorFrame = nullptr;
		hResult = pColorReader->AcquireLatestFrame(&pColorFrame);
		if (SUCCEEDED(hResult)) {
			// Retrieved Color Data
			hResult = pColorFrame->CopyConvertedFrameDataToArray(colorBuffer.size() * sizeof(RGBQUAD), reinterpret_cast <BYTE *> (&colorBuffer[0]), ColorImageFormat::ColorImageFormat_Bgra);
			if (FAILED(hResult)) {
				std::cerr << "Error: IColorFrame :: CopyConvertedFrameDataToArray ()" << std::endl;
			}
		}
		safeRelease(pColorFrame);

		// Acquire Latest Depth Frame
		IDepthFrame * pDepthFrame = nullptr;
		hResult = pDepthReader->AcquireLatestFrame(&pDepthFrame);
		if (SUCCEEDED(hResult)) {
			// Retrieved Depth Data
			hResult = pDepthFrame->CopyFrameDataToArray(depthBuffer.size(), &depthBuffer[0]);
			if (FAILED(hResult)) {
				std::cerr << "Error: IDepthFrame :: CopyFrameDataToArray ()" << std::endl;
			}
		}
		safeRelease(pDepthFrame);

		for (int y = 0; y < depthHeight; y++) {
			for (int x = 0; x < depthWidth; x++) {
				pcl::PointXYZRGB point;

				DepthSpacePoint depthSpacePoint = { static_cast <float> (x), static_cast <float> (y) };
				UINT16 depth = depthBuffer[y * depthWidth + x];

				// Coordinate Mapping Depth to Color Space and Camera Space, and Setting PointCloud XYZRGB
				ColorSpacePoint colorSpacePoint = { 0.0f, 0.0f };
				pCoordinateMapper->MapDepthPointToColorSpace(depthSpacePoint, depth, &colorSpacePoint);
				int  colorX = static_cast <int> (std::floor(colorSpacePoint.X + 0.5f));
				int  colorY = static_cast <int> (std::floor(colorSpacePoint.Y + 0.5f));
				CameraSpacePoint cameraSpacePoint = { 0.0f, 0.0f, 0.0f };
				pCoordinateMapper->MapDepthPointToCameraSpace(depthSpacePoint, depth, &cameraSpacePoint);
				if ((0 <= colorX) && (colorX < colorWidth) && (0 <= colorY) && (colorY < colorHeight)) {
					RGBQUAD color = colorBuffer[colorY * colorWidth + colorX];
					point.b = color.rgbBlue;
					point.g = color.rgbGreen;
					point.r = color.rgbRed;
					point.x = cameraSpacePoint.X;
					point.y = cameraSpacePoint.Y;
					point.z = cameraSpacePoint.Z;
				}

				pointCloud->push_back(point);

				
			}
		}
		// Show Point Cloud on Cloud Viewer
		viewer.showCloud(pointCloud);

		// Input Key ( Exit ESC key )
		if (GetKeyState(VK_ESCAPE) < 0) {
			break;
		}
	}

	if (storeFile) {
		OutputDebugString(L"Saving file...\n");
		pointCloud->width = static_cast <uint32_t> (depthWidth);
		pointCloud->height = static_cast <uint32_t> (depthHeight);
		pointCloud->is_dense = false;
		pcl::io::savePLYFile("data/temp.ply", *pointCloud,true);
		OutputDebugString(L"File Saved.\n");
	}

	return 1;
}

int printMenu() {

	system("CLS");
	char result = 'x';
	OutputDebugString(L"\nMenu Options - Select a Number\n");
	OutputDebugString(L"1. Live View - Point Cloud\n");
	OutputDebugString(L"2. Acquire/Store Point Cloud\n");
	OutputDebugString(L"3. Exit\n");

	result = std::getchar();
	while (result=='x') {
		result = std::getchar();
	}

	return result;
}

