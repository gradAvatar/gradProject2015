
#include <pcl/registration/icp.h>
#include <pcl/common/common_headers.h>
#include <pcl/registration/ia_ransac.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/fpfh.h>
#include <pcl/keypoints/harris_3D.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/fast_bilateral.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/io/ply_io.h>
#include "stdafx.h"
#include "ICP.h"

void icpMain(pcl::PointCloud<pcl::PointXYZRGB>::Ptr finalCloud,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr target,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr source) {

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformedTarget(new pcl::PointCloud<pcl::PointXYZRGB>());
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr transformedSource(new pcl::PointCloud<pcl::PointXYZRGB>());
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr filteredTarget(new pcl::PointCloud<pcl::PointXYZRGB>());
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr filteredSource(new pcl::PointCloud<pcl::PointXYZRGB>());
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr roughSource(new pcl::PointCloud<pcl::PointXYZRGB>());
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr icpSource(new pcl::PointCloud<pcl::PointXYZRGB>());

	Eigen::Matrix4f scale;  
	Eigen::Matrix4f roughTransform;
	Eigen::Matrix4f icpTransform;
	Eigen::Matrix4f tform(Eigen::Matrix4f::Identity());

	//******************************
	//* Rough Alignment Parameters *
	//******************************
	int maxIter = 1000;
	float minSampleDistance = 0.01;
	float maxCorrespondenceDistance = 1;
	float transformationEpsilon = 1e-5;
	// Feature Estimation Parameter
	float featureRadius = 1e-2;
	//Surface Normals Estimation Parameter
	float normalSearchRadius = 1e-2;
	// Keypoint Calculation Parameters
	float keypointRadius = 1e-2;
	float threshold = 0.0;
	//Downsample Parameter
	float leafSize = 0.0075f;

	//ICP Registration Parameters
	double id = 0.5; // Max correspondence distance
	int iN = 10000; // Maximum iterations
	float trans_epsilon = 1e-6; // Transformation Epsilon
	float fitness_epsilon = 1e-6; // Euclidian Fitness Epsilon

	//Upsample and "Blur" for noise removal
	OutputDebugString(L"Pre-processing Data...\n");
	preprocess(target, filteredTarget);
	preprocess(source, filteredSource);
	pcl::io::savePLYFile("data/filteredSource.ply", *filteredSource, true);
	pcl::io::savePLYFile("data/filteredTarget.ply", *filteredTarget, true);


	float scalingFactor = getScalingFactor(filteredTarget, filteredSource);

	Eigen::Matrix4f scale_i;
	scale << scalingFactor, 0, 0, 0,
		0, scalingFactor, 0, 0,
		0, 0, scalingFactor, 0,
		0, 0, 0, 1;

	scale_i = scale.inverse();

	pcl::transformPointCloud(*filteredTarget, *transformedTarget, scale);
	pcl::transformPointCloud(*filteredSource, *transformedSource, scale);
	
	
	roughTransform = roughAlignment(transformedTarget, transformedSource, roughSource, maxIter, 
		minSampleDistance, maxCorrespondenceDistance,featureRadius,normalSearchRadius, keypointRadius, 
		threshold, transformationEpsilon);
	
	//pcl::transformPointCloud(*transformedSource, *roughSource, roughTransform);

	icpTransform = icpAlignment(transformedTarget, roughSource, icpSource, id, iN, trans_epsilon, fitness_epsilon);

	//tform = roughTransform * scale;
	//tform = icpTransform * tform;
	//tform = scale_i * tform;

	pcl::transformPointCloud(*icpSource, *finalCloud, scale_i);

	*finalCloud += *filteredTarget;
}

Eigen::Matrix4f roughAlignment(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr target,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr source,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr finalCloud, 
	float minSampleDistance, float maxCorrespondenceDistance , int maxIter, float featureRadius, float normalSearchRadius,
	float keypointRadius, float threshold, float transformationEpsilon) {


	//Setup Initial Alignment Parameters
	OutputDebugString(L"Starting Initial Alignment...\n");
	pcl::SampleConsensusInitialAlignment<pcl::PointXYZRGB, pcl::PointXYZRGB, pcl::FPFHSignature33> sac;
	sac.setMinSampleDistance(minSampleDistance);
	sac.setMaxCorrespondenceDistance(maxCorrespondenceDistance);
	sac.setMaximumIterations(maxIter);
	sac.setTransformationEpsilon(transformationEpsilon);
	sac.setRANSACIterations(maxIter);
	sac.setNumberOfSamples(500);
	sac.setRANSACOutlierRejectionThreshold(1e-6);

	//Calculate Surface Normals
	OutputDebugString(L"Calculating Surface Normals...\n");
	pcl::PointCloud<pcl::Normal>::Ptr targetNormals(new pcl::PointCloud<pcl::Normal>);
	pcl::PointCloud<pcl::Normal>::Ptr sourceNormals(new pcl::PointCloud<pcl::Normal>);
	calculateSurfaceNormals(target, targetNormals,normalSearchRadius);
	calculateSurfaceNormals(source, sourceNormals,normalSearchRadius);

	//Calculate Keypoints
	OutputDebugString(L"Calculating Keypoints...\n");
	pcl::PointCloud<pcl::PointXYZI>::Ptr targetKeypoints(new pcl::PointCloud<pcl::PointXYZI>());
	calculateKeypoints(target, targetNormals, targetKeypoints, keypointRadius, threshold);
	pcl::PointCloud<pcl::PointXYZI>::Ptr sourceKeypoints(new pcl::PointCloud<pcl::PointXYZI>());
	calculateKeypoints(source, sourceNormals, sourceKeypoints, keypointRadius, threshold);
	
	//Calculate Features
	OutputDebugString(L"Calculating Features...\n");
	pcl::PointCloud<pcl::FPFHSignature33>::Ptr targetFeatures(new pcl::PointCloud<pcl::FPFHSignature33>);
	pcl::PointCloud<pcl::FPFHSignature33>::Ptr sourceFeatures(new pcl::PointCloud<pcl::FPFHSignature33>);
	calculateLocalFeatures(target, targetNormals, targetKeypoints, targetFeatures, featureRadius);
	calculateLocalFeatures(source, sourceNormals, sourceKeypoints, sourceFeatures, featureRadius);
	//calculateFeaturesFromNormals(target, targetNormals, targetFeatures, featureRadius);
	//calculateFeaturesFromNormals(source, sourceNormals, sourceFeatures, featureRadius);

	sac.setInputSource(source);
	sac.setSourceFeatures(sourceFeatures);
	
	sac.setInputTarget(target);
	sac.setTargetFeatures(targetFeatures);
	
	OutputDebugString(L"Final Rigid Alignment...\n");

	//pcl::PointCloud<pcl::PointXYZRGB>::Ptr final(new pcl::PointCloud<pcl::PointXYZRGB>);
	sac.align(*finalCloud);
	return sac.getFinalTransformation();

}

Eigen::Matrix4f icpAlignment(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr target,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr source,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr finalCloud,
	double d, const int N, const float trans_epsilon, const float fitness_epsilon)
{
	pcl::IterativeClosestPoint<pcl::PointXYZRGB, pcl::PointXYZRGB> icp;

	OutputDebugString(L"Refining Alignment via ICP...\n");
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr source2(new pcl::PointCloud<pcl::PointXYZRGB>());
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr target2(new pcl::PointCloud<pcl::PointXYZRGB>());


	pcl::PassThrough<pcl::PointXYZRGB> filter; // can do this without parameters
	filter.setInputCloud(source);
	filter.filter(*source2);
	filter.setInputCloud(target);
	filter.filter(*target2);

	icp.setInputSource(source2);
	icp.setInputTarget(target2);

	icp.setMaxCorrespondenceDistance(d);
	icp.setMaximumIterations(N);
	icp.setTransformationEpsilon(trans_epsilon);
	icp.setEuclideanFitnessEpsilon(fitness_epsilon);

	icp.align(*finalCloud);

	bool conv = icp.hasConverged();
	float score = icp.getFitnessScore();

	if (conv) {
		OutputDebugString(L"***ICP solution converged.***\n");
	}
	else{
		OutputDebugString(L"***ICP solution did not converge.***\n");
	}
	
	return icp.getFinalTransformation();
}

float getScalingFactor(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr target,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr source)
{
	Eigen::Vector4f pt_min_t;
	Eigen::Vector4f pt_max_t;
	Eigen::Vector4f pt_min_s;
	Eigen::Vector4f pt_max_s;

	Eigen::Vector4f pt_dt_t;
	Eigen::Vector4f pt_dt_s;

	pcl::getMinMax3D(*target, pt_min_t, pt_max_t);
	pcl::getMinMax3D(*source, pt_min_s, pt_max_s);

	float d_x(0.0f), d_y(0.0f), d_z(0.0f);
	float d(0.0f);

	pt_dt_t = pt_max_t - pt_min_t;
	pt_dt_s = pt_max_s - pt_min_s;

	d_x = std::max(pt_dt_t[0], pt_dt_s[0]);
	d_y = std::max(pt_dt_t[1], pt_dt_s[1]);
	d_z = std::max(pt_dt_t[2], pt_dt_s[2]);

	d = std::max(d_x, std::max(d_y, d_z));

	return(1 / d);
}

void calculateSurfaceNormals(pcl::PointCloud <pcl::PointXYZRGB>::ConstPtr cloud, 
	pcl::PointCloud<pcl::Normal>::Ptr cloudNormals,
	float normalSearchRadius) {

	// Create the normal estimation class, and pass the input dataset to it
	pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
	ne.setInputCloud(cloud);

	// Create an empty kdtree representation, and pass it to the normal estimation object.
	// Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());
	ne.setSearchMethod(tree);

	// Use all neighbors in a sphere of radius
	ne.setRadiusSearch(normalSearchRadius);

	// Compute the features
	return ne.compute(*cloudNormals);
}

void calculateFeaturesFromNormals(pcl::PointCloud <pcl::PointXYZRGB>::ConstPtr cloud,
	pcl::PointCloud<pcl::Normal>::ConstPtr cloudNormals, 
	pcl::PointCloud <pcl::FPFHSignature33>::Ptr cloudFeatures,
	float featureRadius) {


	pcl::PointCloud<pcl::PointXYZI>::Ptr cloudI(new pcl::PointCloud<pcl::PointXYZI>());
	XYZRGB2XYZI(cloud, cloudI);

	pcl::FPFHEstimation<pcl::PointXYZI, pcl::Normal, pcl::FPFHSignature33> fpfh;
	fpfh.setInputCloud(cloudI);
	fpfh.setInputNormals(cloudNormals);
	pcl::search::KdTree<pcl::PointXYZI>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZI>());
	fpfh.setSearchMethod(tree);
	fpfh.setRadiusSearch(featureRadius);
	fpfh.compute(*cloudFeatures);
}

void calculateLocalFeatures(pcl::PointCloud <pcl::PointXYZRGB>::ConstPtr cloud,
	pcl::PointCloud <pcl::Normal>::ConstPtr cloudNormals,
	pcl::PointCloud <pcl::PointXYZI> ::ConstPtr keypoints,
	pcl::PointCloud <pcl::FPFHSignature33>:: Ptr cloudFeatures,
	float featureRadius) {

	pcl::PointCloud<pcl::PointXYZI>::Ptr cloudI(new pcl::PointCloud<pcl::PointXYZI>());
	XYZRGB2XYZI(cloud, cloudI);

	pcl::FPFHEstimation<pcl::PointXYZI, pcl::Normal, pcl::FPFHSignature33> fpfh;
	pcl::search::KdTree<pcl::PointXYZI>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZI>());
	fpfh.setSearchMethod(tree);
	fpfh.setSearchSurface(cloudI);
	fpfh.setInputNormals(cloudNormals);
	fpfh.setInputCloud(keypoints);
	fpfh.setRadiusSearch(featureRadius);
	fpfh.compute(*cloudFeatures);
}

/*--------------------------------------*/
/*---Method for calculating Keypoints---*/
/*--------------------------------------*/
void calculateKeypoints(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud,
	pcl::PointCloud<pcl::Normal>::ConstPtr cloudNormals,
	pcl::PointCloud<pcl::PointXYZI>::Ptr keypoints,
	const float radius, const float threshold)
{
	keypoints->clear();

	pcl::search::KdTree<pcl::PointXYZRGB>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZRGB>());

	pcl::HarrisKeypoint3D<pcl::PointXYZRGB, pcl::PointXYZI>* harris = new pcl::HarrisKeypoint3D<pcl::PointXYZRGB, pcl::PointXYZI>(pcl::HarrisKeypoint3D<pcl::PointXYZRGB, pcl::PointXYZI>::HARRIS, radius, threshold);

	harris->setInputCloud(cloud);
	harris->setNormals(cloudNormals);
	harris->setSearchMethod(tree);
	harris->setRadius(radius);
	harris->setThreshold(threshold);
	harris->setRefine(false);
	harris->compute(*keypoints);
}

void XYZRGB2XYZI(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, pcl::PointCloud<pcl::PointXYZI>::Ptr output) {
	
	for (int i = 0; i<cloud->points.size(); ++i)
	{
		pcl::PointXYZI intensity_point(1.0f);
		intensity_point.x = cloud->points[i].x;
		intensity_point.y = cloud->points[i].y;
		intensity_point.z = cloud->points[i].z;
		output->points.push_back(intensity_point);
	}

}

void downsampleCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr filtered_cloud, float leafSize)
{
	pcl::VoxelGrid<pcl::PointXYZRGB> vox;
	vox.setInputCloud(cloud);
	vox.setLeafSize(leafSize, leafSize, leafSize);
	vox.filter(*filtered_cloud);
}

int leftRight(const int f, const int lowerBound, const int upperBound)
{
	if (f < lowerBound && f < upperBound)
		return(1);
	else if (f > lowerBound && f > upperBound)
		return(-1);
	else
		return(0);
}

void iterateKeypoints(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud,
	pcl::PointCloud<pcl::Normal>::ConstPtr normals,
	pcl::PointCloud<pcl::PointXYZI>::Ptr keypoints,
	float keypointRadius,
	float threshold)
{

	float keypoint_radius_t = 0.0f;

	int lower_bound_t(cloud->size()), upper_bound_t(cloud->size());
	lower_bound_t = std::min(25, lower_bound_t);
	upper_bound_t = std::min(upper_bound_t, 100);

	float at = keypointRadius;
	float bt = keypointRadius;

	int fat, fbt;

	calculateKeypoints(cloud, normals, keypoints, at, threshold);

	fat = keypoints->size();
	fbt = fat;
	keypoint_radius_t = at;

	while (leftRight(fat, lower_bound_t, upper_bound_t) != 0 && (leftRight(fat, lower_bound_t, upper_bound_t) + leftRight(fbt, lower_bound_t, upper_bound_t)) != 0)
	{
		bt = at;
		fbt = fat;

		at *= 10;
		calculateKeypoints(cloud, normals, keypoints, at, threshold);

		fat = keypoints->size();
		keypoint_radius_t = at;
	}

	if (leftRight(fat, lower_bound_t, upper_bound_t) != 0)
	{
		while ((leftRight(fat, lower_bound_t, upper_bound_t) != 0) && (leftRight(fbt, lower_bound_t, upper_bound_t) != 0))
		{
			if ((lower_bound_t - fbt) < (fat - upper_bound_t))
			{
				at = (at + bt) / 2;
				calculateKeypoints(cloud, normals, keypoints, at, threshold);
				fat = keypoints->size();
				keypoint_radius_t = at;
			}
			else
			{
				bt = (at + bt) / 2;
				calculateKeypoints(cloud, normals, keypoints, bt, threshold);
				fbt = keypoints->size();
				keypoint_radius_t = bt;
			}
		}
	}

}

void preprocess(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr output) {

	pcl::FastBilateralFilter<pcl::PointXYZRGB> filter;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr zFiltered(new pcl::PointCloud<pcl::PointXYZRGB>());

	zFilter(cloud, zFiltered);

	filter.setInputCloud(zFiltered);
	// As SigmaS increases, larger features are smoothed out
	filter.setSigmaS(5);
	// As SigmaR increases, the bilateral filter becomes closer to a Gaussian blur
	filter.setSigmaR(5e-3f);
	filter.applyFilter(*output);



	/*pcl::StatisticalOutlierRemoval<pcl::PointXYZRGB> sor;
	sor.setInputCloud(cloud);
	sor.setMeanK(50);
	sor.setStddevMulThresh(1e-2);
	sor.filter(*output);*/

}

void zFilter(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr output) {

	pcl::PassThrough<pcl::PointXYZRGB> filter;
	filter.setInputCloud(cloud);
	filter.setFilterFieldName("z");
	filter.setFilterLimits(0.5, 1.5);
	filter.setKeepOrganized(true);
	filter.filter(*output);
}