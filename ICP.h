void icpMain(pcl::PointCloud<pcl::PointXYZRGB>::Ptr,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr);

Eigen::Matrix4f roughAlignment(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr, float, float, int, float,float,float,float,float);

Eigen::Matrix4f icpAlignment(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr,
	double, const int, const float, const float);

float getScalingFactor(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr);

void calculateSurfaceNormals(pcl::PointCloud <pcl::PointXYZRGB>::ConstPtr, pcl::PointCloud<pcl::Normal>::Ptr,float);

void calculateLocalFeatures(pcl::PointCloud <pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud <pcl::Normal>::ConstPtr,
	pcl::PointCloud <pcl::PointXYZI> ::ConstPtr,
	pcl::PointCloud <pcl::FPFHSignature33>::Ptr,
	float);

void calculateFeaturesFromNormals(pcl::PointCloud <pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::Normal>::ConstPtr,
	pcl::PointCloud <pcl::FPFHSignature33>::Ptr,
	float);

void calculateKeypoints(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::Normal>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZI>::Ptr,
	const float, const float);

void XYZRGB2XYZI(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr, pcl::PointCloud<pcl::PointXYZI>::Ptr);

void downsampleCloud(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr, float);

int leftRight(const int, const int, const int);

void iterateKeypoints(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr ,
	pcl::PointCloud<pcl::Normal>::ConstPtr ,
	pcl::PointCloud<pcl::PointXYZI>::Ptr ,
	float,
	float);

void preprocess(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr);

void zFilter(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr,
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr);