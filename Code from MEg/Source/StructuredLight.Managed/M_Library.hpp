#pragma once

#include "M_Utilities.hpp"
#include "M_Scanner.hpp"
#include <Config.hpp>

namespace StructuredLight {
namespace Managed {

// User settings wrapper class
public ref class UserSettingsManager
{
public:
    property System::String^ default[System::String^]
    {
        System::String^ get(System::String^ setting);
        void set(System::String^ setting, System::String^ value);
    }

internal:
    UserSettingsManager() { }
};


#define SETTING(type, secondType, name) \
    static property type name { \
    type get() { return static_cast<type>(sl::Config::get##name()); } \
    void set(type value) { sl::Config::set##name(static_cast<secondType>(value)); } }

#define SETTING_STR(name) \
    static property System::String^ name { \
    System::String^ get() { return cppToClr(sl::Config::get##name()); } \
    void set(System::String^ value) { sl::Config::set##name(clrToCpp(value)); } }

public ref class Library abstract sealed // Emulates static
{
public:
    /// <summary>
    /// Executes a one-time initialization of the library using the specified configuration file.
    /// </summary>
    /// <param name="configFile">The location of the XML configuration file.</param>
    static void Initialize(System::String^ configFile);

    /// <summary>
    /// Saves the current configuration data to the previously initialized configuration file.
    /// </summary>
    static void SaveConfiguration();

    /// <summary>
    /// An accessor to an array of arbitrary, client-settable settings that are persisted through
    /// multiple initializations of this library.
    /// </summary>
    static SIMPLE_RO_PROP(UserSettingsManager^, UserSettings, _userSettings);

    SETTING(int, int, ContrastThreshold);
    SETTING(int, int, FrameDelay);
    SETTING(int, int, CircleRows);
    SETTING(int, int, CircleColumns);
    SETTING(float, float, CircleDiameter);
    SETTING(float, float, CircleSpacing);
    SETTING(int, int, ProjectedCircleDiameter);
    SETTING(int, int, ProjectedCircleSpacing);
    SETTING(float, float, MotionThreshold);
    SETTING_STR(CalibrationFolder);
    SETTING(StructuredLight::Managed::ScanType, sl::ScanType, ScanType);

private:
    static UserSettingsManager^ _userSettings = gcnew UserSettingsManager();
};

#undef MIRRORED_SETTING
#undef MIRRORED_SETTING_STR

} }