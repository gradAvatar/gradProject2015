#include <msclr\marshal.h> // Order required b/c of WC_NO_BEST_FIT_CHARS error
#include "M_Utilities.hpp"
#include "M_Scanner.hpp"

using namespace msclr::interop;
using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Imaging;

namespace {

ref class MarshalContext
{
internal:
    static marshal_context^ Context = gcnew marshal_context();
};

}

namespace StructuredLight {
namespace Managed {

String^ cppToClr(std::string str)
{
    return MarshalContext::Context->marshal_as<String^>(str.c_str());
}

Size cppToClr(cv::Size size)
{
    return Size(size.width, size.height);
}

Point cppToClr(cv::Point2i pt)
{
    return Point(pt.x, pt.y);
}

array<double, 2>^ cppToClr(cv::Mat mat)
{
    array<double, 2>^ arr = gcnew array<double, 2>(mat.rows, mat.cols);
    for (int r = 0; r < mat.rows; r++)
    {
        for (int c = 0; c < mat.cols; c++)
        {
            arr[r, c] = mat.at<double>(r, c);
        }
    }

    return arr;
}

std::string clrToCpp(String^ str)
{
    return std::string(MarshalContext::Context->marshal_as<const char*>(str));
}

sl::GrayCodeColor clrToCpp(GrayCodeColor% color)
{
    return sl::GrayCodeColor(color.Red, color.Green, color.Blue);
}

Bitmap^ matToBmp(cv::Mat mat)
{
    PixelFormat bmpType = mat.type() == CV_8UC3 ? PixelFormat::Format24bppRgb : PixelFormat::Format8bppIndexed;
    Bitmap^ bmp = gcnew Bitmap(mat.cols, mat.rows, bmpType);
    BitmapData^ data = bmp->LockBits(System::Drawing::Rectangle(0, 0, mat.cols, mat.rows), ImageLockMode::WriteOnly, bmp->PixelFormat);
    CopyMemory(static_cast<void*>(data->Scan0), mat.data, mat.rows * mat.cols * mat.elemSize());
    
    if (bmpType == PixelFormat::Format8bppIndexed)
    {
        // Create a grayscale palette
        // http://social.msdn.microsoft.com/Forums/en-us/vblanguage/thread/500f7827-06cf-4646-a4a1-e075c16bbb38
        auto palette = bmp->Palette;
        auto entries = palette->Entries;
        for (int i = 0; i < entries->Length; i++)
        {
            entries[i] = Color::FromArgb(i, i, i);
        }

        bmp->Palette = palette;
    }

    bmp->UnlockBits(data);

    return bmp;
};

cv::Mat bmpToTempMat(Bitmap^ bmp)
{
    BitmapData^ data = bmp->LockBits(System::Drawing::Rectangle(Point(0, 0), bmp->Size), 
                                     ImageLockMode::ReadOnly, bmp->PixelFormat);
    cv::Mat m(bmp->Height, bmp->Width, pixelFormatToOpenCvType(bmp->PixelFormat),
              static_cast<uchar*>(data->Scan0.ToPointer()));
    bmp->UnlockBits(data);

    return m;
}

int pixelFormatToOpenCvType(PixelFormat f)
{
    switch (f)
    {
    case PixelFormat::Format24bppRgb: return CV_8UC3;
    case PixelFormat::Format8bppIndexed: return CV_8UC1;
    default: return -1;
    }
}

} }