#include "M_Camera.hpp"
#include "M_Exceptions.hpp"
#include <Exceptions.hpp>
#include <Serialization.hpp>
#include <vcclr.h>

using namespace std::placeholders;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Drawing;

namespace StructuredLight {
namespace Managed {

ManagedCamera::ManagedCamera(std::shared_ptr<sl::Camera>& cam)
    : _camScopedPtr(new std::shared_ptr<sl::Camera>(cam)), _cam(_camScopedPtr.get()->get())
{
}

System::String^ ManagedCamera::FriendlyName::get()
{
    std::string name;
    return sl::getFriendlyName(UnmanagedCamera, &name) ? cppToClr(name) : nullptr;
}

void ManagedCamera::FriendlyName::set(System::String^ name)
{
    sl::setFriendlyName(UnmanagedCamera, clrToCpp(name));
}

List<ManagedCamera^>^ ManagedCamera::EnumerateCameras()
{
    auto cams = gcnew List<ManagedCamera^>();

    auto unmanagedCams = sl::Camera::enumerateCameras();
    for (unsigned int i = 0; i < unmanagedCams.size(); i++)
    {
        cams->Add(gcnew ManagedCamera(unmanagedCams[i]));
    }

    return cams;
}

ManagedCameraStream^ ManagedCamera::OpenStream()
{
    std::shared_ptr<sl::CameraStream> sharedPtr = _cam->openStream();
    return gcnew ManagedCameraStream(this, sharedPtr);
}

VideoMode^ ManagedCamera::CreateCustomVideoMode(int x, int y, int width, int height, int fps, VideoFormat fmt)
{
    if (width < 0 || height < 0 || fps < 0)
    {
        throw gcnew ArgumentException("Arguments must be greater than zero.");
    }

    sl::VideoMode unmanagedMode =
        _cam->createCustomVideoMode(x, y, width, height, fps, static_cast<sl::VideoFormat>(fmt));
    
    return unmanagedMode.width == 0 && unmanagedMode.height == 0 ? nullptr :
        gcnew StructuredLight::Managed::VideoMode(unmanagedMode);
}

void ManagedCamera::SetIntrinsicCalibration(IntrinsicCalibration^ calib)
{
    sl::saveIntrinsicCalibration(_cam, calib->UnmanagedCalibration);
}

CameraSettingRange^ ManagedCamera::GetCameraSettingRange(CameraSetting setting)
{
    double lower = 0, delta = 0, upper = 0, def = 0;
    bool supported = _cam->getCameraSettingRange((sl::CameraSetting)setting, &lower, &delta, &upper, &def);
    CameraSettingRange^ range = gcnew CameraSettingRange(lower, upper, delta, def, supported);
    return range;
}

double ManagedCamera::GetCameraSetting(CameraSetting setting, [Out] CameraSettingFlags %flags)
{
    double value;
    sl::CameraSettingFlags slFlags;

    _cam->getCameraSetting((sl::CameraSetting)setting, &value, &slFlags);

    flags = (CameraSettingFlags)slFlags;
    return value;
}

double ManagedCamera::GetCameraSetting(CameraSetting setting)
{
    double value;
    sl::CameraSettingFlags slFlags;

    _cam->getCameraSetting((sl::CameraSetting)setting, &value, &slFlags);
    return value;
}

void ManagedCamera::SetCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags)
{
    _cam->setCameraSetting((sl::CameraSetting)setting, value, (sl::CameraSettingFlags)flags);
}

} }