#include "M_Library.hpp"
#include "M_Utilities.hpp"
#include <Config.hpp>

using namespace System;

namespace StructuredLight {
namespace Managed {

void Library::Initialize(String^ configFile)
{
    sl::Config::loadConfig(clrToCpp(configFile));
}

void Library::SaveConfiguration()
{
    sl::Config::saveConfig();
}

String^ UserSettingsManager::default::get(String^ setting)
{
    return cppToClr(sl::Config::getUserSetting(clrToCpp(setting)));
}

void UserSettingsManager::default::set(String^ setting, String^ value)
{
    sl::Config::setUserSetting(clrToCpp(setting), clrToCpp(value));
}

} }