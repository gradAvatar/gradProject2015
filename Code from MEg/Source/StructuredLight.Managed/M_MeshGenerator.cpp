#include "M_MeshGenerator.hpp"
#include "M_Utilities.hpp"
#include <msclr/lock.h>

using namespace System;
using namespace System::Collections::Generic;

namespace StructuredLight {
namespace Managed {

MeshGenerator::MeshGenerator()
{
}

std::vector<sl::Point3d> MeshGenerator::clrToCppPointCloud(List<Point3d>^ lst)
{
    std::vector<sl::Point3d> unmanagedList = std::vector<sl::Point3d>();
    for(int i = 0; i < lst->Count; i++)
    {
        unmanagedList.push_back(sl::Point3d(lst[i].X, lst[i].Y, lst[i].Z));
    }
    return unmanagedList;
}

std::vector<sl::Polygon> MeshGenerator::clrToCppMesh(List<TriPolygon^>^ lst)
{
    std::vector<sl::Polygon> unmanagedList = std::vector<sl::Polygon>();
    for(int i = 0; i < lst->Count; i++)
    {
        unmanagedList.push_back(sl::Polygon(
            sl::Point3d(lst[i]->A.X, lst[i]->A.Y, lst[i]->A.Z),
            sl::Point3d(lst[i]->B.X, lst[i]->B.Y, lst[i]->B.Z),
            sl::Point3d(lst[i]->C.X, lst[i]->C.Y, lst[i]->C.Z)));
    }
    return unmanagedList;
}

List<TriPolygon^>^ MeshGenerator::cppToClrMesh(std::vector<sl::Polygon> lst)
{
    List<TriPolygon^>^ polys = gcnew List<TriPolygon^>();
    for(unsigned int i = 0; i < lst.size(); i++)
    {
        polys->Add(gcnew TriPolygon(lst[i]));
    }
    return polys;
}

List<Point3d>^ MeshGenerator::cppToClrMesh(std::vector<sl::Point3d> lst)
{
    List<Point3d>^ polys = gcnew List<Point3d>();
    for(unsigned int i = 0; i < lst.size(); i++)
    {
        polys->Add(*((Point3d*)(&lst[i])));
    }
    return polys;
}

List<TriPolygon^>^ MeshGenerator::SurfaceReconstruction(List<Point3d>^ lst)
{
    return cppToClrMesh(sl::MeshGeneration::convertMeshToManagedMesh(
        sl::MeshGeneration::generateMesh(sl::MeshGeneration::convertManagedPointCloudToPointCloud(clrToCppPointCloud(lst)))));
}

void MeshGenerator::ConvertPointCloudToMesh(String^ inputFileName, String^ outputFileName)
{
	sl::MeshGeneration::convertPointCloudToMesh(clrToCpp(inputFileName), clrToCpp(outputFileName));
}

List<Point3d>^ MeshGenerator::LoadObjPointCloud(String^ filename)
{
    return cppToClrMesh(sl::MeshGeneration::convertPointCloudToManagedPointCloud(
        sl::MeshGeneration::loadPointCloudObj(clrToCpp(filename))));
}

List<TriPolygon^>^ MeshGenerator::GenerateConcaveHull(double alpha, List<Point3d>^ lst)
{
    return cppToClrMesh(sl::MeshGeneration::generateConcaveHull(alpha, 
        sl::MeshGeneration::convertManagedPointCloudToPointCloud(clrToCppPointCloud(lst))));
}

void MeshGenerator::Testing()
{
	sl::PointCloudAlignment::Testing();
}

} }