#include "M_CameraStream.hpp"
#include "M_Camera.hpp"
#include "M_Exceptions.hpp"
#include "NativeCallbackWrapper.hpp"
#include <vcclr.h>
#include <msclr/lock.h>

using namespace std::placeholders;
using namespace System;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;
using namespace StructuredLight::Managed;

namespace {

class ImageCapturedCallbackWrapper 
    : public NativeCallbackWrapper<EventHandler<ImageCapturedEventArgs^>, ImageCapturedEventArgs,
                                   sl::ImageCapturedCallbackArgs>
{
public:
    ImageCapturedCallbackWrapper(EventHandler<ImageCapturedEventArgs^>^ del, ManagedCamera^ cam)
        : NativeCallbackWrapper(del, cam), _cam(cam)
    {
    }

protected:
    ImageCapturedEventArgs^ transformNativeArgs(sl::ImageCapturedCallbackArgs e)
    {
        System::Drawing::Bitmap^ bmp = matToBmp(e.getImage());
        return gcnew ImageCapturedEventArgs(_cam, bmp);
    }

private:
    gcroot<ManagedCamera^> _cam;
};

}

namespace StructuredLight {
namespace Managed {

ManagedCameraStream::ManagedCameraStream(ManagedCamera^ managedCam,
                                         std::shared_ptr<sl::CameraStream>& stream)
        : _cam(managedCam), _streamScopedPtr(new std::shared_ptr<sl::CameraStream>(stream)),
          _stream(_streamScopedPtr.get()->get()),
          _callbacks(gcnew Dictionary<EventHandler<ImageCapturedEventArgs^>^, sl::CallbackToken>())
{
}

void ManagedCameraStream::ImageCaptured::add(EventHandler<ImageCapturedEventArgs^>^ e)
{
    msclr::lock l(_callbacks);

    sl::CallbackToken id = _stream->addCallback(ImageCapturedCallbackWrapper(e, Camera));
    _callbacks->Add(e, id);
}

void ManagedCameraStream::ImageCaptured::remove(EventHandler<ImageCapturedEventArgs^>^ e)
{
    msclr::lock l(_callbacks);

    sl::CallbackToken id;
    if (_callbacks->TryGetValue(e, id))
    {
        _stream->removeCallback(id);
        _callbacks->Remove(e);
    }
}

bool ManagedCameraStream::Start()
{
    return _stream->start();
}

bool ManagedCameraStream::Stop()
{
    return _stream->stop();
}

bool ManagedCameraStream::LockStreamMode(StreamMode mode, [Out] StreamLock^ %streamLock)
{
    sl::StreamLock* unmanagedLock = new sl::StreamLock(_stream, (sl::STREAM_MODE)mode);
    if (!unmanagedLock->isLockActive())
    {
        delete unmanagedLock;
        return false;
    }

    streamLock = gcnew StreamLock(unmanagedLock);
    return true;
}

bool ManagedCameraStream::WaitForFrame()
{
    return _stream->waitForFrame();
}

bool ManagedCameraStream::WaitForFrame(int milliseconds)
{
    return _stream->waitForFrame(milliseconds);
}

void ManagedCameraStream::Trigger()
{
    _stream->trigger();
}

//
// StreamLock implementation
//
StreamLock::~StreamLock()
{
    this->!StreamLock();
}

StreamLock::!StreamLock()
{
    if (_lock)
    {
        delete _lock;
        _lock = 0;
    }
}

StreamLock::StreamLock(sl::StreamLock* lock)
{
    _lock = lock;
}

} }