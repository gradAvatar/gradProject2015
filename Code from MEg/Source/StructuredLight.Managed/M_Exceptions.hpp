#pragma once

#include "M_Utilities.hpp"

#define CUSTOM_EXCEPTION(_name_) \
    public ref class _name_ : System::Exception { public: \
    _name_() { } \
    _name_(System::String^ msg) : System::Exception(msg) { } \
    _name_(System::String^ msg, System::Exception^ inner) : System::Exception(msg, inner) { } \
    _name_(const char* msg) : System::Exception(cppToClr(msg)) { } \
    _name_(const char* msg, System::Exception^ inner) \
        : System::Exception(cppToClr(msg), inner) { } };

namespace StructuredLight {
namespace Managed {

CUSTOM_EXCEPTION(StreamModeLockedException)
CUSTOM_EXCEPTION(CalibrationNotStartedException)
CUSTOM_EXCEPTION(CalibrationAlreadyStartedException)
CUSTOM_EXCEPTION(CalibrationStartedException)

} }

#undef CUSTOM_EXCEPTION