#pragma once

#include "M_CameraStream.hpp"
#include "M_Utilities.hpp"
#include "M_Calibration.hpp"
#include "M_Device.hpp"
#include "clr_scoped_ptr.hpp"
#include <Camera.hpp>
#include <Serialization.hpp>

#using "System.Drawing.dll"

using namespace System::Runtime::InteropServices;

namespace StructuredLight {
namespace Managed {

// Forward declarations
ref class IntrinsicCalibration;

/// <summary>
/// Values that represent different output formats that a Camera can support.
/// </summary>
[System::FlagsAttribute]
public enum class VideoFormat
{
    Unknown = sl::VID_UNKNOWN,
    Rgb24 = sl::VID_RGB24,
    Gray8 = sl::VID_GRAY8,
    Raw8 = sl::VID_RAW8
};

/// <summary>
/// A structure containing information about a video mode supported by a Camera instance.
/// </summary>
public ref class VideoMode
{
public:
    /// <summary>
    /// The width of the image dimension for this video mode.
    /// </summary>
    SIMPLE_RO_PROP(int, Width, _mode->width);

    /// <summary>
    /// The height of the image dimension for this video mode.
    /// </summary>
    SIMPLE_RO_PROP(int, Height, _mode->height);

    /// <summary>
    /// The maximum (but not necessarily only) frame rate supported by this video mode.
    /// </summary>
    SIMPLE_RO_PROP(int, FramesPerSecond, _mode->framesPerSecond);

    /// <summary>
    /// The output format for this video mode.
    /// </summary>
    SIMPLE_RO_PROP(StructuredLight::Managed::VideoFormat, VideoFormat, 
                   static_cast<StructuredLight::Managed::VideoFormat>(_mode->videoFormat));

    /// <summary>
    /// The resolution for this video mode.
    /// </summary>
    SIMPLE_RO_PROP(System::Drawing::Size, Resolution, System::Drawing::Size(Width, Height));

internal:
    VideoMode(sl::VideoMode mode)
        : _mode(new sl::VideoMode(mode))
    {
    }

    SIMPLE_RO_PROP(sl::VideoMode, UnmanagedVideoMode, *_mode);

private:
    clr_scoped_ptr<sl::VideoMode> _mode;
};

/// <summary>
/// A structure containing information about the maximum settings supported by a Camera.
/// </summary>
public ref class CustomVideoModeConstraints
{
public:
    /// <summary>
    /// Whether or not custom video modes are supported by the Camera.
    /// </summary>
    SIMPLE_RO_PROP(bool, IsSupported, _c->supported);

    /// <summary>
    /// The maximum horizontal resolution supported by the Camera.
    /// </summary>
    SIMPLE_RO_PROP(int, MaximumWidth, _c->maxWidth);

    /// <summary>
    /// The maximum vertical resolution supported by the Camera.
    /// </summary>
    SIMPLE_RO_PROP(int, MaximumHeight, _c->maxHeight);

    /// <summary>
    /// The maximum framerate supported by the Camera.
    /// </summary>
    SIMPLE_RO_PROP(int, MaximumFramesPerSecond, _c->maxFramesPerSecond);

    /// <summary>
    /// A bitfield of VideoFormats containing all video formats supported by the Camera.
    /// </summary>
    SIMPLE_RO_PROP(VideoFormat, SupportedVideoFormats,
                   static_cast<VideoFormat>(_c->supportedVideoFormats));

internal:
    CustomVideoModeConstraints(sl::CustomVideoModeConstraints c)
        : _c(new sl::CustomVideoModeConstraints(c))
    {
    }

    SIMPLE_RO_PROP(sl::CustomVideoModeConstraints, UnmanagedConstraints, *_c);

private:
    clr_scoped_ptr<sl::CustomVideoModeConstraints> _c;
};

/// <summary>
/// Values that represent different types of Cameras supported by this library.
/// </summary>
[System::FlagsAttribute]
public enum class CameraType
{
    Unknown = sl::CAM_UNKNOWN,
    DirectShow = sl::CAM_DIRECTSHOW,
    PointGrey = sl::CAM_POINT_GREY
};

/// <summary>
/// Values that represent different properties of a Camera supported by this library.
/// </summary>
[System::FlagsAttribute]
public enum class CameraSetting : long
{
    Brightness,
    Contrast,
    Hue,
    Saturation,
    Sharpness,
    Gamma,
    WhiteBalance,
    Gain,
    Exposure,
    Focus,
    Shutter
};

/// <summary>
/// Values that represent different modes for the various camera settings.
/// </summary>
[System::FlagsAttribute]
public enum class CameraSettingFlags : long
{
    None = 0,

    /// <summary>
    /// Indicates that the setting's value is automatically set by the camera.
    /// </summary>
    Auto = 1,

    /// <summary>
    /// Indicates that the setting's value is manually set by users of the camera.
    /// </summary>
    Manual = 2,

    /// <summary>
    /// Indicates that the setting is not supported.
    /// </summary>
    NotSupported = 4
};

/// <summary>
/// A structure containing information about the allowable range of values of a camera setting.
/// </summary>
public ref class CameraSettingRange
{
public:
    CameraSettingRange(double min, double max, double step, double def, bool isSupported)
        : Minimum(min), Maximum(max), Step(step), Default(def), IsSupported(isSupported)
    {
    }
    
    /// <summary>
    /// Gets a value indicating whether or not the camera setting is supported or not.
    /// </summary>
    bool IsSupported;

    /// <summary>
    /// Gets the minimum acceptable value of the camera setting.
    /// </summary>
    double Minimum;

    /// <summary>
    /// Gets the maximum acceptable value of the camera setting.
    /// </summary>
    double Maximum;

    /// <summary>
    /// Gets the minimum interval in which the camera setting can be modified.
    /// </summary>
    double Step;

    /// <summary>
    /// Gets the default value of the camera setting.
    /// </summary>
    double Default;
};

public ref class ManagedCamera : public ManagedDevice
{
public:
    /// <summary>
    /// Returns a list of all cameras currently connected to the system.
    /// </summary>
    /// <returns>A list of all cameras currently connected to the system.</returns>
    static System::Collections::Generic::List<ManagedCamera^>^ EnumerateCameras();

    /// <summary>
    /// Gets the model name of the camera.
    /// </summary>
    SIMPLE_RO_PROP_OVERRIDE(System::String^, ModelName, cppToClr(_cam->getModelName()));
    
    /// <summary>
    /// The serial number of the camera.
    /// </summary>
    SIMPLE_RO_PROP_OVERRIDE(System::String^, SerialNumber, cppToClr(_cam->getSerialNumber()));
    
    /// <summary>
    /// Gets a value indicating whether or not the camera has been intrinsicly calibrated.
    /// </summary>
    SIMPLE_RO_PROP_OVERRIDE(bool, IsIntrinsiclyCalibrated, sl::loadIntrinsicCalibration(_cam));
    
    /// <summary>
    /// Gets the resolution of the camera.
    /// </summary>
    SIMPLE_RO_PROP_OVERRIDE(System::Drawing::Size, Resolution, cppToClr(_cam->getResolution()));
    
    /// <summary>
    /// Gets a value indicating whether or not a camera stream is currently open.
    /// </summary>
    SIMPLE_RO_PROP(bool, IsStreamOpen, _cam->isStreamOpen());
    
    /// <summary>
    /// Gets the type of camera this instance represents.
    /// </summary>
    SIMPLE_RO_PROP(StructuredLight::Managed::CameraType, CameraType, 
                   static_cast<StructuredLight::Managed::CameraType>(_cam->getType()));

    /// <summary>
    /// Gets a structure indicating the capabilities of the camera in custom video mode (if
    /// supported).
    /// </summary>
    SIMPLE_RO_PROP(StructuredLight::Managed::CustomVideoModeConstraints^,
                   CustomVideoModeConstraints,
                   gcnew StructuredLight::Managed::CustomVideoModeConstraints(
                       _cam->getCustomVideoModeConstraints()));
    
    /// <summary>
    /// Gets the video mode the camera is currently in.
    /// </summary>
    SIMPLE_RW_PROP(StructuredLight::Managed::VideoMode^, VideoMode,
                   gcnew StructuredLight::Managed::VideoMode(_cam->getVideoMode()),
                   _cam->setVideoMode(value->UnmanagedVideoMode));

    /// <summary>
    /// Gets or sets the "friendly name" of the ManagedCamera. This is an arbitrary value, and does
    /// not affect library behavior in any way; it's merely a convenience for the user.
    /// </summary>
    /// <returns>
    /// The value of the "friendly name" of the ManagedCamera, if it is set; <c>null</c> otherwise.
    /// </returns>
    property System::String^ FriendlyName
    {
        virtual System::String^ get() override;
        virtual void set(System::String^ name) override;
    }

    /// <summary>
    /// Opens a CameraStream tied to this Camera, used for accessing images from the underlying
    /// hardware.
    /// </summary>
    /// <returns> 
    /// An instance of <see cref="ManagedCameraStream"/> that is tied to this instance.
    /// </returns>
    ManagedCameraStream^ OpenStream();

    /// <summary><para>
    /// If supported, creates a custom, camera-specific video mode instance that can be assigned
    /// to <see cref="VideoMode"/>.
    /// </para><para>To determine the limits of what the camera can support (e.g. its maximum
    /// resolution), including whether or not custom video modes are supported at all, refer to the
    /// information returned by <see cref="CustomVideoModeConstraints"/>.
    /// </para><para>
    /// Note that values specified as parameters may be rounded (e.g. to the nearest 8th or 16th
    /// integer) as dictated by each individual camera's capabilities. You are not guaranteed to
    /// get the exact resolution or framerate you requested.
    /// </para></summary>
    /// <param name="width">The horizontal resolution of the video mode you wish to create.</param>
    /// <param name="height">The vertical resolution of the video mode you wish to create.</param>
    /// <param name="fps">The framerate of the video mode you wish to create.</param>
    /// <param name="vidFormat">The video format of the video mode you wish to create.</param>
    /// <returns>A custom VideoMode that can be assigned to <see cref="VideoMode"/>.</returns>
    StructuredLight::Managed::VideoMode^ CreateCustomVideoMode(int x, int y, int width, int height,
                                                               int fps, VideoFormat vidFormat);
    
    /// <summary>
    /// Sets the intrinsic calibration of this instance to the specified value.
    /// </summary> 
    /// <param name="calib">The calibration to set.</param>
    /// <seealso cref="StructuredLight.Managed.CameraCalibrator"/>
    void SetIntrinsicCalibration(IntrinsicCalibration^ calib);

    /// <summary>
    /// Gets the value of the specified camera setting.
    /// </summary>
    /// <param name="setting">The camera setting whose value should be retrieved.</param>
    /// <returns>The value of the specified property.</returns>
    double GetCameraSetting(CameraSetting setting);
    
    /// <summary>
    /// Gets the value of the specified camera setting.
    /// </summary>
    /// <param name="setting">The camera setting whose value should be retrieved.</param>
    /// <param name="flags">Various flags that can affect the behavior of the property.</param>
    /// <returns>The value of the specified property.</returns>
    double GetCameraSetting(CameraSetting setting, [Out] CameraSettingFlags %flags);
    
    /// <summary>
    /// Gets an instance of <see cref="CameraSettingRange"/> that describes the range of acceptable
    /// values of a camera setting.
    /// </summary>
    /// <param name="setting">The camera setting whose value should be retrieved.</param>
    /// <returns>A structure describing the range of acceptable values of the specified camera
    /// setting.</returns>
    CameraSettingRange^ ManagedCamera::GetCameraSettingRange(CameraSetting setting);
    
    /// <summary>
    /// Sets the value and flags of the specified camera setting.
    /// </summary>
    /// <param name="setting">The camera setting whose value should be set.</param>
    /// <param name="value">The desired value of the specified camera setting.</param>
    /// <param name="flags">Various flags that can alter the behavior of the specified camera
    /// setting.</param>
    void SetCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags);

internal:
    ManagedCamera(std::shared_ptr<sl::Camera>& cam);

    SIMPLE_RO_PROP(sl::Camera*, UnmanagedCamera, _camScopedPtr->get());

private:
    clr_scoped_ptr<std::shared_ptr<sl::Camera>> _camScopedPtr;
    sl::Camera* const _cam;
};

} }