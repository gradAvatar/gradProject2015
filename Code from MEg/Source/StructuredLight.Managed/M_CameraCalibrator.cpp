#include "M_Calibration.hpp"
#include "M_Exceptions.hpp"
#include <Exceptions.hpp>
#include <vcclr.h>
#include <msclr/lock.h>

using namespace System;
using namespace System::Collections::Generic;

using namespace std::placeholders;

namespace StructuredLight {
namespace Managed {

CameraCalibrator::CameraCalibrator(ManagedCamera^ cam)
    : _cam(cam), _calibrator(new sl::CameraCalibrator(cam->UnmanagedCamera)),
      _previewCallbacks(gcnew Dictionary<PreviewEventHandler^, sl::CallbackToken>()),
      _finishedCallbacks(gcnew Dictionary<EventHandler^, sl::CallbackToken>())
{
}

void CameraCalibrator::NewCalibrationPreview::add(PreviewEventHandler^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token = _calibrator->addPreviewCallback(
        CalibrationPreviewCallbackWrapper(e, this));

    _previewCallbacks->Add(e, token);
}

void CameraCalibrator::NewCalibrationPreview::remove(PreviewEventHandler^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token;
    if (_previewCallbacks->TryGetValue(e, token))
    {
        _calibrator->removePreviewCallback(token);
        _previewCallbacks->Remove(e);
    }
}

void CameraCalibrator::CalibrationFinished::add(System::EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token = _calibrator->addFinishedCallback(
        EmptyNativeCallbackWrapper(e, this));

    _finishedCallbacks->Add(e, token);
}

void CameraCalibrator::CalibrationFinished::remove(System::EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token;
    if (_finishedCallbacks->TryGetValue(e, token))
    {
        _calibrator->removeFinishedCallback(token);
        _finishedCallbacks->Remove(e);
    }
}

int CameraCalibrator::TargetCameraCaptures::get() { return _calibrator->getTargetAcceptedCaptures(); }
void CameraCalibrator::TargetCameraCaptures::set(int val)
{
    try
    {
        _calibrator->setTargetAcceptedCaptures(val);
    }
    catch (sl::CalibrationStartedException ex)
    {
        throw gcnew CalibrationStartedException(ex.what());
    }
}

IntrinsicCalibration^ CameraCalibrator::Results::get()
{
    sl::IntrinsicCalibration calib;
    return _calibrator->getResults(calib) ? gcnew IntrinsicCalibration(calib) : nullptr;
}

void CameraCalibrator::Start()
{
    try
    {
        _calibrator->start();
    }
    catch (sl::CalibrationAlreadyStartedException ex)
    {
        throw gcnew CalibrationAlreadyStartedException(ex.what());
    }
}

void CameraCalibrator::Cancel()
{
    try
    {
        _calibrator->cancel();
    }
    catch (sl::CalibrationNotStartedException ex)
    {
        throw gcnew CalibrationNotStartedException(ex.what());
    }
}

} }