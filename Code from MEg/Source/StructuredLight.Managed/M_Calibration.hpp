#pragma once

#include "M_Utilities.hpp"
#include "M_Camera.hpp"
#include "M_Projector.hpp"
#include "NativeCallbackWrapper.hpp"
#include <Calibration.hpp>

namespace StructuredLight {
namespace Managed {

// Forward declarations
ref class ManagedProjector;

public ref class IntrinsicCalibration
{
private:
    // Macros can't have commas
    typedef array<double, 2> TwoDimArray;

public:
    SIMPLE_RO_PROP(double, ReprojectionError, _error);
    SIMPLE_RO_PROP(TwoDimArray^, Intrinsic, _camera);
    SIMPLE_RO_PROP(TwoDimArray^, Distortion, _distortion);

internal:
    IntrinsicCalibration(sl::IntrinsicCalibration calib)
        : _error(calib.reprojectionError), _camera(cppToClr(cv::Mat(calib.camera))),
          _distortion(cppToClr(cv::Mat(calib.distortion))),
          _unmanaged(new sl::IntrinsicCalibration(calib))
    {
    }

    SIMPLE_RO_PROP(sl::IntrinsicCalibration, UnmanagedCalibration, *_unmanaged);

private:
    initonly double _error;
    initonly array<double, 2>^ _camera;
    initonly array<double, 2>^ _distortion;
    clr_scoped_ptr<sl::IntrinsicCalibration> _unmanaged;
};

public ref class CamProjExtrinsicCalibration
{
private:
    // Macros can't have commas
    typedef array<double, 2> TwoDimArray;

public:
    SIMPLE_RO_PROP(TwoDimArray^, CameraExtrinsic, _camExtrinsic);
    SIMPLE_RO_PROP(TwoDimArray^, ProjectorExtrinsic, _projExtrinsic);

internal:
    CamProjExtrinsicCalibration(sl::CamProjExtrinsicCalibration calib)
        : _camExtrinsic(cppToClr(cv::Mat(calib.camExtrinsic))),
          _projExtrinsic(cppToClr(cv::Mat(calib.projExtrinsic))),
          _unmanaged(new sl::CamProjExtrinsicCalibration(calib))
    {
    }

    SIMPLE_RO_PROP(sl::CamProjExtrinsicCalibration, UnmanagedCalibration, *_unmanaged);

private:
    initonly array<double, 2>^ _camExtrinsic;
    initonly array<double, 2>^ _projExtrinsic;
    clr_scoped_ptr<sl::CamProjExtrinsicCalibration> _unmanaged;
};

public ref class CalibrationPreviewEventArgs : System::EventArgs
{
public:
    SIMPLE_RO_PROP(System::Drawing::Bitmap^, Preview, _preview);
    SIMPLE_RO_PROP(int, AcceptedCaptureCount, _accepted);
    SIMPLE_RO_PROP(bool, CurrentPreviewWasAccepted, _currAccepted);

internal:
    CalibrationPreviewEventArgs(sl::CalibrationPreviewData status)
        : _preview(matToBmp(status.currentPreview)), _accepted(status.numAcceptedCaptures),
          _currAccepted(status.currentPreviewWasAccepted)
    {
    }

private:
    initonly System::Drawing::Bitmap^ _preview;
    initonly int _accepted, _target;
    initonly bool _currAccepted;
};

public ref class CamCamExtrinsicCalibrationPreviewEventArgs : System::EventArgs
{
public:
    SIMPLE_RO_PROP(System::Drawing::Bitmap^, SourcePreview, _sourcePreview);
    SIMPLE_RO_PROP(System::Drawing::Bitmap^, TargetPreview, _targetPreview);
    SIMPLE_RO_PROP(int, AcceptedCaptureCount, _accepted);
    SIMPLE_RO_PROP(bool, CurrentPreviewWasAccepted, _currAccepted);

internal:
    CamCamExtrinsicCalibrationPreviewEventArgs(sl::CamCamExtrinsicCalibrationPreviewData status)
        : _sourcePreview(matToBmp(status.sourceCamPreview)),
          _targetPreview(matToBmp(status.targetCamPreview)),
          _accepted(status.numAcceptedCaptures),
          _currAccepted(status.currentPreviewWasAccepted)
    {
    }

private:
    initonly System::Drawing::Bitmap^ _sourcePreview;
    initonly System::Drawing::Bitmap^ _targetPreview;
    initonly int _accepted, _target;
    initonly bool _currAccepted;
};

typedef System::EventHandler<CalibrationPreviewEventArgs^> PreviewEventHandler;
typedef System::EventHandler<CamCamExtrinsicCalibrationPreviewEventArgs^> CamCamExtrinsicPreviewEventHandler;

public ref class CameraCalibrator
{
public:
    CameraCalibrator(ManagedCamera^ cam);

    event PreviewEventHandler^ NewCalibrationPreview
    {
        void add(PreviewEventHandler^ e);
        void remove(PreviewEventHandler^ e);
    }

    event System::EventHandler^ CalibrationFinished
    {
        void add(System::EventHandler^ e);
        void remove(System::EventHandler^ e);
    }

    SIMPLE_RO_PROP(ManagedCamera^, Camera, _cam);
    SIMPLE_RO_PROP(bool, IsCalibrating, _calibrator->isCalibrating());
    property int TargetCameraCaptures { int get(); void set(int val); }
    property IntrinsicCalibration^ Results { IntrinsicCalibration^ get(); }

    void Start();
    void Cancel();

private:
    initonly ManagedCamera^ _cam;
    initonly System::Collections::Generic::Dictionary<PreviewEventHandler^,
                                                      sl::CallbackToken>^ _previewCallbacks;
    initonly System::Collections::Generic::Dictionary<System::EventHandler^,
                                                      sl::CallbackToken>^ _finishedCallbacks;
    clr_scoped_ptr<sl::CameraCalibrator> _calibrator;
};

public ref class ProjectorCalibrator
{
private:
    typedef System::EventHandler<CalibrationPreviewEventArgs^> PreviewEventHandler;

public:
    ProjectorCalibrator(ManagedCamera^ cam, ManagedProjector^ device);

    event PreviewEventHandler^ NewCalibrationPreview
    {
        void add(PreviewEventHandler^ e);
        void remove(PreviewEventHandler^ e);
    }

    event System::EventHandler^ CalibrationFinished
    {
        void add(System::EventHandler^ e);
        void remove(System::EventHandler^ e);
    }

    SIMPLE_RO_PROP(ManagedCamera^, Camera, _cam);
    SIMPLE_RO_PROP(bool, IsCalibrating, _calibrator->isCalibrating());
    property int TargetCameraCaptures { int get(); void set(int val); }
    property IntrinsicCalibration^ Results { IntrinsicCalibration^ get(); }

    void Start();
    void Cancel();

private:
    initonly ManagedCamera^ _cam;
    initonly System::Collections::Generic::Dictionary<PreviewEventHandler^,
                                                      sl::CallbackToken>^ _previewCallbacks;
    initonly System::Collections::Generic::Dictionary<System::EventHandler^,
                                                      sl::CallbackToken>^ _finishedCallbacks;
    clr_scoped_ptr<sl::ProjectorCalibrator> _calibrator;
};

public ref class CamProjExtrinsicCalibrator
{
private:
    typedef System::EventHandler<CalibrationPreviewEventArgs^> PreviewEventHandler;

public:
    CamProjExtrinsicCalibrator(ManagedCamera^ cam, ManagedProjector^ device);

    event PreviewEventHandler^ NewCalibrationPreview
    {
        void add(PreviewEventHandler^ e);
        void remove(PreviewEventHandler^ e);
    }

    event System::EventHandler^ CalibrationFinished
    {
        void add(System::EventHandler^ e);
        void remove(System::EventHandler^ e);
    }

    SIMPLE_RO_PROP(ManagedCamera^, Camera, _cam);
    SIMPLE_RO_PROP(bool, IsCalibrating, _calibrator->isCalibrating());
    property int TargetCameraCaptures { int get(); void set(int val); }
    property CamProjExtrinsicCalibration^ Results { CamProjExtrinsicCalibration^ get(); }

    void Start();
    void Cancel();

private:
    initonly ManagedCamera^ _cam;
    initonly System::Collections::Generic::Dictionary<PreviewEventHandler^,
                                                      sl::CallbackToken>^ _previewCallbacks;
    initonly System::Collections::Generic::Dictionary<System::EventHandler^,
                                                      sl::CallbackToken>^ _finishedCallbacks;
    clr_scoped_ptr<sl::CamProjExtrinsicCalibrator> _calibrator;
};

public ref class CamCamExtrinsicCalibration
{
private:
    // Macros can't have commas
    typedef array<double, 2> TwoDimArray;

public:
    SIMPLE_RO_PROP(TwoDimArray^, Rotation, _rotation);
    SIMPLE_RO_PROP(TwoDimArray^, Translation, _translation);

internal:
    CamCamExtrinsicCalibration(sl::CamCamExtrinsicCalibration calib)
        : _rotation(cppToClr(cv::Mat(calib.rotation))),
          _translation(cppToClr(cv::Mat(calib.translation))),
          _unmanaged(new sl::CamCamExtrinsicCalibration(calib))
    {
    }

    SIMPLE_RO_PROP(sl::CamCamExtrinsicCalibration, UnmanagedCalibration, *_unmanaged);

private:
    initonly array<double, 2>^ _rotation;
    initonly array<double, 2>^ _translation;
    clr_scoped_ptr<sl::CamCamExtrinsicCalibration> _unmanaged;
};

public ref class CamCamExtrinsicCalibrator
{
private:
    typedef System::EventHandler<CalibrationPreviewEventArgs^> PreviewEventHandler;

public:
    CamCamExtrinsicCalibrator(ManagedCamera^ sourceCam, ManagedCamera^ targetCam);

    event CamCamExtrinsicPreviewEventHandler^ NewCalibrationPreview
    {
        void add(CamCamExtrinsicPreviewEventHandler^ e);
        void remove(CamCamExtrinsicPreviewEventHandler^ e);
    }

    event System::EventHandler^ CalibrationFinished
    {
        void add(System::EventHandler^ e);
        void remove(System::EventHandler^ e);
    }
    
    SIMPLE_RO_PROP(ManagedCamera^, SourceCamera, _sourceCam);
    SIMPLE_RO_PROP(ManagedCamera^, TargetCamera, _targetCam);
    SIMPLE_RO_PROP(bool, IsCalibrating, _calibrator->isCalibrating());
    property int TargetCameraCaptures { int get(); void set(int val); }
    property CamCamExtrinsicCalibration^ Results { CamCamExtrinsicCalibration^ get(); }

    void Start();
    void Cancel();

private:
    initonly ManagedCamera^ _sourceCam;
    initonly ManagedCamera^ _targetCam;
    initonly System::Collections::Generic::Dictionary<CamCamExtrinsicPreviewEventHandler^,
                                                      sl::CallbackToken>^ _previewCallbacks;
    initonly System::Collections::Generic::Dictionary<System::EventHandler^,
                                                      sl::CallbackToken>^ _finishedCallbacks;
    clr_scoped_ptr<sl::CamCamExtrinsicCalibrator> _calibrator;
};

class CalibrationPreviewCallbackWrapper 
    : public NativeCallbackWrapper<PreviewEventHandler, CalibrationPreviewEventArgs,
                                   sl::CalibrationPreviewData>
{
public:
    CalibrationPreviewCallbackWrapper(PreviewEventHandler^ del, System::Object^ sender)
        : NativeCallbackWrapper(del, sender)
    {
    }

protected:
    CalibrationPreviewEventArgs^ transformNativeArgs(sl::CalibrationPreviewData e)
    {
        return gcnew CalibrationPreviewEventArgs(e);
    }
};

class CamCamCalibrationPreviewCallbackWrapper 
    : public NativeCallbackWrapper<CamCamExtrinsicPreviewEventHandler, CamCamExtrinsicCalibrationPreviewEventArgs,
                                   sl::CamCamExtrinsicCalibrationPreviewData>
{
public:
    CamCamCalibrationPreviewCallbackWrapper(CamCamExtrinsicPreviewEventHandler^ del, System::Object^ sender)
        : NativeCallbackWrapper(del, sender)
    {
    }

protected:
    CamCamExtrinsicCalibrationPreviewEventArgs^ transformNativeArgs(sl::CamCamExtrinsicCalibrationPreviewData e)
    {
        return gcnew CamCamExtrinsicCalibrationPreviewEventArgs(e);
    }
};

} }
