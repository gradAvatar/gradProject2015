#pragma once

#include "M_Utilities.hpp"
#include "clr_scoped_ptr.hpp"
#include <MeshGeneration.hpp>
#include <PointCloudAlignment.hpp>

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Runtime::InteropServices;

namespace StructuredLight {
namespace Managed {
    
[StructLayout(LayoutKind::Sequential)]
public value struct Point3d
{
public:
    float X;
    float Y;
    float Z;

    //Point3d() { };

    Point3d(float x, float y, float z)
    {
        X = x;
        Y = y;
        Z = z;
    };

    Point3d(sl::Point3d point)
    {
        X = point.X;
        Y = point.Y;
        Z = point.Z;
    };
};

public ref class TriPolygon
{
public:
    Point3d A;
    Point3d B;
    Point3d C;

    TriPolygon(sl::Polygon poly)
    {
        A = Point3d(poly.A.X, poly.A.Y, poly.A.Z);
        B = Point3d(poly.B.X, poly.B.Y, poly.B.Z);
        C = Point3d(poly.C.X, poly.C.Y, poly.C.Z);
    };
};

public ref class MeshGenerator
{
public:
    MeshGenerator();
    
    List<TriPolygon^>^ GenerateConcaveHull(double alpha, List<Point3d>^ lst);
    List<TriPolygon^>^ SurfaceReconstruction(List<Point3d>^ lst);
	void ConvertPointCloudToMesh(String^ inputFileName, String^ outputFileName);

    List<Point3d>^ LoadObjPointCloud(String^ filename);

	static void Testing();

private:

    std::vector<sl::Polygon> clrToCppMesh(List<TriPolygon^>^ lst);
    std::vector<sl::Point3d> clrToCppPointCloud(List<Point3d>^ lst);
    List<Point3d>^ cppToClrMesh(std::vector<sl::Point3d> lst);
    List<TriPolygon^>^ cppToClrMesh(std::vector<sl::Polygon> lst);
};

} }
