#pragma once

#include "M_Camera.hpp"
#include "M_Utilities.hpp"
#include "clr_scoped_ptr.hpp"
#include <Scanner.hpp>

namespace StructuredLight {
namespace Managed {

public enum class ScanType
{
    /// <summary>
    /// Traditional black and white structured light scanning
    /// </summary>
    BlackAndWhite = sl::BW,
 
    /// <summary>
    /// Takes two structured light patterns and combines them into a single RGB
    /// image by placing one pattern into the green channel and another into both
    /// the red and blue channels. This, in theory, works well with cameras that
    /// utilize a Bayer-arrangement where, for each pixel, there are two green
    /// sensors, one red, and one blue.
    /// </summary>
    Bayer = sl::BAYER,
 
    /// <summary>
    /// Takes three structured light patterns and combines them into a single RGB
    /// image simply by converting each pattern into an intensity map and
    /// assigning each to the red, blue, and green channels in the image.
    /// </summary>
    Rgb = sl::RGB
};

public value struct GrayCodeColor
{
public:
    int Red;
    int Green;
    int Blue;

    GrayCodeColor(byte red_, byte green_, byte blue_)
        : Red(red_), Green(green_), Blue(blue_)
    {
    }
};


public ref class GrayCodeColors abstract sealed
{
public:
    static const GrayCodeColor Red = GrayCodeColor(255, 0, 0);
    static const GrayCodeColor Green = GrayCodeColor(0, 255, 0);
    static const GrayCodeColor Blue = GrayCodeColor(0, 0, 255);
    static const GrayCodeColor Yellow = GrayCodeColor(255, 255, 0);
    static const GrayCodeColor White = GrayCodeColor(255, 255, 255);
};

public ref class ScanPreviewEventArgs : public System::EventArgs
{
public:
    SIMPLE_RO_PROP(System::Collections::ObjectModel::ReadOnlyCollection<System::Drawing::Bitmap^>^,
                   ScanPreviews, _previews->AsReadOnly());
    SIMPLE_RO_PROP(int, ImageIndex, _imgI);
    SIMPLE_RO_PROP(int, TotalImages, _totalImgs);

internal:
    ScanPreviewEventArgs(sl::ScanPreviewData data)
        : _previews(gcnew System::Collections::Generic::List<System::Drawing::Bitmap^>()),
          _imgI(data.getImageIndex()),
          _totalImgs(data.getTotalImages())
    {
        for (unsigned int i = 0; i < data.getPreviewCount(); i++)
        {
            _previews->Add(matToBmp(data.getPreview(i)));
        }
    }

private:
    initonly System::Collections::Generic::List<System::Drawing::Bitmap^>^ _previews;
    const int _imgI, _totalImgs;
};

public ref class Scanner
{
public:
    Scanner(ManagedCamera^ cam, ManagedProjector^ proj);
    Scanner(System::Collections::Generic::IList<ManagedCamera^>^ cams,
            System::Collections::Generic::IList<ManagedProjector^>^ projs);
    Scanner(System::Collections::Generic::IList<ManagedCamera^>^ cams,
            System::Collections::Generic::IList<ManagedProjector^>^ projs,
            System::Collections::Generic::IList<GrayCodeColor>^ colors);

    event System::EventHandler<ScanPreviewEventArgs^>^ NewScanPreview
    {
        void add(System::EventHandler<ScanPreviewEventArgs^>^ e);
        void remove(System::EventHandler<ScanPreviewEventArgs^>^ e);
    }

    event System::EventHandler^ ScanComplete
    {
        void add(System::EventHandler^ e);
        void remove(System::EventHandler^ e);
    }

    SIMPLE_RO_PROP(System::Collections::ObjectModel::ReadOnlyCollection<ManagedCamera^>^,
                   Cameras, gcnew System::Collections::ObjectModel::ReadOnlyCollection<ManagedCamera^>(_cams));
    SIMPLE_RO_PROP(System::Collections::ObjectModel::ReadOnlyCollection<ManagedProjector^>^,
                   Projectors, gcnew System::Collections::ObjectModel::ReadOnlyCollection<ManagedProjector^>(_projs));
    SIMPLE_RO_PROP(bool, IsScanning, _scanner->isScanning());

    SIMPLE_RW_PROP(System::String^, OutputFileName, (System::String^)cppToClr(_scanner->getOutputFileName()),
        _scanner->setOutputFileName(clrToCpp((System::String^)value)));

    void Scan();
    void Cancel();

private:
    initonly System::Collections::Generic::IList<ManagedCamera^>^ _cams;
    initonly System::Collections::Generic::IList<ManagedProjector^>^ _projs;
    clr_scoped_ptr<sl::Scanner> _scannerScopedPtr;
    sl::Scanner* const _scanner;

    initonly System::Collections::Generic::Dictionary<System::EventHandler<ScanPreviewEventArgs^>^,
                                                      sl::CallbackToken>^ _previewCallbacks;
    initonly System::Collections::Generic::Dictionary<System::EventHandler^,
                                                      sl::CallbackToken>^ _finishedCallbacks;
};

} }
