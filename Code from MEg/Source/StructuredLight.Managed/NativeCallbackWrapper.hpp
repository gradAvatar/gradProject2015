#pragma once

#include <vcclr.h>

namespace StructuredLight {
namespace Managed {

template <typename TDelegate, typename TManagedArgs, typename TNativeArgs>
class NativeCallbackWrapper
{
public:
    NativeCallbackWrapper(TDelegate^ del, System::Object^ sender)
        : _sender(sender), _delegate(del)
    {
    }

    void operator()(TNativeArgs e)
    {
        TDelegate^ handler = _delegate;
        handler(_sender, transformNativeArgs(e));
    }

protected:
    virtual TManagedArgs^ transformNativeArgs(TNativeArgs e) = 0;

private:
    gcroot<System::Object^> _sender;
    gcroot<TDelegate^> _delegate;
};

template <>
class NativeCallbackWrapper<System::EventHandler, System::EventArgs, void>
{
public:
    NativeCallbackWrapper(System::EventHandler^ del, System::Object^ sender)
        : _sender(sender), _delegate(del)
    {
    }

    void operator()()
    {
        System::EventHandler^ handler = _delegate;
        handler(_sender, gcnew System::EventArgs());
    }

private:
    gcroot<System::Object^> _sender;
    gcroot<System::EventHandler^> _delegate;
};

class EmptyNativeCallbackWrapper
    : public NativeCallbackWrapper<System::EventHandler, System::EventArgs, void>
{
public:
    EmptyNativeCallbackWrapper(System::EventHandler^ del, System::Object^ sender)
        : NativeCallbackWrapper(del, sender)
    {
    }
};

} }
