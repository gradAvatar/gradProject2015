#include "M_Calibration.hpp"
#include "M_Exceptions.hpp"
#include <Exceptions.hpp>
#include <vcclr.h>
#include <msclr/lock.h>

using namespace System;
using namespace System::Collections::Generic;

using namespace std::placeholders;

namespace StructuredLight {
namespace Managed {

CamProjExtrinsicCalibrator::CamProjExtrinsicCalibrator(ManagedCamera^ c, ManagedProjector^ d)
    : _cam(c), _calibrator(new sl::CamProjExtrinsicCalibrator(c->UnmanagedCamera, d->UnmanagedProjector)),
      _previewCallbacks(gcnew Dictionary<PreviewEventHandler^, sl::CallbackToken>()),
      _finishedCallbacks(gcnew Dictionary<EventHandler^, sl::CallbackToken>())
{
}

void CamProjExtrinsicCalibrator::NewCalibrationPreview::add(PreviewEventHandler^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token = _calibrator->addPreviewCallback(
        CalibrationPreviewCallbackWrapper(e, this));

    _previewCallbacks->Add(e, token);
}

void CamProjExtrinsicCalibrator::NewCalibrationPreview::remove(PreviewEventHandler^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token;
    if (_previewCallbacks->TryGetValue(e, token))
    {
        _calibrator->removePreviewCallback(token);
        _previewCallbacks->Remove(e);
    }
}

void CamProjExtrinsicCalibrator::CalibrationFinished::add(System::EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token = _calibrator->addFinishedCallback(
        EmptyNativeCallbackWrapper(e, this));

    _finishedCallbacks->Add(e, token);
}

void CamProjExtrinsicCalibrator::CalibrationFinished::remove(System::EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token;
    if (_finishedCallbacks->TryGetValue(e, token))
    {
        _calibrator->removeFinishedCallback(token);
        _finishedCallbacks->Remove(e);
    }
}

int CamProjExtrinsicCalibrator::TargetCameraCaptures::get() { return _calibrator->getTargetAcceptedCaptures(); }
void CamProjExtrinsicCalibrator::TargetCameraCaptures::set(int val)
{
    try
    {
        _calibrator->setTargetAcceptedCaptures(val);
    }
    catch (sl::CalibrationStartedException ex)
    {
        throw gcnew CalibrationStartedException(ex.what());
    }
}

CamProjExtrinsicCalibration^ CamProjExtrinsicCalibrator::Results::get()
{
    sl::CamProjExtrinsicCalibration calib;
    return _calibrator->getResults(calib) ? gcnew CamProjExtrinsicCalibration(calib) : nullptr;
}

void CamProjExtrinsicCalibrator::Start()
{
    try
    {
        _calibrator->start();
    }
    catch (sl::CalibrationAlreadyStartedException ex)
    {
        throw gcnew CalibrationAlreadyStartedException(ex.what());
    }
}

void CamProjExtrinsicCalibrator::Cancel()
{
    try
    {
        _calibrator->cancel();
    }
    catch (sl::CalibrationNotStartedException ex)
    {
        throw gcnew CalibrationNotStartedException(ex.what());
    }
}

CamCamExtrinsicCalibrator::CamCamExtrinsicCalibrator(ManagedCamera^ source, ManagedCamera^ target)
    : _sourceCam(source), _targetCam(target), 
      _calibrator(new sl::CamCamExtrinsicCalibrator(source->UnmanagedCamera, target->UnmanagedCamera)),
      _previewCallbacks(gcnew Dictionary<CamCamExtrinsicPreviewEventHandler^, sl::CallbackToken>()),
      _finishedCallbacks(gcnew Dictionary<EventHandler^, sl::CallbackToken>())
{
}

void CamCamExtrinsicCalibrator::NewCalibrationPreview::add(CamCamExtrinsicPreviewEventHandler^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token = _calibrator->addPreviewCallback(
        CamCamCalibrationPreviewCallbackWrapper(e, this));

    _previewCallbacks->Add(e, token);
}

void CamCamExtrinsicCalibrator::NewCalibrationPreview::remove(CamCamExtrinsicPreviewEventHandler^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token;
    if (_previewCallbacks->TryGetValue(e, token))
    {
        _calibrator->removePreviewCallback(token);
        _previewCallbacks->Remove(e);
    }
}

void CamCamExtrinsicCalibrator::CalibrationFinished::add(System::EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token = _calibrator->addFinishedCallback(
        EmptyNativeCallbackWrapper(e, this));

    _finishedCallbacks->Add(e, token);
}

void CamCamExtrinsicCalibrator::CalibrationFinished::remove(System::EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token;
    if (_finishedCallbacks->TryGetValue(e, token))
    {
        _calibrator->removeFinishedCallback(token);
        _finishedCallbacks->Remove(e);
    }
}

int CamCamExtrinsicCalibrator::TargetCameraCaptures::get() { return _calibrator->getTargetAcceptedCaptures(); }
void CamCamExtrinsicCalibrator::TargetCameraCaptures::set(int val)
{
    try
    {
        _calibrator->setTargetAcceptedCaptures(val);
    }
    catch (sl::CalibrationStartedException ex)
    {
        throw gcnew CalibrationStartedException(ex.what());
    }
}

CamCamExtrinsicCalibration^ CamCamExtrinsicCalibrator::Results::get()
{
    sl::CamCamExtrinsicCalibration calib;
    return _calibrator->getResults(calib) ? gcnew CamCamExtrinsicCalibration(calib) : nullptr;
}

void CamCamExtrinsicCalibrator::Start()
{
    try
    {
        _calibrator->start();
    }
    catch (sl::CalibrationAlreadyStartedException ex)
    {
        throw gcnew CalibrationAlreadyStartedException(ex.what());
    }
}

void CamCamExtrinsicCalibrator::Cancel()
{
    try
    {
        _calibrator->cancel();
    }
    catch (sl::CalibrationNotStartedException ex)
    {
        throw gcnew CalibrationNotStartedException(ex.what());
    }
}

} }