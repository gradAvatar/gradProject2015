#include "M_Scanner.hpp"
#include "NativeCallbackWrapper.hpp"
#include <msclr/lock.h>

using namespace System;
using namespace System::Collections::Generic;
using namespace System::ComponentModel;
using namespace System::Threading;
using namespace StructuredLight::Managed;

namespace {

class ScannerPreviewCallbackWrapper
    : public NativeCallbackWrapper<System::EventHandler<ScanPreviewEventArgs^>, ScanPreviewEventArgs,
                                   sl::ScanPreviewData>
{
public:
    ScannerPreviewCallbackWrapper(System::EventHandler<ScanPreviewEventArgs^>^ del, System::Object^ sender)
        : NativeCallbackWrapper(del, sender)
    {
    }

protected:
    ScanPreviewEventArgs^ transformNativeArgs(sl::ScanPreviewData e)
    {
        return gcnew ScanPreviewEventArgs(e);
    }
};

std::vector<sl::Camera*> managedCamListToUnmanaged(IList<ManagedCamera^>^ list)
{
    std::vector<sl::Camera*> cams(list->Count);
    for (int i = 0; i < list->Count; i++)
    {
        cams[i] = list[i]->UnmanagedCamera;
    }

    return cams;
}

std::vector<sl::Projector*> managedProjListToUnmanaged(IList<ManagedProjector^>^ list)
{
    std::vector<sl::Projector*> projs(list->Count);
    for (int i = 0; i < list->Count; i++)
    {
        projs[i] = list[i]->UnmanagedProjector;
    }

    return projs;
}

std::vector<sl::GrayCodeColor> managedColorListToUnmanaged(IList<GrayCodeColor>^ list)
{
    std::vector<sl::GrayCodeColor> colors(list->Count);
    for (int i = 0; i < list->Count; i++)
    {
        colors[i] = clrToCpp(list[i]);
    }

    return colors;
}

}

namespace StructuredLight {
namespace Managed {

Scanner::Scanner(ManagedCamera^ cam, ManagedProjector^ proj)
    : _scannerScopedPtr(new sl::Scanner(cam->UnmanagedCamera, proj->UnmanagedProjector)),
      _scanner(_scannerScopedPtr.get()), _cams(gcnew List<ManagedCamera^>()),
      _projs(gcnew List<ManagedProjector^>()),
      _previewCallbacks(gcnew Dictionary<EventHandler<ScanPreviewEventArgs^>^, sl::CallbackToken>()),
      _finishedCallbacks(gcnew Dictionary<EventHandler^, sl::CallbackToken>())
{
    _cams->Add(cam);
    _projs->Add(proj);
}

Scanner::Scanner(IList<ManagedCamera^>^ cams, IList<ManagedProjector^>^ projs)
    : _scannerScopedPtr(new sl::Scanner(managedCamListToUnmanaged(cams),
                                        managedProjListToUnmanaged(projs))),
      _scanner(_scannerScopedPtr.get()), _cams(cams), _projs(projs),
      _previewCallbacks(gcnew Dictionary<EventHandler<ScanPreviewEventArgs^>^, sl::CallbackToken>()),
      _finishedCallbacks(gcnew Dictionary<EventHandler^, sl::CallbackToken>())
{
}

Scanner::Scanner(IList<ManagedCamera^>^ cams, IList<ManagedProjector^>^ projs,
                 IList<GrayCodeColor>^ colors)
    : _scannerScopedPtr(new sl::Scanner(managedCamListToUnmanaged(cams),
                                        managedProjListToUnmanaged(projs),
                                        managedColorListToUnmanaged(colors))),
      _scanner(_scannerScopedPtr.get()), _cams(cams), _projs(projs),
      _previewCallbacks(gcnew Dictionary<EventHandler<ScanPreviewEventArgs^>^, sl::CallbackToken>()),
      _finishedCallbacks(gcnew Dictionary<EventHandler^, sl::CallbackToken>())
{
}

void Scanner::NewScanPreview::add(EventHandler<ScanPreviewEventArgs^>^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token = _scanner->addPreviewCallback(
        ScannerPreviewCallbackWrapper(e, this));

    _previewCallbacks->Add(e, token);
}

void Scanner::NewScanPreview::remove(EventHandler<ScanPreviewEventArgs^>^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token;
    if (_previewCallbacks->TryGetValue(e, token))
    {
        _scanner->removePreviewCallback(token);
        _previewCallbacks->Remove(e);
    }
}

void Scanner::ScanComplete::add(EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token = _scanner->addFinishedCallback(
        EmptyNativeCallbackWrapper(e, this));

    _finishedCallbacks->Add(e, token);
}

void Scanner::ScanComplete::remove(EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token;
    if (_finishedCallbacks->TryGetValue(e, token))
    {
        _scanner->removeFinishedCallback(token);
        _finishedCallbacks->Remove(e);
    }
}

void Scanner::Scan()
{
    _scanner->scan();
}

void Scanner::Cancel()
{
    _scanner->cancel();
}

} }