#pragma once

#include "M_Utilities.hpp"
#include "clr_scoped_ptr.hpp"
#include <Camera.hpp>
#include <CameraStream.hpp>

namespace StructuredLight {
namespace Managed {

// Forward declarations
ref class ManagedCamera;

/// <summary>
/// Values that represent the two types of stream modes that a CameraStream supports.
/// </summary>
public enum class StreamMode
{
    /// <summary>
    /// Represents a CameraStream mode that causes the stream to continuously capture images from
    /// the underlying Camera.
    /// </summary>
    Auto = sl::AUTO,

    /// <summary>
    /// Represents a CameraStream mode that causes the stream to only capture images when
    /// Trigger() is called.
    /// </summary>
    Manual = sl::MANUAL
};

public ref class StreamLock
{
public:
    ~StreamLock();
    !StreamLock();

internal:
    StreamLock(sl::StreamLock* lock);

private:
    sl::StreamLock* _lock;
};

public ref class ImageCapturedEventArgs : System::EventArgs
{
private:
    initonly ManagedCamera^ _cam;
    initonly System::Drawing::Bitmap^ _preview;

internal:
    ImageCapturedEventArgs(ManagedCamera^ cam,
                           System::Drawing::Bitmap^ preview)
        : _cam(cam), _preview(preview)
    {
    }

public:
    /// <summary>
    /// The camera from which the captured image was taken.
    /// </summary>
    SIMPLE_RO_PROP(ManagedCamera^, Camera, _cam);

    /// <summary>
    /// The captured camera image.
    /// </summary>
    SIMPLE_RO_PROP(System::Drawing::Bitmap^, Preview, _preview);
};

/// <summary>
/// A class that allows a client to obtain a stream of images from a Camera.
/// </summary>
public ref class ManagedCameraStream
{
private:
    clr_scoped_ptr<std::shared_ptr<sl::CameraStream>> _streamScopedPtr;
    sl::CameraStream* const _stream;

    initonly ManagedCamera^ _cam;
    initonly System::Collections::Generic::Dictionary<System::EventHandler<ImageCapturedEventArgs^>^,
                                                      sl::CallbackToken>^ _callbacks;

internal:
    ManagedCameraStream(ManagedCamera^ managedCam,
                        std::shared_ptr<sl::CameraStream>& stream);

public:
    /// <summary>
    /// Occurs when a new image has been captured and is ready to be processed.
    /// </summary>
    event System::EventHandler<ImageCapturedEventArgs^>^ ImageCaptured
    {
        void add(System::EventHandler<ImageCapturedEventArgs^>^ e);
        void remove(System::EventHandler<ImageCapturedEventArgs^>^ e);
    }

    /// <summary>
    /// Gets a value indicating whether or not the camera stream has been started.
    /// </summary>
    SIMPLE_RO_PROP(bool, IsStarted, _stream->isStarted());

    /// <summary>
    /// Gets the camera this stream is connected to.
    /// </summary>
    SIMPLE_RO_PROP(ManagedCamera^, Camera, _cam);

    /// <summary>
    /// Gets the current mode of capturing images.
    /// </summary>
    SIMPLE_RW_PROP(StreamMode, Mode, (StreamMode)_stream->getStreamMode(),
                                     _stream->setStreamMode((sl::STREAM_MODE)value));

    /// <summary>
    /// Attempts to connect to the CameraStream's underlying Camera and begin streaming.
    /// </summary>
    /// <returns> 
    /// <c>true</c> if the connection to the camera hardware succeeds, or if the stream has
    /// already been started; <c>false</c> otherwise.
    /// </returns>
    bool Start();

    /// <summary>
    /// Attempts to disconnect from the underlying Camera and stop streaming.
    /// </summary>
    /// <returns> 
    /// <c>true</c> if the connection to the camera hardware was successfully destroyed, or if
    /// no stream is currently open; <c>false</c> otherwise.
    /// </returns>
    bool Stop();
    
    /// <summary>
    /// Attempts to lock the camera stream to prevent others from changing it
    /// </summary>
    /// <param name="mode">Which mode to change the stream to</param>
    /// <param name="streamLock">The lock</param>
    /// <returns> 
    /// <c>true</c> if the camera stream was successfully locked; <c>false</c> otherwise.
    /// </returns>
    bool LockStreamMode(StreamMode mode, [System::Runtime::InteropServices::Out] StreamLock^ %streamLock);
    
    /// <summary>
    /// Waits for the next image from the underlying Camera to arrive.
    /// </summary>
    /// <returns> 
    /// <c>true</c> if an image arrived within the specified timeout; <c>false</c> otherwise.
    /// </returns>
    bool WaitForFrame();

    /// <summary>
    /// Waits for the next image from the underlying Camera to arrive.
    /// </summary>
    /// <param name="milliseconds">How long to wait for the image to arrive.</param>
    /// <returns> 
    /// <c>true</c> if an image arrived within the specified timeout; <c>false</c> otherwise.
    /// </returns>
    bool WaitForFrame(int milliseconds);

    /// <summary>
    /// Requests that an image should be captured as soon as possible. Only works when the stream
    /// mode is set to manual (i.e.
    /// <see cref="StreamMode"/> == <see cref="StructuredLight.Managed.StreamMode.Manual"/>).
    /// </summary>
    void Trigger();
};

} }