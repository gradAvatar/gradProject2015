#include "M_Projector.hpp"

using namespace System::Collections::Generic;
using namespace System::Drawing;

namespace StructuredLight {
namespace Managed {

List<ManagedProjector^>^ ManagedProjector::EnumerateProjectors()
{
    auto devices = sl::Projector::enumerateProjectors();

    List<ManagedProjector^>^ managedDevs = gcnew List<ManagedProjector^>();
    for (unsigned int i = 0; i < devices.size(); i++)
    {
        managedDevs->Add(gcnew ManagedProjector(devices[i]));
    }

    return managedDevs;
}

ManagedProjector::ManagedProjector(std::shared_ptr<sl::Projector> dev)
    : _devSharedPtr(new std::shared_ptr<sl::Projector>(dev)), _dev(_devSharedPtr->get())
{
}

System::String^ ManagedProjector::FriendlyName::get()
{
    std::string name;
    return sl::getFriendlyName(UnmanagedProjector, &name) ? cppToClr(name) : nullptr;
}

void ManagedProjector::FriendlyName::set(System::String^ name)
{
    sl::setFriendlyName(UnmanagedProjector, clrToCpp(name));
}

void ManagedProjector::SetIntrinsicCalibration(IntrinsicCalibration^ calib)
{
    sl::saveIntrinsicCalibration(_dev, calib->UnmanagedCalibration);
}

} }