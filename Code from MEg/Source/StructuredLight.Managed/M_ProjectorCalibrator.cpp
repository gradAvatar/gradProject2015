#include "M_Calibration.hpp"
#include "M_Exceptions.hpp"
#include <Exceptions.hpp>
#include <vcclr.h>
#include <msclr/lock.h>

using namespace System;
using namespace System::Collections::Generic;

using namespace std::placeholders;

namespace StructuredLight {
namespace Managed {

ProjectorCalibrator::ProjectorCalibrator(ManagedCamera^ cam, ManagedProjector^ device)
    : _cam(cam), _calibrator(new sl::ProjectorCalibrator(cam->UnmanagedCamera, device->UnmanagedProjector)),
      _previewCallbacks(gcnew Dictionary<PreviewEventHandler^, sl::CallbackToken>()),
      _finishedCallbacks(gcnew Dictionary<EventHandler^, sl::CallbackToken>())
{
}

void ProjectorCalibrator::NewCalibrationPreview::add(PreviewEventHandler^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token = _calibrator->addPreviewCallback(
        CalibrationPreviewCallbackWrapper(e, this));

    _previewCallbacks->Add(e, token);
}

void ProjectorCalibrator::NewCalibrationPreview::remove(PreviewEventHandler^ e)
{
    msclr::lock l(_previewCallbacks);

    sl::CallbackToken token;
    if (_previewCallbacks->TryGetValue(e, token))
    {
        _calibrator->removePreviewCallback(token);
        _previewCallbacks->Remove(e);
    }
}

void ProjectorCalibrator::CalibrationFinished::add(System::EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token = _calibrator->addFinishedCallback(
        EmptyNativeCallbackWrapper(e, this));

    _finishedCallbacks->Add(e, token);
}

void ProjectorCalibrator::CalibrationFinished::remove(System::EventHandler^ e)
{
    msclr::lock l(_finishedCallbacks);

    sl::CallbackToken token;
    if (_finishedCallbacks->TryGetValue(e, token))
    {
        _calibrator->removeFinishedCallback(token);
        _finishedCallbacks->Remove(e);
    }
}

int ProjectorCalibrator::TargetCameraCaptures::get() { return _calibrator->getTargetAcceptedCaptures(); }
void ProjectorCalibrator::TargetCameraCaptures::set(int val)
{
    try
    {
        _calibrator->setTargetAcceptedCaptures(val);
    }
    catch (sl::CalibrationStartedException ex)
    {
        throw gcnew CalibrationStartedException(ex.what());
    }
}

IntrinsicCalibration^ ProjectorCalibrator::Results::get()
{
    sl::IntrinsicCalibration calib;
    return _calibrator->getResults(calib) ? gcnew IntrinsicCalibration(calib) : nullptr;
}

void ProjectorCalibrator::Start()
{
    try
    {
        _calibrator->start();
    }
    catch (sl::CalibrationAlreadyStartedException ex)
    {
        throw gcnew CalibrationAlreadyStartedException(ex.what());
    }
}

void ProjectorCalibrator::Cancel()
{
    try
    {
        _calibrator->cancel();
    }
    catch (sl::CalibrationNotStartedException ex)
    {
        throw gcnew CalibrationNotStartedException(ex.what());
    }
}

} }