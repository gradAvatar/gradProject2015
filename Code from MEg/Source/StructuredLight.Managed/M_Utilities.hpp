#pragma once

#using "System.Drawing.dll"

#include <string>
#include <opencv2/opencv.hpp>

#define SIMPLE_RO_PROP(type, name, retVal) property type name { type get() { return retVal; } }
#define SIMPLE_RO_PROP_ABSTRACT(type, name) property type name { virtual type get() abstract; }
#define SIMPLE_RO_PROP_OVERRIDE(type, name, retVal) property type name { virtual type get() override { return retVal; } }
#define SIMPLE_RW_PROP(type, name, retVal, setter) property type name { type get() { return retVal; } void set(type value) { setter; } }

namespace sl { struct GrayCodeColor; }

namespace StructuredLight {
namespace Managed {

value struct GrayCodeColor;

System::Drawing::Bitmap^ matToBmp(cv::Mat m);
cv::Mat bmpToTempMat(System::Drawing::Bitmap^ bmp);

int pixelFormatToOpenCvType(System::Drawing::Imaging::PixelFormat f);

System::String^ cppToClr(std::string str);
System::Drawing::Size cppToClr(cv::Size size);
System::Drawing::Point cppToClr(cv::Point2i pt);
array<double, 2>^ cppToClr(cv::Mat mat);

std::string clrToCpp(System::String^ str);
sl::GrayCodeColor clrToCpp(GrayCodeColor% color);

} }