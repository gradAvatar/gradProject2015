#pragma once

namespace StructuredLight {
namespace Managed {

public ref class ManagedDevice abstract
{
public:
    /// <summary>
    /// Gets the model name of the camera.
    /// </summary>
    SIMPLE_RO_PROP_ABSTRACT(System::String^, ModelName);

    /// <summary>
    /// The serial number of the camera.
    /// </summary>
    SIMPLE_RO_PROP_ABSTRACT(System::String^, SerialNumber);

    /// <summary>
    /// Gets a value indicating whether or not the camera has been intrinsicly calibrated.
    /// </summary>
    SIMPLE_RO_PROP_ABSTRACT(bool, IsIntrinsiclyCalibrated);

    /// <summary>
    /// Gets the resolution of the camera.
    /// </summary>
    SIMPLE_RO_PROP_ABSTRACT(System::Drawing::Size, Resolution);

    /// <summary>
    /// Gets or sets the "friendly name" of the ManagedCamera. This is an arbitrary value, and does
    /// not affect library behavior in any way; it's merely a convenience for the user.
    /// </summary>
    /// <returns>
    /// The value of the "friendly name" of the ManagedCamera, if it is set; <c>null</c> otherwise.
    /// </returns>
    property System::String^ FriendlyName
    {
        virtual System::String^ get() abstract;
        virtual void set(System::String^ name) abstract;
    }

internal:
    ManagedDevice() { }
};

} }
