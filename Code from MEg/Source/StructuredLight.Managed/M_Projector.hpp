#pragma once

#include "M_Utilities.hpp"
#include "M_Calibration.hpp"
#include "M_Device.hpp"
#include "clr_scoped_ptr.hpp"
#include <Projector.hpp>
#include <Serialization.hpp>

namespace StructuredLight {
namespace Managed {

// Forward declarations
ref class IntrinsicCalibration;

public ref class ManagedProjector : public ManagedDevice
{
public:
    /// <summary>
    /// Gets a list of all projectors currently connected to the system.
    /// </summary>
    /// <returns>A list of all projectors currently connected to the system.</returns>
    static System::Collections::Generic::List<ManagedProjector^>^ EnumerateProjectors();
    
    /// <summary>
    /// Gets the device ID of the projector.
    /// </summary>
    SIMPLE_RO_PROP(System::String^, DeviceId, cppToClr(_dev->getDeviceId()));

    /// <summary>
    /// Gets the model name of the projector.
    /// </summary>
    SIMPLE_RO_PROP_OVERRIDE(System::String^, ModelName, cppToClr(_dev->getModelName()));

    /// <summary>
    /// Gets the serial number of the projector.
    /// </summary>
    SIMPLE_RO_PROP_OVERRIDE(System::String^, SerialNumber, cppToClr(_dev->getSerialNumber()));

    /// <summary>
    /// Gets the location where the top-left corner of this projector is in the monitor space.
    /// </summary>
    SIMPLE_RO_PROP(System::Drawing::Point, Origin, cppToClr(_dev->getOrigin()));

    /// <summary>
    /// Gets the current resolution of the projector.
    /// </summary>
    SIMPLE_RO_PROP_OVERRIDE(System::Drawing::Size, Resolution, cppToClr(_dev->getResolution()));

    /// <summary>
    /// Gets a value indicating whether or not the projector has been intrinsically calibrated.
    /// </summary>
    SIMPLE_RO_PROP_OVERRIDE(bool, IsIntrinsiclyCalibrated, loadIntrinsicCalibration(_dev));

    /// <summary>
    /// Gets a value indicating whether or not this projector is the primary monitor of the system.
    /// </summary>
    SIMPLE_RO_PROP(bool, IsPrimary, _dev->isPrimary());

    /// <summary>
    /// Gets or sets the "friendly name" of the ManagedProjector. This is an arbitrary value, and
    /// does not affect library behavior in any way; it's merely a convenience for the user.
    /// </summary>
    /// <returns>
    /// The value of the "friendly name" of the ManagedProjector, if it is set; <c>null</c>
    /// otherwise.
    /// </returns>
    property System::String^ FriendlyName
    {
        virtual System::String^ get() override;
        virtual void set(System::String^ name) override;
    }

    /// <summary>
    /// Sets the intrinsic calibration of this projector to the specified value.
    /// </summary> 
    /// <param name="calib">The calibration to set.</param>
    void SetIntrinsicCalibration(IntrinsicCalibration^ calib);

internal:
    ManagedProjector(std::shared_ptr<sl::Projector> dev);

    SIMPLE_RO_PROP(sl::Projector* const, UnmanagedProjector, _dev);

private:
    clr_scoped_ptr<std::shared_ptr<sl::Projector>> _devSharedPtr;
    sl::Projector* const _dev;
};

} }
