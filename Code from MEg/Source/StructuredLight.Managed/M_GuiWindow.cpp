#include "M_GuiWindow.hpp"
#include "M_Utilities.hpp"
#include <msclr/lock.h>

using namespace System;
using namespace System::Collections::Generic;

namespace StructuredLight {
namespace Managed {

GuiWindow::GuiWindow(ManagedProjector^ proj)
    : _windowScopedPtr(new sl::GuiWindow(proj->UnmanagedProjector)),
      _window(_windowScopedPtr.get())
{
}

void GuiWindow::Clear()
{
    _window->clear();
}

void GuiWindow::Close()
{
    _window->clear();
}

void GuiWindow::SetImage(System::Drawing::Bitmap^ bmp)
{
    _window->setImage(bmpToTempMat(bmp));
}

} }