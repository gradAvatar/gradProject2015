#pragma once

#include "M_Camera.hpp"
#include "M_Utilities.hpp"
#include "clr_scoped_ptr.hpp"
#include <GuiWindow.hpp>

namespace StructuredLight {
namespace Managed {

public ref class GuiWindow
{
public:
    GuiWindow(ManagedProjector^ proj);
    
    void SetImage(System::Drawing::Bitmap^ bmp);
    void Close();
    void Clear();

private:
    clr_scoped_ptr<sl::GuiWindow> _windowScopedPtr;
    sl::GuiWindow* const _window;
};

} }
