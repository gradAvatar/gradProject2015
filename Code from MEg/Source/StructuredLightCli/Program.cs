﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuredLight.Managed;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics.Contracts;
using System.Diagnostics;

namespace StructuredLightCli
{
    static class Program
    {
        static void Main(string[] args)
        {
            //MeshGenerator.Testing();
            Library.Initialize("config.xml");

            bool exit = false;
            while (!exit)
            {
                Console.WriteLine();
                Console.WriteLine("1: Scan a subject");
                Console.WriteLine("2: View camera stream");
                Console.WriteLine("3: Calibrate camera");
                Console.WriteLine("4: Calibrate projector");
                Console.WriteLine("5: Align projector and camera");
                Console.WriteLine("6: Align camera and camera");
                Console.WriteLine("7: Manage friendly names");
                Console.WriteLine("0: Exit");
                Console.WriteLine();
                Console.Write("Choose an option: ");

                int choice;
                while (!ReadInt32(out choice))
                {
                }

                switch (choice)
                {
                    case 1:
                        {
                            List<ManagedCamera> cams = new List<ManagedCamera>();
                            List<ManagedProjector> projs = new List<ManagedProjector>();
                            List<GrayCodeColor> colors = new List<GrayCodeColor>();
                            Dictionary<GrayCodeColor, string> colorConverter = new Dictionary<GrayCodeColor, string>
                            {
                                { GrayCodeColors.Blue, "Blue" },
                                { GrayCodeColors.Green, "Green" },
                                { GrayCodeColors.Red, "Red" },
                                { GrayCodeColors.White, "White" },
                                { GrayCodeColors.Yellow, "Yellow" }
                            };
                            if(!string.IsNullOrEmpty(Library.UserSettings["LastScanSettingsExist"]))
                            {
                                Console.WriteLine("Do you want to use the same settings as the last scan?");
                                Console.WriteLine("0: No");
                                Console.WriteLine("1: Yes");
                                if(Console.ReadLine() == "1")
                                {
                                    var allCams = ManagedCamera.EnumerateCameras();
                                    var allProjs = ManagedProjector.EnumerateProjectors();
                                    cams = new List<ManagedCamera>(Library.UserSettings["LastScanSettingsCameras"].Split(',').Select<string, ManagedCamera>((s) => allCams.First((mc) => mc.SerialNumber == s)));
                                    projs = new List<ManagedProjector>(Library.UserSettings["LastScanSettingsProjectors"].Split(',').Select<string, ManagedProjector>((s) => allProjs.First((mc) => mc.SerialNumber == s)));
                                    colors = new List<GrayCodeColor>(Library.UserSettings["LastScanSettingsColors"].Split(',').Select<string, GrayCodeColor>((s) => colorConverter.First((mc) => mc.Value == s).Key));
                                }
                            }
                            if (cams.Count == 0)
                            {
                                cams = ManagedCamera.EnumerateCameras().PromptForManyCameras();
                                if (cams == null)
                                {
                                    break;
                                }

                                projs = ManagedProjector.EnumerateProjectors().PromptForManyProjectors();
                                colors = PromptForColors(cams.Count);
                            }
                                
                            Library.UserSettings["LastScanSettingsExist"] = "true";
                            Library.UserSettings["LastScanSettingsCameras"] = string.Join(",", cams.ConvertAll<string>((cam) => cam.SerialNumber));
                            Library.UserSettings["LastScanSettingsProjectors"] = string.Join(",", projs.ConvertAll<string>((proj) => proj.SerialNumber));
                            Library.UserSettings["LastScanSettingsColors"] = string.Join(",", colors.ConvertAll<string>((c) => colorConverter[c]));
                            Library.SaveConfiguration();

                            using (var scanner = new Scanner(cams, projs, colors))
                            using (var preview = new CameraPreview())
                            {
                                scanner.NewScanPreview += (s, e) => UpdateImage(preview, e.ScanPreviews[0]);
                                scanner.ScanComplete += (s, e) =>
                                    {
                                        if (preview.Visible)
                                        {
                                            preview.Invoke(new MethodInvoker(() => preview.Close()));
                                        }
                                    };

                                scanner.Scan();

                                Application.Run(preview);

                                if (scanner.IsScanning)
                                {
                                    scanner.Cancel();
                                }
                            }

                            projs.DisposeList();
                            cams.DisposeList();

                            break;
                        }

                    case 2:
                        {
                            var cam = ManagedCamera.EnumerateCameras().PromptForCamera();
                            if (cam == null)
                            {
                                break;
                            }

                            using (cam)
                            using (ManagedCameraStream stream = cam.OpenStream())
                            using (var preview = new CameraPreview())
                            {
                                stream.ImageCaptured += (s, e) => UpdateImage(preview, e.Preview);
                                stream.Start();

                                Application.Run(preview);

                                stream.Stop();
                            }

                            break;
                        }

                    case 3:
                        {
                            // Detect the cameras on the user's system
                            ManagedCamera cam = ManagedCamera.EnumerateCameras().PromptForCamera();
                            if (cam == null)
                            {
                                break;
                            }

                            using (cam)
                            using (var camCalib = new CameraCalibrator(cam))
                            using (var preview = new CameraPreview(cam))
                            {
                                Console.Write("How many images would you like to use for calibration? ");

                                int numCalibImgs;
                                while (!ReadInt32(out numCalibImgs))
                                {
                                }

                                camCalib.TargetCameraCaptures = numCalibImgs;
                                camCalib.NewCalibrationPreview += (s, e) => UpdateImage(preview, e.Preview);
                                camCalib.CalibrationFinished += (s, e) => 
                                    preview.Invoke(new MethodInvoker(() => preview.Close()));

                                camCalib.Start();

                                Application.Run(preview);

                                if (camCalib.IsCalibrating)
                                {
                                    camCalib.Cancel();
                                    break;
                                }

                                Console.WriteLine("Pixel error: " + camCalib.Results.ReprojectionError);
                                cam.SetIntrinsicCalibration(camCalib.Results);
                            }

                            break;
                        }

                    case 4:
                        {
                            var cam = ManagedCamera.EnumerateCameras().PromptForCamera();
                            if (cam == null)
                            {
                                break;
                            }

                            var display = ManagedProjector.EnumerateProjectors().PromptForProjector();
                            if (display == null)
                            {
                                break;
                            }

                            using (cam)
                            using (display)
                            using (var preview = new CameraPreview())
                            using (var calib = new ProjectorCalibrator(cam, display))
                            {
                                Console.Write("How many images would you like to use for calibration? ");

                                int numCalibImgs;
                                while (!ReadInt32(out numCalibImgs))
                                {
                                }

                                calib.TargetCameraCaptures = numCalibImgs;
                                calib.NewCalibrationPreview += (s, e) => UpdateImage(preview, e.Preview);
                                calib.CalibrationFinished += (s, e) =>
                                    preview.Invoke(new MethodInvoker(() => preview.Close()));
                                calib.Start();

                                Application.Run(preview);

                                if (calib.IsCalibrating)
                                {
                                    calib.Cancel();
                                    break;
                                }

                                Console.WriteLine("Pixel error: " + calib.Results.ReprojectionError);
                                display.SetIntrinsicCalibration(calib.Results);
                            }

                            break;
                        }
                    case 5:
                        {
                            ManagedCamera cam = ManagedCamera.EnumerateCameras().PromptForCamera();
                            if (cam == null)
                            {
                                break;
                            }

                            var display = ManagedProjector.EnumerateProjectors().PromptForProjector();
                            if (display == null)
                            {
                                break;
                            }

                            using (cam)
                            using (display)
                            using (var preview = new CameraPreview())
                            using (var calib = new CamProjExtrinsicCalibrator(cam, display))
                            {
                                calib.NewCalibrationPreview += (s, e) => UpdateImage(preview, e.Preview);
                                calib.CalibrationFinished += (s, e) =>
                                    preview.Invoke(new MethodInvoker(() => preview.Close()));
                                calib.Start();

                                Application.Run(preview);

                                if (calib.IsCalibrating)
                                {
                                    calib.Cancel();
                                    break;
                                }
                            }

                            break;
                        }
                    case 6:
                        {
                            ManagedCamera target = ManagedCamera.EnumerateCameras().PromptForCamera("Select the target camera: ");
                            if (target == null)
                            {
                                break;
                            }

                            ManagedCamera source = ManagedCamera.EnumerateCameras().PromptForCamera("Select the source camera: ");
                            if (source == null)
                            {
                                break;
                            }

                            using (target)
                            using (source)
                            using (var targetPreview = new CameraPreview())
                            using (var sourcePreview = new CameraPreview())
                            using (var calibrator = new CamCamExtrinsicCalibrator(source, target))
                            {
                                calibrator.NewCalibrationPreview += (s, e) =>
                                    {
                                        UpdateImage(targetPreview, e.TargetPreview);
                                        UpdateImage(sourcePreview, e.SourcePreview);
                                    };

                                calibrator.TargetCameraCaptures = 2;
                                calibrator.Start();
                                calibrator.CalibrationFinished += (s, e) =>
                                {
                                    targetPreview.Invoke(new MethodInvoker(() => targetPreview.Close()));
                                    sourcePreview.Invoke(new MethodInvoker(() => sourcePreview.Close()));
                                };

                                targetPreview.Show();

                                new Thread(() =>
                                {
                                    Application.Run(sourcePreview);
                                    if (targetPreview.Visible)
                                    {
                                        targetPreview.Invoke(new MethodInvoker(() => targetPreview.Close()));
                                    }
                                }).Start();

                                Application.Run(targetPreview);

                                if (calibrator.IsCalibrating)
                                {
                                    calibrator.Cancel();
                                }
                            }

                            break;
                        }
                    case 7:
                        {
                            ManagedDevice device = PromptForAnyDevice();
                            if (device == null)
                            {
                                break;
                            }

                            Console.WriteLine();
                            Console.WriteLine("The following is information about your device:");
                            Console.WriteLine("Device type:   {0}", device is ManagedCamera ? "Camera" : "Projector");
                            Console.WriteLine("Model name:    {0}", device.ModelName);
                            Console.WriteLine("Serial number: {0}", device.SerialNumber);
                            Console.WriteLine("Friendly name: {0}", string.IsNullOrEmpty(device.FriendlyName) ?
                                "None" : "\"" + device.FriendlyName + "\"");

                            Console.WriteLine();
                            Console.WriteLine("Please enter what you want the device's friendly name to be.");
                            Console.Write("Friendly name (empty to clear): ");

                            device.FriendlyName = Console.ReadLine();

                            Console.WriteLine("Friendly name successfully set.");
                            break;
                        }

                    case 0:
                        exit = true;
                        break;
                }
            }
        }

        private static void PrintDeviceList(this IEnumerable<ManagedDevice> devs, int start = 1)
        {
            int dispI = start;
            foreach (ManagedDevice dev in devs)
            {
                string text = !string.IsNullOrEmpty(dev.FriendlyName)
                    ? string.Format("\"{0}\"", dev.FriendlyName)
                    : string.Format("{0} (S/N: {1})", dev.ModelName, dev.SerialNumber);

                Console.WriteLine("{0}: {1}", dispI++, text);
            }
        }

        private static T PromptForDevice<T>(this IList<T> devices, string device, string prompt = null) where T : ManagedDevice
        {
            int selected;
            do
            {
                Console.WriteLine();
                Console.WriteLine("The following {0}s were detected on your system:", device);
                Console.WriteLine();

                PrintDeviceList(devices);

                Console.WriteLine("0: Cancel");
                Console.WriteLine();
                Console.Write(prompt ?? "Select a {0}: ", device);
            } while (!(ReadInt32(out selected) && selected >= 0 && selected <= devices.Count));

            return selected == 0 ? null : devices[selected - 1];
        }

        private static ManagedCamera PromptForCamera(this IList<ManagedCamera> cams, string prompt = null)
        {
            return PromptForDevice(cams, "camera", prompt);
        }

        private static ManagedProjector PromptForProjector(this IList<ManagedProjector> projs, string prompt = null)
        {
            return PromptForDevice(projs, "projector", prompt);
        }

        private static IList<ManagedDevice> PromptForManyDevices(this IList<ManagedDevice> devs, string device, string prompt = null)
        {
            bool success = false;
            List<int> indices = null;
            while (!success)
            {
                Console.WriteLine();
                Console.WriteLine("The following {0}s were detected on your system:", device);
                Console.WriteLine();

                PrintDeviceList(devs);

                Console.WriteLine("0: Cancel");
                Console.WriteLine();
                Console.Write(prompt ?? "Select a series of {0}s (separated by spaces): ", device);

                string input = Console.ReadLine();
                string[] split = input.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (split.Length > 0 && split[0] == "0")
                {
                    // Cancel
                    break;
                }

                indices = new List<int>();
                foreach (string token in split)
                {
                    int parsed;
                    if (!int.TryParse(token, out parsed))
                    {
                        // Failed, try again
                        break;
                    }

                    indices.Add(parsed);
                }

                if (indices.Count > 0)
                {
                    success = true;
                }
            }

            if (!success)
            {
                return null;
            }

            Debug.Assert(indices != null);

            List<ManagedDevice> output = new List<ManagedDevice>();
            foreach (int index in indices)
            {
                output.Add(devs[index - 1]);
            }

            return output;
        }

        private static List<ManagedCamera> PromptForManyCameras(this IList<ManagedCamera> cams, string prompt = null)
        {
            var devs = PromptForManyDevices(cams.Cast<ManagedDevice>().ToList(), "camera", prompt);
            if (devs == null) return null;
            return devs.Cast<ManagedCamera>().ToList();
        }

        private static List<ManagedProjector> PromptForManyProjectors(this IList<ManagedProjector> projs, string prompt = null)
        {
            var devs = PromptForManyDevices(projs.Cast<ManagedDevice>().ToList(), "projector", prompt);
            if (devs == null) return null;
            return devs.Cast<ManagedProjector>().ToList();
        }

        private static ManagedDevice PromptForAnyDevice()
        {
            var cams = ManagedCamera.EnumerateCameras();
            var projs = ManagedProjector.EnumerateProjectors();

            int selected;
            do
            {
                Console.WriteLine();
                Console.WriteLine("The following devices were detected on your system:");
                Console.WriteLine();

                Console.WriteLine("Cameras:");
                PrintDeviceList(cams);
                Console.WriteLine();

                Console.WriteLine("Projectors:");
                PrintDeviceList(projs, cams.Count + 1);
                Console.WriteLine();

                Console.WriteLine("0: Cancel");
                Console.WriteLine();
                Console.Write("Select a device: ");
            }
            while (!(ReadInt32(out selected) && selected >= 0 && selected <= (cams.Count + projs.Count)));

            if (selected == 0)
            {
                return null;
            }

            return selected <= cams.Count ? (ManagedDevice)cams[selected - 1] : projs[selected - cams.Count - 1];
        }

        private static List<GrayCodeColor> PromptForColors(int numDevicePairs)
        {
            bool success = false;
            List<GrayCodeColor> colors = null;

            while (!success)
            {
                Console.WriteLine();
                Console.WriteLine("The following colors are supported:");
                Console.WriteLine();
                Console.WriteLine("R = red");
                Console.WriteLine("G = green");
                Console.WriteLine("B = blue");
                Console.WriteLine("Y = yellow");
                Console.WriteLine("W = white");
                Console.WriteLine();

                GrayCodeColor[] defaultColors = new[]
                {
                    GrayCodeColors.Red,
                    GrayCodeColors.Green,
                    GrayCodeColors.Blue,
                    GrayCodeColors.Yellow
                };

                string[] colorStrs = new[]
                {
                    "R", "G", "B", "Y"
                };

                string defaultStr = numDevicePairs == 1 ? "W" :
                    string.Join(", ", colorStrs.Take(numDevicePairs));

                Console.WriteLine("Type \"0\" to cancel.");
                Console.Write("Select a series of colors ({0}): ", defaultStr);

                string input = Console.ReadLine();
                string[] split = input.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                if (split.Length == 0)
                {
                    return new List<GrayCodeColor>(new[] { GrayCodeColors.White });
                }

                if (split.Length > 0 && split[0] == "0")
                {
                    // Cancel
                    break;
                }

                colors = new List<GrayCodeColor>();
                foreach (string token in split)
                {
                    bool cancel = false;

                    int index = 0;
                    switch (token.ToUpper())
                    {
                        case "R": index = 0; break;
                        case "G": index = 1; break;
                        case "B": index = 2; break;
                        case "Y": index = 3; break;
                        default: cancel = true; break;
                    }

                    if (cancel)
                    {
                        colors.Clear();
                        break;
                    }

                    colors.Add(defaultColors[index]);
                }

                if (colors.Count == numDevicePairs)
                {
                    success = true;
                }
            }

            return colors;
        }

        private static void UpdateImage(CameraPreview preview, Bitmap bmp)
        {
            try
            {
                if (preview.InvokeRequired)
                {
                    preview.Invoke(new MethodInvoker(() => UpdateImage(preview, bmp)));
                    return;
                }

                if (preview.pictureBox1.Image != null)
                {
                    preview.pictureBox1.Image.Dispose();
                }

                preview.pictureBox1.Image = bmp;
            }
            catch (ObjectDisposedException)
            {
                bmp.Dispose();
            }
        }

        private static bool ReadInt32(out int value)
        {
            string rawInput = Console.ReadLine();
            return int.TryParse(rawInput, out value);
        }

        private static void DisposeList(this IEnumerable<IDisposable> objs)
        {
            foreach (var obj in objs)
            {
                obj.Dispose();
            }
        }
    }
}
