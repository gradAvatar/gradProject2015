﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StructuredLight.Managed;

namespace StructuredLightCli
{
    public partial class CameraPreview : Form
    {
        public CameraPreview()
        {
            InitializeComponent();
        }

        public CameraPreview(ManagedCamera cam)
            : this()
        {
        }
    }
}
