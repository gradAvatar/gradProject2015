﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for CameraPreviewWindow.xaml
    /// </summary>
    public partial class CameraPreviewWindow : Window
    {
        public static void CreatePreviewWindow(ManagedCamera cam)
        {
            CameraPreviewWindow window = new CameraPreviewWindow();
            window.Initialize(cam);
            window.Show();
        }

        private ManagedCamera _camera;

        public CameraPreviewWindow()
        {
            InitializeComponent();
            cameraPreview.cameraPreview.Width = 600;
            cameraPreview.cameraPreview.Height = 400;
        }

        public void Initialize(ManagedCamera camera)
        {
            _camera = camera;
            if (!_camera.Connected)
                _camera.Connect();
            var lk = camera.OpenCameraStream();
            lk.NewCameraPreview += camera_preview;
            cameraSettings.ChangeCamera(this, _camera);
            SetTitle();
        }

        public void camera_preview(object sender, NewCameraPreviewEventArgs e)
        {
            BitmapPreviewer.LoadBitmapImage(e.CurrentPreview, cameraPreview.cameraPreview);
        }

        public void SetTitle()
        {
            this.Title = cameraSettings.Camera.FriendlyName;
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            if (!expander.IsExpanded)
                cameraSettings.Visibility = System.Windows.Visibility.Visible;
            else
                cameraSettings.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (_camera != null && _camera.IsCameraStreamOpen)
            {
                _camera.CameraStream.NewCameraPreview -= camera_preview;
                _camera.CameraStream.RemoveAnEvent();
            }
        }
    }
}
