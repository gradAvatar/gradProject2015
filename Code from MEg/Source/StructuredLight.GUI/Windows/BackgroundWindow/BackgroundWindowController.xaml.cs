﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for BackgroundWindowController.xaml
    /// </summary>
    public partial class BackgroundWindowController : UserControl
    {
        #region Declares

        public static BackgroundWindowController WindowController { get; private set; }

        #endregion

        #region Constructor

        public BackgroundWindowController()
        {
            WindowController = this;

            InitializeComponent();
        }

        #endregion

        #region UserControl Events

        private void BackgroundWindowController_Loaded(object sender, RoutedEventArgs e)
        {
            dock.Children.Add(new SplashPage()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
                Width = ActualWidth,
                Height = ActualHeight
            });
            dock.Children.Add(new CameraPreview()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
                Width = ActualWidth,
                Height = ActualHeight
            });
            dock.Children.Add(new MeshViewer()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
                Width = ActualWidth,
                Height = ActualHeight
            });
            dock.Children.Add(new SettingsEditor()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
                Width = ActualWidth,
                Height = ActualHeight
            });
            dock.Children.Add(new HardwareSetup()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
                Width = ActualWidth,
                Height = ActualHeight
            });
            SetVisibleWindow(WindowItem.SplashPage);
        }

        public void ResizeWindow(bool taskBarOpen)
        {
            foreach (var child in dock.Children)
            {
                (child as UserControl).Width = ActualWidth;
                (child as UserControl).Height = ActualHeight;
            }
        }

        #endregion

        #region Set Visible Window Item

        /// <summary>
        /// Sets the currently visible background window
        /// (internal dispatching)
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public dynamic SetVisibleWindow(WindowItem item)
        {
            if (Dispatcher.Thread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                return InternalSetWindow(item);
            object window = null;
            Dispatcher.Invoke(new Action(() => window = InternalSetWindow(item)));

            return window;
        }

        private object InternalSetWindow(WindowItem item)
        {
            dynamic window = dock.Children[(int)item];
            ResizeWindow(false);
            if(window is IBackgroundWindow)
                ((IBackgroundWindow)window).Display();

            foreach (UIElement element in dock.Children)
            {
                if (element.Visibility == System.Windows.Visibility.Visible && element is IBackgroundWindow)
                    ((IBackgroundWindow)element).Hide();
                element.Visibility = System.Windows.Visibility.Collapsed;
            }

            window.Visibility = System.Windows.Visibility.Visible;

            return window;
        }

        #endregion
    }
}
