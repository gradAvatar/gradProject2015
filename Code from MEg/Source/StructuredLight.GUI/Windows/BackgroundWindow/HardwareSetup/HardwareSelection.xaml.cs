﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for HardwareSelection.xaml
    /// </summary>
    public partial class HardwareSelection : UserControl, IHardwareSetupControl
    {
        private HardwareSetup _setup;

        public HardwareSelection()
        {
            InitializeComponent();
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _setup.SetTitleText("Please select the number of projectors you will be using in your setup.");
            _setup.DisableNextButton();
        }

        public void RequestedNext()
        {
        }

        private void projector_selected(object sender, RoutedEventArgs e)
        {
            _setup.SetupInformation["HardwareType"] = sender == oneProjector ? HardwareSetupType.OneProjector :
                sender == twoProjector ? HardwareSetupType.TwoProjectors : sender == threeProjector ?
                HardwareSetupType.ThreeProjectors : sender == fourProjector ?
                HardwareSetupType.FourProjectors : HardwareSetupType.Other;
            _setup.GoTo(sender == oneProjector ? CurrentHardwareWindow.OneProjector :
                sender == twoProjector ? CurrentHardwareWindow.TwoProjectors : sender == threeProjector ?
                CurrentHardwareWindow.ThreeProjectors : sender == fourProjector ?
                CurrentHardwareWindow.FourProjectors : CurrentHardwareWindow.OtherProjectors);
        }
    }
}
