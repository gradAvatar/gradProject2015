﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for OneProjectorGUI.xaml
    /// </summary>
    public partial class OneProjectorGUI : UserControl
    {
        public event OnClicked OnCameraClicked;

        public OneProjectorGUI()
        {
            InitializeComponent();
        }

        private void camera1Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (OnCameraClicked != null)
                OnCameraClicked(1);
        }

        private void camera2Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (OnCameraClicked != null)
                OnCameraClicked(2);
        }
    }
}
