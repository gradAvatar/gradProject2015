﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for ProjectorSelector.xaml
    /// </summary>
    public partial class AddProjectorSelector : UserControl
    {
        public ManagedProjector Monitor { get; private set; }
        public NoParam Calibrate { get; set; }
        public NoParam ProjectorSelected { get; set; }

        public AddProjectorSelector()
        {
            InitializeComponent();
        }

        private void projectorSelectionButton_Click(object sender, RoutedEventArgs e)
        {
            MonitorSelectorWindow selectionWindow = new MonitorSelectorWindow();
            selectionWindow.ShowDialog();
            Monitor = selectionWindow.monitorSelection.SelectedMonitor;
            if (Monitor != null)
            {
                projectorCalibrationButton.Visibility = Monitor.IsIntrinsiclyCalibrated ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                projectorSelectionText.Text = Utils.GetNameOfProjector(Monitor);
                projectorSelectionText.IsEnabled = true;
                projectorCalibratedText.Content = Monitor.IsIntrinsiclyCalibrated ? "Projector is calibrated" : "Projector is not calibrated";
                if (Monitor.IsIntrinsiclyCalibrated)
                    addProjectorButton.Visibility = System.Windows.Visibility.Visible;
                else
                    addProjectorButton.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
                Clear();
        }

        public void Clear()
        {
            projectorCalibrationButton.Visibility = System.Windows.Visibility.Collapsed;
            projectorCalibratedText.Content = "";
            projectorSelectionText.Text = "No Projector Selected";
            projectorSelectionText.IsEnabled = false;
            addProjectorButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void addProjectorButton_Click(object sender, RoutedEventArgs e)
        {
            if (ProjectorSelected != null)
                ProjectorSelected();
        }

        private void projectorCalibrationButton_Click(object sender, RoutedEventArgs e)
        {
            if(Calibrate != null)
                Calibrate();
            if (Monitor.IsIntrinsiclyCalibrated)
                addProjectorButton.Visibility = System.Windows.Visibility.Visible;
            else
                addProjectorButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void projectorSelectionText_KeyDown(object sender, KeyEventArgs e)
        {
            if (Monitor != null && projectorSelectionText.Text != "")
                Monitor.FriendlyName = projectorSelectionText.Text;
        }
    }
}
