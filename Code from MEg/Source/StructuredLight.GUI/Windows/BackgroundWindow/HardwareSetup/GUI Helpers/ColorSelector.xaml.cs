﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for ColorSelector.xaml
    /// </summary>
    public partial class ColorSelector : UserControl
    {
        public string Color
        {
            get
            {
                if (colors.SelectedItem == null)
                    return null;
                return colors.SelectedItem.ToString();
            }
        }

        public List<string> SelectionColors = new List<string>
        { 
            "White",
            "Red",
            "Green", 
            "Blue",
            "Yellow"
        };

        public ColorSelector()
        {
            InitializeComponent();
            colors.ItemsSource = SelectionColors;
        }

        private void colors_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (colors.SelectedItem == null)
                colorSelectionText.Text = "No Color Selected";
            else
                colorSelectionText.Text = colors.SelectedItem.ToString() + " Selected";
        }
    }
}
