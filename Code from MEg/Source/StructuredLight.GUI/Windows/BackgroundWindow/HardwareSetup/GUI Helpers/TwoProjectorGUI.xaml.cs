﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for TwoProjectorGUI.xaml
    /// </summary>
    public partial class TwoProjectorGUI : UserControl
    {
        public event OnClicked OnCameraClicked;
        public event OnClicked OnProjectorClicked;

        public TwoProjectorGUI()
        {
            InitializeComponent();
        }

        private void camera1Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (OnCameraClicked != null)
                OnCameraClicked(1);
        }

        private void camera2Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (OnCameraClicked != null)
                OnCameraClicked(2);
        }

        private void projector1Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (OnProjectorClicked != null)
                OnProjectorClicked(1);
        }

        private void projector2Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (OnProjectorClicked != null)
                OnProjectorClicked(2);
        }
    }
}
