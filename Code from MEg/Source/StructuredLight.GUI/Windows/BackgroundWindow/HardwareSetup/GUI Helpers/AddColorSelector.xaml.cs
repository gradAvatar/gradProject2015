﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for AddColorSelector.xaml
    /// </summary>
    public partial class AddColorSelector : UserControl
    {
        public string Color
        {
            get
            {
                if (colors.SelectedItem == null)
                    return null;
                return colors.SelectedItem.ToString();
            }
        }

        public List<string> SelectionColors = new List<string>
        { 
            "White",
            "Red",
            "Green", 
            "Blue",
            "Yellow"
        };

        public NoParam ColorSelected = null;

        public AddColorSelector()
        {
            InitializeComponent();
            colors.ItemsSource = SelectionColors;
        }

        private void addColorButton_Click(object sender, RoutedEventArgs e)
        {
            if (Color != null && ColorSelected != null)
                ColorSelected();
        }

        private void colors_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (colors.SelectedItem == null)
            {
                colorSelectionText.Text = "No Color Selected";
                addColorButton.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                colorSelectionText.Text = colors.SelectedItem.ToString() + " Selected";
                addColorButton.Visibility = System.Windows.Visibility.Visible;
            }
        }

        public void Clear()
        {
            addColorButton.Visibility = System.Windows.Visibility.Collapsed;
            colors.SelectedItem = null;
            colorSelectionText.Text = "No Color Selected";
        }
    }
}
