﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for AddCameraSelector.xaml
    /// </summary>
    public partial class AddCameraSelector : UserControl
    {
        public ManagedCamera Camera { get; private set; }
        public NoParam CameraSelected = null;

        public AddCameraSelector()
        {
            InitializeComponent();
        }

        private void cameraSelectionButton_Click(object sender, RoutedEventArgs e)
        {
            CameraSelectorWindow selectionWindow = new CameraSelectorWindow();
            selectionWindow.ShowDialog();
            Camera = selectionWindow.cameraSelection.SelectedCamera;
            if (Camera != null)
            {
                cameraCalibrationButton.Visibility = Camera.IsIntrinsiclyCalibrated ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;
                cameraSelectionText.Text = Utils.GetNameOfCamera(Camera);
                cameraSelectionText.IsEnabled = true;
                cameraCalibratedText.Content = Camera.IsIntrinsiclyCalibrated ? "Camera is calibrated" : "Camera is not calibrated";
                if (Camera.IsIntrinsiclyCalibrated)
                    addCameraButton.Visibility = System.Windows.Visibility.Visible;
                else
                    addCameraButton.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                cameraCalibrationButton.Visibility = System.Windows.Visibility.Collapsed;
                cameraCalibratedText.Content = "";
                cameraSelectionText.Text = "No Camera Selected";
                cameraSelectionText.IsEnabled = false;
            }
        }

        private void addCameraButton_Click(object sender, RoutedEventArgs e)
        {
            if (CameraSelected != null)
                CameraSelected();
        }

        private void cameraCalibrationButton_Click(object sender, RoutedEventArgs e)
        {
            IntrinsicCalibration calibration = new IntrinsicCalibration();
            calibration.Calibrate(Camera);
            calibration.ShowDialog();
            if (Camera.IsIntrinsiclyCalibrated)
                addCameraButton.Visibility = System.Windows.Visibility.Visible;
            else
                addCameraButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void Clear()
        {
            cameraCalibrationButton.Visibility = System.Windows.Visibility.Collapsed;
            addCameraButton.Visibility = System.Windows.Visibility.Collapsed;
            cameraCalibratedText.Content = "";
            cameraSelectionText.Text = "No Camera Selected";
            cameraSelectionText.IsEnabled = false;
            Camera = null;
        }

        private void cameraSelectionText_KeyDown(object sender, KeyEventArgs e)
        {
            if (Camera != null && cameraSelectionText.Text != "")
                Camera.FriendlyName = cameraSelectionText.Text;
        }
    }
}
