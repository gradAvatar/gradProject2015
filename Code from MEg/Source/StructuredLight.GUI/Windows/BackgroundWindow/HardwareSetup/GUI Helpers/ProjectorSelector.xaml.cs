﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for ProjectorSelector.xaml
    /// </summary>
    public partial class ProjectorSelector : UserControl
    {
        public ManagedProjector Monitor { get; private set; }
        public NoParam Calibrate { get; set; }
        public ProjectorSelector()
        {
            InitializeComponent();
        }

        private void projectorSelectionButton_Click(object sender, RoutedEventArgs e)
        {
            MonitorSelectorWindow selectionWindow = new MonitorSelectorWindow();
            selectionWindow.ShowDialog();
            Monitor = selectionWindow.monitorSelection.SelectedMonitor;
            if (Monitor != null)
            {
                projectorCalibrationButton.Visibility = System.Windows.Visibility.Visible;
                projectorSelectionText.Text = Utils.GetNameOfProjector(Monitor);
                projectorSelectionText.IsEnabled = true;
                projectorCalibratedText.Content = Monitor.IsIntrinsiclyCalibrated ? "Projector is already calibrated" : "Projector is not calibrated";
            }
            else
            {
                projectorCalibrationButton.Visibility = System.Windows.Visibility.Collapsed;
                projectorCalibratedText.Content = "";
                projectorSelectionText.Text = "No Projector Selected";
                projectorSelectionText.IsEnabled = false;
            }
        }

        private void projectorCalibrationButton_Click(object sender, RoutedEventArgs e)
        {
            if(Calibrate != null)
                Calibrate();
        }

        private void projectorSelectionText_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (Monitor != null && projectorSelectionText.Text != "")
                Monitor.FriendlyName = projectorSelectionText.Text;
        }
    }
}
