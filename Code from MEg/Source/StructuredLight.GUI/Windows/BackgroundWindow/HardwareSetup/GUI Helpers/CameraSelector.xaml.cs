﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for CameraSelector.xaml
    /// </summary>
    public partial class CameraSelector : UserControl
    {
        public ManagedCamera Camera { get; private set; }

        public CameraSelector()
        {
            InitializeComponent();
        }

        private void cameraSelectionButton_Click(object sender, RoutedEventArgs e)
        {
            CameraSelectorWindow selectionWindow = new CameraSelectorWindow();
            selectionWindow.ShowDialog();
            Camera = selectionWindow.cameraSelection.SelectedCamera;
            if (Camera != null)
            {
                cameraCalibrationButton.Visibility = System.Windows.Visibility.Visible;
                cameraSelectionText.Text = Utils.GetNameOfCamera(Camera);
                cameraSelectionText.IsEnabled = true;
                cameraCalibratedText.Content = Camera.IsIntrinsiclyCalibrated ? "Camera is already calibrated" : "Camera is not calibrated";
            }
            else
            {
                cameraCalibrationButton.Visibility = System.Windows.Visibility.Collapsed;
                cameraCalibratedText.Content = "";
                cameraSelectionText.Text = "No Camera Selected";
                cameraSelectionText.IsEnabled = false;
            }
        }

        private void cameraCalibrationButton_Click(object sender, RoutedEventArgs e)
        {
            IntrinsicCalibration calibration = new IntrinsicCalibration();
            calibration.Calibrate(Camera);
            calibration.ShowDialog();
        }

        private void cameraSelectionText_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if(cameraSelectionText.Text != "")
                Camera.FriendlyName = cameraSelectionText.Text;
        }
    }
}
