﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for FinishedWindow.xaml
    /// </summary>
    public partial class FinishedWindow : UserControl, IHardwareSetupControl
    {
        private HardwareSetup _setup;
        public FinishedWindow()
        {
            InitializeComponent();
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            HardwareSetupType setupType = (HardwareSetupType)_setup.SetupInformation["HardwareType"];
            Library.ScanType = (setupType == HardwareSetupType.ThreeProjectors ||
                setupType == HardwareSetupType.FourProjectors) ? ScanType.Rgb :
                setupType == HardwareSetupType.TwoProjectors ? ScanType.Bayer :
                ScanType.BlackAndWhite;
            List<ManagedProjector> projectors = (List<ManagedProjector>)_setup.SetupInformation["Projectors"];
            List<ManagedCamera> cameras = (List<ManagedCamera>)_setup.SetupInformation["Cameras"];
            List<string> colors = (List<string>)_setup.SetupInformation["Colors"];
            Library.UserSettings["NumberofProjectors"] = projectors.Count.ToString();
            Library.UserSettings["NumberofCameras"] = cameras.Count.ToString();
            for (int i = 0; i < cameras.Count; )
                Library.UserSettings["Camera" + i] = cameras[i++].SerialNumber;
            for (int i = 0; i < colors.Count; )
                Library.UserSettings["Color" + i] = colors[i++];
            for (int i = 0; i < projectors.Count; )
                Library.UserSettings["Projector" + i] = projectors[i++].DeviceId;
            Library.SaveConfiguration();
            _setup.DisableNextButton();

            rootCamText.Text = string.Format("The root camera-projector pair of this system is the pair containing the camera {0} and the projector {1}. Before taking a scan, orient the subject to face this pair to get optimal results (it can be done in any direction, however, this gives the best result generally).",
                cameras[0].FriendlyName, projectors[0].FriendlyName);
        }

        public void RequestedNext()
        {
        }
    }
}
