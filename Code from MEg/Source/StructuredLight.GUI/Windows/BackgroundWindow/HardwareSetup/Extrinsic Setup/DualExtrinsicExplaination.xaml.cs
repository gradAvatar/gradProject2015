﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for DualExtrinsicExplaination.xaml
    /// </summary>
    public partial class DualExtrinsicExplaination : UserControl
    {
        private HardwareSetup _setup;

        public DualExtrinsicExplaination()
        {
            InitializeComponent();
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _setup.SetTitleText("Dual-Extrinsic Calibration Explaination");
        }

        public void RequestedNext()
        {
            _setup.GoTo(CurrentHardwareWindow.DualExtrinsicCalibration);
        }
    }
}
