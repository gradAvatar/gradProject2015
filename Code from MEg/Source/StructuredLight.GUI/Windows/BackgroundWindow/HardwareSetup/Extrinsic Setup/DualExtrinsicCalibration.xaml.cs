﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;
using System.Threading;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for DualExtrinsicCalibration.xaml
    /// </summary>
    public partial class DualExtrinsicCalibration : UserControl, IHardwareSetupControl
    {
        private HardwareSetup _setup;
        private List<ManagedCamera> _cameras;
        private int _current;
        private HardwareSetupType _setupType;
        private CamCamExtrinsicCalibrator _currentcalibration;

        public DualExtrinsicCalibration()
        {
            InitializeComponent();
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _cameras = (List<ManagedCamera>)_setup.SetupInformation["Cameras"];
            _setupType = (HardwareSetupType)_setup.SetupInformation["HardwareType"];
            _setup.SetTitleText("Dual-Extrinsic Calibration");
            _setup.DisableNextButton();
            SetCurrentlyExtrinsicallyCalibrating(getNextCamera(0), getNextCamera(++_current));
        }

        private void SetCurrentlyExtrinsicallyCalibrating(ManagedCamera targetCamera, ManagedCamera sourceCamera)
        {
            cameraName.Content = Utils.GetNameOfCamera(sourceCamera) + " to " + 
                Utils.GetNameOfCamera(targetCamera);
            calibrationCompleteText.Visibility = System.Windows.Visibility.Collapsed;
            nextCamera.Visibility = System.Windows.Visibility.Collapsed;

            _currentcalibration = new CamCamExtrinsicCalibrator(sourceCamera, targetCamera);
            _currentcalibration.NewCalibrationPreview += (o, e) => 
                Dispatcher.Invoke(new Action(() => BitmapPreviewer.LoadBitmapImages(new List<System.Drawing.Bitmap> { e.SourcePreview, e.TargetPreview },
                cameraPreview1.cameraPreview, true)));

            _currentcalibration.CalibrationFinished += (o, e) => FinishedCalibrating();
            _currentcalibration.TargetCameraCaptures = 2;
            _currentcalibration.Start();
        }

        private void FinishedCalibrating()
        {
            Dispatcher.Invoke(new Action(delegate()
            {
                nextCamera.Visibility = System.Windows.Visibility.Visible;
                calibrationCompleteText.Visibility = System.Windows.Visibility.Visible;
                if (_current + 1 == _cameras.Count)
                {
                    cameraPreview1.cameraPreview = null;
                    nextCamera.Visibility = System.Windows.Visibility.Collapsed;
                    _setup.EnableNextButton();
                }
            }));
        }

        public void RequestedNext()
        {
            _setup.GoTo(CurrentHardwareWindow.Finished);
        }

        private void nextCamera_Click(object sender, RoutedEventArgs e)
        {
            SetCurrentlyExtrinsicallyCalibrating(getNextCamera(0), getNextCamera(++_current));
        }

        private void skipCamera_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("If you skip this pair, and the extrinsic calibration either does not exist, " +
                "or is wrong because the pairs have moved, scanning will return very bad results. " +
                "Do you wish to continue?", "Are you sure you want to skip this pair?", MessageBoxButton.YesNo)
                == MessageBoxResult.Yes)
            {
                _currentcalibration.Cancel();
                nextCamera.Visibility = System.Windows.Visibility.Visible;
                calibrationCompleteText.Visibility = System.Windows.Visibility.Visible;
                if (_current + 1 == _cameras.Count)
                {
                    cameraPreview1.cameraPreview = null;
                    nextCamera.Visibility = System.Windows.Visibility.Collapsed;
                    _setup.EnableNextButton();
                }
                else
                    SetCurrentlyExtrinsicallyCalibrating(getNextCamera(0), getNextCamera(++_current));
            }
        }

        private ManagedCamera getNextCamera(int camera)
        {
            if (camera == _cameras.Count)
                throw new ArgumentOutOfRangeException("camera");
            return _cameras[camera];
        }
    }
}
