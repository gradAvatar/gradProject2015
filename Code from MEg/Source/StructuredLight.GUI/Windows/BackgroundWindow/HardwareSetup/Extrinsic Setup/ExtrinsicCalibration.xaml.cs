﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;
using System.Threading;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for ExtrinsicCalibration.xaml
    /// </summary>
    public partial class ExtrinsicCalibration : UserControl, IHardwareSetupControl
    {
        private HardwareSetup _setup;
        private List<ManagedCamera> _cameras;
        private List<ManagedProjector> _projectors;
        private int _current;
        private HardwareSetupType _setupType;
        private CamProjExtrinsicCalibrator _currentcalibration;

        public ExtrinsicCalibration()
        {
            InitializeComponent();
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _cameras = (List<ManagedCamera>)_setup.SetupInformation["Cameras"];
            _projectors = ((List<ManagedProjector>)_setup.SetupInformation["Projectors"]);
            _setupType = (HardwareSetupType)_setup.SetupInformation["HardwareType"];
            _setup.SetTitleText("Extrinsic Calibration");
            _setup.DisableNextButton();
            SetCurrentlyExtrinsicallyCalibrating(_cameras[_current], _projectors[_current++]);
        }

        private void SetCurrentlyExtrinsicallyCalibrating(ManagedCamera camera, ManagedProjector projector)
        {
            cameraName.Content = Utils.GetNameOfCamera(camera);
            calibrationCompleteText.Visibility = System.Windows.Visibility.Collapsed;
            nextCamera.Visibility = System.Windows.Visibility.Collapsed;

            _currentcalibration = new CamProjExtrinsicCalibrator(camera, projector);
            _currentcalibration.NewCalibrationPreview += (o, e) => BitmapPreviewer.LoadBitmapImage(e.Preview, cameraPreview1.cameraPreview, true);

            _currentcalibration.CalibrationFinished += (o, e) => FinishedCalibrating();

            _currentcalibration.Start();
        }

        private void FinishedCalibrating()
        {
            Dispatcher.Invoke(new Action(delegate()
            {
                nextCamera.Visibility = System.Windows.Visibility.Visible;
                calibrationCompleteText.Visibility = System.Windows.Visibility.Visible;
                if ((_setupType == HardwareSetupType.ThreeProjectors ||
                    _setupType == HardwareSetupType.FourProjectors ||
                    _setupType == HardwareSetupType.TwoProjectors) &&
                    _current == _projectors.Count)
                {
                    cameraPreview1.cameraPreview = null;
                    nextCamera.Visibility = System.Windows.Visibility.Collapsed;
                    _setup.EnableNextButton();
                }
                else if ((_setupType == HardwareSetupType.OneProjector ||
                    _setupType == HardwareSetupType.Other) &&
                    _current == _cameras.Count)
                {
                    cameraPreview1.cameraPreview = null;
                    nextCamera.Visibility = System.Windows.Visibility.Collapsed;
                    _setup.EnableNextButton();
                }
            }));
        }

        public void RequestedNext()
        {
            if (_projectors.Count > 1)
                _setup.GoTo(CurrentHardwareWindow.DualExtrinsicExplaination);
            else
                _setup.GoTo(CurrentHardwareWindow.Finished);
        }

        private void nextCamera_Click(object sender, RoutedEventArgs e)
        {
            if (_projectors.Count == _cameras.Count)
                SetCurrentlyExtrinsicallyCalibrating(_cameras[_current], _projectors[_current++]);
            else
                SetCurrentlyExtrinsicallyCalibrating(_cameras[_current++], _projectors[0]);
        }

        private void skipCamera_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("If you skip this pair, and the extrinsic calibration either does not exist, " +
                "or is wrong because the camera has moved, scanning will return very bad results. " +
                "Do you wish to continue?", "Are you sure you want to skip this pair?", MessageBoxButton.YesNo)
                == MessageBoxResult.Yes)
            {
                _currentcalibration.Cancel();
                if ((_setupType == HardwareSetupType.ThreeProjectors ||
                       _setupType == HardwareSetupType.FourProjectors ||
                       _setupType == HardwareSetupType.TwoProjectors) &&
                       _current == _projectors.Count)
                {
                    cameraPreview1.cameraPreview = null;
                    nextCamera.Visibility = System.Windows.Visibility.Collapsed;
                    _setup.EnableNextButton();
                }
                else if ((_setupType == HardwareSetupType.OneProjector ||
                    _setupType == HardwareSetupType.Other) &&
                    _current == _cameras.Count)
                {
                    cameraPreview1.cameraPreview = null;
                    nextCamera.Visibility = System.Windows.Visibility.Collapsed;
                    _setup.EnableNextButton();
                }
                else
                {
                    if (_projectors.Count == _cameras.Count)
                        SetCurrentlyExtrinsicallyCalibrating(_cameras[_current], _projectors[_current++]);
                    else
                        SetCurrentlyExtrinsicallyCalibrating(_cameras[_current++], _projectors[0]);
                }
            }
        }
    }
}
