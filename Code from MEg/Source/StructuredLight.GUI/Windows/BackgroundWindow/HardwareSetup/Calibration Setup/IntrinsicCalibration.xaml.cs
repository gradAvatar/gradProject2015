﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StructuredLight.Managed;
using System.Threading;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for IntrinsicCalibration.xaml
    /// </summary>
    public partial class IntrinsicCalibration : Window
    {
        private CameraCalibrator _calibrator;
        private ManagedCamera _camera;

        public IntrinsicCalibration()
        {
            InitializeComponent();
        }

        public void Calibrate(ManagedCamera managedCamera)
        {
            _camera = managedCamera;
            int calibImagesNumber = 15;
            cameraName.Content = Utils.GetNameOfCamera(_camera);
            calibrationCompleteText.Content = string.Format("Currently captured {0} of {1} images", 0, calibImagesNumber);
            closeWindow.Visibility = System.Windows.Visibility.Collapsed;

            _calibrator = new CameraCalibrator(_camera);
            _calibrator.NewCalibrationPreview += (o, e) =>
            {
                if (e.CurrentPreviewWasAccepted)
                    Dispatcher.Invoke(new Action(() => calibrationCompleteText.Content = string.Format("Currently captured {0} of {1} images",
                        e.AcceptedCaptureCount, calibImagesNumber)));
                BitmapPreviewer.LoadBitmapImage(e.Preview, cameraPreview1.cameraPreview, true);
            };
            _calibrator.CalibrationFinished += (o, e) =>
            {
                Dispatcher.Invoke(new Action(delegate()
                {
                    calibrationCompleteText.Content = string.Format("This camera's calibration is complete with a re-projection error of {0}.", _calibrator.Results.ReprojectionError);
                    closeWindow.Visibility = System.Windows.Visibility.Visible;
                    cameraPreview1.cameraPreview = null;
                    _calibrator = null;
                }));
                };
            _calibrator.TargetCameraCaptures = calibImagesNumber;
            _calibrator.Start();
        }

        private void cameraSettings_Click(object sender, RoutedEventArgs e)
        {
            CameraSettingsWindow window = new CameraSettingsWindow();
            window.cameraSettings.ChangeCamera(_camera);
            window.Show();
        }

        public void SetTitle()
        {
            cameraName.Content = Utils.GetNameOfCamera(_camera);
        }

        private void closeWindow_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_calibrator != null)
                _calibrator.Cancel();
        }
    }
}
