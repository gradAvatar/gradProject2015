﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for FourProjectorColorSelector.xaml
    /// </summary>
    public partial class FourProjectorColorSelector : UserControl, IHardwareSetupControl
    {
        private HardwareSetup _setup;

        public FourProjectorColorSelector()
        {
            InitializeComponent();
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _setup.SetTitleText("Please fill in the blanks");
        }

        public void RequestedNext()
        {
            if (Camera1.Color != null &&
                Camera2.Color != null &&
                Camera3.Color != null &&
                Camera4.Color != null ||
                Library.UserSettings["DebugGUI"] == "1")
            {
                _setup.SetupInformation["Colors"] = new List<string>() 
                { 
                    Camera1.Color,
                    Camera2.Color, 
                    Camera3.Color, 
                    Camera4.Color 
                };
                _setup.GoTo(CurrentHardwareWindow.FourProjectorsProjectorSelection);
            }
            else
                BackgroundWindow.Window.AddNotification("You must fill out all the necessary information before continuing.", false);
        }
    }
}
