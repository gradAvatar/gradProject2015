﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for OneProjector.xaml
    /// </summary>
    public partial class OneProjector : UserControl, IHardwareSetupControl
    {
        #region Declares

        private HardwareSetup _setup;

        #endregion

        #region Constructor

        public OneProjector()
        {
            InitializeComponent();
        }

        #endregion

        #region IHardwareSetupControl members

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
            Projector1.Calibrate += () =>
            {
                guiPicture.OnCameraClicked -= guiPicture_OnCameraClicked;
                guiPicture.OnCameraClicked += guiPicture_OnCameraClicked;
                MessageBox.Show("Please click on the image of the calibrated camera you wish to use for the projector calibration.");
            };
        }

        public void Display()
        {
            _setup.SetTitleText("Please fill in the blanks");
        }

        public void RequestedNext()
        {
            if (Camera1.Camera != null && Camera1.Camera.IsIntrinsiclyCalibrated &&
                Camera2.Camera != null && Camera2.Camera.IsIntrinsiclyCalibrated &&
                Projector1.Monitor != null && Projector1.Monitor.IsIntrinsiclyCalibrated
                || Library.UserSettings["DebugGUI"] == "1")
            {
                _setup.SetupInformation["Cameras"] = new List<ManagedCamera>() { Camera1.Camera, Camera2.Camera };
                _setup.SetupInformation["Projectors"] = new List<ManagedProjector>() { Projector1.Monitor };
                _setup.SetupInformation["Colors"] = new List<string>() { "White" };
                _setup.GoTo(CurrentHardwareWindow.ExtrinsicExplaination);
            }
            else
                BackgroundWindow.Window.AddNotification("You must fill out all the necessary information before continuing.", false);
        }

        #endregion

        #region Calibration

        #region Projector Calibration

        void guiPicture_OnCameraClicked(int cameraNumber)
        {
            ManagedCamera cam = cameraNumber == 1 ? Camera1.Camera : Camera2.Camera;
            if (cam == null)
                MessageBox.Show("Please select a valid camera, quitting projector calibration.");
            else if (!cam.IsIntrinsiclyCalibrated)
                MessageBox.Show("You must select a calibrated camera, quitting projector calibration.");
            else
            {
                ProjectorCalibration calibration = new ProjectorCalibration();
                calibration.Calibrate(Projector1.Monitor, cam);
                calibration.ShowDialog();
            }
            guiPicture.OnCameraClicked -= guiPicture_OnCameraClicked;
        }

        #endregion

        #endregion
    }
}
