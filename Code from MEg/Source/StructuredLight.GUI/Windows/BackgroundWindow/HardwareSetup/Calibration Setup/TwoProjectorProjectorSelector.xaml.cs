﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for TwoProjectorProjectorSelector.xaml
    /// </summary>
    public partial class TwoProjectorProjectorSelector : UserControl, IHardwareSetupControl
    {
        private HardwareSetup _setup;

        public TwoProjectorProjectorSelector()
        {
            InitializeComponent();
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
            Projector1.Calibrate += () => SetupProjectorCalibration(1);
            Projector2.Calibrate += () => SetupProjectorCalibration(2);
        }

        void SetupProjectorCalibration(int cameraNumber)
        {
            ManagedCamera cam = ((dynamic)_setup.SetupInformation["Cameras"])[cameraNumber];
            ManagedProjector proj = cameraNumber == 1 ? Projector1.Monitor : Projector2.Monitor;
            if (cam == null)
                MessageBox.Show("Please select a valid camera, quitting projector calibration.");
            else if (!cam.IsIntrinsiclyCalibrated)
                MessageBox.Show("You must select a calibrated camera, quitting projector calibration.");
            else
            {
                ProjectorCalibration calibration = new ProjectorCalibration();
                calibration.Calibrate(proj, cam);
                calibration.ShowDialog();
            }
        }

        public void Display()
        {
            _setup.SetTitleText("Please fill in the blanks");
        }

        public void RequestedNext()
        {
            if (Projector1.Monitor != null && Projector1.Monitor.IsIntrinsiclyCalibrated &&
                Projector2.Monitor != null && Projector2.Monitor.IsIntrinsiclyCalibrated ||
                Library.UserSettings["DebugGUI"] == "1")
            {
                _setup.SetupInformation["Projectors"] = new List<ManagedProjector>() 
                { 
                    Projector1.Monitor,
                    Projector2.Monitor
                };
                _setup.GoTo(CurrentHardwareWindow.ExtrinsicExplaination);
            }
            else
                BackgroundWindow.Window.AddNotification("You must fill out all the necessary information before continuing.", false);
            }
    }
}
