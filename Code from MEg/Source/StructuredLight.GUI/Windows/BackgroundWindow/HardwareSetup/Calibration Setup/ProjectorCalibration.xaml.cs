﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StructuredLight.Managed;
using System.Threading;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for ProjectorCalibration.xaml
    /// </summary>
    public partial class ProjectorCalibration : Window
    {
        private ProjectorCalibrator _calibrator;
        private ManagedCamera _camera;
        private ManagedProjector _projector;

        public ProjectorCalibration()
        {
            InitializeComponent();
        }

        public void Calibrate(ManagedProjector monitor, ManagedCamera managedCamera)
        {
            _camera = managedCamera;
            _projector = monitor;

            int calibImagesNumber = 15;

            cameraName.Content = Utils.GetNameOfProjector(_projector);
            calibrationCompleteText.Content = string.Format("Currently captured {0} of {1} images", 0, calibImagesNumber);
            closeWindow.Visibility = System.Windows.Visibility.Collapsed;
           
            _calibrator = new ProjectorCalibrator(_camera, _projector);
            _calibrator.NewCalibrationPreview += (o, e) =>
            {
                if (e.CurrentPreviewWasAccepted)
                    Dispatcher.Invoke(new Action(() => calibrationCompleteText.Content = string.Format("Currently captured {0} of {1} images",
                        e.AcceptedCaptureCount, calibImagesNumber)));
                BitmapPreviewer.LoadBitmapImage(e.Preview, cameraPreview1.cameraPreview, true); 
            };
            _calibrator.CalibrationFinished += (o, e) =>
            {
                calibrationCompleteText.Content = string.Format("This projectors's calibration is complete with a re-projection error of {0}.", _calibrator.Results.ReprojectionError);
                closeWindow.Visibility = System.Windows.Visibility.Visible;
                _calibrator = null;
                cameraPreview1.cameraPreview = null;
            };
            _calibrator.TargetCameraCaptures = calibImagesNumber;
            _calibrator.Start();
        }

        private void cameraSettings_Click(object sender, RoutedEventArgs e)
        {
            CameraSettingsWindow window = new CameraSettingsWindow();
            window.cameraSettings.ChangeCamera(_camera);
            window.Show();
        }

        private void closeWindow_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_calibrator != null)
                _calibrator.Cancel();
        }
    }
}
