﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for OtherProjectorProjectorSelection.xaml
    /// </summary>
    public partial class OtherProjectorProjectorSelection : UserControl
    {
        private HardwareSetup _setup;

        public OtherProjectorProjectorSelection()
        {
            InitializeComponent();
            Projector1.ProjectorSelected += projectorSelected;
            Projector1.Calibrate += calibrate;
            #region Factory
            FrameworkElementFactory tb = new FrameworkElementFactory(typeof(TextBlock));
            tb.SetBinding(TextBlock.TextProperty, new Binding("ModelName"));
            tb.SetValue(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis);
            tb.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Left);
            tb.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Left);
            tb.SetValue(TextBlock.MarginProperty, new Thickness(15, 0, 0, 0));
            DataTemplate dt = new DataTemplate();
            dt.VisualTree = tb;
            ProjectorSelection.ItemTemplate = dt;
            #endregion
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _setup.SetTitleText("Please fill in the blanks");
            ProjectorSelection.Items.Clear();
            Projector1.Clear();
        }

        public void RequestedNext()
        {
            if ((ProjectorSelection.Items.Count != 0 &&
                !ProjectorSelection.Items.OfType<ManagedProjector>().Any(c => !c.IsIntrinsiclyCalibrated))
                || Library.UserSettings["DebugGUI"] == "1")
            {
                if(ProjectorSelection.Items.Count != 0)
                    _setup.SetupInformation["Projectors"] = new List<ManagedProjector>(ProjectorSelection.Items.OfType<ManagedProjector>());
                _setup.GoTo(CurrentHardwareWindow.ExtrinsicExplaination);
            }
            else
                BackgroundWindow.Window.AddNotification("You must fill out all the necessary information before continuing.", false);
        }

        private void projectorSelected()
        {
            Projector1.Monitor.FriendlyName =
                string.IsNullOrEmpty(Projector1.Monitor.FriendlyName) ? 
                Projector1.Monitor.ModelName : Projector1.Monitor.FriendlyName;
            ProjectorSelection.Items.Add(Projector1.Monitor);
            Projector1.Clear();
        }

        private void calibrate()
        {
            CameraSelectorWindow window = new CameraSelectorWindow();
            window.cameraSelection.UpdateCameras((List<ManagedCamera>)_setup.SetupInformation["Cameras"]);
            window.ShowDialog();
            ManagedCamera cam = window.cameraSelection.SelectedCamera;

            ProjectorCalibration calibration = new ProjectorCalibration();
            calibration.Calibrate(Projector1.Monitor, cam);
            calibration.ShowDialog();
        }
    }
}
