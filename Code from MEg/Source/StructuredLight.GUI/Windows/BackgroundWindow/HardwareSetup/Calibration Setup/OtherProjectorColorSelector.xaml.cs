﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for OtherProjectorColorSelector.xaml
    /// </summary>
    public partial class OtherProjectorColorSelector : UserControl
    {
        private HardwareSetup _setup;

        public OtherProjectorColorSelector()
        {
            InitializeComponent();
            Color.ColorSelected += colorSelected;
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _setup.SetTitleText("Please fill in the blanks");
            ColorSelection.Items.Clear();
            Color.Clear();
        }

        public void RequestedNext()
        {
            if (ColorSelection.Items.Count != 0 ||
                Library.UserSettings["DebugGUI"] == "1")
            {
                if (ColorSelection.Items.Count != 0)
                    _setup.SetupInformation["Colors"] = new List<string>(ColorSelection.Items.OfType<string>());
                _setup.GoTo(CurrentHardwareWindow.OtherProjectorsProjectorSelection);
            }
            else
                BackgroundWindow.Window.AddNotification("You must fill out all the necessary information before continuing.", false);
        }

        private void colorSelected()
        {
            ColorSelection.Items.Add(Color.Color);
            Color.Clear();
        }
    }
}
