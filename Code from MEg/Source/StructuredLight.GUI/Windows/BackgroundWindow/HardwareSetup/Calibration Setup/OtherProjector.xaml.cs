﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for OtherProjector.xaml
    /// </summary>
    public partial class OtherProjector : UserControl
    {
        private HardwareSetup _setup;

        public OtherProjector()
        {
            InitializeComponent();
            Camera1.CameraSelected += cameraSelected;
            #region Factory
            FrameworkElementFactory tb = new FrameworkElementFactory(typeof(TextBlock));
            tb.SetBinding(TextBlock.TextProperty, new Binding("ModelName"));
            tb.SetValue(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis);
            tb.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Left);
            tb.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Left);
            tb.SetValue(TextBlock.MarginProperty, new Thickness(15, 0, 0, 0));
            DataTemplate dt = new DataTemplate();
            dt.VisualTree = tb;
            CameraSelection.ItemTemplate = dt;
            #endregion
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _setup.SetTitleText("Please fill in the blanks");
            CameraSelection.Items.Clear();
            Camera1.Clear();
        }

        public void RequestedNext()
        {
            if ((CameraSelection.Items.Count != 0 &&
                !CameraSelection.Items.OfType<ManagedCamera>().Any(c => !c.IsIntrinsiclyCalibrated))
                || Library.UserSettings["DebugGUI"] == "1")
            {
                if (CameraSelection.Items.Count != 0)
                    _setup.SetupInformation["Cameras"] = new List<ManagedCamera>(CameraSelection.Items.OfType<ManagedCamera>());
                _setup.GoTo(CurrentHardwareWindow.OtherProjectorsColorSelection);
            }
            else
                BackgroundWindow.Window.AddNotification("You must fill out all the necessary information before continuing.", false);
        }

        private void cameraSelected()
        {
            Camera1.Camera.FriendlyName =
                string.IsNullOrEmpty(Camera1.Camera.FriendlyName) ?
                Camera1.Camera.ModelName : Camera1.Camera.FriendlyName;
            CameraSelection.Items.Add(Camera1.Camera);
            Camera1.Clear();
        }
    }
}
