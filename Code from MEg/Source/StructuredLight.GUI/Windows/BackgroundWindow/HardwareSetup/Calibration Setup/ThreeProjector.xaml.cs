﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for ThreeProjector.xaml
    /// </summary>
    public partial class ThreeProjector : UserControl, IHardwareSetupControl
    {
        private HardwareSetup _setup;

        public ThreeProjector()
        {
            InitializeComponent();
        }

        public void Initialize(HardwareSetup setup)
        {
            _setup = setup;
        }

        public void Display()
        {
            _setup.SetTitleText("Please fill in the blanks");
        }

        public void RequestedNext()
        {
            if (Camera1.Camera != null && Camera1.Camera.IsIntrinsiclyCalibrated &&
                Camera2.Camera != null && Camera2.Camera.IsIntrinsiclyCalibrated &&
                Camera3.Camera != null && Camera3.Camera.IsIntrinsiclyCalibrated ||
                Library.UserSettings["DebugGUI"] == "1")
            {
                _setup.SetupInformation["Cameras"] = new List<ManagedCamera>() 
                { 
                    Camera1.Camera,
                    Camera2.Camera, 
                    Camera3.Camera 
                };
                _setup.GoTo(CurrentHardwareWindow.ThreeProjectorsColorSelection);
            }
            else
                BackgroundWindow.Window.AddNotification("You must fill out all the necessary information before continuing.", false);
        }
    }
}
