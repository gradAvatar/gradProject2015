﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for SettingsEditor.xaml
    /// </summary>
    public partial class SettingsEditor : UserControl, IBackgroundWindow
    {
        private bool _initialized = false;
        public SettingsEditor()
        {
            InitializeComponent();
            _initialized = true;
        }

        #region Display Method

        public void Display()
        {
            _initialized = false;
            circle_rows.TextContent = Library.CircleRows.ToString();
            circle_columns.TextContent = Library.CircleColumns.ToString();
            circle_diameter.TextContent = Library.CircleDiameter.ToString();
            circle_spacing.TextContent = Library.CircleSpacing.ToString();
            frame_delay.TextContent = Library.FrameDelay.ToString();
            disable_internal_viewer.IsChecked = Library.UserSettings["DisableInternalPointCloudViewer"] == "1";
            mesh_after_scanning.IsChecked = Library.UserSettings["MeshAfterScanning"] == "1";
            open_mesh_in_meshlab.IsChecked = Library.UserSettings["OpenMeshInMeshLab"] == "1";
            open_point_cloud_in_meshlab.IsChecked = Library.UserSettings["OpenPointCloudInMeshLab"] == "1";
            open_mesh_in_meshlab.IsChecked = Library.UserSettings["OpenMeshInMeshLab"] == "1";
            debug_gui.IsChecked = Library.UserSettings["DebugGUI"] == "1";
            locationOfMeshlab.Text = Library.UserSettings["MeshLabLocation"];
            if (locationOfMeshlab.Text == "")
            {
                locationOfMeshlab.Text = MeshLab.MeshlabHelper.TryFindMeshlabInstance();
                Library.UserSettings["MeshLabLocation"] = locationOfMeshlab.Text;
                Library.SaveConfiguration();
            }
            _initialized = true;
        }

        public void Hide() { }

        #endregion

        private void meshAfterScanning_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.UserSettings["MeshAfterScanning"] = mesh_after_scanning.IsChecked == true ? "1" : "0";
            Library.SaveConfiguration();
        }

        private void disableInternalPointCloudViewer_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.UserSettings["DisableInternalPointCloudViewer"] = disable_internal_viewer.IsChecked == true ? "1" : "0";
            Library.SaveConfiguration();
        }

        private void openPointCloudInMeshlab_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.UserSettings["OpenPointCloudInMeshLab"] = open_point_cloud_in_meshlab.IsChecked == true ? "1" : "0";
            Library.SaveConfiguration();
        }

        private void openMeshInMeshlab_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.UserSettings["OpenMeshInMeshLab"] = open_mesh_in_meshlab.IsChecked == true ? "1" : "0";
            Library.SaveConfiguration();
        }

        private void locationOfMeshlab_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            if (!_initialized) return;
            Library.UserSettings["MeshLabLocation"] = locationOfMeshlab.Text;
            Library.SaveConfiguration();
        }

        private void debug_gui_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.UserSettings["DebugGUI"] = debug_gui.IsChecked == true ? "1" : "0";
            Library.SaveConfiguration();
        }

        private void meshlabLocationBrowse_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            string file;
            if (Utils.GetFile("Application File|*.exe", out file))
                locationOfMeshlab.Text = file;
            Library.UserSettings["MeshLabLocation"] = locationOfMeshlab.Text;
            Library.SaveConfiguration();
        }

        public void circle_diameter_changed(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.CircleDiameter = float.Parse(circle_diameter.TextContent);
            Library.SaveConfiguration();
        }

        public void circle_spacing_changed(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.CircleSpacing = float.Parse(circle_spacing.TextContent);
            Library.SaveConfiguration();
        }

        public void circle_rows_changed(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.CircleRows = int.Parse(circle_rows.TextContent);
            Library.SaveConfiguration();
        }

        public void circle_columns_changed(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.CircleColumns = int.Parse(circle_columns.TextContent);
            Library.SaveConfiguration();
        }

        public void frame_delay_changed(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            Library.FrameDelay = int.Parse(frame_delay.TextContent);
            Library.SaveConfiguration();
        }
    }
}
