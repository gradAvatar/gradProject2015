﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for SplashPage.xaml
    /// </summary>
    public partial class SplashPage : UserControl, IBackgroundWindow
    {
        #region Constructor

        public SplashPage()
        {
            InitializeComponent();
        }

        #endregion

        #region Display Method

        public void Display() { }

        public void Hide() 
        {
        }

        #endregion
    }
}
