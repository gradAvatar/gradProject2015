﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for CameraPreview.xaml
    /// </summary>
    public partial class CameraPreview : UserControl, IBackgroundWindow
    {
        #region Declares

        private ManagedCamera _currentCamera = null;
        private ManagedCameraStream _currentStream = null;

        #endregion

        #region Constructor

        public CameraPreview()
        {
            InitializeComponent();
        }

        #endregion

        #region Display Method

        public void Display()
        {
            this.cameraPreview.Height = this.Height;
            this.cameraPreview.Width = this.Width;
        }

        public void Hide() 
        {
            CloseCameraPreview();
        }

        #endregion

        #region Open/Close Camera Previews

        private bool _waitingForClose = false;
        public void PreviewCamera(ManagedCamera cam)
        {
            if (cam == null)
                return;
            if (_currentCamera != null)
                CloseCameraPreview();

            _currentCamera = cam;

            _currentStream = cam.OpenStream();
            _currentStream.ImageCaptured += (o, e) =>
                {
                    if (_waitingForClose)
                        return;
                    BitmapPreviewer.LoadBitmapImage(e.Preview, cameraPreview);
                };
            if (!_currentStream.Start())
                MessageBox.Show("Failed to open camera stream");
        }

        public void CloseCameraPreview()
        {
            if (_currentStream != null && _currentStream.IsStarted)
            {
                _waitingForClose = true;
                _currentStream.WaitForFrame();
                _currentStream.Stop();
                _currentStream = null;
                _waitingForClose = false;
            }
        }

        #endregion
    }
}
