﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.GUI.MeshLab;
using System.Windows.Media.Media3D;
using System.IO;
using Path = System.IO.Path;
using StructuredLight.Managed;
using HelixToolkit.Wpf;
using System.Runtime;

namespace StructuredLight.GUI
{
    public delegate void CutSelectionSelected(RayMeshGeometry3DHitTestResult result);
    public delegate void ChangeSelectedPoints(bool changing);

    /// <summary>
    /// Interaction logic for MeshViewer.xaml
    /// </summary>
    public partial class MeshViewer : UserControl, IBackgroundWindow
    {
        #region Declares

        private Point3D FirstSelectedPoint = new Point3D(0, 0, 0);
        private Point3D SecondSelectedPoint = new Point3D(0, 0, 0);
        private Point3D ThirdSelectedPoint = new Point3D(0, 0, 0);
        private Point3D LastSelectedFirstSelectedPoint = new Point3D(0, 0, 0);
        private Point3D LastSelectedSecondSelectedPoint = new Point3D(0, 0, 0);
        private Point3D LastSelectedThirdSelectedPoint = new Point3D(0, 0, 0);
        private Visual3D _mesh = null;
        private Visual3D _unCutMesh = null;
        private Rect3D _meshBounds;
        private int _meshTriangleIndices = 0;
        private int _meshVertices = 0;
        private DisplayType m_displayType = DisplayType.DrawContourLine;
        private bool m_processing = false;
        private IList<Point3D> _previouslySelectedPointsPlane;
        private CalcType _currentCalcType = CalcType.Contour;
        private bool _selectingPoints = false;
        private bool _selectingCutSelection = false;
        private CutSelectionSelected _cutSelectionSelected;
        private Vector3DCollection _vectorCollection = null;
        private bool _initialized = false;

        #endregion

        #region Constructor

        public MeshViewer()
        {
            InitializeComponent();
            _initialized = true;
        }

        #endregion

        #region Display Method

        public void Display() { }

        public void Hide() { }

        #endregion

        #region Events

        public void ResetView()
        {
            if(_meshBounds != null)
                CameraHelper.ZoomExtents((ProjectionCamera)view1.Camera, view1.Viewport, _meshBounds);
        }

        public void ClearViewport()
        {
            ResetViewport();
        }

        public void BeginSelectingPoints()
        {
            ChangeIsSelectingPoints(!_selectingPoints);
        }

        public void SetCurrentCalculationType(CalcType type)
        {
            _currentCalcType = type;
        }

        public void SetCurrentDisplayType(DisplayType type)
        {
            m_displayType = type;
            SetUpGUILines();
        }

        #endregion

        #region Mesh Loading

        private int _requested;
        /// <summary>
        /// Loads a mesh or point cloud into the internal viewer
        /// (internal dispatching)
        /// </summary>
        /// <param name="mesh"></param>
        /// <param name="forceMeshing"></param>
        public void LoadMesh(string mesh, bool forceMeshing = true)
        {
            if (Dispatcher.Thread.ManagedThreadId == System.Threading.Thread.CurrentThread.ManagedThreadId)
                InternalLoadMesh(mesh, forceMeshing);
            else
                Dispatcher.Invoke(new Action(() => InternalLoadMesh(mesh, forceMeshing)));
        }

        private void InternalLoadMesh(string mesh, bool forceMeshing)
        {
            OpenMeshTaskBar.AddRecentOpenedMesh(mesh);
            view1.ShowFrameRate = true;
            _requested++;
            SetTitle("");
            this.view1.Children.Clear();
            var notification = BackgroundWindow.Window.AddNotification("Loading " + Path.GetFileName(mesh) + ".");
            
            System.Threading.Thread t = new System.Threading.Thread(() =>
            {
                if (Library.UserSettings["OpenPointCloudInMeshLab"] == "1")
                {
                    MeshlabHelper.StartMeshlab(mesh);
                    BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.ViewAndMeasure);
                }
                else if (!ConvertMesh(ref mesh, forceMeshing))
                {
                    BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.ViewAndMeasure);
                    BackgroundWindow.Window.RemoveNotification(notification);
                    return;
                }
                else if (Library.UserSettings["OpenMeshInMeshLab"] == "1")
                {
                    MeshlabHelper.StartMeshlab(mesh);
                    BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.ViewAndMeasure);
                }
                else
                {
                    view1.Dispatcher.Invoke(new Action(() => ClearViewport()));
                    Visual3D visual = null;
                    if (!forceMeshing)
                        visual = LoadPointCloud(mesh);
                    else
                        visual = LoadConvertedMesh(mesh, forceMeshing);
                    view1.Dispatcher.Invoke(new Action(() =>
                        {
                            if (visual != null)
                            {
                                _mesh = visual;
                                _unCutMesh = _mesh;
                                CameraHelper.ZoomExtents((ProjectionCamera)view1.Camera, view1.Viewport, _meshBounds);
                                view1.Children.Add(visual);
                                DisplayMeshInformation();
                                SetCurrentlyViewing(mesh);
                            }
                            ForceGC();
                        }));
                }
                notification = BackgroundWindow.Window.ReplaceNotification(notification, "Finished loading " + Path.GetFileName(mesh) + ".", false);
            });
            t.Start();
        }

        private ModelVisual3D LoadPointCloud(string mesh)
        {
            IModelReader reader = ModelImporter.LoadReader(mesh);
            reader.Read(mesh);
            PointsVisual3D p = null;
            Dispatcher.Invoke(new Action(() =>
            {
                p = new PointsVisual3D();
                p.Points = reader.Points;

                MeshGeometry3D geo = GetMeshGeometry(p);
                _meshBounds = new Rect3D(geo.Bounds.X, geo.Bounds.Y, geo.Bounds.Z, geo.Bounds.SizeX, geo.Bounds.SizeY, geo.Bounds.SizeZ);
                _meshTriangleIndices = geo.TriangleIndices.Count;
                _meshVertices = geo.Positions.Count;
                
                p.Size = 0.5;
                p.Color = Colors.White;
            }));

            return p;
        }

        public bool ConvertMesh(ref string mesh, bool forceMeshing)
        {
            switch (Path.GetExtension(mesh))
            {
                case ".3ds":
                case ".lwo":
                case ".obj":
                case ".objz":
                case ".ply":
                case ".stl":
                case ".off":
                    break;
                default:
                    {
                        string newFileName = Path.ChangeExtension(mesh, ".obj");
                        if (!File.Exists(newFileName))
                        {
                            if (!MeshLabPointCloudConversion.ConvertMeshFormats(mesh, ".obj") || !File.Exists(mesh))
                            {
                                BackgroundWindow.Window.AddNotification("Failed to convert mesh from " + Path.GetExtension(mesh) + " to .obj, mesh loading has failed.", false);
                                return false;
                            }
                        }
                        mesh = newFileName;
                    }
                    break;
            }

            IModelReader reader = ModelImporter.LoadReader(mesh);
            bool isMesh = reader.IsMesh(mesh);

            if (!isMesh && forceMeshing)
            {
                string newMeshFile = Path.Combine(Path.GetDirectoryName(mesh), Path.GetFileNameWithoutExtension(mesh) + "-mesh.ply");
                if (File.Exists(newMeshFile))
                {
                    mesh = newMeshFile;
                    return true;
                }
                var notification = BackgroundWindow.Window.AddNotification("Converting to mesh, this may take awhile.");
                //Meshlab meshing, older way of doing it
                /*if (!MeshLabPointCloudConversion.ConvertPointCloudToMesh(mesh, newMeshFile) || !File.Exists(newMeshFile))
                {
                    BackgroundWindow.Window.AddNotification("Failed to convert point cloud into a mesh, mesh loading has failed.", false);
                    return false;
                }*/
                MeshGenerator meshGenerator = new MeshGenerator();
                meshGenerator.ConvertPointCloudToMesh(mesh, newMeshFile);
                notification.Close();
                if (!File.Exists(newMeshFile))
                {
                    BackgroundWindow.Window.AddNotification("Failed to convert point cloud into a mesh, mesh loading has failed.", false);
                    return false;
                }
                mesh = newMeshFile;
            }
            return true;
        }

        private ModelVisual3D LoadConvertedMesh(string meshFile, bool forceMeshing = true)
        {
            ModelVisual3D mv = null;
            using (IModelReader reader = ModelImporter.LoadReader(meshFile))
            {
                reader.Read(meshFile);

                Dispatcher.Invoke(new Action(() =>
                {
                    Model3DGroup mesh = ((dynamic)reader).BuildModel();
                    mesh.Children.Add(new AmbientLight(new Color() { R = 100, G = 100, B = 100 }));
                    mv = new ModelVisual3D { Content = mesh };
                    _mesh = mv;
                    
                    MeshGeometry3D geo = GetMeshGeometry(mv);
                    _meshBounds = new Rect3D(geo.Bounds.X, geo.Bounds.Y, geo.Bounds.Z, geo.Bounds.SizeX, geo.Bounds.SizeY, geo.Bounds.SizeZ);
                    _meshTriangleIndices = geo.TriangleIndices.Count;
                    _meshVertices = geo.Positions.Count;

                    MeshDrawingHelpers.FixColors(ref mv, Colors.White);
                }));
                reader.Dispose();
            }
            return mv;
        }

        #endregion

        #region Mesh loading helpers

        #region Setup lights

        private DirectionalLight _light;
        private void SetupLights()
        {
            ModelVisual3D lightsVisual = new ModelVisual3D();
            var i = (byte)(255 * 0.55);
            _light = new DirectionalLight(Color.FromRgb(i, i, i), view1.CameraController.CameraLookDirection);
            lightsVisual.Content = _light;

            view1.Children.Add(lightsVisual);

            view1.CameraChanged += view1_CameraChanged;
        }

        void view1_CameraChanged(object sender, RoutedEventArgs e)
        {
            _light.Direction = view1.CameraController.CameraLookDirection;
        }

        #endregion

        #region Title Setting

        private void SetTitle(string title, double size)
        {
            view1.TitleSize = size;
            view1.Title = title;
            view1.TextBrush = new SolidColorBrush(Colors.White);
        }

        private void SetTitle(string title)
        {
            SetTitle(title, 12.0);
        }

        private void SetSubTitle(string title, double size)
        {
            view1.SubTitleSize = size;
            view1.SubTitle = title;
        }

        private void SetSubTitle(string title)
        {
            SetSubTitle(title, 12.0);
        }

        #endregion

        #region Changing Points

        private void ResetViewport()
        {
            this.view1.Children.Clear();
            SetupLights();
        }

        public void ClearSelectedPoints()
        {
            DisplayMeshInformation();
            ClearShownPoints(true);
            FirstSelectedPoint = new Point3D();
            SecondSelectedPoint = new Point3D();
            ThirdSelectedPoint = new Point3D();
            LastSelectedFirstSelectedPoint = new Point3D();
            LastSelectedSecondSelectedPoint = new Point3D();
            LastSelectedThirdSelectedPoint = new Point3D();
        }

        public ChangeSelectedPoints ChangeSelectingPoints;

        private void ChangeIsSelectingPoints(bool changing, bool removeShownPoints = true)
        {
            if (removeShownPoints)
                ClearSelectedPoints();

            begin_selecting_points_Click(null, null);
        }

        #endregion

        #region Shown Point Addition

        private void AddShownPoint(Point3D point)
        {
            AddShownPoint(point, Colors.Red);
        }

        private void AddShownPoint(Point3D point, Color c)
        {
            view1.Children.Add(MeshDrawingHelpers.CreateSphere(
                 _meshBounds.SizeX / 100f, point, c));
        }

        private void ClearShownPoints(bool removeShownPoints = true)
        {
            var lst = new List<Visual3D>(view1.Children.Where(v => (removeShownPoints && v is SphereVisual3D) ||
                v is RectangleVisual3D || v is LinesVisual3D));
            foreach (var c in lst)
                view1.Children.Remove(c);
        }

        #endregion

        #endregion

        #region Mesh Raycasting

        private void Window_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == System.Windows.Input.MouseButtonState.Pressed && 
                (!m_processing || _selectingCutSelection) && (_selectingCutSelection ||
                _selectingPoints))
            {
                Point mousePos = e.GetPosition(view1);
                PointHitTestParameters hitParams = new PointHitTestParameters(mousePos);
                VisualTreeHelper.HitTest(view1, null, ResultCallback, hitParams);
            }
        }

        public HitTestResultBehavior ResultCallback(HitTestResult result)
        {
            // Did we hit brain?
            RayMeshGeometry3DHitTestResult rayMeshResult = result as RayMeshGeometry3DHitTestResult;
            if (rayMeshResult != null)
            {
                if (_selectingCutSelection)
                {
                    _cutSelectionSelected(rayMeshResult);
                }
                else if (_selectingPoints)
                {
                    // Yes we did!
                    if (FirstSelectedPoint.X == 0 && FirstSelectedPoint.Y == 0 && FirstSelectedPoint.Z == 0)
                    {
                        FirstSelectedPoint = rayMeshResult.PointHit;
                        ClearShownPoints();
                        AddShownPoint(rayMeshResult.PointHit);
                    }
                    else if (SecondSelectedPoint.X == 0 && SecondSelectedPoint.Y == 0 && SecondSelectedPoint.Z == 0)
                    {
                        SecondSelectedPoint = rayMeshResult.PointHit;
                        AddShownPoint(rayMeshResult.PointHit);

                        if (_currentCalcType == CalcType.Direct)
                        {
                            PrepareForProcessing();

                            Visual3D lineVisual = MeshDrawingHelpers.DrawLineThroughPoints(new List<Point3D>() { LastSelectedFirstSelectedPoint, LastSelectedSecondSelectedPoint }, Colors.Red);
                            double distance = MeshCalculations.FindDirectDistance(LastSelectedFirstSelectedPoint, LastSelectedSecondSelectedPoint);
                            view1.Children.Add(lineVisual);
                            SetSubTitle("Direct Distance: " + distance + "mm");

                            FinishProcessing();
                        }
                    }
                    else if (ThirdSelectedPoint.X == 0 && ThirdSelectedPoint.Y == 0 && ThirdSelectedPoint.Z == 0)
                    {
                        ThirdSelectedPoint = rayMeshResult.PointHit;
                        AddShownPoint(rayMeshResult.PointHit);

                        if (_currentCalcType == CalcType.Contour)
                        {
                            PrepareForProcessing();

                            DrawContourLine(FirstSelectedPoint, SecondSelectedPoint, ThirdSelectedPoint);

                            FinishProcessing();
                        }
                        else if (_currentCalcType == CalcType.Cut)
                        {
                            PrepareForProcessing(false);

                            DoMeshCut();
                        }
                    }
                }
            }
            else if (_selectingCutSelection)
            {
                _cutSelectionSelected(rayMeshResult);
            }

            return HitTestResultBehavior.Stop;
        }

        #region Processing Setup

        private void FinishProcessing()
        {
            m_processing = false;
            ChangeIsSelectingPoints(false, false);
        }

        private void PrepareForProcessing(bool addAdorner = true)
        {
            SetSubTitle("");

            LastSelectedFirstSelectedPoint = FirstSelectedPoint;
            LastSelectedSecondSelectedPoint = SecondSelectedPoint;
            LastSelectedThirdSelectedPoint = ThirdSelectedPoint;
            FirstSelectedPoint = new Point3D();
            SecondSelectedPoint = new Point3D();
            ThirdSelectedPoint = new Point3D();

            m_processing = true;
        }

        private MeshGeometry3D GetMeshGeometry(Visual3D visual = null)
        {
            if (visual == null) visual = _mesh;
            if (_mesh == null)
                return null;
            if (((ModelVisual3D)visual).Content is GeometryModel3D)
                return (MeshGeometry3D)(((ModelVisual3D)visual).Content as GeometryModel3D).Geometry;
            return (MeshGeometry3D)((((ModelVisual3D)visual).Content as Model3DGroup).Children[0] as GeometryModel3D).Geometry;
        }

        #endregion

        #endregion

        #region Mesh Cutting

        private void DoMeshCut()
        {
            System.Threading.Thread t = new System.Threading.Thread(() =>
                {
                    ModelVisual3D cutUp, cutDown;
                    MeshGeometry3D geometry = null;
                    Dispatcher.Invoke(new Action(() => geometry = GetMeshGeometry()));
                    MeshCuttingHelpers.CutAlongPoints(LastSelectedFirstSelectedPoint,
                        LastSelectedSecondSelectedPoint,
                        LastSelectedThirdSelectedPoint,
                        geometry, Colors.SteelBlue, Colors.White, out cutUp, out cutDown);
                    if (cutUp == null || cutDown == null)
                    {
                        MessageBox.Show("Cut failed, unable to generate the convex hull necessary for splitting the mesh. Please try again.");
                        return;
                    }
                    Dispatcher.Invoke(new Action(() =>
                        {
                            ResetViewport();
                            view1.Children.Add(cutUp);
                            view1.Children.Add(cutDown);
                        }));

                    NotificationBar notification = BackgroundWindow.Window.AddNotification("Click on which side you wish to remove", false);
                    _selectingCutSelection = true;

                    ForceGC();
                    _cutSelectionSelected = delegate(RayMeshGeometry3DHitTestResult r)
                    {
                        notification.Close();
                        ResetViewport();
                        if (r == null)
                        {
                            view1.Children.Add(_mesh);
                            _selectingCutSelection = false;
                            FinishProcessing();
                            ForceGC();
                            return;
                        }
                        ModelVisual3D survivingMesh = r.VisualHit == cutUp ? cutDown : cutUp;
                        
                        MeshDrawingHelpers.FixColors(ref survivingMesh, Colors.White);
                        view1.Children.Add(survivingMesh);
                        _mesh = survivingMesh;

                        _selectingCutSelection = false;
                        FinishProcessing();
                        ForceGC();
                    };
                });
            t.Start();
        }

        private static void ForceGC()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }

        public void StopCuttingMesh()
        {
            ResetViewport();
            view1.Children.Add(_mesh);

            _selectingCutSelection = false;
            FinishProcessing();
        }

        #endregion

        #region Mesh Contour Distance

        private void DrawContourLine(Point3D FirstSelectedPoint, Point3D SecondSelectedPoint, Point3D ThirdSelectedPoint)
        {
            _previouslySelectedPointsPlane = MeshCalculations.CreateContourLine(LastSelectedFirstSelectedPoint,
                LastSelectedSecondSelectedPoint,
                LastSelectedThirdSelectedPoint,
                GetMeshGeometry());
            if (_previouslySelectedPointsPlane != null && _previouslySelectedPointsPlane.Count > 0)
            {
                MeshContourDistanceHelpers.CalculateMeshContourDistances(LastSelectedFirstSelectedPoint,
                    LastSelectedSecondSelectedPoint,
                    LastSelectedThirdSelectedPoint,
                    _previouslySelectedPointsPlane,
                    delegate(List<Point3D> abPath, List<Point3D> bcPath, List<Point3D> acPath,
                        double abLength, double bcLength, double acLength)
                    {
                        view1.Dispatcher.Invoke(new Action(delegate()
                        {
                            double distance = SetUpGUILines(_previouslySelectedPointsPlane,
                                LastSelectedFirstSelectedPoint,
                                LastSelectedSecondSelectedPoint,
                                LastSelectedThirdSelectedPoint,
                                true,
                                abPath,
                                bcPath,
                                acPath,
                                abLength,
                                bcLength,
                                acLength);
                            SetSubTitle("Contour Distance: " + distance + "mm");
                            m_processing = false;
                        }));
                    });
            }
            else
            {
                m_processing = false;
            }
        }

        #endregion

        #region Show distances in the GUI

        private void SetUpGUILines()
        {
            ClearShownPoints(false);
            if (_currentCalcType == CalcType.Contour || _currentCalcType == CalcType.Cut)
            {
                if (FirstSelectedPoint != new Point3D())
                    SetUpGUILines(_previouslySelectedPointsPlane, FirstSelectedPoint, SecondSelectedPoint, ThirdSelectedPoint);
                SetUpGUILines(_previouslySelectedPointsPlane, LastSelectedFirstSelectedPoint, LastSelectedSecondSelectedPoint, LastSelectedThirdSelectedPoint);
            }
        }

        private double SetUpGUILines(IList<Point3D> points, Point3D f1, Point3D f2, Point3D f3,
            bool clearPoints = true, List<Point3D> abPath = null, List<Point3D> bcPath = null,
            List<Point3D> acPath = null, double abdistance = 0, double bcdistance = 0, double acdistance = 0)
        {
            if (clearPoints)
                ClearShownPoints(false);
            if (_meshBounds == null)
                return 0;
            if (m_displayType == DisplayType.DrawTangentline)
                view1.Children.Add(MeshDrawingHelpers.CreatePlane(f1, f2, f3, _meshBounds.Size.X * 3));
            if (abPath == null || acPath == null || bcPath == null)
            {
                double len = 0;
                MeshContourDistanceHelpers.CalculateMeshContourDistances(LastSelectedFirstSelectedPoint,
                    LastSelectedSecondSelectedPoint,
                    LastSelectedThirdSelectedPoint,
                    _previouslySelectedPointsPlane,
                    delegate(List<Point3D> ab, List<Point3D> bc, List<Point3D> ac,
                        double abLength, double bcLength, double acLength)
                    {
                        len = SetUpGUILines(points, f1, f2, f3, clearPoints,
                            ab, bc, ac, abLength, bcLength, acLength);
                    });
                while (len == 0)
                    System.Threading.Thread.Sleep(100);
                return len;
            }

            double distance = 0;
            if(abdistance == 0)
                abdistance = MeshCalculations.CalculateDistanceAlongPoints(abPath);
            if (bcdistance == 0)
                bcdistance = MeshCalculations.CalculateDistanceAlongPoints(bcPath);
            if (acdistance == 0)
                acdistance = MeshCalculations.CalculateDistanceAlongPoints(acPath);
            if (acdistance > abdistance && acdistance > bcdistance)
            {
                if (m_displayType == DisplayType.DrawContourLine)
                {
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(abPath, Colors.Red));
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(bcPath, Colors.Red));
                }
                else if (m_displayType == DisplayType.DrawFullContourLine)
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(points));
                distance += abdistance + bcdistance;
            }
            else if (abdistance > acdistance && abdistance > bcdistance)
            {
                if (m_displayType == DisplayType.DrawContourLine)
                {
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(acPath, Colors.Red));
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(bcPath, Colors.Red));
                }
                else if (m_displayType == DisplayType.DrawFullContourLine)
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(points));
                distance += bcdistance + acdistance;
            }
            else if (bcdistance > acdistance && bcdistance > abdistance)
            {
                if (m_displayType == DisplayType.DrawContourLine)
                {
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(abPath, Colors.Red));
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(acPath, Colors.Red));
                }
                else if (m_displayType == DisplayType.DrawFullContourLine)
                    view1.Children.Add(MeshDrawingHelpers.DrawLineThroughPoints(points));
                distance += abdistance + acdistance;
            }
            return distance;
        }

        #endregion

        #region Mesh Info

        public void DisplayMeshInformation()
        {
            SetTitle("Number of points: " + _meshVertices);
            SetSubTitle("Number of triangles: " + _meshTriangleIndices / 3);
        }

        public void DisplaySurfaceAreaVolumeInformation()
        {
            var geo = GetMeshGeometry();
            if (geo != null)
            {
                SetTitle("Surface Area: " + MeshCalculations.SurfaceAreaOfMesh(geo));
                SetSubTitle("Volume: " + MeshCalculations.VolumeOfMesh(geo) / 1000 + " cc");
            }
        }

        #endregion

        private void view_mesh_click(object sender, RoutedEventArgs e)
        {
            MeshGeometry3D mesh = GetMeshGeometry(_mesh);
            if (mesh.Normals == null)
                return;
            MeshGeometry3D newMesh = mesh.Clone();
            _vectorCollection = newMesh.Normals;
            newMesh.Normals = null;
            var model = MeshCuttingHelpers.buildModel(Colors.White, newMesh);
            _mesh = model;
            ResetViewport();
            view1.Children.Add(_mesh);
        }

        private void view_mesh_normals_Click(object sender, RoutedEventArgs e)
        {
            if (_vectorCollection == null)
                return;
            MeshGeometry3D mesh = GetMeshGeometry(_mesh);
            MeshGeometry3D newMesh = mesh.Clone();
            newMesh.Normals = _vectorCollection;
            var model = MeshCuttingHelpers.buildModel(Colors.White, newMesh);
            _mesh = model;
            ResetViewport();
            view1.Children.Add(_mesh);
        }

        private void view_point_cloud_Click(object sender, RoutedEventArgs e)
        {
            MeshGeometry3D mesh = GetMeshGeometry(_mesh);

            PointsVisual3D points = new PointsVisual3D();
            points.Points = new List<Point3D>(mesh.Positions);
            points.Color = Colors.White;
            ResetViewport();
            view1.Children.Add(points);
        }

        private void SetCurrentlyViewing(string fileName)
        {
            Application.Current.MainWindow.Title = "Structured Light Scanning - " + Path.GetFileName(fileName);
        }


        private void begin_selecting_points_Click(object sender, RoutedEventArgs e)
        {
            _selectingPoints = !_selectingPoints;
            if (_selectingPoints)
                begin_selecting_points.Content = "Cancel Selecting Points";
            else
                begin_selecting_points.Content = "Begin Selecting Points";
        }

        private void clear_selected_points_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            ClearSelectedPoints();
        }

        private void reset_viewport_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            ResetView();
        }

        private void clear_viewport_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            ClearViewport();
        }

        private void display_information_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            DisplayMeshInformation();
        }

        private void display_volume_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            DisplaySurfaceAreaVolumeInformation();
        }

        private void measurementType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_initialized) return;
            var calcType = (CalcType)measurementType.SelectedIndex;
            SetCurrentCalculationType(calcType);
        }
    }
}
