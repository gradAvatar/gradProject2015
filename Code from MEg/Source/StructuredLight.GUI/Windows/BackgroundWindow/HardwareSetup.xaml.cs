﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for HardwareSetup.xaml
    /// </summary>
    public partial class HardwareSetup : UserControl, IBackgroundWindow
    {
        public Dictionary<string, object> SetupInformation = new Dictionary<string, object>();
        private CurrentHardwareWindow _currentWindow = CurrentHardwareWindow.HardwareSelection;


        public HardwareSetup()
        {
            ConfigurationManagement.LoadConfiguration();
            InitializeComponent();

            foreach (dynamic d in Controls)
                d.Initialize(this);
            GoTo(CurrentHardwareWindow.HardwareSelection);
        }

        public void SetTitleText(string text)
        {
            TitleBlock.Text = text;
        }

        public void DisableNextButton()
        {
            nextButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void EnableNextButton()
        {
            nextButton.Visibility = System.Windows.Visibility.Visible;
        }

        public void GoTo(CurrentHardwareWindow wind)
        {
            EnableNextButton();
            _currentWindow = wind;

            HideAllWindows();
            GetWindow(wind).Visibility = System.Windows.Visibility.Visible;
            GetWindow(wind).Display();
        }

        private void HideAllWindows()
        {
            foreach (var c in Controls)
                c.Visibility = System.Windows.Visibility.Collapsed;
        }

        private dynamic GetWindow(CurrentHardwareWindow window)
        {
            switch (window)
            {
                case CurrentHardwareWindow.HardwareSelection:
                    return hardwareSelection;
                case CurrentHardwareWindow.OneProjector:
                    return oneProjector;
                case CurrentHardwareWindow.TwoProjectors:
                    return twoProjectors;
                case CurrentHardwareWindow.TwoProjectorsColorSelection:
                    return twoProjectorsColorSelection;
                case CurrentHardwareWindow.TwoProjectorsProjectorSelection:
                    return twoProjectorsProjectorSelection;
                case CurrentHardwareWindow.ThreeProjectors:
                    return threeProjectors;
                case CurrentHardwareWindow.ThreeProjectorsColorSelection:
                    return threeProjectorsColorSelection;
                case CurrentHardwareWindow.ThreeProjectorsProjectorSelection:
                    return threeProjectorsProjectorSelection;
                case CurrentHardwareWindow.FourProjectors:
                    return fourProjectors;
                case CurrentHardwareWindow.FourProjectorsColorSelection:
                    return fourProjectorsColorSelection;
                case CurrentHardwareWindow.FourProjectorsProjectorSelection:
                    return fourProjectorsProjectorSelection;
                case CurrentHardwareWindow.ExtrinsicExplaination:
                    return extrinsicExplaination;
                case CurrentHardwareWindow.ExtrinsicCalibration:
                    return extrinsicCalibration;
                case CurrentHardwareWindow.DualExtrinsicExplaination:
                    return dualExtrinsicExplaination;
                case CurrentHardwareWindow.DualExtrinsicCalibration:
                    return dualExtrinsicCalibration;
                case CurrentHardwareWindow.OtherProjectors:
                    return otherCalibration;
                case CurrentHardwareWindow.OtherProjectorsColorSelection:
                    return otherCalibrationColorSelection;
                case CurrentHardwareWindow.OtherProjectorsProjectorSelection:
                    return otherCalibrationProjectorSelection;
                case CurrentHardwareWindow.Finished:
                    return finishedWindow;
                default:
                    return null;
            }
        }

        public List<UserControl> Controls
        {
            get { return new List<UserControl>() { hardwareSelection, oneProjector, 
                twoProjectors, twoProjectorsProjectorSelection, twoProjectorsColorSelection,
                threeProjectors, threeProjectorsProjectorSelection, threeProjectorsColorSelection, 
                otherCalibration, otherCalibrationColorSelection, otherCalibrationProjectorSelection, 
                fourProjectors, fourProjectorsColorSelection, fourProjectorsProjectorSelection,
                extrinsicExplaination, extrinsicCalibration,
                dualExtrinsicExplaination, dualExtrinsicCalibration, finishedWindow };
            }
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            GetWindow(_currentWindow).RequestedNext();
        }

        public void Display()
        {
            GoTo(CurrentHardwareWindow.HardwareSelection);
        }

        public void Hide()
        {
        }
    }

    public enum CurrentHardwareWindow
    {
        HardwareSelection,
        OneProjector,
        TwoProjectors,
        TwoProjectorsColorSelection,
        TwoProjectorsProjectorSelection,
        ThreeProjectors,
        ThreeProjectorsColorSelection,
        ThreeProjectorsProjectorSelection,
        FourProjectors,
        FourProjectorsColorSelection,
        FourProjectorsProjectorSelection,
        OtherProjectors,
        OtherProjectorsColorSelection,
        OtherProjectorsProjectorSelection,
        ExtrinsicExplaination,
        ExtrinsicCalibration,
        DualExtrinsicExplaination,
        DualExtrinsicCalibration,
        Finished
    }

    public enum HardwareSetupType
    {
        OneProjector,
        TwoProjectors,
        ThreeProjectors,
        FourProjectors,
        Other
    }
}
