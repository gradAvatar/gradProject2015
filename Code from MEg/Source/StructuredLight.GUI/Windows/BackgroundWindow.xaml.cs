﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for BackgroundWindow.xaml
    /// </summary>
    public partial class BackgroundWindow : Window
    {
        #region Static Variables

        public static BackgroundWindow Window { get; private set; }

        #endregion

        #region Constructor

        public BackgroundWindow()
        {
            Window = this;
            try
            {
                ConfigurationManagement.LoadConfiguration();
            }
            catch
            {
                MessageBox.Show("You do not have all of the dependencies installed, please check C:\\libs\\ folder to see what is missing");
                Environment.Exit(0);
            }

            InitializeComponent();
        }

        #endregion

        #region Splash Screen

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //LoadSplashScreen();
        }

        private void LoadSplashScreen()
        {
            LoadingAdorner.AddAdorner("Structured Light Scanner", BaseGrid, this);
            System.Timers.Timer t = new System.Timers.Timer(3000) { AutoReset = false };
            t.Elapsed += (s, e) => Dispatcher.Invoke(new Action(() => LoadingAdorner.RemoveAdorner(BaseGrid)));
            t.Start();
        }

        #endregion

        #region Task Bar Changes

        private UIElement _currentTaskBar;
        public void SetTaskBar(UIElement element)
        {
            if (_currentTaskBar != null)
                RemoveTaskBar(false);
            _currentTaskBar = element;
            BaseGrid.Children.Add(element);
            Grid.SetColumnSpan(windowController, 2);
            Grid.SetColumn(windowController, 2);
            Grid.SetRowSpan(_currentTaskBar, 5);
            Grid.SetColumn(_currentTaskBar, 1);

            Dispatcher.Invoke(new Action(() => windowController.ResizeWindow(true)), System.Windows.Threading.DispatcherPriority.Render);
        }

        public UIElement GetCurrentTaskBar()
        {
            return _currentTaskBar;
        }

        public void RemoveTaskBar(bool resize=true)
        {
            Grid.SetColumnSpan(windowController, 3);
            Grid.SetColumn(windowController, 1);
            BaseGrid.Children.Remove(_currentTaskBar);
            if (resize)
                Dispatcher.Invoke(new Action(() => windowController.ResizeWindow(false)), System.Windows.Threading.DispatcherPriority.Render);
            _currentTaskBar = null;
        }

        #endregion

        #region Notifications

        private NotificationBar _notification1, _notification2;
        /// <summary>
        /// Adds a non-blocking notification to the side of the user's screen
        /// (internal dispatching)
        /// </summary>
        /// <param name="notificationText"></param>
        /// <param name="isAsynchronous"></param>
        /// <returns></returns>
        public NotificationBar AddNotification(string notificationText, bool isAsynchronous = true)
        {
            if (Dispatcher.Thread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                return InnerAddNotification(notificationText, isAsynchronous);
            else
            {
                NotificationBar bar = null;
                Dispatcher.Invoke(new Action(() => bar = InnerAddNotification(notificationText, isAsynchronous)));
                return bar;
            }
        }

        private NotificationBar InnerAddNotification(string notificationText, bool isAsynchronous)
        {
            NotificationBar bar;

            if (_notification1 == null)
                bar = _notification1 = new NotificationBar(notificationText, isAsynchronous);
            else
            {
                if (_notification2 != null)
                    InnerRemoveNotification(_notification2);
                bar = _notification2 = new NotificationBar(notificationText, isAsynchronous);
            }
            Grid.SetColumn(bar, 3);
            if (bar == _notification1)
                Grid.SetRow(bar, 1);
            else
                Grid.SetRow(bar, 3);
            bar.Opacity = 0;
            BaseGrid.Children.Add(bar);
            const string windowsNotificationSound = 
@"C:\Windows\WinSxS\amd64_microsoft-windows-shell-sounds_31bf3856ad364e35_6.2.8400.0_none_fc5c807345eebdcf\Windows Notify System Generic.wav";
            if (System.IO.File.Exists(windowsNotificationSound))
                new System.Media.SoundPlayer(windowsNotificationSound).Play();
            else
                System.Media.SystemSounds.Exclamation.Play();
            //Fade in
            bar.BeginAnimation(UIElement.OpacityProperty,
                new System.Windows.Media.Animation.DoubleAnimation(1, new Duration(TimeSpan.FromMilliseconds(500))));
            return bar;
        }

        /// <summary>
        /// Replaces an already added notification with a new one
        /// (internal dispatching)
        /// </summary>
        /// <param name="old"></param>
        /// <param name="notificationText"></param>
        /// <param name="isAsynchronous"></param>
        /// <returns></returns>
        public NotificationBar ReplaceNotification(NotificationBar old, string notificationText, bool isAsynchronous = true)
        {
            NotificationBar notification = null;
            if (Dispatcher.Thread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                notification = InnerReplaceNotification(old, notificationText, isAsynchronous);
            else
                Dispatcher.Invoke(new Action(() => notification = InnerReplaceNotification(old, notificationText, isAsynchronous)));
            return notification;
        }

        private NotificationBar InnerReplaceNotification(NotificationBar old, string notificationText, bool isAsynchronous)
        {
            BaseGrid.Children.Remove(old);
            if (old == _notification1)
                _notification1 = null;
            else
                _notification2 = null;
            return InnerAddNotification(notificationText, isAsynchronous);
        }

        /// <summary>
        /// Removes a notification from the currently visible notification area
        /// (internal dispatching)
        /// </summary>
        /// <param name="notification"></param>
        public void RemoveNotification(NotificationBar notification)
        {
            if (Dispatcher.Thread.ManagedThreadId == Thread.CurrentThread.ManagedThreadId)
                InnerRemoveNotification(notification);
            else
                Dispatcher.Invoke(new Action(() => InnerRemoveNotification(notification)));
        }

        private void InnerRemoveNotification(NotificationBar notification)
        {
            var animation = new System.Windows.Media.Animation.DoubleAnimation(0, new Duration(TimeSpan.FromMilliseconds(250)));
            notification.BeginAnimation(UIElement.OpacityProperty, animation);
            animation.Completed += (o, e) => BaseGrid.Children.Remove(notification);
            if (notification == _notification1)
                _notification1 = null;
            else
                _notification2 = null;
        }

        #endregion

        #region Deconstructor

        private void Window_Closed(object sender, EventArgs e)
        {
            foreach (ManagedCamera cam in ManagedCamera.EnumerateCameras())
            {
                if (cam.IsStreamOpen)
                    cam.OpenStream().Stop();
            }
        }

        #endregion
    }
}
