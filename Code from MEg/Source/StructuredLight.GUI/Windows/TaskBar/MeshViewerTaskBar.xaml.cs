﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.GUI.MeshLab;
using System.Runtime.InteropServices;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for MeshViewerTaskBar.xaml
    /// </summary>
    public partial class MeshViewerTaskBar : UserControl
    {
        #region Declares

        private MeshViewer _meshViewerInstance;
        private bool _initialized = false;

        public MeshViewer MeshViewerInstance { get { return _meshViewerInstance; } }

        #endregion

        #region Constructor

        public static MeshViewerTaskBar SetAsTaskbar()
        {
            if (BackgroundWindow.Window.GetCurrentTaskBar() is MeshViewerTaskBar)
                BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.ViewAndMeasure);
            else
                BackgroundWindow.Window.SetTaskBar(new MeshViewerTaskBar());
            return BackgroundWindow.Window.GetCurrentTaskBar() as MeshViewerTaskBar;
        }

        public MeshViewerTaskBar()
        {
            _meshViewerInstance = BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.ViewAndMeasure);
            _meshViewerInstance.ChangeSelectingPoints += SelectingPointsChanged;
            InitializeComponent();
            _initialized = true;
        }

        #endregion

        #region UserControl Events

        private void Close_Pressed(object sender, MouseButtonEventArgs e)
        {
            BackgroundWindow.Window.RemoveTaskBar();
        }

        private void SelectingPointsChanged(bool selecting)
        {
            if (selecting)
                begin_selecting_points.Content = "Cancel Selecting Points";
            else
                begin_selecting_points.Content = "Begin Selecting Points";
        }

        private void display_information_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            _meshViewerInstance.DisplayMeshInformation();
        }

        private void display_volume_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            _meshViewerInstance.DisplaySurfaceAreaVolumeInformation();
        }

        private void measurementType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_initialized) return;
            var calcType = (CalcType)measurementType.SelectedIndex;
            _meshViewerInstance.SetCurrentCalculationType(calcType);
            if (calcType != CalcType.Contour)
            {
                displayTypeSelection.Visibility = System.Windows.Visibility.Hidden;
                ContourDistanceFormatText.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                displayTypeSelection.Visibility = System.Windows.Visibility.Visible;
                ContourDistanceFormatText.Visibility = System.Windows.Visibility.Visible;
            }
            /*if (calcType == CalcType.Cut)
                rebuild_hull.Visibility = System.Windows.Visibility.Visible;
            else
                rebuild_hull.Visibility = System.Windows.Visibility.Collapsed;*/
        }

        private void reset_viewport_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            _meshViewerInstance.ResetView();
        }

        private void clear_viewport_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            _meshViewerInstance.ClearViewport();
        }

        private void begin_selecting_points_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            _meshViewerInstance.BeginSelectingPoints();
        }

        private void clear_selected_points_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            _meshViewerInstance.ClearSelectedPoints();
        }

        private void displayTypeSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_initialized) return;
            _meshViewerInstance.SetCurrentDisplayType((DisplayType)displayTypeSelection.SelectedIndex);
        }

        internal void EnableStopCutMesh()
        {
            if (!_initialized) return;
            stop_cutting_mesh.Visibility = System.Windows.Visibility.Visible;
        }

        internal void DisableStopCutMesh()
        {
            if (!_initialized) return;
            stop_cutting_mesh.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void stop_cutting_mesh_Click(object sender, RoutedEventArgs e)
        {
            if (!_initialized) return;
            _meshViewerInstance.StopCuttingMesh();
        }

        private void rebuild_hull_Click(object sender, RoutedEventArgs e)
        {
        }

        #endregion
    }
}
