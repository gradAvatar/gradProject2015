﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.GUI.MeshLab;
using System.Runtime.InteropServices;
using StructuredLight.Managed;
using Path = System.IO.Path;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for ScanTaskBar.xaml
    /// </summary>
    public partial class ScanTaskBar : UserControl
    {
        #region Constructor

        private CameraPreview _cameraPreviewer;
        private Scanner _scanner;

        public ScanTaskBar()
        {
            InitializeComponent();
            progressText.Text = "";
            _cameraPreviewer = BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.CameraPreviewer);
        }

        #endregion

        #region Scan

        public void Scan()
        {
            List<ManagedCamera> camsToScanWith = new List<ManagedCamera>();
            List<ManagedProjector> projectorsToScanWith = new List<ManagedProjector>();
            int numCameras = int.Parse(Library.UserSettings["NumberofCameras"]);
            int numProjectors = int.Parse(Library.UserSettings["NumberofProjectors"]);

            List<ManagedCamera> allCameras = ManagedCamera.EnumerateCameras();
            for (int i = 0; i < numCameras; i++)
                camsToScanWith.Add(allCameras.Single((c) =>
                    c.SerialNumber == Library.UserSettings["Camera" + i]));

            List<ManagedProjector> allDisplays = ManagedProjector.EnumerateProjectors();
            for (int i = 0; i < numProjectors; i++)
                projectorsToScanWith.Add(allDisplays.Single((d) =>
                    d.DeviceId == Library.UserSettings["Projector" + i]));

            List<GrayCodeColor> allColors = new List<GrayCodeColor>();
            for (int i = 0; i < numCameras; i++)
            {
                string color = Library.UserSettings["Color" + i];
                switch (color)
                {
                    case "White":
                        allColors.Add(GrayCodeColors.White);
                        break;
                    case "Red":
                        allColors.Add(GrayCodeColors.Red);
                        break;
                    case "Green":
                        allColors.Add(GrayCodeColors.Green);
                        break;
                    case "Blue":
                        allColors.Add(GrayCodeColors.Blue);
                        break;
                    case "Yellow":
                        allColors.Add(GrayCodeColors.Yellow);
                        break;
                    default://Unknown color?
                        allColors.Add(GrayCodeColors.White);
                        break;
                }
            }
            _scanner = new Scanner(camsToScanWith, projectorsToScanWith, allColors);

            _scanner.NewScanPreview += (o, e) =>
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    progressText.Text =
                        string.Format("Currently captured {0} of {1} images",
                        e.ImageIndex, e.TotalImages);
                    if (e.TotalImages - e.ImageIndex == 1)
                        BackgroundWindow.Window.AddNotification("Building mesh, please wait, this may take a few minutes...", true);
                }));
                Dispatcher.Invoke(new Action(() =>
                {
                    BitmapPreviewer.LoadBitmapImage(e.ScanPreviews[0], _cameraPreviewer.cameraPreview, true);
                }));
            };
            _scanner.OutputFileName = GetMeshFilePath();
            _scanner.ScanComplete += (o, e) =>
            {
                BackgroundWindow.Window.AddNotification("Creating clouds from data...");
                //Start meshlab to view the point cloud
                if (Library.UserSettings["OpenPointCloudInMeshLab"] == "1")
                    MeshlabHelper.StartMeshlab("all_aligned_" + _scanner.OutputFileName);
                if (Library.UserSettings["DisableInternalPointCloudViewer"] == "0")
                {
                    //Load the mesh into the internal viewer
                    MeshViewerTaskBar taskBar = null;
                    Dispatcher.Invoke(new Action(() => taskBar = MeshViewerTaskBar.SetAsTaskbar()));
                    taskBar.MeshViewerInstance.
                                LoadMesh(_scanner.OutputFileName, Library.UserSettings["MeshAfterScanning"] == "1");
                }
                else if (Library.UserSettings["OpenMeshInMeshLab"] == "1")
                {
                    //Otherwise, open it in meshlab after converting it
                    MeshlabHelper.StartMeshlab(_scanner.OutputFileName);
                }
            };
            _scanner.Scan();
        }

        private string GetMeshFilePath()
        {
            string timeStamp = " " + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" +
                DateTime.Now.Year + " " + (DateTime.Now.Hour > 12 ? DateTime.Now.Hour - 12 :
                DateTime.Now.Hour) + "." + DateTime.Now.Minute +
                " " + (DateTime.Now.Hour > 12 ? "PM" : "AM");
            return outputFileName.Text + timeStamp + ".ply";
        }

        #endregion

        #region UserControl Events

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            Scan();
        }

        private void Close_Pressed(object sender, MouseButtonEventArgs e)
        {
            BackgroundWindow.Window.RemoveTaskBar();
            if(_scanner != null)
                _scanner.Cancel();
        }

        #endregion
    }
}
