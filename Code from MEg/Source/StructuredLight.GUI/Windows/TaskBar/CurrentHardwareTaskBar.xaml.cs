﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.GUI.MeshLab;
using System.Runtime.InteropServices;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for CurrentHardwareTaskBar.xaml
    /// </summary>
    public partial class CurrentHardwareTaskBar : UserControl
    {
        #region Constructor

        public CurrentHardwareTaskBar()
        {
            InitializeComponent();
            cameraSelection.Selected = () =>
                ((CameraPreview)BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.CameraPreviewer)).
                PreviewCamera(cameraSelection.SelectedCamera);
            cameraSelection.Display();
            monitorSelection.Display();
        }

        #endregion

        #region UserControl Events

        private void Close_Pressed(object sender, MouseButtonEventArgs e)
        {
            BackgroundWindow.Window.RemoveTaskBar();
        }

        #endregion
    }
}
