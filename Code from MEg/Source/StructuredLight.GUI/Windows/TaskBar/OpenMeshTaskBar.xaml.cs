﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.GUI.MeshLab;
using System.Runtime.InteropServices;
using StructuredLight.Managed;
using Path = System.IO.Path;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for OpenMeshTaskBar.xaml
    /// </summary>
    public partial class OpenMeshTaskBar : UserControl
    {
        #region Declares

        public MeshViewer MeshViewerInstance { get; private set; }

        #endregion

        #region Constructor

        public static OpenMeshTaskBar SetAsTaskbar()
        {
            if (BackgroundWindow.Window.GetCurrentTaskBar() is OpenMeshTaskBar)
                BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.ViewAndMeasure);
            else
                BackgroundWindow.Window.SetTaskBar(new OpenMeshTaskBar());
            return BackgroundWindow.Window.GetCurrentTaskBar() as OpenMeshTaskBar;
        }

        public OpenMeshTaskBar()
        {
            MeshViewerInstance = BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.ViewAndMeasure);
            InitializeComponent();

            UpdateRecentlyOpenedMeshFiles();
        }

        private void UpdateRecentlyOpenedMeshFiles()
        {
            if (Library.UserSettings["RecentlyOpenedMeshFilesCount"] == "")
                return;
            int count = int.Parse(Library.UserSettings["RecentlyOpenedMeshFilesCount"]);
            for (int i = 1; i <= 5; i++)
            {
                if (i <= count)
                {
                    string filePath = Library.UserSettings["RecentlyOpenedMeshFiles" + i];
                    switch (i)
                    {
                        case 1:
                            RecentlyOpened1.Visibility = System.Windows.Visibility.Visible;
                            RecentlyOpened1.TitleText = Path.GetFileNameWithoutExtension(filePath);
                            RecentlyOpened1.ItemClicked += (o, e) => LoadMeshFromFile(filePath);
                            break;
                        case 2:
                            RecentlyOpened2.Visibility = System.Windows.Visibility.Visible;
                            RecentlyOpened2.TitleText = Path.GetFileNameWithoutExtension(filePath);
                            RecentlyOpened2.ItemClicked += (o, e) => LoadMeshFromFile(filePath);
                            break;
                        case 3:
                            RecentlyOpened3.Visibility = System.Windows.Visibility.Visible;
                            RecentlyOpened3.TitleText = Path.GetFileNameWithoutExtension(filePath);
                            RecentlyOpened3.ItemClicked += (o, e) => LoadMeshFromFile(filePath);
                            break;
                        case 4:
                            RecentlyOpened4.Visibility = System.Windows.Visibility.Visible;
                            RecentlyOpened4.TitleText = Path.GetFileNameWithoutExtension(filePath);
                            RecentlyOpened4.ItemClicked += (o, e) => LoadMeshFromFile(filePath);
                            break;
                        case 5:
                            RecentlyOpened5.Visibility = System.Windows.Visibility.Visible;
                            RecentlyOpened5.TitleText = Path.GetFileNameWithoutExtension(filePath);
                            RecentlyOpened5.ItemClicked += (o, e) => LoadMeshFromFile(filePath);
                                
                            break;
                    }
                }
            }
        }

        private void LoadMeshFromFile(string meshFile)
        {
            OpenMeshTaskBar.SetAsTaskbar().MeshViewerInstance.LoadMesh(meshFile);
            BackgroundWindow.Window.RemoveTaskBar();
        }

        public static void AddRecentOpenedMesh(string mesh)
        {
            int count = 1;
            if (Library.UserSettings["RecentlyOpenedMeshFilesCount"] != "")
                count = int.Parse(Library.UserSettings["RecentlyOpenedMeshFilesCount"])+1;
            if (count > 5)
            {
                count = 5;
                List<string> files = new List<string>();
                for (int i = 1; i <= count; i++)
                    files.Add(Library.UserSettings["RecentlyOpenedMeshFiles" + i]);

                for (int i = 1; i < count; i++)
                {
                    Library.UserSettings["RecentlyOpenedMeshFiles" + (i+1)] = files[i-1];
                }
                Library.UserSettings["RecentlyOpenedMeshFiles1"] = mesh;
            }
            else
            {
                Library.UserSettings["RecentlyOpenedMeshFilesCount"] = count.ToString();
                Library.UserSettings["RecentlyOpenedMeshFiles" + count] = mesh;
            }
            Library.SaveConfiguration();
        }

        #endregion

        #region UserControl Events

        private void Close_Pressed(object sender, MouseButtonEventArgs e)
        {
            BackgroundWindow.Window.RemoveTaskBar();
        }

        private void OpenNewFileMesh_Click(object sender, RoutedEventArgs e)
        {
            string file;
            if (Utils.GetFile("All Mesh Files(*.3ds;*.lwo;*.ply;*.obj;*.objz;*.stl;*.off;*.ply;*.dae;*.wrl)|*.3ds;*.lwo;*.ply;*.obj;*.objz;*.stl;*.off;*.ply;*.dae;*.wrl|Mesh Files(*.3ds;*.lwo;*.obj;*.objz;*.stl;*.off;*.wrl)|*.3ds;*.lwo;*.obj;*.objz;*.stl;*.off;*.wrl|Conversion Required Mesh Files(*.dae)|*.dae", out file))
                LoadMeshFromFile(file);
        }

        #endregion
    }
}
