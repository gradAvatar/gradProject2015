﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for NotificationBar.xaml
    /// </summary>
    public partial class NotificationBar : UserControl
    {
        #region Declares

        public NoParam Clicked = null;

        #endregion

        #region Constructor

        public NotificationBar(string notificationText, bool isAsynchronous)
        {
            InitializeComponent();
            NotificationText.Text = notificationText;
            if (!isAsynchronous)
                IsAsyncronousText.Visibility = System.Windows.Visibility.Collapsed;
            CloseButton.Opacity = 0;
        }

        #endregion

        #region UserControl

        private void NotificationBar_MouseEnter(object sender, MouseEventArgs e)
        {
            CloseButton.BeginAnimation(UIElement.OpacityProperty,
                new System.Windows.Media.Animation.DoubleAnimation(1, new Duration(TimeSpan.FromMilliseconds(250))));
        }

        private void NotificationBar_MouseLeave(object sender, MouseEventArgs e)
        {
            CloseButton.BeginAnimation(UIElement.OpacityProperty,
                new System.Windows.Media.Animation.DoubleAnimation(0, new Duration(TimeSpan.FromMilliseconds(250))));
        }

        private void CloseButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        public void Close()
        {
            BackgroundWindow.Window.RemoveNotification(this);
        }

        #endregion

        private void NotificationBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (Clicked != null)
                Clicked();

            Close();
        }
    }
}
