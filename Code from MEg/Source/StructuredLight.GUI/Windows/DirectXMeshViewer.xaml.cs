﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SlimDX.Direct3D10;

namespace StructuredLight.GUI.Windows
{
    /// <summary>
    /// Interaction logic for DirectXMeshViewer.xaml
    /// </summary>
    public partial class DirectXMeshViewer : UserControl
    {
        public DirectXMeshViewer()
        {
            InitializeComponent();
        }
    }
}
