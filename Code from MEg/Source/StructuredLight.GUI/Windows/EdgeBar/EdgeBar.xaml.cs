﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for EdgeBar.xaml
    /// </summary>
    public partial class EdgeBar : UserControl
    {
        #region Constructor

        public EdgeBar()
        {
            InitializeComponent();
            dock.Children.Add(BuildEdgeBarItem("View Hardware", () =>
                BackgroundWindow.Window.SetTaskBar(new CurrentHardwareTaskBar())));
            dock.Children.Add(BuildEdgeBarItem("Hardware Setup", () => 
                BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.HardwareSetup)));
            dock.Children.Add(BuildEdgeBarItem("Take a Scan", () =>
                BackgroundWindow.Window.SetTaskBar(new ScanTaskBar())));
            dock.Children.Add(BuildEdgeBarItem("Settings", () =>
                BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.Settings)));
            dock.Children.Add(BuildEdgeBarItem("Open a Mesh", () =>
                BackgroundWindow.Window.SetTaskBar(new OpenMeshTaskBar())));
            dock.Children.Add(BuildEdgeBarItem("Mesh Viewer", () =>
                {
                    BackgroundWindow.Window.RemoveTaskBar();
                    BackgroundWindowController.WindowController.SetVisibleWindow(WindowItem.ViewAndMeasure);
                }));
        }

        #endregion

        #region Build new EdgeBarItem

        private EdgeBarItem BuildEdgeBarItem(string name, NoParam onClicked)
        {
            var itm = new EdgeBarItem(name, onClicked);
            itm.MouseEnter += itm_MouseEnter;
            itm.MouseLeave += itm_MouseLeave;
            itm.MouseDown += itm_MouseDown;
            return itm;
        }

        #endregion

        #region EdgeBarItem events

        void itm_MouseDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var child in dock.Children)
                (child as EdgeBarItem).Deselect();

            EdgeBarItem itm = (EdgeBarItem)sender;
            itm.SetSelected();
        }

        void itm_MouseLeave(object sender, MouseEventArgs e)
        {
            EdgeBarItem itm = (EdgeBarItem)sender;
            itm.ResetColor();
        }

        void itm_MouseEnter(object sender, MouseEventArgs e)
        {
            EdgeBarItem itm = (EdgeBarItem)sender;
            itm.SetColor((Color)ColorConverter.ConvertFromString("#FF47A3FF"));
        }

        #endregion
    }
}
