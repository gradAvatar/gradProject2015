﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for EdgeBarItem.xaml
    /// </summary>
    public partial class EdgeBarItem : UserControl
    {
        #region Declares

        private NoParam ClickedDelegate;
        public bool Selected { get; set; }

        #endregion

        #region Constructor

        public EdgeBarItem(string itemName, NoParam onClicked)
        {
            InitializeComponent();
            TextItem.Text = itemName;
            ClickedDelegate = onClicked;
            DockPanel.SetDock(this, Dock.Top);
        }

        #endregion

        #region Set Color

        public void SetColor(Color color)
        {
            if (Selected)
                return;
            TextItem.Foreground = new SolidColorBrush(color);
            ColoredRectangle.Fill = new SolidColorBrush(color);
        }

        public void ResetColor()
        {
            if (Selected)
                return;
            TextItem.Foreground = new SolidColorBrush(Colors.Black);
            ColoredRectangle.Fill = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFAAAAAA"));
        }

        #endregion

        public void SetSelected()
        {
            SetColor((Color)ColorConverter.ConvertFromString("#FF3783FF"));
            Selected = true;
            if (ClickedDelegate != null)
                ClickedDelegate();
        }

        public void Deselect()
        {
            if (Selected)
            {
                Selected = false;
                ResetColor();
            }
        }
    }
}
