﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for TextInput.xaml
    /// </summary>
    public partial class TextInput : UserControl
    {
        public static readonly DependencyProperty LabelProperty = DependencyProperty.Register(
            "TextLabel", typeof(string), typeof(TextInput), new UIPropertyMetadata("", SetupChanged));
        public static readonly DependencyProperty TextContentProperty = DependencyProperty.Register(
            "TextContent", typeof(string), typeof(TextInput), new UIPropertyMetadata("", SetupChanged));
        public static readonly RoutedEvent TextChangedEvent = EventManager.RegisterRoutedEvent(
            "TextChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TextInput));
        public string TextLabel
        {
            get
            {
                return (string)this.GetValue(LabelProperty);
            }

            set
            {
                this.SetValue(LabelProperty, value);
            }
        }

        public string TextContent
        {
            get
            {
                return (string)this.GetValue(TextContentProperty);
            }

            set
            {
                this.SetValue(TextContentProperty, value);
            }
        }

        public event RoutedEventHandler TextChanged
        {
            add { AddHandler(TextChangedEvent, value); }
            remove { RemoveHandler(TextChangedEvent, value); }
        }

        public TextInput()
        {
            InitializeComponent();
        }

        protected static void SetupChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == LabelProperty)
                (d as TextInput).Label.Text = e.NewValue.ToString();
            if (e.Property == TextContentProperty)
                (d as TextInput).TextBoxContent.Text = e.NewValue.ToString();
        }

        private void Content_TextChanged(object sender, TextChangedEventArgs e)
        { 
            RaiseEvent(new RoutedEventArgs(TextChangedEvent));
        }
    }
}
