﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for CameraSelection.xaml
    /// </summary>
    public partial class CameraSelection : UserControl
    {
        #region Declares

        public ManagedCamera SelectedCamera { get { return Cameras.SelectedItem as ManagedCamera; } }
        public NoParam Selected = null;

        #endregion

        #region Constructor

        public CameraSelection()
        {
            InitializeComponent();
            #region Factory
            FrameworkElementFactory tb = new FrameworkElementFactory(typeof(TextBlock));
            tb.SetBinding(TextBlock.TextProperty, new Binding("FriendlyName"));
            tb.SetValue(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis);
            tb.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Left);
            tb.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Left);
            tb.SetValue(TextBlock.MarginProperty, new Thickness(15, 0, 0, 0));
            DataTemplate dt = new DataTemplate();
            dt.VisualTree = tb;
            Cameras.ItemTemplate = dt;
            #endregion
        }

        #endregion

        #region Display Method

        public void Display()
        {
            UpdateCameras();
        }

        #endregion

        #region Camera Panel Selection

        private DateTime _lastSelectedCamerasChangedTimestamp = new DateTime();
        private bool _camerasPanelIsLeftClick = false;
        private bool _disableSelection = false;

        private void Cameras_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_camerasPanelIsLeftClick)
                return;
            if (_disableSelection)
                return;
            if(Selected != null)
                Selected();
            _lastSelectedCamerasChangedTimestamp = DateTime.Now;
        }

        private void Cameras_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _camerasPanelIsLeftClick = e.ChangedButton == MouseButton.Left;
        }

        private void Cameras_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if ((DateTime.Now - _lastSelectedCamerasChangedTimestamp).TotalMilliseconds > 100 && e.ChangedButton == MouseButton.Left)
            {
                int index = Cameras.SelectedIndex;
                Cameras.SelectedIndex = -1;
                Cameras.SelectedIndex = index;
            }
        }

        #endregion

        #region Camera List Updating

        public void UpdateCameras(List<ManagedCamera> cams=null)
        {
            Cameras.ItemsSource = cams == null ? ManagedCamera.EnumerateCameras() : cams;
            foreach (ManagedCamera camera in Cameras.ItemsSource.Cast<ManagedCamera>())
            {
                camera.FriendlyName = string.IsNullOrEmpty(camera.FriendlyName) ? camera.ModelName : camera.FriendlyName;
            }
        }

        #endregion
    }
}
