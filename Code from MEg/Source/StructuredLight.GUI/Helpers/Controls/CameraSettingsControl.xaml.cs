﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for CameraSettingsControl.xaml
    /// </summary>
    public partial class CameraSettingsControl : UserControl
    {
        #region Declares

        private bool _initialized = false;
        private const int _gridHeight = 26;

        public ManagedCamera Camera { get; private set; }

        #endregion

        #region Constructor

        public CameraSettingsControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Change Camera / Initialize Properties

        public void ChangeCamera(ManagedCamera cam)
        {
            _initialized = false;
            Camera = cam;
            cameraName.Text = Utils.GetNameOfCamera(cam);
            UpdateProperties(cam);
            _initialized = true;
        }

        private void UpdateProperties(ManagedCamera c)
        {
            CameraSettingFlags flags;
            double value = Camera.GetCameraSetting(CameraSetting.Brightness, out flags);
            CameraSettingRange range = Camera.GetCameraSettingRange(CameraSetting.Brightness);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(brightnessSlider)].Height = new GridLength(_gridHeight);
                brightnessSlider.Value = value;
                brightnessSlider.Interval = (int)range.Step;
                brightnessSlider.Minimum = range.Minimum;
                brightnessSlider.Maximum = range.Maximum;
            }
            else
            {
                brightnessSlider.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(brightnessSlider)].Height = new GridLength(0);
            }

            value = Camera.GetCameraSetting(CameraSetting.Contrast, out flags);
            range = Camera.GetCameraSettingRange(CameraSetting.Contrast);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(contrastSlider)].Height = new GridLength(_gridHeight);
                contrastSlider.Value = value;
                contrastSlider.Interval = (int)range.Step;
                contrastSlider.Minimum = range.Minimum;
                contrastSlider.Maximum = range.Maximum;
            }
            else
            {
                contrastSlider.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(contrastSlider)].Height = new GridLength(0);
            }

            value = Camera.GetCameraSetting(CameraSetting.Gain, out flags);
            range = Camera.GetCameraSettingRange(CameraSetting.Gain);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(gainSlider)].Height = new GridLength(_gridHeight);
                gainSlider.Value = value;
                gainAuto.IsChecked = flags == CameraSettingFlags.Auto;
                gainSlider.Interval = (int)range.Step;
                gainSlider.Minimum = range.Minimum;
                gainSlider.Maximum = range.Maximum;
            }
            else
            {
                gainSlider.IsEnabled = false;
                gainAuto.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(gainSlider)].Height = new GridLength(0);
            }

            value = Camera.GetCameraSetting(CameraSetting.Saturation, out flags);
            range = Camera.GetCameraSettingRange(CameraSetting.Saturation);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(saturationSlider)].Height = new GridLength(_gridHeight);
                saturationSlider.Value = value;
                saturationAuto.IsChecked = flags == CameraSettingFlags.Auto;
                saturationSlider.Interval = (int)range.Step;
                saturationSlider.Minimum = range.Minimum;
                saturationSlider.Maximum = range.Maximum;
            }
            else
            {
                saturationAuto.IsEnabled = false;
                saturationSlider.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(saturationSlider)].Height = new GridLength(0);
            }

            value = Camera.GetCameraSetting(CameraSetting.Gamma, out flags);
            range = Camera.GetCameraSettingRange(CameraSetting.Gamma);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(gammaSlider)].Height = new GridLength(_gridHeight);
                gammaSlider.Value = value;
                gammaAuto.IsChecked = flags == CameraSettingFlags.Auto;
                gammaSlider.Interval = (int)range.Step;
                gammaSlider.Minimum = range.Minimum;
                gammaSlider.Maximum = range.Maximum;
            }
            else
            {
                gammaAuto.IsEnabled = false;
                gammaSlider.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(gammaSlider)].Height = new GridLength(0);
            }

            value = Camera.GetCameraSetting(CameraSetting.Sharpness, out flags);
            range = Camera.GetCameraSettingRange(CameraSetting.Sharpness);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(sharpnessSlider)].Height = new GridLength(_gridHeight);
                sharpnessSlider.Value = value;
                sharpnessAuto.IsChecked = flags == CameraSettingFlags.Auto;
                sharpnessSlider.Interval = (int)range.Step;
                sharpnessSlider.Minimum = range.Minimum;
                sharpnessSlider.Maximum = range.Maximum;
            }
            else
            {
                sharpnessAuto.IsEnabled = false;
                sharpnessSlider.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(sharpnessSlider)].Height = new GridLength(0);
            }

            value = Camera.GetCameraSetting(CameraSetting.WhiteBalance, out flags);
            range = Camera.GetCameraSettingRange(CameraSetting.WhiteBalance);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(whiteBalSlider)].Height = new GridLength(_gridHeight);
                whiteBalSlider.Value = value;
                whiteBalAuto.IsChecked = flags == CameraSettingFlags.Auto;
                whiteBalSlider.Interval = (int)range.Step;
                whiteBalSlider.Minimum = range.Minimum;
                whiteBalSlider.Maximum = range.Maximum;
            }
            else
            {
                whiteBalAuto.IsEnabled = false;
                whiteBalSlider.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(whiteBalSlider)].Height = new GridLength(0);
            }

            value = Camera.GetCameraSetting(CameraSetting.Exposure, out flags);
            range = Camera.GetCameraSettingRange(CameraSetting.Exposure);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(exposureSlider)].Height = new GridLength(_gridHeight);
                exposureSlider.Value = value;
                exposureAuto.IsChecked = flags == CameraSettingFlags.Auto;
                exposureSlider.Interval = (int)range.Step;
                exposureSlider.Minimum = range.Minimum;
                exposureSlider.Maximum = range.Maximum;
            }
            else
            {
                exposureAuto.IsEnabled = false;
                exposureSlider.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(exposureSlider)].Height = new GridLength(0);
            }

            value = Camera.GetCameraSetting(CameraSetting.Focus, out flags);
            range = Camera.GetCameraSettingRange(CameraSetting.Focus);
            if (range.IsSupported)
            {
                backgroundGrid.RowDefinitions[Grid.GetRow(focusSlider)].Height = new GridLength(_gridHeight);
                focusSlider.Value = value;
                focusAuto.IsChecked = flags == CameraSettingFlags.Auto;
                focusSlider.Interval = (int)range.Step;
                focusSlider.Minimum = range.Minimum;
                focusSlider.Maximum = range.Maximum;
            }
            else
            {
                focusAuto.IsEnabled = false;
                focusSlider.IsEnabled = false;
                backgroundGrid.RowDefinitions[Grid.GetRow(focusSlider)].Height = new GridLength(0);
            }

            rotation.Items.Clear();

            rotation.Items.Add("0");
            rotation.Items.Add("90");
            rotation.Items.Add("180");
            rotation.Items.Add("270");
            rotation.SelectedIndex = 3;

            isMirrored.IsChecked = true;
        }

        #endregion

        #region UserControl Events

        private void sharpnessSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.Sharpness, sharpnessSlider.Value, CameraSettingFlags.Manual);
        }

        private void gammaSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.Gamma, gammaSlider.Value, CameraSettingFlags.Manual);
        }

        private void saturationSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.Saturation, saturationSlider.Value, CameraSettingFlags.Manual);
        }

        private void gainSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.Gain, gainSlider.Value, CameraSettingFlags.Manual);
        }

        private void brightnessSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.Brightness, brightnessSlider.Value, CameraSettingFlags.Manual);
        }

        private void contrastSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.Contrast, contrastSlider.Value, CameraSettingFlags.Manual);
        }

        private void whiteBalSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.WhiteBalance, whiteBalSlider.Value, CameraSettingFlags.Manual);
        }

        private void focusSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.Focus, focusSlider.Value, CameraSettingFlags.Manual);
        }

        private void exposureSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (!_initialized)
                return;
            Camera.SetCameraSetting(CameraSetting.Exposure, exposureSlider.Value, CameraSettingFlags.Manual);
        }

        private void whiteBalAuto_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            whiteBalSlider.IsEnabled = !(whiteBalAuto.IsChecked == true);
            Camera.SetCameraSetting(CameraSetting.WhiteBalance, whiteBalSlider.Value, (whiteBalAuto.IsChecked == true) ? CameraSettingFlags.Auto : CameraSettingFlags.Manual);
            if (whiteBalSlider.IsEnabled)
                whiteBalSlider.Value = Camera.GetCameraSetting(CameraSetting.WhiteBalance);
        }

        private void focusAuto_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            focusSlider.IsEnabled = !(focusAuto.IsChecked == true);
            Camera.SetCameraSetting(CameraSetting.Focus, focusSlider.Value, (focusAuto.IsChecked == true) ? CameraSettingFlags.Auto : CameraSettingFlags.Manual);
            if (focusSlider.IsEnabled)
                focusSlider.Value = Camera.GetCameraSetting(CameraSetting.Focus);
        }

        private void exposureAuto_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            exposureSlider.IsEnabled = !(exposureAuto.IsChecked == true);
            Camera.SetCameraSetting(CameraSetting.Exposure, focusSlider.Value, (exposureAuto.IsChecked == true) ? CameraSettingFlags.Auto : CameraSettingFlags.Manual);
            if (exposureSlider.IsEnabled)
                exposureSlider.Value = Camera.GetCameraSetting(CameraSetting.Exposure);
        }

        private void sharpnessAuto_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            sharpnessSlider.IsEnabled = !(sharpnessAuto.IsChecked == true);
            Camera.SetCameraSetting(CameraSetting.Sharpness, sharpnessSlider.Value, (sharpnessAuto.IsChecked == true) ? CameraSettingFlags.Auto : CameraSettingFlags.Manual);
            if (sharpnessSlider.IsEnabled)
                sharpnessSlider.Value = Camera.GetCameraSetting(CameraSetting.Sharpness);
        }

        private void gammaAuto_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            gammaSlider.IsEnabled = !(gammaAuto.IsChecked == true);
            Camera.SetCameraSetting(CameraSetting.Gamma, gammaSlider.Value, (gammaAuto.IsChecked == true) ? CameraSettingFlags.Auto : CameraSettingFlags.Manual);
            if (gammaSlider.IsEnabled)
                gammaSlider.Value = Camera.GetCameraSetting(CameraSetting.Gamma);
        }

        private void saturationAuto_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            saturationSlider.IsEnabled = !(saturationAuto.IsChecked == true);
            Camera.SetCameraSetting(CameraSetting.Saturation, saturationSlider.Value, (saturationAuto.IsChecked == true) ? CameraSettingFlags.Auto : CameraSettingFlags.Manual);
            if (saturationSlider.IsEnabled)
                saturationSlider.Value = Camera.GetCameraSetting(CameraSetting.Saturation);
        }

        private void gainAuto_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            gainSlider.IsEnabled = !(gainAuto.IsChecked == true);
            Camera.SetCameraSetting(CameraSetting.Gain, gainSlider.Value, (gainAuto.IsChecked == true) ? CameraSettingFlags.Auto : CameraSettingFlags.Manual);
            if (gainSlider.IsEnabled)
                gainSlider.Value = Camera.GetCameraSetting(CameraSetting.Gain);
        }

        private void isMirrored_Checked(object sender, RoutedEventArgs e)
        {
            if (!_initialized)
                return;
            rotation_SelectionChanged(null, null);
        }

        private void rotation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_initialized || rotation.SelectedItem == null)
                return;
            switch (rotation.SelectedItem.ToString())
            {
                case "0":
                    if ((bool)isMirrored.IsChecked)
                        BitmapPreviewer.SetCameraRotation(RotateFlipType.RotateNoneFlipX);
                    else
                        BitmapPreviewer.SetCameraRotation(RotateFlipType.RotateNoneFlipNone);
                    break;
                case "90":
                    if ((bool)isMirrored.IsChecked)
                        BitmapPreviewer.SetCameraRotation(RotateFlipType.Rotate90FlipX);
                    else
                        BitmapPreviewer.SetCameraRotation(RotateFlipType.Rotate90FlipNone);
                    break;
                case "180":
                    if ((bool)isMirrored.IsChecked)
                        BitmapPreviewer.SetCameraRotation(RotateFlipType.Rotate180FlipX);
                    else
                        BitmapPreviewer.SetCameraRotation(RotateFlipType.Rotate180FlipNone);
                    break;
                case "270":
                    if ((bool)isMirrored.IsChecked)
                        BitmapPreviewer.SetCameraRotation(RotateFlipType.Rotate270FlipX);
                    else
                        BitmapPreviewer.SetCameraRotation(RotateFlipType.Rotate270FlipNone);
                    break;
            }
        }

        private void cameraName_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (cameraName.Text.Length == 0)
                return;
            char lastChar = cameraName.Text[cameraName.Text.Length - 1];
            if (lastChar == '\n')
            {
                cameraName.Text = cameraName.Text.Substring(0, cameraName.Text.Length - 2);
                FrameworkElement parent = (FrameworkElement)cameraName.Parent;
                while (parent != null && parent is IInputElement && !((IInputElement)parent).Focusable)
                {
                    parent = (FrameworkElement)parent.Parent;
                }

                DependencyObject scope = FocusManager.GetFocusScope(cameraName);
                FocusManager.SetFocusedElement(scope, parent as IInputElement);
            }
        }

        #endregion
    }
}
