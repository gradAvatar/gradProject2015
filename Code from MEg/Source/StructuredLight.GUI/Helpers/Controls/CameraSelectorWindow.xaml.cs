﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for CameraSelectorWindow.xaml
    /// </summary>
    public partial class CameraSelectorWindow : Window
    {
        #region Constructor

        public CameraSelectorWindow()
        {
            InitializeComponent();
            cameraSelection.Display();
        }

        #endregion 

        #region Window Events

        private void selectButton_Click(object sender, RoutedEventArgs e)
        {
            if (cameraSelection.SelectedCamera != null)
                this.Close();
        }

        #endregion
    }
}
