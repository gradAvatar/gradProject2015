﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for MeshTaskBarItem.xaml
    /// </summary>
    public partial class MeshTaskBarItem : UserControl
    {
        public static readonly RoutedEvent ItemClickedEvent = EventManager.RegisterRoutedEvent(
            "ItemClicked", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(MeshTaskBarItem));
        public static readonly DependencyProperty TitleTextContentProperty = DependencyProperty.Register(
            "TitleText", typeof(string), typeof(MeshTaskBarItem), new UIPropertyMetadata("", SetupChanged));
        public static readonly DependencyProperty SubTitleTextContentProperty = DependencyProperty.Register(
            "SubTitleText", typeof(string), typeof(MeshTaskBarItem), new UIPropertyMetadata("", SetupChanged));
        public static readonly DependencyProperty SubTitleVisibilityProperty = DependencyProperty.Register(
            "SubTitleVisibility", typeof(bool), typeof(MeshTaskBarItem), new UIPropertyMetadata(false, SetupChanged));

        public string TitleText
        {
            get
            {
                return (string)this.GetValue(TitleTextContentProperty);
            }
            set
            {
                this.SetValue(TitleTextContentProperty, value);
            }
        }

        public string SubTitleText
        {
            get
            {
                return (string)this.GetValue(SubTitleTextContentProperty);
            }
            set
            {
                this.SetValue(SubTitleTextContentProperty, value);
            }
        }

        public bool SubTitleVisibility
        {
            get
            {
                return (bool)this.GetValue(SubTitleVisibilityProperty);
            }
            set
            {
                this.SetValue(SubTitleVisibilityProperty, value);
            }
        }

        public event RoutedEventHandler ItemClicked
        {
            add { AddHandler(ItemClickedEvent, value); }
            remove { RemoveHandler(ItemClickedEvent, value); }
        }

        public MeshTaskBarItem()
        {
            InitializeComponent();
            border.Background = new SolidColorBrush(Colors.LightBlue);
            border.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF47A3FF"));
            border.Background.Opacity = 0;
            border.BorderBrush.Opacity = 0;
        }

        private void Border_MouseEnter(object sender, MouseEventArgs e)
        {
            border.Background = new SolidColorBrush(Colors.LightBlue);
            border.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF47A3FF"));
            DoubleAnimation animation = new DoubleAnimation(1, new Duration(TimeSpan.FromSeconds(0.25)));
            border.Background.BeginAnimation(SolidColorBrush.OpacityProperty, animation);
            animation = new DoubleAnimation(1, new Duration(TimeSpan.FromSeconds(0.25)));
            border.BorderBrush.BeginAnimation(SolidColorBrush.OpacityProperty, animation);

            if (!SubTitleVisibility)
                SubTitleVisibility = true;
        }

        private void Border_MouseLeave(object sender, MouseEventArgs e)
        {
            DoubleAnimation animation = new DoubleAnimation(0, new Duration(TimeSpan.FromSeconds(0.25)));
            border.Background.BeginAnimation(SolidColorBrush.OpacityProperty, animation);
            animation = new DoubleAnimation(0, new Duration(TimeSpan.FromSeconds(0.25)));
            border.BorderBrush.BeginAnimation(SolidColorBrush.OpacityProperty, animation);

            if (SubTitleVisibility)
                SubTitleVisibility = false;
        }

        private void border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            RaiseEvent(new RoutedEventArgs(ItemClickedEvent));
        }

        protected static void SetupChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == TitleTextContentProperty)
                (d as MeshTaskBarItem).TitleTextBox.Text = e.NewValue.ToString();
            if (e.Property == SubTitleTextContentProperty)
                (d as MeshTaskBarItem).SubTitleTextBox.Text = e.NewValue.ToString();
            if (e.Property == SubTitleVisibilityProperty)
                (d as MeshTaskBarItem).SubTitleTextBox.Visibility = ((bool)e.NewValue) ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
