﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for MonitorSelectorWindow.xaml
    /// </summary>
    public partial class MonitorSelectorWindow : Window
    {
        public MonitorSelectorWindow()
        {
            InitializeComponent();
            monitorSelection.Display();
        }

        private void selectButton_Click(object sender, RoutedEventArgs e)
        {
            if (monitorSelection.SelectedMonitor != null)
                this.Close();
        }
    }
}
