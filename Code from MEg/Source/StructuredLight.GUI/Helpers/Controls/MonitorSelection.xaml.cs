﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Interaction logic for MonitorSelection.xaml
    /// </summary>
    public partial class MonitorSelection : UserControl
    {
        #region Declares

        public ManagedProjector SelectedMonitor { get { return Monitors.SelectedItem as ManagedProjector; } }
        public NoParam Selected = null;
        private Dictionary<double, Bitmap> _projectorWindowImages = new Dictionary<double, Bitmap>();

        #endregion

        #region Constructor

        public MonitorSelection()
        {
            InitializeComponent();
            #region Factory
            FrameworkElementFactory tb = new FrameworkElementFactory(typeof(TextBlock));
            tb.SetBinding(TextBlock.TextProperty, new Binding("FriendlyName"));
            tb.SetValue(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis);
            tb.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Left);
            tb.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Left);
            tb.SetValue(TextBlock.MarginProperty, new Thickness(15, 0, 0, 0));
            DataTemplate dt = new DataTemplate();
            dt.VisualTree = tb;
            Monitors.ItemTemplate = dt;
            #endregion
        }

        #endregion

        #region Display Method

        public void Display()
        {
            Monitors.ItemsSource = ManagedProjector.EnumerateProjectors().Where((p) => p.Resolution.Width != 0 && p.Resolution.Height != 0);
            foreach (ManagedProjector proj in Monitors.ItemsSource.Cast<ManagedProjector>())
            {
                proj.FriendlyName = string.IsNullOrEmpty(proj.FriendlyName) ? proj.ModelName : proj.FriendlyName;
            }
            Thread t = new Thread(() =>
                {
                    Bitmap original = new Bitmap("projectorTestDisplay.png");
                    foreach (var mon in ManagedProjector.EnumerateProjectors())
                    {
                        int monNum = mon.Resolution.Width + 1 ^ mon.Resolution.Height;
                        _projectorWindowImages[monNum] = (Bitmap)BitmapPreviewer.ResizeImage(original,
                            mon.Resolution.Width, mon.Resolution.Height);
                    }
                });
            t.Start();
        }

        #endregion

        #region Monitor Panel Selection

        private DateTime _lastSelectedMonitorsChangedTimestamp = new DateTime();
        private bool _monitorsPanelIsLeftClick = false;
        private bool _disableSelection = false;
        private GuiWindow _currentWindow = null;

        private void Monitors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!_monitorsPanelIsLeftClick)
                return;
            if (_disableSelection)
                return;
            _lastSelectedMonitorsChangedTimestamp = DateTime.Now;
            if (SelectedMonitor == null)
                return;
            if (SelectedMonitor.IsPrimary)
            {
                if (MessageBox.Show("You should not put the projector window on the primary monitor.\nDo you wish to ignore this warning?", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;
            }

            DisplayTestImage();

            if (Selected != null)
                Selected();
        }

        private void Monitors_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _monitorsPanelIsLeftClick = e.ChangedButton == MouseButton.Left;
        }

        private void Monitors_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if ((DateTime.Now - _lastSelectedMonitorsChangedTimestamp).TotalMilliseconds > 100 && e.ChangedButton == MouseButton.Left)
            {
                int index = Monitors.SelectedIndex;
                Monitors.SelectedIndex = -1;
                Monitors.SelectedIndex = index;
            }
        }

        private void CloseMonitorPreview_Click(object sender, RoutedEventArgs e)
        {
            CloseCurrentDisplayingMonitor();
        }

        public void CloseCurrentDisplayingMonitor()
        {
            if (_currentWindow != null)
            {
                _currentWindow.Close();
                _currentWindow.Dispose();
                _currentWindow = null;
            }
        }

        #endregion

        #region UserControl Events

        private void displayTestImage_Checked(object sender, RoutedEventArgs e)
        {
            DisplayTestImage();
        }

        private void DisplayTestImage()
        {
            if (SelectedMonitor == null)
                return;
            if (_currentWindow != null)
                CloseCurrentDisplayingMonitor();

            if (displayTestImage.IsChecked == true)
            {
                _currentWindow = new GuiWindow(SelectedMonitor);
                int monNum = SelectedMonitor.Resolution.Width + 1 ^ SelectedMonitor.Resolution.Height;
                _currentWindow.SetImage(_projectorWindowImages[monNum]);
            }
        }

        #endregion
    }
}
