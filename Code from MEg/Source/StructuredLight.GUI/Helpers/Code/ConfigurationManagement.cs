﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    public static class ConfigurationManagement
    {
        private static bool _previouslyLoaded;
        public const string ConfigFile = "config.xml";

        public static void LoadConfiguration()
        {
            if (_previouslyLoaded)
                return;
            _previouslyLoaded = true;

            Library.Initialize(ConfigFile);
        }
    }
}
