﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;

namespace StructuredLight.GUI
{
    public delegate void MeshContourDistanceCalculationsComplete(List<Point3D> abPath, List<Point3D> bcPath, List<Point3D> acPath, double abLength, double bcLength, double acLength);
    public class MeshContourDistanceHelpers
    {
        public static void CalculateMeshContourDistances(Point3D a, Point3D b, Point3D c, IList<Point3D> points, MeshContourDistanceCalculationsComplete finished)
        {
            System.Threading.Thread t = new System.Threading.Thread(() =>
            {
                Point3D nearestPointA = new Point3D();
                double nearestDistanceA = double.PositiveInfinity;
                Point3D nearestPointB = new Point3D();
                double nearestDistanceB = double.PositiveInfinity;
                Point3D nearestPointC = new Point3D();
                double nearestDistanceC = double.PositiveInfinity;
                List<Point3D> orderedPoints = MeshContourDistanceHelpers.OrderPointsByDistance(points, a);
                foreach (Point3D p in orderedPoints)
                {
                    if (p.DistanceTo(a) < nearestDistanceA)
                    {
                        nearestDistanceA = p.DistanceTo(a);
                        nearestPointA = p;
                    }
                    if (p.DistanceTo(b) < nearestDistanceB)
                    {
                        nearestDistanceB = p.DistanceTo(b);
                        nearestPointB = p;
                    }
                    if (p.DistanceTo(c) < nearestDistanceC)
                    {
                        nearestDistanceC = p.DistanceTo(c);
                        nearestPointC = p;
                    }
                }

                Dictionary<Point3D, List<Point3D>> linked = MeshContourDistanceHelpers.BuildGraph(orderedPoints);
                double abLength, acLength, bcLength;
                List<Point3D> abPath = MeshContourDistanceHelpers.GetPath(linked, nearestPointA, nearestPointB, new List<Point3D>(), out abLength);
                List<Point3D> acPath = MeshContourDistanceHelpers.GetPath(linked, nearestPointA, nearestPointC, new List<Point3D>(), out acLength);
                List<Point3D> bcPath = MeshContourDistanceHelpers.GetPath(linked, nearestPointB, nearestPointC, new List<Point3D>(), out bcLength);
                finished(abPath, bcPath, acPath, abLength, bcLength, acLength);
            });
            t.Start();
        }

        public static List<Point3D> OrderPointsByDistance(IList<Point3D> points, Point3D startingPoint)
        {
            List<Point3D> orderedList = new List<Point3D>();
            List<Point3D> visitedPoints = new List<Point3D>();
            Point3D next = startingPoint;
            visitedPoints.Add(startingPoint);
            List<Point3D> newPoints = new List<Point3D>(points);
            while (true)
            {
                next = FindNextNearestPoint(newPoints, next, visitedPoints);
                if (next.X == double.MaxValue)
                    break;
                orderedList.Add(next);
                visitedPoints.Add(next);
                newPoints.RemoveAll((a) => (a.X == next.X && a.Y == next.Y && a.Z == next.Z));
            }
            return orderedList;
        }

        public static Point3D FindNextNearestPoint(IList<Point3D> points, Point3D point, List<Point3D> orderedList)
        {
            Point3D closest = new Point3D(double.MaxValue, double.MaxValue, double.MaxValue);
            double closestDist = double.MaxValue;
            foreach (Point3D p in points)
                if (p.DistanceTo(point) < closestDist && p != point)
                {
                    closestDist = p.DistanceTo(point);
                    closest = p;
                }
            return closest;
        }

        public static int GetPathLength(Dictionary<Point3D, List<Point3D>> linked, Point3D p1, Point3D p2, List<Point3D> alreadyChecked)
        {
            List<Point3D> lists = linked[p1];
            List<int> lengths = new List<int>();
            foreach (Point3D p in lists)
            {
                if (alreadyChecked.Contains(p))
                    continue;
                alreadyChecked.Add(p);
                if (p == p2)
                    return 0;
                lengths.Add(GetPathLength(linked, p, p2, alreadyChecked));
            }
            return lengths.Count == 0 ? 10000000 : lengths.Min() + 1;
        }

        public static List<Point3D> GetPath(Dictionary<Point3D, List<Point3D>> linked, Point3D p1, Point3D p2, List<Point3D> alreadyChecked, out double len)
        {
            len = 0;
            if (linked.Count == 0)
                return null;
            List<Point3D> lists = linked[p1];
            List<List<Point3D>> lengths = new List<List<Point3D>>();
            foreach (Point3D p in lists)
            {
                if (alreadyChecked.Contains(p))
                    continue;
                alreadyChecked.Add(p);
                List<Point3D> pts = new List<Point3D>() { p };
                if (p == p2)
                    break;
                double length;
                List<Point3D> newPts = GetPath(linked, p, p2, alreadyChecked, out length);
                if (newPts != null)
                    pts.AddRange(newPts);
                lengths.Add(pts);
            }

            if (lengths.Count == 0)
                return null;
            double shortestLength = double.MaxValue;
            List<Point3D> shortest = new List<Point3D>();
            foreach (List<Point3D> pts in lengths)
            {
                double distance = MeshCalculations.CalculateDistanceAlongPoints(pts);
                if (distance < shortestLength)
                {
                    shortest = pts;
                    shortestLength = distance;
                }
            }
            len = shortestLength;
            return shortest;
        }

        public static Dictionary<Point3D, List<Point3D>> BuildGraph(IList<Point3D> points)
        {
            Dictionary<Point3D, List<Point3D>> dict = new Dictionary<Point3D, List<Point3D>>();
            Point3D prevPoint = new Point3D(double.MaxValue, 0, 0);
            for (int i = 0; i < points.Count; i++)
            {
                /*if (i + 1 == points.Count && type != CalcType.Contour)
                    dict[points[i]] = new List<Point3D>(new Point3D[1] { points[i - 1] });
                else if (i - 1 < 0 && type != CalcType.Contour)
                    dict[points[i]] = new List<Point3D>(new Point3D[1] { i + 1 == points.Count ? points[0] : points[i + 1] });
                else*/
                    dict[points[i]] = new List<Point3D>(new Point3D[2] { i - 1 < 0 ? points[points.Count - 1] : points[i - 1], i + 1 == points.Count ? points[0] : points[i + 1] });
                dict[points[i]].Reverse();
            }
            return dict;
        }
    }

    public enum CalcType
    {
        Contour,
        Direct,
        Cut,
        Volume
    }
}
