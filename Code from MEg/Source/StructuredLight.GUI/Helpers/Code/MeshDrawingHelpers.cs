﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;

namespace StructuredLight.GUI
{
    public class MeshDrawingHelpers
    {
        #region Line Through Points Creation

        public static Visual3D DrawLineThroughPoints(IList<Point3D> pts)
        {
            return DrawLineThroughPoints(pts, Colors.Green);
        }

        public static Visual3D DrawLineThroughPoints(IList<Point3D> pts, Color c)
        {
            LinesVisual3D lines = new LinesVisual3D();
            lines.Color = c;
            lines.Thickness = 2;
            lines.Points = pts;
            return lines;
        }

        #endregion

        #region Plane Creation

        public static Visual3D CreatePlane(Point3D a, Point3D b, Point3D c, double size)
        {
            Point3D center = MeshCalculations.CalculateCenter(a, b, c);
            Vector3D normal = MeshCalculations.CalculateNormal(a, b, c);
            Model3DGroup plane = new Model3DGroup();
            RectangleVisual3D r = new RectangleVisual3D();
            r.Normal = normal;
            r.Width = size;
            r.Length = size;
            r.Origin = center;
            r.Fill = new SolidColorBrush(Colors.SkyBlue) { Opacity = 0.5 };
            return r;
        }

        #endregion

        #region Sphere Creation

        public static ModelVisual3D CreateSphere(double r, Point3D center, Color color, double polygons = 7.0)
        {
            SphereVisual3D s = new SphereVisual3D();
            s.BeginEdit();
            s.Radius = r;
            s.Fill = new SolidColorBrush(color);
            s.Center = center;
            s.EndEdit();
            return s;
        }

        #endregion

        #region Change Mesh Colors

        public static void FixColors(ref ModelVisual3D mv, Color color)
        {
            if (mv.Content is Model3DGroup)
            {
                foreach (var m in ((Model3DGroup)mv.Content).Children)
                {
                    if (m is GeometryModel3D)
                    {
                        GeometryModel3D mod = m as GeometryModel3D;
                        mod.Material = new DiffuseMaterial
                        {
                            Brush = new SolidColorBrush(color),
                            AmbientColor = color,
                            Color = color
                        };
                        mod.BackMaterial = new DiffuseMaterial
                        {
                            Brush = new SolidColorBrush(color),
                            AmbientColor = color,
                            Color = color
                        };
                    }
                }
            }
            else if (mv.Content is GeometryModel3D)
            {
                GeometryModel3D mod = mv.Content as GeometryModel3D;
                mod.Material = new DiffuseMaterial
                {
                    Brush = new SolidColorBrush(color),
                    AmbientColor = color,
                    Color = color
                };
                mod.BackMaterial = new DiffuseMaterial
                {
                    Brush = new SolidColorBrush(color),
                    AmbientColor = color,
                    Color = color
                };
            }
        }

        #endregion
    }
}
