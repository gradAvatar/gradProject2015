﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StructuredLight.GUI
{
    public delegate void OnClicked(int cameraNumber);

    public interface IHardwareSetupControl
    {
        /// <summary>
        /// Sets up the page, and gets it ready to eventually display its information
        /// </summary>
        /// <param name="setup"></param>
        void Initialize(HardwareSetup setup);

        /// <summary>
        /// Called when the page is ready to be displayed
        /// </summary>
        void Display();

        /// <summary>
        /// If the next button on the HardwareSetup window is pressed, this will be called to tell the form that the user wants to move on
        /// </summary>
        void RequestedNext();
    }
}
