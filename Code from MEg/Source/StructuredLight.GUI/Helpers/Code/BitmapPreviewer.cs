﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace StructuredLight.GUI
{
    public static class BitmapPreviewer
    {
        private static System.Drawing.RotateFlipType _currentFlip = System.Drawing.RotateFlipType.RotateNoneFlipNone;

        public static void LoadBitmapImage(System.Drawing.Bitmap img, Image host, bool destroyImage = false)
        {
            lock (img)
            {
                var newImg = ResizeImage(img, (int)host.ActualWidth, (int)host.ActualHeight);
                newImg.RotateFlip(_currentFlip);

                if (Application.Current != null)
                    Application.Current.Dispatcher.Invoke(new Action(() => host.Source =
                        Imaging.CreateBitmapSourceFromBitmap(newImg as System.Drawing.Bitmap, host)), 
                        System.Windows.Threading.DispatcherPriority.Send);
                newImg.Dispose();
                if (destroyImage)
                    img.Dispose();
            }
        }

        public static void LoadBitmapImages(IList<System.Drawing.Bitmap> imgs, Image host, bool destroyImage = false)
        {
            lock (imgs)
            {
                int imageWidth = (int)(host.ActualWidth == 0 ? (double.IsNaN(host.Width) ? (host.Parent as FrameworkElement).ActualWidth : host.Width) : host.ActualWidth);
                int imageHeight = (int)(host.ActualHeight == 0 ? (double.IsNaN(host.Height) ? (host.Parent as FrameworkElement).ActualHeight : host.Height) : host.ActualHeight);
                if (imgs.Count > 2)
                {
                    using (var bitmap = new System.Drawing.Bitmap(imageWidth, imageHeight))
                    {
                        using (var canvas = System.Drawing.Graphics.FromImage(bitmap))
                        {
                            for (int i = 0; i < Math.Min(4, imgs.Count); i++)
                            {
                                int w = i % 2 == 1 ? imageWidth / 2 : 0;
                                int h = i >= 2 ? imageHeight / 2 : 0;
                                var newImg = ResizeImage(imgs[i], imageWidth / 2, imageHeight / 2);
                                canvas.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                canvas.DrawImage(newImg, new System.Drawing.Rectangle(w, h, imageWidth / 2, imageHeight / 2),
                                    new System.Drawing.Rectangle(0, 0, newImg.Width, newImg.Height),
                                    System.Drawing.GraphicsUnit.Pixel);
                            }
                            canvas.Save();
                        }
                        bitmap.RotateFlip(_currentFlip);
                        host.Source = Imaging.CreateBitmapSourceFromBitmap(bitmap, host);
                        if (destroyImage)
                        {
                            foreach (System.Drawing.Image i in imgs)
                                i.Dispose();
                        }
                    }
                }
                else if (imgs.Count == 2)
                {
                    using (var bitmap = new System.Drawing.Bitmap(imageWidth, imageHeight))
                    {
                        using (var canvas = System.Drawing.Graphics.FromImage(bitmap))
                        {
                            for (int i = 0; i < 2; i++)
                            {
                                int h = i == 1 ? imageHeight / 2 : 0;
                                var newImg = ResizeImage(imgs[i], imageWidth, imageHeight / 2);
                                newImg.RotateFlip(_currentFlip);
                                canvas.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                                canvas.DrawImage(newImg, new System.Drawing.Rectangle(0, h, imageWidth, imageHeight / 2),
                                    new System.Drawing.Rectangle(0, 0, newImg.Width, newImg.Height),
                                    System.Drawing.GraphicsUnit.Pixel);
                            }
                            canvas.Save();
                        }
                        host.Source = Imaging.CreateBitmapSourceFromBitmap(bitmap, host);
                        if (destroyImage)
                        {
                            foreach (System.Drawing.Image i in imgs)
                                i.Dispose();
                        }
                    }
                }
                else
                    LoadBitmapImage(imgs[0], host, destroyImage);
            }
        }

        public static System.Drawing.Image ResizeImage(System.Drawing.Image FullsizeImage, int NewWidth, int MaxHeight)
        {
            int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
            if (NewHeight > MaxHeight)
            {
                // Resize with height instead
                NewWidth = FullsizeImage.Width * MaxHeight / FullsizeImage.Height;
                NewHeight = MaxHeight;
            }
            return FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);
        }

        public static void SetCameraRotation(System.Drawing.RotateFlipType flip)
        {
            _currentFlip = flip;
        }
    }

    public static class Imaging
    {
        /// <summary>
        /// Converts Bitmap to BitmapSource
        /// </summary>
        /// <param name="bitmap"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public static BitmapSource CreateBitmapSourceFromBitmap(System.Drawing.Image bitmap, Image host)
        {
            if (bitmap == null)
                throw new ArgumentNullException("bitmap");

            if (System.Windows.Application.Current.Dispatcher == null)
                return null; // Is it possible?

            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    // You need to specify the image format to fill the stream. 
                    // I'm assuming it is PNG
                    bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Png);
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    // Make sure to create the bitmap in the UI thread
                    return (BitmapSource)System.Windows.Application.Current.Dispatcher.Invoke(
                            new Func<Stream, BitmapSource>(CreateBitmapSourceFromBitmap),
                            System.Windows.Threading.DispatcherPriority.Normal,
                            memoryStream);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static BitmapSource CreateBitmapSourceFromBitmap(Stream stream)
        {
            BitmapDecoder bitmapDecoder = BitmapDecoder.Create(
                stream,
                BitmapCreateOptions.PreservePixelFormat,
                BitmapCacheOption.OnLoad);

            // This will disconnect the stream from the image completely...
            WriteableBitmap writable = new WriteableBitmap(bitmapDecoder.Frames.Single());
            writable.Freeze();
            return writable;
        }
    }
}
