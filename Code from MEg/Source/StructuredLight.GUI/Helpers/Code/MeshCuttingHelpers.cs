﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using HelixToolkit.Wpf;
using System.Windows;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    public class MeshCuttingHelpers
    {
        private static MeshGenerator _meshGenerator = new MeshGenerator();
        public static ModelVisual3D CutAlongPoints(Point3D a, Point3D b, Point3D c, MeshGeometry3D geometry, bool needsNegated = false)
        {
            return CutAlongPoints(a, b, c, geometry, Colors.Red, needsNegated);
        }

        public static void CutAlongPoints(Point3D a, Point3D b, Point3D c,
            MeshGeometry3D geometry, Color colorFirst, Color colorSecond, out ModelVisual3D first, out ModelVisual3D second)
        {
            Vector3DCollection vectors = MeshGeometryHelper.CalculateNormals(new List<Point3D>(new Point3D[3] 
                                { 
                                    a, 
                                    b,
                                    c
                                }),
            new List<int>(new int[3] { 0, 1, 2 }));
            var ourVector = vectors[0];
            var ourVectorNegated = vectors[0];
            ourVectorNegated.Negate();

            IList<Point3D> firstBorder, secondBorder;
            ModelVisual3D firstInvoke = null, secondInvoke = null;

            AsyncMeshBuilder geoFirst = MeshGeometryHelper.CutBuilderWithoutExtraPoints(geometry,
                MeshCalculations.CalculateCenter(a, b, c), ourVector, out firstBorder);
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                firstInvoke = buildModel(colorFirst, FixWaterTight(ourVector, firstBorder, geoFirst));
            }));
            first = firstInvoke;
            if (first == null)
            {
                second = null;
                return;
            }

            AsyncMeshBuilder geoSecond = MeshGeometryHelper.CutBuilderWithoutExtraPoints(geometry,
                MeshCalculations.CalculateCenter(a, b, c), ourVectorNegated, out secondBorder);
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                secondInvoke = buildModel(colorSecond, FixWaterTight(ourVectorNegated, secondBorder, geoSecond));
            }));
            second = secondInvoke;
        }

        public static ModelVisual3D CutAlongPoints(Point3D a, Point3D b, Point3D c, MeshGeometry3D geometry, Color color, bool needsNegated = false)
        {
            Vector3DCollection vectors = MeshGeometryHelper.CalculateNormals(new List<Point3D>(new Point3D[3] 
                                { 
                                    a, 
                                    b,
                                    c
                                }),
            new List<int>(new int[3] { 0, 1, 2 }));
            var ourVector = vectors[0];
            if (needsNegated)
                ourVector.Negate();
            IList<Point3D> border;
            AsyncMeshBuilder meshCut = MeshGeometryHelper.CutBuilderWithoutExtraPoints(geometry,
                MeshCalculations.CalculateCenter(a, b, c), ourVector, out border);
            return buildModel(color, FixWaterTight(ourVector, border, meshCut));
        }

        internal static ModelVisual3D buildModel(Color color, MeshGeometry3D meshCut)
        {
            ModelVisual3D mv = new ModelVisual3D();
            Model3DGroup mesh = new Model3DGroup();
            MaterialGroup grp = new MaterialGroup();
            grp.Children.Add(new DiffuseMaterial(new SolidColorBrush(color)) { AmbientColor = color, Color = color });
            GeometryModel3D geo = new GeometryModel3D(meshCut, grp);
            geo.BackMaterial = grp;
            geo.Geometry = meshCut;
            mesh.Children.Add(geo);
            mesh.Children.Add(new AmbientLight(new Color() { R = 100, G = 100, B = 100 }));
            mv.Content = mesh;
            return mv;
        }

        private static MeshGeometry3D FixWaterTight(Vector3D normal, IList<Point3D> border, AsyncMeshBuilder mesh)
        {
            MeshGeometry3D geo = mesh.ToMesh(false);

            List<Point3d> vertices = new List<Point3d>();
            foreach (var p in border)
                vertices.Add(managedPtToUnmanaged(p));

            List<TriPolygon> polys = _meshGenerator.GenerateConcaveHull(200, vertices);
            Dictionary<Point3d, Point3D> pts = new Dictionary<Point3d, Point3D>();
            foreach (TriPolygon poly in polys)
            {
                if (!pts.ContainsKey(poly.A))
                    pts.Add(poly.A, unmanagedPtToManaged(poly.A));
                if (!pts.ContainsKey(poly.B))
                    pts.Add(poly.B, unmanagedPtToManaged(poly.B));
                if (!pts.ContainsKey(poly.C))
                    pts.Add(poly.C, unmanagedPtToManaged(poly.C));
            }

            List<TriPolygon> newFaces = new List<TriPolygon>();
            //Vector3D compNormal = new Vector3D(normal.X, normal.Y, normal.Z);
            //compNormal.Negate();
            foreach (TriPolygon poly in polys)
            {
                //if (ApproxEqual(compNormal, new Vector3D(face.Normal[0], face.Normal[1], face.Normal[2]), 0.1))
                //Add anything not opposite to our border
                if (!ApproxEqual(normal, MeshCalculations.CalculateNormal(unmanagedPtToManaged(poly.A),
                    unmanagedPtToManaged(poly.B), unmanagedPtToManaged(poly.C)), 0.005))
                    newFaces.Add(poly);
            }

            Dictionary<Point3d, int> positionDict = new Dictionary<Point3d, int>();
            foreach (var face in newFaces)
            {
                int onePos;
                if (!positionDict.TryGetValue(face.A, out onePos))
                {
                    onePos = geo.Positions.Count;
                    geo.Positions.Add(unmanagedPtToManaged(face.A));
                    positionDict[face.A] = onePos;
                }
                int twoPos;
                if (!positionDict.TryGetValue(face.B, out twoPos))
                {
                    twoPos = geo.Positions.Count;
                    geo.Positions.Add(unmanagedPtToManaged(face.B));
                    positionDict[face.B] = twoPos;
                }
                int threePos;
                if (!positionDict.TryGetValue(face.C, out threePos))
                {
                    threePos = geo.Positions.Count;
                    geo.Positions.Add(unmanagedPtToManaged(face.C));
                    positionDict[face.C] = threePos;
                }
                geo.TriangleIndices.Add(onePos);
                geo.TriangleIndices.Add(twoPos);
                geo.TriangleIndices.Add(threePos);
            }
            geo.Freeze();
            return geo;
        }

        private static Point3d managedPtToUnmanaged(Point3D pt)
        {
            return new Point3d((float)pt.X, (float)pt.Y, (float)pt.Z);
        }

        private static Point3D unmanagedPtToManaged(Point3d pt)
        {
            return new Point3D(pt.X, pt.Y, pt.Z);
        }

        private static bool ApproxEqual(Vector3D v, Vector3D u, double tolerance)
        {
            return
                (
                (System.Math.Abs(v.X - u.X) <= tolerance) &&
                (System.Math.Abs(v.Y - u.Y) <= tolerance) &&
                (System.Math.Abs(v.Z - u.Z) <= tolerance)
                );
        }
    }
}
