﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace StructuredLight.GUI.MeshLab
{
    public class MeshLabPointCloudConversion
    {
        private static string MeshLabReconstructionArgs = "-i {0} -o {1} -s {2} -om vn";
        private static string MeshLabConversionArgs = "-i {0} -o {1} -om vn";
        private const string _MeshReconstructionFilter = "MeshLabReconstruction.mlx";
        private const string _QuickMeshReconstructionFilter = "QuickMeshLabReconstruction.mlx";
        private static bool _debug = true;

        public static bool ConvertPointCloudToMesh(string fileName, string newFileName, bool quickNDirty=false)
        {
            string meshlabLocation = MeshlabHelper.TryFindMeshlabServerInstance();
            string args = string.Format(MeshLabReconstructionArgs,
                '"' + fileName + '"', '"' + newFileName + '"', '"' + Path.Combine(Environment.CurrentDirectory, quickNDirty ? _QuickMeshReconstructionFilter : _MeshReconstructionFilter) + '"');
            Process p = Process.Start(_debug ? new ProcessStartInfo(meshlabLocation, args)
                : new ProcessStartInfo(meshlabLocation, args) { CreateNoWindow = true, UseShellExecute = false });
            try
            {
                p.WaitForExit();
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool ConvertMeshFormats(string filename, string newFormat)
        {
            string meshlabLocation = MeshlabHelper.TryFindMeshlabServerInstance();
            string args = string.Format(MeshLabConversionArgs,
                '"' + filename + '"', '"' + Path.Combine(Path.GetDirectoryName(filename), Path.GetFileNameWithoutExtension(filename) + newFormat) + '"');
            Process p = Process.Start(new ProcessStartInfo(meshlabLocation, args) { CreateNoWindow = true, UseShellExecute = false });
            try
            {
                p.WaitForExit();
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
