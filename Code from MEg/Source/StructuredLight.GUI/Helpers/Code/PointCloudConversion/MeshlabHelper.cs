﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using StructuredLight.Managed;
using System.Diagnostics;

namespace StructuredLight.GUI.MeshLab
{
    public class MeshlabHelper
    {
        public static void StartMeshlab(string lastMeshFileName)
        {
            string path = Library.UserSettings["MeshLabLocation"];
            if (path == null) path = TryFindMeshlabInstance();
            Process.Start(path, "\"" + lastMeshFileName + "\"");
        }

        public static string TryFindMeshlabInstance()
        {
            if (Directory.Exists(Path.Combine("C:/", "Program Files", "VCG", "MeshLab")))
                return Path.Combine("C:/", "Program Files", "VCG", "MeshLab", "meshlab.exe");
            if (Directory.Exists(Path.Combine("C:/", "Program Files (x86)", "VCG", "MeshLab")))
                return Path.Combine("C:/", "Program Files (x86)", "VCG", "MeshLab", "meshlab.exe");
            return "";
        }

        public static string TryFindMeshlabServerInstance()
        {
            if (Directory.Exists(Path.Combine("C:/", "Program Files", "VCG", "MeshLab")))
                return Path.Combine("C:/", "Program Files", "VCG", "MeshLab", "meshlabserver.exe");
            if (Directory.Exists(Path.Combine("C:/", "Program Files (x86)", "VCG", "MeshLab")))
                return Path.Combine("C:/", "Program Files (x86)", "VCG", "MeshLab", "meshlabserver.exe");
            return "";
        }
    }
}
