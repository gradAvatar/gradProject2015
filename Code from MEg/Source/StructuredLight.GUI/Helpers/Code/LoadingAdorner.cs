﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows;
using System.Windows.Media;
using System.Threading;

namespace StructuredLight.GUI
{
    /// <summary>
    /// Adorner that disables all controls that fall under it
    /// </summary>
    public class LoadingAdorner : Adorner
    {
        #region Properties

        /// <summary>
        /// Gets or sets the color to paint
        /// </summary>
        public Brush Color
        {
            get { return (Brush)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        /// <summary>
        /// Gets or sets the color to paint
        /// </summary>
        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("Color", typeof(Brush), typeof(LoadingAdorner),
            new PropertyMetadata((Brush)new BrushConverter().ConvertFromString("#7F4047F7")));


        /// <summary>
        /// Gets or sets the border 
        /// </summary>
        public Pen Border
        {
            get { return (Pen)GetValue(BorderProperty); }
            set { SetValue(BorderProperty, value); }
        }

        /// <summary>
        /// Gets or sets the border 
        /// </summary>
        public static readonly DependencyProperty BorderProperty =
            DependencyProperty.Register("Border", typeof(Pen), typeof(LoadingAdorner),
            new UIPropertyMetadata(new Pen(Brushes.Gray, 1)));

        //the start point where to start drawing
        private static readonly Point startPoint =
            new Point(0, 0);

        /// <summary>
        /// Gets or sets the text to display 
        /// </summary>
        public string OverlayedText
        {
            get { return (string)GetValue(OverlayedTextProperty); }
            set { SetValue(OverlayedTextProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text to display 
        /// </summary>
        public static readonly DependencyProperty OverlayedTextProperty =
            DependencyProperty.Register("OverlayedText", typeof(string), typeof(LoadingAdorner), new UIPropertyMetadata(""));

        /// <summary>
        /// Gets or sets the foreground to use for the text
        /// </summary>
        public Brush ForeGround
        {
            get { return (Brush)GetValue(ForeGroundProperty); }
            set { SetValue(ForeGroundProperty, value); }
        }

        /// <summary>
        /// Gets or sets the foreground to use for the text
        /// </summary>
        public static readonly DependencyProperty ForeGroundProperty =
            DependencyProperty.Register("ForeGround", typeof(Brush), typeof(LoadingAdorner),
            new UIPropertyMetadata(Brushes.Black));


        /// <summary>
        /// Gets or sets the font size for the text
        /// </summary>
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        /// <summary>
        /// Gets or sets the font size for the text
        /// </summary>
        public static readonly DependencyProperty FontSizeProperty =
            DependencyProperty.Register("FontSize", typeof(double), typeof(LoadingAdorner), new UIPropertyMetadata(10.0));


        /// <summary>
        /// Gets or sets the Typeface for the text
        /// </summary>
        public Typeface Typeface
        {
            get { return (Typeface)GetValue(TypefaceProperty); }
            set { SetValue(TypefaceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the Typeface for the text
        /// </summary>
        public static readonly DependencyProperty TypefaceProperty =
            DependencyProperty.Register("Typeface", typeof(Typeface), typeof(LoadingAdorner),
            new UIPropertyMetadata(new Typeface("Verdana")));



        #endregion

        /// <summary>
        /// Constructor for the adorner
        /// </summary>
        /// <param name="adornerElement">The element to be adorned</param>
        public LoadingAdorner(UIElement adornerElement)
            : base(adornerElement)
        { }

        public static void AddAdorner(string text, UIElement attachee, Window w)
        {
            LoadingAdorner m_adorner = new LoadingAdorner(attachee);
            m_adorner.FontSize = 50;
            m_adorner.Color = new SolidColorBrush(Colors.LightGray) { Opacity = 0.5 };
            m_adorner.OverlayedText = text;
            m_adorner.Typeface = new Typeface(w.FontFamily, FontStyles.Italic,
                FontWeights.Bold, w.FontStretch);
            AdornerLayer.GetAdornerLayer(attachee).Add(m_adorner);
        }

        public static void RemoveAdorner(UIElement attachee)
        {
            Adorner[] all = AdornerLayer.GetAdornerLayer(attachee).GetAdorners(attachee);
            if (all != null)
            {
                foreach (Adorner a in all)
                    AdornerLayer.GetAdornerLayer(attachee).Remove(a);
            }
        }


        /// <summary>
        /// Called to draw on screen
        /// </summary>
        /// <param name="drawingContext">The drawind context in which we can draw</param>
        protected override void OnRender(System.Windows.Media.DrawingContext drawingContext)
        {
            FormattedText text = new FormattedText(OverlayedText, Thread.CurrentThread.CurrentUICulture,
                FlowDirection.LeftToRight, Typeface, FontSize, ForeGround) { TextAlignment = TextAlignment.Center };
            double textOffset = 0;
            int lines = 0;
            if ((lines = OverlayedText.Split('\n').Length) > 1)
                textOffset = FontSize / 2 * lines;//If there are multiple lines, make sure it is put in the center!

            drawingContext.DrawText(text, new Point(DesiredSize.Width / 2, DesiredSize.Height / 2 - textOffset));
            drawingContext.DrawRectangle(Color, Border, new Rect(startPoint, DesiredSize));
            base.OnRender(drawingContext);
        }
    }
}
