﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;

namespace StructuredLight.GUI
{
    public class MeshCalculations
    {
        #region Calculate Normals and Center

        public static Point3D CalculateCenter(Point3D a, Point3D b, Point3D c)
        {
            return new Point3D((a.X + b.X + c.X) / 3D, (a.Y + b.Y + c.Y) / 3D, (a.Z + b.Z + c.Z) / 3D);
        }

        public static Point3D CalculateCenter(IEnumerable<Point3D> pts)
        {
            return new Point3D(pts.Sum((p) => p.X) / (double)pts.Count(), 
                pts.Sum((p) => p.Y) / (double)pts.Count(),
                pts.Sum((p) => p.Z) / (double)pts.Count());
        }

        public static Vector3D CalculateNormal(Point3D p0, Point3D p1, Point3D p2)
        {
            Vector3D v0 = new Vector3D(
                p1.X - p0.X, p1.Y - p0.Y, p1.Z - p0.Z);
            Vector3D v1 = new Vector3D(
                p2.X - p1.X, p2.Y - p1.Y, p2.Z - p1.Z);
            Vector3D normal = Vector3D.CrossProduct(v0, v1);
            normal.Normalize();
            return normal;
        }

        #endregion

        #region Calculate Distances

        public static double FindDirectDistance(Point3D a, Point3D b)
        {
            return a.DistanceTo(b);
        }

        public static IList<Point3D> CreateContourLine(Point3D a, Point3D b, Point3D c, MeshGeometry3D geometry)
        {
            Point3D center = MeshCalculations.CalculateCenter(a, b, c);
            //Vector3DCollection vectors = MeshGeometryHelper.CalculateNormals(new List<Point3D>() { a, b, c },
            //    new List<int>() { 0, 1, 2 });
            //return MeshGeometryHelper.GetContourSegments(geometry, center, vectors[0]);    
            return MeshGeometryHelper.GetContourSegments(geometry, center, CalculateNormal(a, b, c));
        }

        public static double CalculateDistanceAlongPoints(List<Point3D> acPath)
        {
            double distance = 0;
            for (int i = 0; i < acPath.Count - 1; i++)
                distance += acPath[i].DistanceTo(acPath[i + 1]);
            return distance;
        }

        #endregion

        #region Volume

        /// <summary>
        /// Code example and derivations from
        /// http://www.gamedev.net/page/resources/_/technical/game-programming/area-and-volume-calculations-r2247
        /// </summary>
        /// <param name="geometry"></param>
        /// <returns></returns>
        public static double VolumeOfMesh(MeshGeometry3D geometry)
        {
            double vol = 0;
            for (int i = 0; i < geometry.TriangleIndices.Count; i += 3)
            {
                int a = geometry.TriangleIndices[i], b = geometry.TriangleIndices[i + 1], c = geometry.TriangleIndices[i + 2];
                vol += SignedVolumeOfTriangle(geometry.Positions[a], geometry.Positions[b], geometry.Positions[c]);
            }
            return Math.Abs(vol);
        }

        #endregion

        #region Surface Area

        public static double SurfaceAreaOfMesh(MeshGeometry3D geometry)
        {
            return SurfaceAreaOfPoints(geometry.Positions.ToList());
        }

        public static double SurfaceAreaOfPoints(List<Point3D> PtList)
        {
            int nPts = PtList.Count;
            Point3D a = new Point3D();
            int j = 0;

            for (int i = 0; i < nPts; ++i)
            {
                j = (i + 1) % nPts;
                var ptr = Cross(PtList[i], PtList[j]);
                a = new Point3D(a.X + ptr.X, a.Y + ptr.Y, a.Z + ptr.Z);
            }
            a = new Point3D(a.X / 2, a.Y / 2, a.Z / 2);
            return a.DistanceTo(default(Point3D));
        }

        #endregion

        #region Helpers

        private static Point3D Cross(Point3D v0, Point3D v1)
        {
            return new Point3D(v0.Y * v1.Z - v0.Z * v1.Y,
                v0.Z * v1.X - v0.X * v1.Z,
                v0.X * v1.Y - v0.Y * v1.X);
        }

        private static double SignedVolumeOfTriangle(Point3D p1, Point3D p2, Point3D p3)
        {
            var v321 = p3.X * p2.Y * p1.Z;
            var v231 = p2.X * p3.Y * p1.Z;
            var v312 = p3.X * p1.Y * p2.Z;
            var v132 = p1.X * p3.Y * p2.Z;
            var v213 = p2.X * p1.Y * p3.Z;
            var v123 = p1.X * p2.Y * p3.Z;
            return (1.0 / 6.0) * (-v321 + v231 + v312 - v132 - v213 + v123);
        }

        #endregion
    }
}
