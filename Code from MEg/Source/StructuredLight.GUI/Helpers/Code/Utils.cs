﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using StructuredLight.Managed;

namespace StructuredLight.GUI
{
    public delegate void NoParam();

    public static class Utils
    {
        public static bool GetFile(string filter, out string file)
        {
            System.Windows.Forms.OpenFileDialog sfd = new System.Windows.Forms.OpenFileDialog();
            sfd.Filter = filter;
            var d = sfd.ShowDialog();
            file = sfd.FileName;
            return d == System.Windows.Forms.DialogResult.OK;
        }

        public static string GetNameOfCamera(ManagedCamera camera)
        {
            return string.IsNullOrEmpty(camera.FriendlyName) ? camera.ModelName : camera.FriendlyName;
        }

        public static string GetNameOfProjector(ManagedProjector projector)
        {
            return string.IsNullOrEmpty(projector.FriendlyName) ? projector.ModelName : projector.FriendlyName;
        }

        public static Bitmap FillImage(double width, double height, Color c)
        {
            System.Drawing.Bitmap Bmp = new Bitmap((int)width, (int)height);
            using (Graphics gfx = Graphics.FromImage(Bmp))
            using (SolidBrush brush = new SolidBrush(c))
            {
                gfx.FillRectangle(brush, 0, 0, (int)width, (int)height);
            }
            return Bmp;
        }
    }

    public enum DisplayType
    {
        DrawContourLine,
        DrawFullContourLine,
        DrawTangentline
    }

    public enum WindowItem
    {
        SplashPage,
        CameraPreviewer,
        ViewAndMeasure,
        Settings,
        HardwareSetup
    }

    public enum SortModifier
    {
        DateTimeAscending,
        DateTimeDecending,
        NameAscending,
        NameDecending
    }

    public interface IBackgroundWindow
    {
        /// <summary>
        /// Called when the window is now on the top and is ready for draing
        /// </summary>
        void Display();

        /// <summary>
        /// Called when the window is being moved to the bottom of the stack
        /// </summary>
        void Hide();
    }

    public static class Extensions
    {
        public static double DistanceTo(this System.Windows.Media.Media3D.Point3D a, System.Windows.Media.Media3D.Point3D b)
        {
            return Math.Sqrt((a.X - b.X) * (a.X - b.X) +
                (a.Y - b.Y) * (a.Y - b.Y) +
                (a.Z - b.Z) * (a.Z - b.Z));
        }

        public static void FillImage(this Image img, int div, Color[] colors)
        {
            if (img == null) throw new ArgumentNullException();
            if (div < 1) throw new ArgumentOutOfRangeException();
            if (colors == null) throw new ArgumentNullException();
            if (colors.Length < 1) throw new ArgumentException();

            int xstep = img.Width / div;
            int ystep = img.Height / div;
            List<SolidBrush> brushes = new List<SolidBrush>();
            foreach (Color color in colors)
                brushes.Add(new SolidBrush(color));

            using (Graphics g = Graphics.FromImage(img))
            {
                for (int x = 0; x < div; x++)
                    for (int y = 0; y < div; y++)
                        g.FillRectangle(brushes[(y * div + x) % colors.Length],
                            new Rectangle(x * xstep, y * ystep, xstep, ystep));
            }
        }
    }
}
