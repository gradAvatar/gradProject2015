﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Management;
using System.Reflection;
using System.Runtime.InteropServices;
using Forms = System.Windows.Forms;
using ImagingScienceProject;

namespace ImagingScienceRendering
{
    /// <summary>
    /// Interaction logic for ProjectorSetup.xaml
    /// </summary>
    public partial class ProjectorSetup : UserControl
    {
        public Monitor SelectedMonitor;
        private int _ourInitializationValue = 0;
        private bool _started = false;

        public ProjectorSetup()
        {
            InitializeComponent();
            _started = true;
        }

        #region Initialization

        internal void Init(int p)
        {
            AvailableProjectorsLabel.Text = string.Format(AvailableProjectorsLabel.Text, p);
            _ourInitializationValue = p;

            InitConfig();
        }

        private void InitConfig()
        {
            if (ProjCalibrationFile.Text != "")
            {
                projector_calibration c = ConfigurationManagement.MainConfiguration.GetProjectorCalibration(SelectedMonitor);
                currentProjCalibrationResolution.Text = c.width + "x" + c.height;
                if (c.height != SelectedMonitor.Screen.Bounds.Height ||
                    c.width != SelectedMonitor.Screen.Bounds.Width)
                    resolutionError.Visibility = System.Windows.Visibility.Visible;
                else
                    resolutionError.Visibility = System.Windows.Visibility.Collapsed;
                hertzSettings.Text = SelectedMonitor.Hz + " hz";
            }
            else
            {
                currentProjCalibrationResolution.Text = "";
                hertzSettings.Text = "";

            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        #endregion

        private void AddAnotherProjector_Click(object sender, RoutedEventArgs e)
        {
            if (DeviceManager.GetMonitors().Count == 1)
                MessageBox.Show("You only have one monitor to display on.");
            else
            {
                Main parentWindow = (Main)Window.GetWindow(this);
                //parentWindow.ScanningPage.AddAnotherProjector(this);
            }
        }

        private void RemoveProjector_Click(object sender, RoutedEventArgs e)
        {
            Main parentWindow = (Main)Window.GetWindow(this);
            //parentWindow.ScanningPage.RemoveProjector(this);
        }

        private void BrowseProjectorCali_Click(object sender, RoutedEventArgs e)
        {
            Forms.OpenFileDialog dia = new Forms.OpenFileDialog();
            dia.CheckFileExists = true;
            dia.Filter = "Projector Config files|*.projcalib|All Files|*.*";
            if (dia.ShowDialog() == Forms.DialogResult.OK)
            {
                ProjCalibrationFile.Text = dia.FileName;
                ReadProjConfig(dia.FileName);
            }
        }

        private void ViewProjectorCali_Click(object sender, RoutedEventArgs e)
        {
            if(ProjCalibrationFile.Text != "")
                System.Diagnostics.Process.Start(ProjCalibrationFile.Text);
        }

        private void resolutionError_Click(object sender, RoutedEventArgs e)
        {
            projector_calibration c = ConfigurationManagement.MainConfiguration.GetProjectorCalibration(SelectedMonitor);
            c.height = SelectedMonitor.Screen.Bounds.Height;
            c.width = SelectedMonitor.Screen.Bounds.Width;
            ConfigurationManagement.MainConfiguration.Save();
            InitConfig();
        }

        #region Configuration Changed Events

        private void useBW_Checked(object sender, RoutedEventArgs e)
        {
            if (useRGB != null)
            {
                useBayer.IsChecked = false;
                useRGB.IsChecked = false;
            }
            if (ConfigurationManagement.MainConfiguration != null)
                ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode = (int)ScanType.BW;
            if (!ValidateProjectorConfiguration())
                MessageBox.Show("Invalid configuration!");
            else if (ConfigurationManagement.MainConfiguration != null)
                ConfigurationManagement.MainConfiguration.Save();
        }

        private void useBayer_Checked(object sender, RoutedEventArgs e)
        {
            if (useRGB != null)
            {
                useRGB.IsChecked = false;
                useBW.IsChecked = false;
            }
            if(ConfigurationManagement.MainConfiguration != null)
                ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode = (int)ScanType.BAYER;
            if (!ValidateProjectorConfiguration())
                MessageBox.Show("Invalid configuration!");
            else if( ConfigurationManagement.MainConfiguration != null)
                ConfigurationManagement.MainConfiguration.Save();
        }

        private void useRGB_Checked(object sender, RoutedEventArgs e)
        {
            if (useRGB != null)
            {
                useBayer.IsChecked = false;
                useBW.IsChecked = false;
            }
            if (ConfigurationManagement.MainConfiguration != null)
                ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode = (int)ScanType.RGB;
            if (!ValidateProjectorConfiguration())
                MessageBox.Show("Invalid configuration!");
            else if (ConfigurationManagement.MainConfiguration != null)
                ConfigurationManagement.MainConfiguration.Save();
        }

        private void useHorizontalPatterns_Checked(object sender, RoutedEventArgs e)
        {
            if (!ValidateProjectorConfiguration())
                MessageBox.Show("Invalid configuration!");
            else if (ConfigurationManagement.MainConfiguration != null)
                ConfigurationManagement.MainConfiguration.Save();
        }

        private void useVerticalPatterns_Checked(object sender, RoutedEventArgs e)
        {
            if (!ValidateProjectorConfiguration())
                MessageBox.Show("Invalid configuration!");
            else if (ConfigurationManagement.MainConfiguration != null)
                ConfigurationManagement.MainConfiguration.Save();
        }

        #endregion

        #region Applying/Get/Verify Configuration Settings

        private void ReadProjConfig(string p)
        {
        }

        private bool ValidateProjectorConfiguration()
        {
            if (!_started)
                return true;
            if (useHorizontalPatterns.IsChecked == false &&
                useVerticalPatterns.IsChecked == false)
                return false;

            return true;
        }

        #endregion

        internal void SetProjector(string p)
        {
            Monitor set = null;
            foreach (var proj in DeviceManager.GetMonitors())
            {
                if (proj.Name == p)
                    set = proj;
            }
            if (set != null)
                SelectedMonitor = set;
            InitConfig();
        }
    }
}
