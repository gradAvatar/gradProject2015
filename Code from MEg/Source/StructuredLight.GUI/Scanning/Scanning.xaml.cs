﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Management;
using Forms = System.Windows.Forms;
using ImagingScienceProject;

namespace ImagingScienceRendering
{
    /// <summary>
    /// Interaction logic for Scanning.xaml
    /// </summary>
    public partial class Scanning : UserControl, PageUserControl
    {
        public string currentConfig = "config.xml";

        public Scanning()
        {
            InitializeComponent();
        }

        public void SwitchingTo()
        {
            ProjSettings1.Init(1);
            ProjSettings2.Init(2);
            CamSettings1.Init(1);
            CamSettings2.Init(2);
            LoadPresetConfig(currentConfig);
        }

        internal void AddAnotherProjector(ProjectorSetup sender)
        {
            ProjSettings2.AddAnotherProjector.IsEnabled = false;
            ProjSettings1.AddAnotherProjector.IsEnabled = false;
            ProjSettings1.RemoveProjector.Visibility = System.Windows.Visibility.Visible;
            ProjSettings2.RemoveProjector.Visibility = System.Windows.Visibility.Visible;
            if (ProjSettings2.Visibility == System.Windows.Visibility.Collapsed)
            {
                ProjSettings2.Visibility = System.Windows.Visibility.Visible;
                this.Height += ProjSettings1.ActualHeight;
            }
            if (ProjSettings1.Visibility == System.Windows.Visibility.Collapsed)
            {
                ProjSettings1.Visibility = System.Windows.Visibility.Visible;
                this.Height += ProjSettings1.ActualHeight;
            }
        }

        internal void RemoveProjector(ProjectorSetup projectorSetup)
        {
            ProjSettings2.AddAnotherProjector.IsEnabled = true;
            ProjSettings1.AddAnotherProjector.IsEnabled = true;
            ProjSettings1.RemoveProjector.Visibility = System.Windows.Visibility.Collapsed;
            ProjSettings2.RemoveProjector.Visibility = System.Windows.Visibility.Collapsed;
            if (projectorSetup == ProjSettings2)
            {
                ProjSettings2.Visibility = System.Windows.Visibility.Collapsed;
                this.Height -= ProjSettings1.ActualHeight;
            }
            else
            {
                ProjSettings1.Visibility = System.Windows.Visibility.Collapsed;
                this.Height -= ProjSettings2.ActualHeight;
            }
        }

        internal void AddAnotherCamera(CameraSetup camSetup)
        {
            CamSettings1.AddAnotherCamera.IsEnabled = false;
            CamSettings2.AddAnotherCamera.IsEnabled = false;
            CamSettings1.RemoveCamera.Visibility = System.Windows.Visibility.Visible;
            CamSettings2.RemoveCamera.Visibility = System.Windows.Visibility.Visible;

            if (CamSettings2.Visibility == System.Windows.Visibility.Collapsed)
            {
                CamSettings2.Visibility = System.Windows.Visibility.Visible;
                this.Height += CamSettings1.ActualHeight;
            }
            if (CamSettings1.Visibility == System.Windows.Visibility.Collapsed)
            {
                CamSettings1.Visibility = System.Windows.Visibility.Visible;
                this.Height += CamSettings1.ActualHeight;
            }
        }

        internal void RemoveCamera(CameraSetup camSetup)
        {
            CamSettings1.AddAnotherCamera.IsEnabled = true;
            CamSettings2.AddAnotherCamera.IsEnabled = true;
            CamSettings1.RemoveCamera.Visibility = System.Windows.Visibility.Collapsed;
            CamSettings2.RemoveCamera.Visibility = System.Windows.Visibility.Collapsed;

            if (camSetup == CamSettings2)
            {
                CamSettings2.Visibility = System.Windows.Visibility.Collapsed;
                this.Height -= CamSettings1.ActualHeight;
                if(ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files.Count > 1)
                    ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[1] = "";
                ConfigurationManagement.MainConfiguration.LoadCameraCalibrations();
                ConfigurationManagement.MainConfiguration.Save();
            }
            else
            {
                CamSettings1.Visibility = System.Windows.Visibility.Collapsed;
                this.Height -= CamSettings2.ActualHeight;
                if (ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files.Count > 0)
                    ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[0] = "";
                ConfigurationManagement.MainConfiguration.LoadCameraCalibrations();
                ConfigurationManagement.MainConfiguration.Save();
            }
        }

        private void cancelScan_Click(object sender, RoutedEventArgs e)
        {
            Main parentWindow = (Main)Window.GetWindow(this);
            parentWindow.SwitchPages(PageSelection.Main);
        }

        private List<ManagedCamera> GetCamerasInUse()
        {
            List<ManagedCamera> cams = new List<ManagedCamera>();
            bool multiple = CamSettings1.Visibility == System.Windows.Visibility.Visible && CamSettings2.Visibility == System.Windows.Visibility.Visible;
            if (multiple)
            {
                cams.Add(CamSettings1.SelectedCamera);
                cams.Add(CamSettings2.SelectedCamera);
            }
            else
            {
                if (CamSettings1.Visibility == System.Windows.Visibility.Visible)
                    cams.Add(CamSettings1.SelectedCamera);
                else
                    cams.Add(CamSettings2.SelectedCamera);
            }
            return cams;
        }

        private List<Monitor> GetMonitorsInUse()
        {
            List<Monitor> cams = new List<Monitor>();
            bool multiple = ProjSettings1.Visibility == System.Windows.Visibility.Visible && ProjSettings2.Visibility == System.Windows.Visibility.Visible;
            if (multiple)
            {
                cams.Add(ProjSettings1.SelectedMonitor);
                cams.Add(ProjSettings2.SelectedMonitor);
            }
            else
            {
                if (ProjSettings1.Visibility == System.Windows.Visibility.Visible)
                    cams.Add(ProjSettings1.SelectedMonitor);
                else
                    cams.Add(ProjSettings2.SelectedMonitor);
            }
            return cams;
        }

        private void scanNow_Click(object sender, RoutedEventArgs e)
        {
            var cams = GetCamerasInUse();
            var mons = GetMonitorsInUse();
            foreach (ManagedCamera cam in cams)
            {
                if (cam == null || ConfigurationManagement.MainConfiguration.GetCameraCalibration(cam) == null)
                {
                    MessageBox.Show("You do not have calibrations for all cameras selected.");
                    return;
                }
            }
            foreach (Monitor mon in mons)
            {
                if (mon == null || ConfigurationManagement.MainConfiguration.GetProjectorCalibration(mon) == null)
                {
                    MessageBox.Show("You do not have calibrations for all projectors selected.");
                    return;
                }
            }
            bool multiple = CamSettings1.Visibility == System.Windows.Visibility.Visible && CamSettings2.Visibility == System.Windows.Visibility.Visible;
            ManagedCamera c = cams.Count == 1 ? cams[0] : null;
            Main parentWindow = (Main)Window.GetWindow(this);
            parentWindow.SwitchPages(PageSelection.ScanWindow);
            //parentWindow.ScanWindowPage.Setup(c);
        }

        private void loadPreset_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dia = new Forms.OpenFileDialog();
            dia.Filter = "Preset Files|*.preset";
            dia.AddExtension = true;
            if (dia.ShowDialog() == Forms.DialogResult.OK)
                LoadPresetConfig(dia.FileName);
        }

        private void LoadPresetConfig(string currentConfig)
        {
            this.currentConfig = currentConfig;

            restart:
            if (ConfigurationManagement.MainConfiguration.CameraCalibrations.Count > 0)
            {
                CamSettings1.CamCalibrationFile.Text = ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[0];
                CamSettings1.SetCamera(ConfigurationManagement.MainConfiguration.CameraCalibrations[0].device_id);
                if (CamSettings1.SelectedCamera == null)
                {
                    CamSettings1.CamCalibrationFile.Text = "";
                    ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files.RemoveAt(0);
                    ConfigurationManagement.MainConfiguration.LoadCameraCalibrations();
                    ConfigurationManagement.MainConfiguration.Save();
                    goto restart;
                }
            }

            if (ConfigurationManagement.MainConfiguration.CameraCalibrations.Count > 1)
            {
                CamSettings2.CamCalibrationFile.Text = ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[1];
                CamSettings2.SetCamera(ConfigurationManagement.MainConfiguration.CameraCalibrations[1].device_id);
            }

            currentConfigLabel.Content = string.Format("Current Configuration: {0}", currentConfig);

            if (ConfigurationManagement.MainConfiguration.CameraCalibrations.Count > 1)
                AddAnotherCamera(CamSettings2);

            //ProjSettings1.ProjCalibrationFile.Text = presetConfig.Projector1Calibration;
            //ProjSettings1.SetProjector(presetConfig.Projector1Name);
            ProjSettings1.useBW.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode == 0;
            ProjSettings1.useBayer.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode == 1;
            ProjSettings1.useRGB.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode == 2;
            ProjSettings1.useHorizontalPatterns.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.horizontal_patterns == 1;
            ProjSettings1.useVerticalPatterns.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.vertical_patterns == 1;

            //ProjSettings2.ProjCalibrationFile.Text = presetConfig.Projector2Calibration;
            //ProjSettings2.SetProjector(presetConfig.Projector2Name);
            ProjSettings2.useBW.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode == 0;
            ProjSettings2.useBayer.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode == 1;
            ProjSettings2.useRGB.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.mode == 2;
            ProjSettings2.useHorizontalPatterns.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.horizontal_patterns == 1;
            ProjSettings2.useVerticalPatterns.IsChecked = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.vertical_patterns == 1;

        }

        private PresetConfig GetCurrentConfigs(string fileName)
        {
            PresetConfig config = new PresetConfig(fileName);

            if (CamSettings1.CamCalibrationFile.Text != "")
            {
                ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[0] = CamSettings1.CamCalibrationFile.Text;
            }

            if (CamSettings2.CamCalibrationFile.Text != "")
            {
                ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[1] = CamSettings2.CamCalibrationFile.Text;
            }
            
            config.MultipleCameras = !CamSettings2.AddAnotherCamera.IsEnabled;

            config.Projector1Calibration = ProjSettings1.ProjCalibrationFile.Text;
            config.Projector1Name = ProjSettings1.SelectedMonitor.Name;
            config.Projector1BW = ProjSettings1.useBW.IsChecked == true;
            config.Projector1Bayer = ProjSettings1.useBayer.IsChecked == true;
            config.Projector1RGB = ProjSettings1.useRGB.IsChecked == true;
            config.Projector1PatternHori = ProjSettings1.useHorizontalPatterns.IsChecked == true;
            config.Projector1PatternVert = ProjSettings1.useVerticalPatterns.IsChecked == true;

            config.Projector2Calibration = ProjSettings2.ProjCalibrationFile.Text;
            config.Projector2Name = ProjSettings2.SelectedMonitor.Name;
            config.Projector2BW = ProjSettings2.useBW.IsChecked == true;
            config.Projector2Bayer = ProjSettings2.useBayer.IsChecked == true;
            config.Projector2RGB = ProjSettings2.useRGB.IsChecked == true;
            config.Projector2PatternHori = ProjSettings2.useHorizontalPatterns.IsChecked == true;
            config.Projector2PatternVert = ProjSettings2.useVerticalPatterns.IsChecked == true;

            return config;
        }

        private void savePreset_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.SaveFileDialog dia = new Forms.SaveFileDialog();
            dia.Filter = "Preset Files|*.preset";
            dia.AddExtension = true;
            if(dia.ShowDialog() == Forms.DialogResult.OK)
                GetCurrentConfigs(dia.FileName).Save();
        }
    }

    public class PresetConfig
    {
        public string FileName;
        public bool MultipleCameras;
        public string Camera1Name;
        public string Projector1Name;
        public string Projector1Calibration;
        public bool Projector1BW;
        public bool Projector1Bayer;
        public bool Projector1RGB;
        public bool Projector1PatternVert;
        public bool Projector1PatternHori;
        public string Projector2Name;
        public string Projector2Calibration;
        public bool Projector2BW;
        public bool Projector2Bayer;
        public bool Projector2RGB;
        public bool Projector2PatternVert;
        public bool Projector2PatternHori;
        protected string _path;

        public PresetConfig()
        {
        }

        public PresetConfig(string path)
        {
            _path = path;
            FileName = System.IO.Path.GetFileNameWithoutExtension(_path);
        }

        public static PresetConfig Load(string path)
        {
            if (!System.IO.File.Exists(path))
                return null;
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(PresetConfig));
            var stream = System.IO.File.OpenRead(path);
            PresetConfig config = (PresetConfig)x.Deserialize(stream);
            config._path = path;
            config.FileName = System.IO.Path.GetFileNameWithoutExtension(path);
            stream.Close();
            return config;
        }

        public void Save()
        {
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(GetType());
            var stream = System.IO.File.OpenWrite(_path);
            x.Serialize(stream, this);
            x.Serialize(Console.Out, this);
            stream.Close();
        }
    }

    public enum DeviceCap
    {
        /// <summary>
        /// Device driver version
        /// </summary>
        DRIVERVERSION = 0,
        /// <summary>
        /// Device classification
        /// </summary>
        TECHNOLOGY = 2,
        /// <summary>
        /// Horizontal size in millimeters
        /// </summary>
        HORZSIZE = 4,
        /// <summary>
        /// Vertical size in millimeters
        /// </summary>
        VERTSIZE = 6,
        /// <summary>
        /// Horizontal width in pixels
        /// </summary>
        HORZRES = 8,
        /// <summary>
        /// Vertical height in pixels
        /// </summary>
        VERTRES = 10,
        /// <summary>
        /// Number of bits per pixel
        /// </summary>
        BITSPIXEL = 12,
        /// <summary>
        /// Number of planes
        /// </summary>
        PLANES = 14,
        /// <summary>
        /// Number of brushes the device has
        /// </summary>
        NUMBRUSHES = 16,
        /// <summary>
        /// Number of pens the device has
        /// </summary>
        NUMPENS = 18,
        /// <summary>
        /// Number of markers the device has
        /// </summary>
        NUMMARKERS = 20,
        /// <summary>
        /// Number of fonts the device has
        /// </summary>
        NUMFONTS = 22,
        /// <summary>
        /// Number of colors the device supports
        /// </summary>
        NUMCOLORS = 24,
        /// <summary>
        /// Size required for device descriptor
        /// </summary>
        PDEVICESIZE = 26,
        /// <summary>
        /// Curve capabilities
        /// </summary>
        CURVECAPS = 28,
        /// <summary>
        /// Line capabilities
        /// </summary>
        LINECAPS = 30,
        /// <summary>
        /// Polygonal capabilities
        /// </summary>
        POLYGONALCAPS = 32,
        /// <summary>
        /// Text capabilities
        /// </summary>
        TEXTCAPS = 34,
        /// <summary>
        /// Clipping capabilities
        /// </summary>
        CLIPCAPS = 36,
        /// <summary>
        /// Bitblt capabilities
        /// </summary>
        RASTERCAPS = 38,
        /// <summary>
        /// Length of the X leg
        /// </summary>
        ASPECTX = 40,
        /// <summary>
        /// Length of the Y leg
        /// </summary>
        ASPECTY = 42,
        /// <summary>
        /// Length of the hypotenuse
        /// </summary>
        ASPECTXY = 44,
        /// <summary>
        /// Shading and Blending caps
        /// </summary>
        SHADEBLENDCAPS = 45,

        /// <summary>
        /// Logical pixels inch in X
        /// </summary>
        LOGPIXELSX = 88,
        /// <summary>
        /// Logical pixels inch in Y
        /// </summary>
        LOGPIXELSY = 90,

        /// <summary>
        /// Number of entries in physical palette
        /// </summary>
        SIZEPALETTE = 104,
        /// <summary>
        /// Number of reserved entries in palette
        /// </summary>
        NUMRESERVED = 106,
        /// <summary>
        /// Actual color resolution
        /// </summary>
        COLORRES = 108,

        // Printing related DeviceCaps. These replace the appropriate Escapes
        /// <summary>
        /// Physical Width in device units
        /// </summary>
        PHYSICALWIDTH = 110,
        /// <summary>
        /// Physical Height in device units
        /// </summary>
        PHYSICALHEIGHT = 111,
        /// <summary>
        /// Physical Printable Area x margin
        /// </summary>
        PHYSICALOFFSETX = 112,
        /// <summary>
        /// Physical Printable Area y margin
        /// </summary>
        PHYSICALOFFSETY = 113,
        /// <summary>
        /// Scaling factor x
        /// </summary>
        SCALINGFACTORX = 114,
        /// <summary>
        /// Scaling factor y
        /// </summary>
        SCALINGFACTORY = 115,

        /// <summary>
        /// Current vertical refresh rate of the display device (for displays only) in Hz
        /// </summary>
        VREFRESH = 116,
        /// <summary>
        /// Horizontal width of entire desktop in pixels
        /// </summary>
        DESKTOPVERTRES = 117,
        /// <summary>
        /// Vertical height of entire desktop in pixels
        /// </summary>
        DESKTOPHORZRES = 118,
        /// <summary>
        /// Preferred blt alignment
        /// </summary>
        BLTALIGNMENT = 119
    }
}
