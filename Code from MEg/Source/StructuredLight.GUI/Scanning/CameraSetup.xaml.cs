﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Management;
using Forms = System.Windows.Forms;
using DirectShowLib;
using ImagingScienceProject;

namespace ImagingScienceRendering
{
    /// <summary>
    /// Interaction logic for ProjectorSetup.xaml
    /// </summary>
    public partial class CameraSetup : UserControl
    {
        public ManagedCamera SelectedCamera { get; set; }
        private int _ourInitializationValue = 0;

        public CameraSetup()
        {
            InitializeComponent();
        }

        #region Initialization

        internal void Init(int p)
        {
            AvailableProjectorsLabel.Text = string.Format(AvailableProjectorsLabel.Text, p);
            _ourInitializationValue = p;
        }

        private void InitConfig()
        {
            //Make sure the calibration exists, and that the camera that the calibration is for is connected
            if (CamCalibrationFile.Text != "" && SelectedCamera != null)
            {
                SelectedCameraNameLabel.Text = SelectedCamera.FriendlyName + " (" + SelectedCamera.Name + ")";
                SelectedCamera.ResolutionChanged += new EventHandler(SelectedCamera_ResolutionChanged);
                fpsSettings.Text = Math.Round((1.0 / ((((double)SelectedCamera.CurrentResolution.Header.AvgTimePerFrame) / 10000) / 1000)), 0) + " fps";
                camera_calibration c = ConfigurationManagement.MainConfiguration.GetCameraCalibration(SelectedCamera);
                if (c != null)
                {
                    currentProjCalibrationResolution.Text = c.width + "x" + c.height;
                    if (c.height != SelectedCamera.CurrentResolution.Header.BmiHeader.Height ||
                        c.width != SelectedCamera.CurrentResolution.Header.BmiHeader.Width)
                        resolutionError.Visibility = System.Windows.Visibility.Visible;
                    else
                        resolutionError.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            else
            {
                SelectedCameraNameLabel.Text = "";
                fpsSettings.Text = "";
                currentProjCalibrationResolution.Text = "";

            }
        }

        void SelectedCamera_ResolutionChanged(object sender, EventArgs e)
        {
            InitConfig();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        #endregion

        private void AddAnotherProjector_Click(object sender, RoutedEventArgs e)
        {
            if (DeviceManager.GetCameras().Count == 1)
                MessageBox.Show("You only have one camera connected.");
            else
            {
                Main parentWindow = (Main)Window.GetWindow(this);
                //parentWindow.ScanningPage.AddAnotherCamera(this);
            }
        }

        private void RemoveProjector_Click(object sender, RoutedEventArgs e)
        {
            Main parentWindow = (Main)Window.GetWindow(this);
            //parentWindow.ScanningPage.RemoveCamera(this);
        }

        private void BrowseProjectorCali_Click(object sender, RoutedEventArgs e)
        {
            Forms.OpenFileDialog dia = new Forms.OpenFileDialog();
            dia.CheckFileExists = true;
            dia.Filter = "Camera Config files|*.camcalib|All Files|*.*";
            if (dia.ShowDialog() == Forms.DialogResult.OK)
            {
                CamCalibrationFile.Text = dia.FileName;
                ReadCameraConfig(dia.FileName);
            }
            else
            {
                CamCalibrationFile.Text = "";
                ReadCameraConfig("");
            }
        }

        private void ViewProjectorCali_Click(object sender, RoutedEventArgs e)
        {
            if(CamCalibrationFile.Text != "")
                System.Diagnostics.Process.Start(CamCalibrationFile.Text);
        }

        private void resolutionError_Click(object sender, RoutedEventArgs e)
        {
            camera_calibration c = ConfigurationManagement.MainConfiguration.CameraCalibrations[_ourInitializationValue - 1]    ;
            c.height = SelectedCamera.CurrentResolution.Header.BmiHeader.Height;
            c.width = SelectedCamera.CurrentResolution.Header.BmiHeader.Width;
            ConfigurationManagement.MainConfiguration.Save();
            InitConfig();
        }

        private void ReadCameraConfig(string filename)
        {
            camera_calibration camCalibration = camera_calibration.LoadFromFile<camera_calibration>(filename);
            if (camCalibration == null)
            {
                if(filename != "")
                    MessageBox.Show("This calibration is invalid");
                if (ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files.Count > _ourInitializationValue - 1)
                    ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[_ourInitializationValue - 1] = "";
                InitConfig();
                ConfigurationManagement.MainConfiguration.Save();
                return;
            }
            
            if (ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files.Count <= _ourInitializationValue - 1)
                ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files.Add(filename);
            else
                ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[_ourInitializationValue - 1] = filename;
            
            ConfigurationManagement.MainConfiguration.LoadCameraCalibrations();
            ConfigurationManagement.MainConfiguration.Save();
            DeviceManager.FindDevices(true, true);
            SelectedCamera = ConfigurationManagement.MainConfiguration.GetCamera(ConfigurationManagement.MainConfiguration.CameraCalibrations[_ourInitializationValue - 1]);
            InitConfig();
            if (SelectedCamera == null)
            {
                MessageBox.Show("The camera for this calibration is not connected. Please connect it and then try again");
                ConfigurationManagement.MainConfiguration.cameras.camera_calibration_files[_ourInitializationValue - 1] = "";
            }
            ConfigurationManagement.MainConfiguration.Save();
        }

        private void fpsSettings_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        public void SetCamera(string p)
        {
            ManagedCamera set = null;
            foreach (var cam in DeviceManager.GetCameras())
            {
                if (cam.DevicePath == p)
                    set = cam;
            }
            if (set != null)
                SelectedCamera = set;
            InitConfig();
        }

        private void camProperties_click(object sender, RoutedEventArgs e)
        {
            CameraProperties props = new CameraProperties(SelectedCamera);
            props.Show();
        }
    }
}
