﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms.Integration;
using DirectShowLib;
using System.Windows.Interop;
using ImagingScienceProject;

namespace ImagingScienceRendering
{
    /// <summary>
    /// Interaction logic for ScanWindow.xaml
    /// </summary>
    public partial class ScanWindow : UserControl
    {
        private int _maxFrames = 40;
        private int _currentFrame = 0;
        private VideoPlayer _cameraFrame;
        private VideoPlayer _camera2Frame;
        private ManagedCamera SelectedCamera { get { return (ManagedCamera)cameraPreviewSelection.SelectedItem; } }
        private ManagedCamera _initialSelectedCamera = null;

        public ScanWindow()
        {
            InitializeComponent();
        }

        public void Setup(ManagedCamera selectedCamera)
        {
            _initialSelectedCamera = selectedCamera;
            InitializeCamerasAvailable();

            captureMode.Items.Add("Automatic");
            captureMode.Items.Add("Manual");
            captureMode.SelectedIndex = 0;

            delayTime.Text = ConfigurationManagement.MainConfiguration.scanning_and_reconstruction.frame_delay_ms.ToString();
            UpdateFrames();
        }

        private void SetCameraRenderer()
        {
            bool n = _cameraFrame == null;
            if (_cameraFrame != null)
                _cameraFrame.Dispose();

            _cameraFrame = new VideoPlayer(SelectedCamera);
            cameraPlayer.Child = _cameraFrame;
            if (!n)
                FixCameraRendererSize();
        }

        private void SetAllCameraRenderer()
        {
            bool n = _cameraFrame == null;
            if (_cameraFrame != null)
                _cameraFrame.Dispose();
            if (_camera2Frame != null)
                _camera2Frame.Dispose();
            List<ManagedCamera> c = DeviceManager.GetCameras();
            if(c.Count > 0)
                _cameraFrame = new VideoPlayer(c[0]);
            cameraPlayer.Child = _cameraFrame;
            if (c.Count > 1)
                _camera2Frame = new VideoPlayer(c[1]);
            camera2Player.Child = _camera2Frame;
            if (!n)
                FixCameraRendererSize();
        }

        private void InitializeCamerasAvailable()
        {
            #region Factory
            FrameworkElementFactory tb = new FrameworkElementFactory(typeof(TextBlock));
            tb.SetBinding(TextBlock.TextProperty, new Binding("Name"));
            tb.SetValue(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis);
            tb.SetValue(TextBlock.HorizontalAlignmentProperty, HorizontalAlignment.Left);
            tb.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Left);
            tb.SetValue(TextBlock.MarginProperty, new Thickness(15, 0, 0, 0));
            DataTemplate dt = new DataTemplate();
            //dp.AppendChild(tb);
            //dt.VisualTree = dp;
            dt.VisualTree = tb;
            cameraPreviewSelection.ItemTemplate = dt;
            #endregion

            List<ManagedCamera> cameras = DeviceManager.GetCameras();
            cameras.Add(new ManagedCamera(null) { Name = "All", FriendlyName="All" });
            cameraPreviewSelection.ItemsSource = cameras;
            if (_initialSelectedCamera == null)
                cameraPreviewSelection.SelectedIndex = cameraPreviewSelection.Items.Count - 1;
            else
                cameraPreviewSelection.SelectedItem = _initialSelectedCamera;
            DeviceManager.CamerasChanged += new EventHandler(DeviceManager_CamerasChanged);

            //_capture = new Capture(SelectedCamera);
        }

        void DeviceManager_CamerasChanged(object sender, EventArgs e)
        {
            Dispatcher.Invoke(new Action(delegate()
            {
                List<ManagedCamera> cameras = (List<ManagedCamera>)sender;
                cameras.Add(new ManagedCamera(null) { Name = "All" });
                cameraPreviewSelection.ItemsSource = cameras;
                cameraPreviewSelection.SelectedIndex = 0;
                cameraPreviewSelection.ItemsSource = cameras;
            }));
        }

        private void UpdateFrames()
        {
            framesLabel.Text = string.Format(" {0}/{1} Frames", _currentFrame, _maxFrames); 
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Main parentWindow = (Main)Window.GetWindow(this);
            parentWindow.SwitchPages(PageSelection.Scanning);
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            //ImagingScienceProject.StructuredLightWrapper.Scan(FinishedScanning);
        }

        private void testProjectors_Click(object sender, RoutedEventArgs e)
        {
            //Does a test thing to help setup projector and camera
        }

        private void captureFrame_Click(object sender, RoutedEventArgs e)
        {
            /*if (_currentFrame == 0)
            {
                ImagingScienceProject.StructuredLightWrapper.setUpProjector();
            }
            IntPtr bitmapPtr = _capture.Click();
            System.Drawing.Bitmap b = new System.Drawing.Bitmap(_capture.Width, _capture.Height, _capture.Stride, System.Drawing.Imaging.PixelFormat.Format24bppRgb, bitmapPtr);
            b.Save("capture" + _currentFrame + ".jpg");
            if (!ImagingScienceProject.StructuredLightWrapper.scanNext())
            {
                ImagingScienceProject.StructuredLightWrapper.tearDownProjector();
                ImagingScienceProject.StructuredLightWrapper.processImageSequence("", FinishedScanning);
            }*/
        }

        private void FinishedScanning(string pointCloudFilename)
        {
            //Do Stuff Here...
            Main parentWindow = (Main)Window.GetWindow(this);
            parentWindow.SwitchPages(PageSelection.Viewer);
            //parentWindow.ViewerPage.Setup(new Viewer.Config() { MeshFile = pointCloudFilename });
        }

        private void nextFrame_Click(object sender, RoutedEventArgs e)
        {
            _currentFrame++;
        }

        private void captureMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (captureMode.SelectedItem.ToString() == "Automatic")
            {
                startButton.IsEnabled = true;
                delayTimeLabel.Visibility = System.Windows.Visibility.Visible;
                delayTime.Visibility = System.Windows.Visibility.Visible;
                captureFrame.Visibility = System.Windows.Visibility.Collapsed;
                nextFrame.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                startButton.IsEnabled = false;
                delayTimeLabel.Visibility = System.Windows.Visibility.Collapsed;
                delayTime.Visibility = System.Windows.Visibility.Collapsed;
                captureFrame.Visibility = System.Windows.Visibility.Visible;
                nextFrame.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void cameraPreviewSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CreateCameraRenderer();
        }

        private void CreateCameraRenderer()
        {
            if (SelectedCamera != null)
            {
                if (SelectedCamera.Name == "All")
                    SetAllCameraRenderer();
                else
                    SetCameraRenderer();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            FixCameraRendererSize();
        }

        private void FixCameraRendererSize()
        {
            if (SelectedCamera.Name == "All" && _camera2Frame != null)
            {
                double width = gridLayout.ColumnDefinitions[2].ActualWidth + gridLayout.ColumnDefinitions[3].ActualWidth;
                _cameraFrame.videoWindow.put_Width((int)width / 2);
                _cameraFrame.videoWindow.put_Height((int)cameraPlayer.RenderSize.Height);
                cameraPlayer.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                cameraPlayer.Width = (int)width / 2;
                camera2Player.Visibility = System.Windows.Visibility.Visible;
                _camera2Frame.videoWindow.put_Width((int)width / 2);
                _camera2Frame.videoWindow.put_Height((int)cameraPlayer.RenderSize.Height);
                camera2Player.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                camera2Player.Width = (int)width / 2;
            }
            else
            {
                double width = gridLayout.ColumnDefinitions[2].ActualWidth + gridLayout.ColumnDefinitions[3].ActualWidth;
                cameraPlayer.Width = width;
                camera2Player.Width = width;
                if (_camera2Frame != null)
                {
                    _camera2Frame.Dispose();
                    _camera2Frame = null;
                    camera2Player.Visibility = System.Windows.Visibility.Collapsed;
                }
                _cameraFrame.videoWindow.put_Width((int)width);
                _cameraFrame.videoWindow.put_Height((int)cameraPlayer.RenderSize.Height);
                cameraPlayer.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                camera2Player.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            }
        }

        private void cameraPropertiesButton_Click(object sender, RoutedEventArgs e)
        {
            CameraProperties properties = new CameraProperties(SelectedCamera);
            properties.Show();
        }
    }
}
