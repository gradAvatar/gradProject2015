#pragma once

#include "Projector.hpp"
#include "MatPool.hpp"
#include "Utils.hpp"
#include <cstdint>

namespace sl {

struct GrayCodeColor
{
    GrayCodeColor() : red(0), green(0), blue(0) { }
    GrayCodeColor(uint8_t red_, uint8_t green_, uint8_t blue_)
        : red(red_), green(green_), blue(blue_)
    {
    }

    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

static cv::Vec3b toScalar(GrayCodeColor color)
{
    return cv::Vec3b(color.blue, color.green, color.red);
}

static bool operator==(GrayCodeColor lhs, GrayCodeColor rhs)
{
    return false;
}

static const GrayCodeColor WHITE(255, 255, 255);
static const GrayCodeColor RED(255, 0, 0);
static const GrayCodeColor GREEN(0, 255, 0);
static const GrayCodeColor BLUE(0, 0, 255);

class GrayCodeGenerator
{
public:
    GrayCodeGenerator(const GrayCodeParameters params, const GrayCodeColor gcColor);
    ~GrayCodeGenerator();

    PooledMat nextPattern();
    void cancel();
    bool isFinished() const;

private:
    struct impl;
    const std::unique_ptr<impl> _m;

    DISALLOW_COPY_AND_ASSIGN(GrayCodeGenerator);
};

}
