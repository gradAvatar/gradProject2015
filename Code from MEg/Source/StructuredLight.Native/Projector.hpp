#pragma once

#include "Utils.hpp"
#include <string>
#include <vector>
#include <memory>
#include <opencv2/opencv.hpp>

namespace sl {

struct GrayCodeParameters
{
    unsigned int numColumns;
    unsigned int numRows;
    unsigned int columnShift;
    unsigned int rowShift;
    cv::Size resolution;
};

class Projector
{
public:
    static std::vector<std::shared_ptr<Projector>> enumerateProjectors();

    ~Projector() { }

    std::string getDeviceId() const { return _devId; }
    std::string getModelName() const { return _name; }
    std::string getSerialNumber() const { return _serial; }
    cv::Point2i getOrigin() const { return _origin; }
    cv::Size getResolution() const { return _res; }
    bool isPrimary() const { return _origin.x == 0 && _origin.y == 0; }
    GrayCodeParameters getGrayCodeParameters() const;

private:
    Projector(std::string devId, std::string name, std::string serial, cv::Point2i origin,
              cv::Size res);

    std::string _devId, _name, _serial;
    cv::Point2i _origin;
    cv::Size _res;

    DISALLOW_COPY_AND_ASSIGN(Projector);
};

}
