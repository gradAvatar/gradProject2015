#pragma once

#include "Camera.hpp"
#include "MatPool.hpp"
#include "GrayCodeGenerator.hpp"
#include <memory>

namespace sl {

enum Orientation
{
    ORIENT_COLUMNS,
    ORIENT_ROWS
};

struct ProcessedGrayCodes
{
    cv::Mat_<bool> mask;
    cv::Mat_<ushort> columns, rows;
};

class GrayCodeDetector
{
public:
    GrayCodeDetector(const Camera* const cam, const Projector* const proj,
                     const GrayCodeColor color);
    ~GrayCodeDetector();

    void processImage(PooledMat image);
    void cancel();
    ProcessedGrayCodes finish();

private:
    class impl;
    const std::unique_ptr<impl> _m;

    DISALLOW_COPY_AND_ASSIGN(GrayCodeDetector);
};

}
