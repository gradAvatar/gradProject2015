#pragma once

#include "Camera.hpp"
#include <FlyCapture2.h> // Fine since clients shouldn't be including PointGreyCamera.hpp anyways

namespace pgr = FlyCapture2;

namespace sl {

class PointGreyCamera : public Camera
{
public:
    ~PointGreyCamera();

    std::vector<VideoMode> enumerateVideoModes();
    VideoMode createCustomVideoMode(unsigned int x, unsigned int y, unsigned int width,
                                    unsigned int height, unsigned int fps,
                                    VideoFormat vidFormat) const;

    // Accessors
    std::string getModelName() const;
    std::string getSerialNumber() const;
    VideoMode getVideoMode() const;
    CustomVideoModeConstraints getCustomVideoModeConstraints() const;
    bool getCameraSetting(CameraSetting setting, double* value, CameraSettingFlags* flags);
    bool getCameraSettingRange(CameraSetting setting, double* lower, double* delta, double* upper, double* def);
    bool setCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags);
    // Mutators
    bool setVideoMode(VideoMode mode);

protected:
    bool connect();
    bool disconnect();
    bool capture();
    bool retrieve(cv::Mat &image);

private:
    static void enumerateCameras(std::vector<std::shared_ptr<Camera>> &cams);

    PointGreyCamera(FlyCapture2::PGRGuid guid);

    class impl;
    const std::unique_ptr<impl> _m;

    friend class Camera;
};

}