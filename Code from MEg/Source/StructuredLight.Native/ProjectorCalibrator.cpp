#include "Calibration.hpp"
#include "CameraStream.hpp"
#include "Exceptions.hpp"
#include "Timer.hpp"
#include "Config.hpp"
#include "Serialization.hpp"
#include "GuiWindow.hpp"

namespace sl {

bool ProjectorCalibrator::calibrationImpl(IntrinsicCalibration &results)
{
    std::shared_ptr<CameraStream> stream = getCamera()->openStream();

    StreamLock streamLock(stream.get(), MANUAL);
    if (!streamLock.isLockActive())
    {
        throw streamModeLockFailedEx;
    }

    stream->start();

    cv::Size patternSize(Config::getCircleRows(), Config::getCircleColumns());
    cv::Size projRes = _device->getResolution();

    std::vector<cv::Point2f> circlePoints;

    cv::Mat grid = generateAsymmGrid(projRes, patternSize, Config::getProjectedCircleSpacing(),
                                     Config::getProjectedCircleDiameter(), circlePoints);
    cv::Mat white = cv::Mat(_device->getResolution(), CV_8UC1, cv::Scalar(255));
    cv::Mat img, grayImg, grayProjImg;

    GuiWindow window(_device);
    window.setImage(white);

    std::vector<std::vector<cv::Point2f>> physicalImgPts, projectedImgPts;

    Timer t;

    unsigned int i = 0;
    bool foundCircles = false;
    while (i < getTargetAcceptedCaptures() && !isCancelRequested())
    {
        window.setImage(white);
        Sleep(Config::getFrameDelay());

        cv::Mat img;
        while (!stream->triggerWaitRetrieve(img))
        {
        }

        std::vector<cv::Point2f> physCircles;
        bool found = cv::findCirclesGrid(img, patternSize, physCircles, cv::CALIB_CB_ASYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING);

        if (found)
        {
            // Display the projected circle grid and give the camera some time to refresh
            window.setImage(grid);
            Sleep(Config::getFrameDelay());

            cv::Mat projImg;
            while (!stream->triggerWaitRetrieve(projImg))
            {
            }

            // Isolate the projected circle grid by subtracting the
            // white calibration board from the black projected circle grid
            if (getCamera()->getVideoMode().videoFormat == VID_RGB24)
            {
                /*std::vector<cv::Mat> split;
                cv::split(img, split);
                grayImg = split[1];

                cv::split(projImg, split);
                grayProjImg = split[1];*/
                cv::cvtColor(img, grayImg, CV_RGB2GRAY);
                cv::cvtColor(projImg, grayProjImg, CV_RGB2GRAY);
            }
            else
            {
                grayImg = img;
                grayProjImg = projImg;
            }

            grayImg -= grayProjImg;

            // Set the image to only black or white (i.e. only 0 or 255) to
            // prevent the calibration board from interfering with the projected grid
            grayImg = grayImg >= Config::getContrastThreshold();
            grayImg ^= 255; // Invert

            // Find the projected circle grid
            std::vector<cv::Point2f> projCircles;
            bool foundProj = cv::findCirclesGrid(grayImg, patternSize, projCircles, cv::CALIB_CB_ASYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING);

            if (getCamera()->getVideoMode().videoFormat == VID_GRAY8)
            {
                // Convert to color before drawing the corners
                cv::cvtColor(grayProjImg, projImg, CV_GRAY2RGB);
            }

            cv::drawChessboardCorners(projImg, patternSize, projCircles, foundProj);

            if (foundProj && t.elapsed() > boost::chrono::seconds(3))
            {
                cv::bitwise_not(projImg, projImg);
                foundCircles = true;
                i++;

                // Save the camera and projector points
                physicalImgPts.push_back(physCircles);
                projectedImgPts.push_back(projCircles);

                t.reset();
            }

            img = projImg;

            // Clear the projected image
            window.setImage(white);
            Sleep(Config::getFrameDelay());
        }

        CalibrationPreviewData status = {};
        status.currentPreview = img;
        status.numAcceptedCaptures = i;
        status.currentPreviewWasAccepted = foundCircles;

        invokePreviewCallbacks(status);

        foundCircles = false;
    }

    if (isCancelRequested())
    {
        return false;
    }

    std::vector<cv::Point2f> objectPoints = generateCircleGridObjectPoints(patternSize, Config::getCircleDiameter(), Config::getCircleSpacing());

    std::vector<std::vector<cv::Point3f>> objectPtsCollection;
    std::vector<std::vector<cv::Point2f>> finalCirclePts;

    IntrinsicCalibration camCalib;
    if (!loadIntrinsicCalibration(getCamera(), &camCalib))
    {
        PRINT_WARN("intrinsic camera calibration not found!");
        return false;
    }

    for (unsigned int i = 0; i < getTargetAcceptedCaptures(); i++)
    {
        // Undistort the image points from both the camera and projector
        std::vector<cv::Point2f> physicalRectImgPts, projectedRectImgPts;
        cv::undistortPoints(physicalImgPts[i], physicalRectImgPts, camCalib.camera,
                            camCalib.distortion);
        cv::undistortPoints(projectedImgPts[i], projectedRectImgPts, camCalib.camera,
                            camCalib.distortion);

        cv::Mat homography = cv::findHomography(physicalRectImgPts, objectPoints);

        std::vector<cv::Point2f> projDst;
        cv::perspectiveTransform(projectedRectImgPts, projDst, homography);

        std::vector<cv::Point3f> objPts, objPoints2;
        for (unsigned int j = 0; j < projDst.size(); j++)
        {
            objPoints2.push_back(cv::Point3f(objectPoints[j].x, objectPoints[j].y, 0));
            objPts.push_back(cv::Point3f(projDst[j].x, projDst[j].y, 0));
        }

        objectPtsCollection.push_back(objPts);
        finalCirclePts.push_back(circlePoints);
    }

    cv::Mat projIntrinsic, projDistortion;
    std::vector<cv::Mat> rvecs, tvecs;
    double error = cv::calibrateCamera(objectPtsCollection, finalCirclePts,
                                       _device->getResolution(), projIntrinsic, projDistortion,
                                       rvecs, tvecs, CV_CALIB_FIX_K3);

    IntrinsicCalibration calib;
    calib.camera = projIntrinsic;
    calib.distortion = projDistortion;
    calib.reprojectionError = error;

    results = calib;
    return true;
}

//
// ProjectorCalibrator stubs
//
ProjectorCalibrator::ProjectorCalibrator(Camera* const cam, Projector* const device)
    : Calibrator(cam), _device(device)
{
}

ProjectorCalibrator::~ProjectorCalibrator() { }

}
