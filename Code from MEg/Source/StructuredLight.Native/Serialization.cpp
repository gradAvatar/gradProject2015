#include "Serialization.hpp"
#include "Calibration.hpp"
#include "Camera.hpp"
#include "Config.hpp"
#include "Projector.hpp"
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

namespace {

static const std::string DEVICES_XML_NAME = "devices.xml";

inline std::string getExtension(const sl::Camera* const dev) { return ".camera.xml"; }
inline std::string getExtension(const sl::Projector* const dev) { return ".proj.xml"; }

inline std::string getDeviceTypeStr(const sl::Camera* const dev) { return "camera"; }
inline std::string getDeviceTypeStr(const sl::Projector* const dev) { return "projector"; }

template <typename TDevice>
inline bool typeMatchesDeviceType(const std::string& type, const TDevice* const dev)
{
    return type == getDeviceTypeStr(dev);
}

void getCalibrationFiles(const std::string& ext, std::vector<std::string>& files)
{
    fs::path calibFolder = fs::absolute(sl::Config::getCalibrationFolder());

    fs::directory_iterator end;
    for (fs::directory_iterator it(calibFolder); it != end; it++)
    {
        std::string fileName = it->path().string();
        if (std::equal(ext.rbegin(), ext.rend(), fileName.rbegin()))
        {
            files.push_back(fileName);
        }
    }
}

std::string createFileName(std::string ext)
{
    // Note on the double parenthesis: the first set creates a
    // random_generator, the second set calls operator(), creating a UUID
    std::string guid = boost::lexical_cast<std::string>(boost::uuids::random_generator()());

    fs::path file = guid + ext;
    file = sl::Config::getCalibrationFolder() / file;
    return file.generic_string();
}

template <typename TDevice>
bool findIntrinsicCalibration(const TDevice* const dev, cv::FileStorage* const readFs = nullptr,
                              std::string* const calibFileName = nullptr)
{
    std::vector<std::string> files;
    getCalibrationFiles(getExtension(dev), files);

    for (auto it = begin(files); it != end(files); ++it)
    {
        // Found a file, check its serial number
        cv::FileStorage fs(*it, cv::FileStorage::READ);

        std::string calibSerial;
        fs["serial"] >> calibSerial;

        if (calibSerial == dev->getSerialNumber())
        {
            // Found the calibration
            if (calibFileName != nullptr)
            {
                *calibFileName = *it;
            }
            
            if (readFs != nullptr)
            {
                *readFs = fs;
            }

            return true;
        }
    }
 
    return false;
}

template <typename TSecondDevice>
bool findExtrinsicCalibrationFile(const sl::Camera* const cam,
                                  const TSecondDevice* const proj,
                                  const std::string& extension, const std::string& first,
                                  const std::string& second, cv::FileStorage* readFs = nullptr,
                                  std::string* fileName = nullptr)
{
    std::vector<std::string> files;
    getCalibrationFiles(extension, files);

    for (auto it = begin(files); it != end(files); it++)
    {
        cv::FileStorage fs(*it, cv::FileStorage::READ);

        std::string camSerial, projSerial;
        fs["serials"][first] >> camSerial;
        fs["serials"][second] >> projSerial;
        
        if (camSerial == cam->getSerialNumber() &&
            projSerial == proj->getSerialNumber())
        {
            if (readFs != nullptr)
            {
                *readFs = fs;
            }

            if (fileName != nullptr)
            {
                *fileName = *it;
            }

            return true;
        }
    }

    return false;
}

template <typename TDevice>
bool loadIntrinsicCalibrationImpl(const TDevice* const cam, sl::IntrinsicCalibration* const calib)
{
    cv::FileStorage fs;
    if (!findIntrinsicCalibration(cam, &fs))
    {
        return false;
    }

    if (calib == nullptr)
    {
        // Found the calibration, but no IntrinsicCalibration instance was provided
        return true;
    }

    cv::Mat camera, distortion;
    fs["camera"] >> camera;
    fs["distortion"] >> distortion;
    calib->camera = camera;
    calib->distortion = distortion;

    fs["reprojectionError"] >> calib->reprojectionError;

    return true;
}

template <typename TDevice>
void saveIntrinsicCalibrationImpl(const TDevice* const dev, const sl::IntrinsicCalibration calib)
{
    std::string fileName;
    if (!findIntrinsicCalibration(dev, nullptr, &fileName))
    {
        // Create a new calibration file
        fileName = createFileName(getExtension(dev));
    }

    cv::FileStorage writeFs(fileName, cv::FileStorage::WRITE);
    
    writeFs << "serial" << dev->getSerialNumber();
    writeFs << "camera" << cv::Mat(calib.camera);
    writeFs << "distortion" << cv::Mat(calib.distortion);
    writeFs << "reprojectionError" << calib.reprojectionError;
}

template <typename TDevice>
bool getFriendlyNameImpl(const TDevice* const dev, std::string* const name)
{
    cv::FileStorage fs(DEVICES_XML_NAME, cv::FileStorage::READ);

    auto root = fs["devices"];
    for (auto it = root.begin(); it != root.end(); it++)
    {
        auto node = *it;
        if (!typeMatchesDeviceType(node["device_type"], dev))
        {
            // Not the device type we want
            continue;
        }

        if (static_cast<std::string>(node["serial_number"]) == dev->getSerialNumber())
        {
            // Found the device
            if (name != nullptr)
            {
                *name = node["friendly_name"];
            }

            return true;
        }
    }

    return false;
}

void writeFriendlyName(cv::FileStorage& fs, const std::string& devType, const std::string& serial,
                       const std::string& name)
{
    fs << 
        "{" <<
            "device_type" << devType <<
            "serial_number" << serial <<
            "friendly_name" << name <<
        "}";
}

template <typename TDevice>
void setFriendlyNameImpl(const TDevice* const dev, const std::string& name)
{
    cv::FileStorage fs(DEVICES_XML_NAME, cv::FileStorage::READ);
    cv::FileStorage outputTemp(DEVICES_XML_NAME, cv::FileStorage::WRITE);
    
    outputTemp << "devices" << "[";

    auto root = fs["devices"];
    for (auto it = root.begin(); it != root.end(); it++)
    {
        auto node = *it;
        
        if (!(typeMatchesDeviceType(node["device_type"], dev) &&
              static_cast<std::string>(node["serial_number"]) == dev->getSerialNumber()))
        {
            writeFriendlyName(outputTemp, node["device_type"], node["serial_number"],
                              node["friendly_name"]);
        }
    }

    writeFriendlyName(outputTemp, getDeviceTypeStr(dev), dev->getSerialNumber(), name);

    outputTemp << "]";
}

}

namespace sl {

bool loadIntrinsicCalibration(const Camera* const cam, IntrinsicCalibration* const calib)
{
    return loadIntrinsicCalibrationImpl(cam, calib);
}

bool loadIntrinsicCalibration(const Projector* const proj, IntrinsicCalibration* const calib)
{
    return loadIntrinsicCalibrationImpl(proj, calib);
}

void saveIntrinsicCalibration(const Camera* const cam, const IntrinsicCalibration& calib)
{
    return saveIntrinsicCalibrationImpl(cam, calib);
}

void saveIntrinsicCalibration(const Projector* const proj, const IntrinsicCalibration& calib)
{
    return saveIntrinsicCalibrationImpl(proj, calib);
}

bool loadCamProjExtrinsicCalibration(const Camera* const cam, const Projector* const proj,
                                     CamProjExtrinsicCalibration* const calib)
{
    cv::FileStorage fs;
    if (!findExtrinsicCalibrationFile(cam, proj, ".extrinsic.xml", "cam", "proj", &fs))
    {
        return false;
    }

    if (calib == nullptr)
    {
        // Found a calibration, but they didn't want the data anyways
        return true;
    }

    cv::Mat camExt, projExt;
    fs["cam"] >> camExt;
    fs["proj"] >> projExt;

    calib->camExtrinsic = camExt;
    calib->projExtrinsic = projExt;
    return true;
}

void saveCamProjExtrinsicCalibration(const Camera* const cam, const Projector* const proj,
                                     const CamProjExtrinsicCalibration& calib)
{
    std::string fileName;
    if (!findExtrinsicCalibrationFile(cam, proj, ".extrinsic.xml", "cam", "proj", nullptr,
                                      &fileName))
    {
        // Create a new calibration
        fileName = createFileName(".extrinsic.xml");
    }

    cv::FileStorage fs(fileName, cv::FileStorage::WRITE);

    fs << "serials" << "{" << "cam" << cam->getSerialNumber() << 
        "proj" << proj->getSerialNumber() << "}";

    fs << "cam" << cv::Mat(calib.camExtrinsic);
    fs << "proj" << cv::Mat(calib.projExtrinsic);
}

bool loadCamCamExtrinsicCalibration(const Camera* const source, const Camera* const target,
                                    CamCamExtrinsicCalibration* const calib)
{
    cv::FileStorage fs;
    if (!findExtrinsicCalibrationFile(source, target, ".extrinsic.xml", "source", "target", &fs))
    {
        return false;
    }

    if (calib == nullptr)
    {
        // Found a calibration, but they didn't want the data anyways
        return true;
    }

    cv::Mat rotation, translation;
    fs["rotation"] >> rotation;
    fs["translation"] >> translation;

    calib->rotation = rotation;
    calib->translation = translation;

    return true;
}

void saveCamCamExtrinsicCalibration(const Camera* const source, const Camera* const target,
                                    const CamCamExtrinsicCalibration& calib)
{
    std::string fileName;
    if (!findExtrinsicCalibrationFile(source, target, ".extrinsic.xml", "source", "target",
                                      nullptr, &fileName))
    {
        // Create a new calibration
        fileName = createFileName(".extrinsic.xml");
    }

    cv::FileStorage fs(fileName, cv::FileStorage::WRITE);

    fs << "serials" << "{" << "source" << source->getSerialNumber() << 
        "target" << target->getSerialNumber() << "}";

    fs << "rotation" << cv::Mat(calib.rotation);
    fs << "translation" << cv::Mat(calib.translation);
}

bool getFriendlyName(const Camera* const cam, std::string* const name)
{
    return getFriendlyNameImpl(cam, name);
}

bool getFriendlyName(const Projector* const proj, std::string* const name)
{
    return getFriendlyNameImpl(proj, name);
}

void setFriendlyName(const Camera* const cam, const std::string& name)
{
    setFriendlyNameImpl(cam, name);
}

void setFriendlyName(const Projector* const proj, const std::string& name)
{
    setFriendlyNameImpl(proj, name);
}

}
