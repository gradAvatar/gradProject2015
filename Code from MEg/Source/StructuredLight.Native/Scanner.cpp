#include "Scanner.hpp"
#include "CameraStream.hpp"
#include "Exceptions.hpp"
#include "GrayCodeDetector.hpp"
#include "Calibration.hpp"
#include "Serialization.hpp"
#include "Config.hpp"
#include "GrayCodeGenerator.hpp"
#include "GuiWindow.hpp"
#include "MeshGeneration.hpp"
#include <algorithm>
#include <fstream>
#include <boost/timer/timer.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <tbb/compat/thread>

using namespace std::placeholders;

namespace {

cv::Mat_<double> computeOpticalRays(cv::Matx33d intrinsic, cv::Matx15d distortion, cv::Size resolution);

bool generateReconstructionParams(sl::Camera* cam, sl::Projector* proj, cv::Mat_<double>& camRays,
                                  cv::Point3d& camCenter, cv::Mat_<double>& projRays,
                                  cv::Point3d& projCenter);

cv::Point3d intersectLineWithLine3D(const cv::Point3d& q1, const cv::Point3d& v1,
                                    const cv::Point3d& q2, const cv::Point3d& v2);

bool constructPointCloud(sl::Camera* cam, sl::Projector* proj, sl::ProcessedGrayCodes codes,
                         std::vector<cv::Point3d>& points);

struct CameraProjectorPair
{
    CameraProjectorPair(sl::Camera* const cam_, sl::Projector* const proj_,
                        sl::GrayCodeColor scanColor_)
        : cam(cam_), proj(proj_), gcParams(proj_->getGrayCodeParameters()), scanColor(scanColor_),
          numPatterns(2 * (gcParams.numColumns + gcParams.numRows))
    {
    }

    ~CameraProjectorPair()
    {
        releaseAll();
    }

    void releaseAll()
    {
        streamLock.reset();
        stream.reset();
        detector.reset();
        gen.reset();
        window.reset();
        camCapturePool.reset();

        processed.mask.release();
        processed.rows.release();
        processed.columns.release();
    }

    sl::Camera* const cam;
    sl::Projector* const proj;
    const sl::GrayCodeParameters gcParams;
    const sl::GrayCodeColor scanColor;
    const unsigned int numPatterns;

    std::shared_ptr<sl::CameraStream> stream;
    std::unique_ptr<sl::StreamLock> streamLock;
    std::unique_ptr<sl::GrayCodeDetector> detector;
    std::unique_ptr<sl::GrayCodeGenerator> gen;
    std::unique_ptr<sl::GuiWindow> window;
    std::unique_ptr<sl::MatPool> camCapturePool;
    sl::ProcessedGrayCodes processed;

    DISALLOW_COPY_AND_ASSIGN(CameraProjectorPair);
};

}

namespace sl {

class Scanner::impl
{
public:
    impl()
        : _scanning(false), _cancelRequested(false), _outputFileName("output.ply")
    {
        _previewCbs.setPreInvokeFunc([](ScanPreviewData data)
        {
            for (unsigned int i = 0; i < data.getPreviewCount(); i++)
            {
                data._previews[i].addRef();
            }
        });

        _previewCbs.setPostInvokeFunc([](ScanPreviewData data)
        {
            for (unsigned int i = 0; i < data.getPreviewCount(); i++)
            {
                data._previews[i].removeRef();
            }
        });
    }

    void _processingThread();
    void _initPair(CameraProjectorPair& pair);
    void _releasePairResources();

    boost::ptr_vector<CameraProjectorPair> _camProjPairs;
    CallbackManager<ScanPreviewData> _previewCbs;
    CallbackManager<void> _finishedCbs;
    std::thread _thread;
    bool _scanning, _cancelRequested;
    std::string _outputFileName;
};

Scanner::Scanner(Camera* const cam, Projector* const proj)
    : _m(new impl())
{
    auto pair = new CameraProjectorPair(cam, proj, WHITE);
    _m->_camProjPairs.push_back(pair);
}

Scanner::Scanner(std::vector<Camera*> cams, std::vector<Projector*> projs,
                 std::vector<GrayCodeColor> colors)
    : _m(new impl())
{
    if (cams.size() == 1 && colors.size() == 0)
    {
        // White scanning only
        auto pair = new CameraProjectorPair(cams[0], projs[0], WHITE);
        _m->_camProjPairs.push_back(pair);
        return;
    }

    static const GrayCodeColor defaultColors[3] = { RED, GREEN, BLUE };
    for (unsigned int i = 0; i < cams.size(); i++)
    {
        GrayCodeColor color = colors.size() == 0 ? defaultColors[i] : colors[i];

        auto pair = new CameraProjectorPair(cams[i], projs[i], color);
        _m->_camProjPairs.push_back(pair);
    }
}

Scanner::~Scanner()
{
}

void Scanner::scan()
{
    _m->_scanning = true;
    _m->_thread = std::thread([this]
    {
        _m->_processingThread();
        _m->_scanning = false;
        _m->_finishedCbs.invoke();
    });
}

void Scanner::cancel()
{
    if (_m->_thread.joinable())
    {
        _m->_cancelRequested = true;
        _m->_thread.join();
    }
}

void Scanner::impl::_processingThread()
{
    std::for_each(begin(_camProjPairs), end(_camProjPairs), std::bind(&impl::_initPair, this, std::placeholders::_1));

    /* In regards to skipping the first image (as seen in the for loop below):
     *
     * The speed increases seen with using OpenGL as the display backend are sometimes _too_ fast;
     * so fast, in fact, that the camera will pick up the image on the projector that was displayed
     * before the one that is being displayed at the time the camera capture is requested. To
     * deal with this, we ignore the first image we get, and then capture an extra image at the
     * end of the scanning sequence. While this does mean we have to capture one additional image,
     * the increase in speed over the OpenCV backend makes it worth it.
     */

    boost::timer::cpu_timer timer;
    unsigned int maxIters = std::max_element(begin(_camProjPairs), end(_camProjPairs),
        [this](CameraProjectorPair const& pair1, CameraProjectorPair const& pair2)
        {
            return pair1.numPatterns < pair2.numPatterns;
        })->numPatterns;

    for (unsigned int i = 0; i < maxIters + 1; i++)
    {
        if (_cancelRequested)
        {
            _releasePairResources();
            return;
        }

        for (auto pair = begin(_camProjPairs); pair != end(_camProjPairs); pair++)
        {
            if (i > pair->numPatterns)
            {
                // No more patterns to scan
                continue;
            }

            if (i < pair->numPatterns)
            {
                PooledMat mat = pair->gen->nextPattern();
                pair->window->setImage(*mat);
                mat.removeRef();
            }

            pair->stream->trigger();
        }

        std::vector<PooledMat> previews;
        for (auto pair = begin(_camProjPairs); pair != end(_camProjPairs); pair++)
        {
            if (i > pair->numPatterns)
            {
                // No more patterns to scan
                continue;
            }

            PooledMat pooled = pair->camCapturePool->acquireMat();
            pair->stream->waitForFrame();
            if (!pair->stream->retrieveLastCapture(*pooled))
            {
                // Image grab error, retry up to two times
                bool success = false;
                for (unsigned int j = 0; j < 2 && !success; j++)
                {
                    success = pair->stream->triggerWaitRetrieve(*pooled);
                }

                if (!success)
                {
                    // Failed grabbing an image three times, fail the entire scan
                    THROW_EX(scanImageCaptureFailedEx);
                }
            }

            if (i == 0)
            {
                // Ignore the first image
                pooled.removeRef();
                Sleep(20);
                continue;
            }

            previews.push_back(pooled);
            pooled.addRef();

            pair->detector->processImage(pooled);

            if (i == pair->numPatterns)
            {
                // End of the scan sequence
                pair->window->clear();
            }
        }

        if (previews.size() > 0) 
        {
            ScanPreviewData data(previews, i - 1, maxIters);
            _previewCbs.invoke(data);

            for (unsigned int i = 0; i < previews.size(); i++)
            {
                previews[i].removeRef();
            }
        }
    }

    std::cout << timer.format() << std::endl;
    
    // Release memory resources as soon as possible
    for (auto pair = begin(_camProjPairs); pair != end(_camProjPairs); pair++)
    {
        pair->processed = pair->detector->finish();
        pair->window->close();
        pair->camCapturePool->reset();
    }

    std::vector<cv::Point3d> points;
    constructPointCloud(_camProjPairs[0].cam, _camProjPairs[0].proj, _camProjPairs[0].processed, points);

    std::stringstream ss;
    ss << "0_" << _outputFileName;
	pcl::PointCloud<pcl::PointNormal>::Ptr ptNormals = MeshGeneration::calculateSmoothNormals(
		MeshGeneration::convertCloudToPCL(points).makeShared());
	pcl::io::savePLYFile(ss.str(), *ptNormals);

	pcl::PointCloud<pcl::PointNormal> combinedCloud;
	combinedCloud += *ptNormals;
	
	unsigned int i = 1;
    for (auto pair = ++begin(_camProjPairs); pair != end(_camProjPairs); pair++)
    {
        if (_cancelRequested)
        {
            _releasePairResources();
            return;
        }

        std::vector<cv::Point3d> pts;
        constructPointCloud(pair->cam, pair->proj, pair->processed, pts);
		
#if DEBUG
        // Save unaligned first
        std::stringstream ss3;
        ss3 << i << "_" << _outputFileName;
		MeshGeneration::savePointCloudPLY(ss3.str(), pts);
#endif

        CamCamExtrinsicCalibration camcamCalib;
        if (loadCamCamExtrinsicCalibration(pair->cam, _camProjPairs[0].cam, &camcamCalib))
        {
            // Align the two clouds
            /*for (unsigned int pt = 0; pt < pts->size(); pt++)
            {
				cv::Vec3d ptMat(pts[pt]);
                ptMat = camcamCalib.rotation * ptMat;
                ptMat += camcamCalib.translation;
                pts[pt] = ptMat;
            }*/
			pcl::PointCloud<pcl::PointNormal> normals = *MeshGeneration::calculateSmoothNormals(
				MeshGeneration::convertCloudToPCL(pts).makeShared());
			for (unsigned int pt = 0; pt < normals.size(); pt++)
            {
				pcl::PointNormal nm = normals[pt];
				cv::Vec3d ptMat(nm.x, nm.y, nm.z);
                ptMat = camcamCalib.rotation * ptMat;
                ptMat += camcamCalib.translation;
				nm.x = (float)ptMat.val[0];
				nm.y = (float)ptMat.val[1];
				nm.z = (float)ptMat.val[2];

				cv::Vec3d nmMat(nm.normal_x, nm.normal_y, nm.normal_z);
                nmMat = camcamCalib.rotation * nmMat;

				nm.normal_x = (float)nmMat.val[0];
				nm.normal_y = (float)nmMat.val[1];
				nm.normal_z = (float)nmMat.val[2];
				normals[pt] = nm;
            }
            std::stringstream ss2;
            ss2 << i << "_aligned_" << _outputFileName;
			pcl::io::savePLYFile(ss2.str(), normals);
            std::stringstream ss22;
            ss22 << i << "_aligned_vcg_" << _outputFileName;
			pcl::PointCloud<pcl::PointNormal> alignedNormals = PointCloudAlignment::alignPointCloud(ptNormals,
				normals.makeShared());
			pcl::io::savePLYFile(ss22.str(), alignedNormals);
			combinedCloud += alignedNormals;
			//For no vcg alignment, this is needed
			//combinedCloud += normals;
        }

        i++;
    }
	//Now save all clouds with normals combined
	std::stringstream ssa;
    ssa << "all_aligned_" << _outputFileName;
	pcl::io::savePLYFile(ssa.str(), combinedCloud);
	pcl::io::savePLYFile(_outputFileName, MeshGeneration::poissonReconstruction(combinedCloud.makeShared()));
}

void Scanner::impl::_initPair(CameraProjectorPair& pair)
{
    // Synchronize with a monitor (projector) refreshing at 60Hz, in milliseconds
    pair.cam->setCameraSetting(SHUTTER, 1000 / 60.0, MANUALSET);

    pair.stream = pair.cam->openStream();
    pair.streamLock = std::unique_ptr<StreamLock>(new StreamLock(pair.stream.get(), MANUAL));
    if (!pair.streamLock->isLockActive())
    {
        THROW_EX(streamModeLockFailedEx);
    }

    pair.stream->start();

    pair.detector = std::unique_ptr<GrayCodeDetector>(
        new GrayCodeDetector(pair.cam, pair.proj, pair.scanColor));

    pair.gen = std::unique_ptr<GrayCodeGenerator>(
        new GrayCodeGenerator(pair.gcParams, pair.scanColor));

    pair.window = std::unique_ptr<GuiWindow>(new GuiWindow(pair.proj));

    pair.camCapturePool = std::unique_ptr<MatPool>(
        new MatPool(pair.cam->getResolution(), CV_8UC3));
}

void Scanner::impl::_releasePairResources()
{
    for (unsigned int i = 0; i < _camProjPairs.size(); i++)
    {
        _camProjPairs[i].releaseAll();
    }
}

CallbackToken Scanner::addPreviewCallback(ScanPreviewCallback cb)
{
    return _m->_previewCbs.add(cb);
}

void Scanner::removePreviewCallback(CallbackToken token)
{
    _m->_previewCbs.remove(token);
}

CallbackToken Scanner::addFinishedCallback(ScanCompleteCallback cb)
{
    return _m->_finishedCbs.add(cb);
}

void Scanner::removeFinishedCallback(CallbackToken token)
{
    _m->_finishedCbs.remove(token);
}

std::string Scanner::getOutputFileName()
{
    return _m->_outputFileName;
}

bool Scanner::isScanning()
{
    return _m->_scanning;
}

void Scanner::setOutputFileName(std::string const& fileName)
{
    if (_m->_scanning)
    {
        THROW_EX(scanInProgressEx);
    }

    _m->_outputFileName = fileName;
}

}

namespace {

cv::Mat_<double> computeOpticalRays(cv::Matx33d intrinsic, cv::Matx15d distortion, cv::Size resolution)
{
    int numElems = resolution.area();
    cv::Mat_<cv::Point2d> points(numElems, 1);
    for (int r = 0; r < resolution.height; r++)
    {
        for (int c = 0; c < resolution.width; c++)
        {
            points(resolution.width * r + c, 0) = cv::Point2d(c, r);
        }
    }

    cv::undistortPoints(points, points, intrinsic, distortion);

    cv::Mat_<double> rays(3, resolution.area());
    double* data = (double*)rays.data;
    for (int i = 0; i < numElems; i++)
    {
        cv::Point2d pd = points(i, 0);

        // Normalize the vector
        double norm = sqrt(pd.x * pd.x + pd.y * pd.y + 1);
        data[i] = pd.x / norm;
        data[i + numElems] = pd.y / norm;
        data[i + 2 * numElems] = 1 / norm;
    }

    return rays;
}

bool generateReconstructionParams(sl::Camera* cam, sl:: Projector* proj, cv::Mat_<double>& camRays,
                                  cv::Point3d& camCenter, cv::Mat_<double>& projRays,
                                  cv::Point3d& projCenter)
{
    sl::IntrinsicCalibration camCalib, projCalib;
    if (!loadIntrinsicCalibration(cam, &camCalib) || !loadIntrinsicCalibration(proj, &projCalib))
    {
        return false;
    }

    sl::CamProjExtrinsicCalibration extrinsic;
    if (!loadCamProjExtrinsicCalibration(cam, proj, &extrinsic))
    {
        return false;
    }

    cv::Matx33d camRotation = extrinsic.getCamRotation();
    cv::Matx31d camTranslation = extrinsic.getCamTranslation();
    cv::Matx33d projRotation = extrinsic.getProjRotation();
    cv::Matx31d projTranslation = extrinsic.getProjTranslation();

    camCenter = cv::Point3d(0.0, 0.0, 0.0);
    camRays = computeOpticalRays(camCalib.camera, camCalib.distortion, cam->getResolution());

    // Determine the center of the projector (in camera coordinates)
    cv::Matx31d projCenterMat = camRotation * -projRotation.t() * projTranslation + camTranslation;
    projCenter = cv::Point3d(projCenterMat(0, 0), projCenterMat(1, 0), projCenterMat(2, 0));

    // Rotate projector optical rays into the camera coordinate system
    projRays = computeOpticalRays(projCalib.camera, projCalib.distortion, proj->getResolution());
    cv::Matx33d R = camRotation * projRotation.t();
    projRays = cv::Mat(R) * projRays;

    return true;
}

cv::Point3d intersectLineWithLine3D(const cv::Point3d& q1, const cv::Point3d& v1,
                                    const cv::Point3d& q2, const cv::Point3d& v2)
{
    // http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline3d/
    double v1_dot_v1 = v1.dot(v1), v2_dot_v2 = v2.dot(v2), v1_dot_v2 = v1.dot(v2),
           q12_dot_v1 = (q1 - q2).dot(v1), q12_dot_v2 = (q1 - q2).dot(v2);

    // Calculate scale factors.
    double s, t, denom;
    denom = v1_dot_v1*v2_dot_v2 - v1_dot_v2*v1_dot_v2;
    s = (v1_dot_v2/denom)*q12_dot_v2 - (v2_dot_v2/denom)*q12_dot_v1;
    t = (v1_dot_v1/denom)*q12_dot_v2-(v1_dot_v2/denom)*q12_dot_v1;

    // Evaluate midpoint
    return ( (q1+s*v1) + (q2+t*v2) ) * (0.5);
}

bool constructPointCloud(sl::Camera* cam, sl::Projector* proj, sl::ProcessedGrayCodes codes,
                         std::vector<cv::Point3d>& points)
{
    cv::Point3d projCenter, camCenter;
    cv::Mat_<double> camRays, projRays;
    if (!generateReconstructionParams(cam, proj, camRays, camCenter, projRays, projCenter))
    {
        std::cout << "calibration parameter generation failed!" << std::endl;
        return false;
    }

    unsigned int numPoints = cv::countNonZero(codes.mask);
    points.resize(numPoints);

    unsigned int j = 0;
    for (int r = 0; r < codes.mask.rows; r++)
    {
        for (int c = 0; c < codes.mask.cols; c++)
        {
            if (codes.mask(r, c))
            {
                // Reconstruct surface using "ray-ray" triangulation
                int correspondingColumn = codes.columns(r, c);
                int correspondingRow    = codes.rows(r, c);
                cv::Vec3d v1, v2;
                int rc_cam  = cam->getResolution().width * r + c;
                int rc_proj = proj->getResolution().width * correspondingRow + correspondingColumn;
                for(int i=0; i<3; i++)
                {
                    v1[i] = ((double*)camRays.data)[rc_cam+cam->getResolution().area()*i];
                    v2[i] = ((double*)projRays.data)[rc_proj+proj->getResolution().area()*i];
                }

                points[j++] = intersectLineWithLine3D(camCenter, v1, projCenter, v2);
            }
        }
    }

    return true;
}

}
