#include "PointGreyCamera.hpp"
#include <bitset>
#include <boost/lexical_cast.hpp>
#include <tbb/recursive_mutex.h>
#include <tbb/compat/condition_variable>

namespace pgr = FlyCapture2;

#define BITMASK_CHECK(bitfield, bit) ((bitfield) & (bit))

namespace {

struct VideoModeDetails
{
    pgr::Format7Info f7Info;
    pgr::Format7ImageSettings settings;
    pgr::Format7PacketInfo packetInfo;
};

pgr::PixelFormat videoFormatToPgrPixelFormat(sl::VideoFormat format)
{
    switch (format)
    {
    case sl::VID_RGB24: return pgr::PIXEL_FORMAT_BGR; // OpenCV is BGR by default
    case sl::VID_GRAY8: return pgr::PIXEL_FORMAT_MONO8;
    case sl::VID_RAW8: return pgr::PIXEL_FORMAT_RAW8;
    }

    return pgr::UNSPECIFIED_PIXEL_FORMAT;
}

bool findClosestSupportedPixelFormat(sl::VideoFormat format, pgr::Format7Info f7Info,
                                     pgr::PixelFormat* result)
{
    switch (format)
    {
    case sl::VID_RGB24:
        if (BITMASK_CHECK(f7Info.pixelFormatBitField, pgr::PIXEL_FORMAT_RGB8))
        {
            *result = pgr::PIXEL_FORMAT_RGB8;
            return true;
        }

        break;
    case sl::VID_GRAY8:
        if (BITMASK_CHECK(f7Info.pixelFormatBitField, pgr::PIXEL_FORMAT_MONO8))
        {
            *result = pgr::PIXEL_FORMAT_MONO8;
            return true;
        }

        break;
    }

    // Fallback to RAW8 for RGB24 or GRAY8
    if (BITMASK_CHECK(f7Info.pixelFormatBitField, pgr::PIXEL_FORMAT_RAW8))
    {
        *result = pgr::PIXEL_FORMAT_RAW8;
        return true;
    }

    return false;
}

pgr::PropertyType cameraSettingToPgrPropertyType(sl::CameraSetting setting)
{
    pgr::PropertyType type;
    switch (setting)
    {
    case sl::BRIGHTNESS: type = pgr::BRIGHTNESS; break;
    case sl::HUE: type = pgr::HUE; break;
    case sl::SATURATION: type = pgr::SATURATION; break;
    case sl::SHARPNESS: type = pgr::SHARPNESS; break;
    case sl::GAMMA: type = pgr::GAMMA; break;
    case sl::WHITE_BALANCE: type = pgr::WHITE_BALANCE; break;
    case sl::GAIN: type = pgr::GAIN; break;
    case sl::EXPOSURE: type = pgr::AUTO_EXPOSURE; break;
    case sl::SHUTTER: type = pgr::SHUTTER; break;
    default:
        type = pgr::UNSPECIFIED_PROPERTY_TYPE; break;
    }

    return type;
}

}

namespace sl {

class PointGreyCamera::impl
{
public:
    PointGreyCamera* const _pgrCam;
    tbb::recursive_mutex _lock;

    pgr::PGRGuid _guid;
    pgr::Camera _camera;
    std::string _modelName, _serialNumber;
    VideoMode _vidMode;
    pgr::Image _buffer;

    impl(PointGreyCamera* const pgrCam, pgr::PGRGuid guid);
    ~impl();

    std::vector<VideoMode> enumerateVideoModes();
    VideoMode createCustomVideoMode(unsigned int x, unsigned int y, unsigned int width,
                                    unsigned int height, unsigned int fps,
                                    VideoFormat vidFormat) const;

    // Accessors
    std::string getModelName() const { return _modelName; }
    std::string getSerialNumber() const { return _serialNumber; }
    VideoMode getVideoMode() const { return _vidMode; }
    CustomVideoModeConstraints getCustomVideoModeConstraints() const;
    bool getCameraSetting(CameraSetting setting, double* value, CameraSettingFlags* flags);
    bool getCameraSettingRange(CameraSetting setting, double* lower, double* delta, double* upper, double* def);
    bool setCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags);

    // Mutators
    bool setVideoMode(VideoMode mode);

    bool connect();
    bool disconnect();
    bool capture();
    bool retrieve(cv::Mat &image);
};

void PointGreyCamera::enumerateCameras(std::vector<std::shared_ptr<Camera>> &cams)
{
    // C++/CLI support requires that this never be destructed because of an issue
    // where BusManager's destructor takes on the order of tens of second to
    // execute. Not destroying this is acceptable because we only need a single
    // manager, and it can last for the entire lifetime of the DLL.
    static pgr::BusManager* bus = new pgr::BusManager;
    static bool firstInit = true;
    if (!firstInit)
    {
        bus->RescanBus();
        firstInit = false;
    }

    unsigned int numCams;
    bus->GetNumOfCameras(&numCams);

    for (unsigned int i = 0; i < numCams; i++)
    {
        pgr::PGRGuid guid;
        bus->GetCameraFromIndex(i, &guid);

        cams.push_back(createSharedPtr(new PointGreyCamera(guid)));
    }
}

PointGreyCamera::impl::impl(PointGreyCamera* const pgrCam, pgr::PGRGuid guid)
    : _pgrCam(pgrCam), _guid(guid)
{
    _camera.Connect(&_guid);

    // Get various information about the camera
    pgr::CameraInfo info;
    _camera.GetCameraInfo(&info);

    _modelName = info.modelName;
    _serialNumber = boost::lexical_cast<std::string>(info.serialNumber);

    // Default is maximum capabilities, RGB24 if it has it, otherwise GRAY8
    auto c = getCustomVideoModeConstraints();
    auto videoMode = createCustomVideoMode(0, 0, c.maxWidth, c.maxHeight, c.maxFramesPerSecond,
        c.supportedVideoFormats & sl::VID_RGB24 ? sl::VID_RGB24 : sl::VID_GRAY8);

    assert(setVideoMode(videoMode));

    // Enable "one-shot" mode
    pgr::TriggerMode trigger;
    trigger.onOff = true;
    trigger.mode = 0; // Software mode (http://www.ptgrey.com/support/kb/index.asp?a=4&q=239)
    trigger.parameter = 0; // Unused, default of zero

    // Software source (see example in URI below, search for "triggerMode.source")
    // http://www.ptgrey.com/products/pgrflycapture/examples/AsyncTriggerEx.html
    trigger.source = 7;
    _camera.SetTriggerMode(&trigger);
}

PointGreyCamera::impl::~impl()
{
    if (_pgrCam->isConnected())
    {
        disconnect();
    }

    _camera.Disconnect();
}

std::vector<VideoMode> PointGreyCamera::impl::enumerateVideoModes()
{
    return std::vector<VideoMode>();
}

bool checkFormat7Settings(pgr::Camera* cam, pgr::Format7ImageSettings &settings, pgr::Format7PacketInfo &info)
{
    bool isValid;
    cam->ValidateFormat7Settings(&settings, &isValid, &info);
    return isValid;
}

// http://stackoverflow.com/questions/3407012
int roundUp(int numToRound, int multiple)
{
    if (multiple == 0)
    {
        return numToRound;
    }

    int remainder = numToRound % multiple;
    if (remainder == 0)
    {
        return numToRound;
    }

    return numToRound + multiple - remainder;
}

VideoMode PointGreyCamera::impl::createCustomVideoMode(
    unsigned int x, unsigned int y, unsigned int width, unsigned int height, unsigned int fps,
    VideoFormat vidFormat) const
{
    pgr::Camera* nonconstCamera = const_cast<pgr::Camera*>(&_camera);

    pgr::Format7Info info;
    bool supported;
    nonconstCamera->GetFormat7Info(&info, &supported);

    VideoMode mode = {};
    if (!supported)
    {
        mode.data = nullptr; // NULL pointer indicates unsupported
        return mode;
    }

    pgr::Format7ImageSettings settings;

    // Round up to nearest multiple
    settings.width = roundUp(width, info.imageHStepSize);
    settings.height = roundUp(height, info.imageVStepSize);

    // Round down
    settings.offsetX = x - (x % info.imageHStepSize);
    settings.offsetY = y - (y % info.imageVStepSize);
    settings.mode = pgr::MODE_0;

    pgr::PixelFormat pgrPf;
    if (!findClosestSupportedPixelFormat(vidFormat, info, &pgrPf))
    {
        mode.data = nullptr; // NULL pointer indicates unsupported
        return mode;
    }

    settings.pixelFormat = pgrPf;

    pgr::Format7PacketInfo packetInfo;
    bool isValid = checkFormat7Settings(nonconstCamera, settings, packetInfo);
    if (!isValid)
    {
        return mode;
    }

    mode.x = settings.offsetX;
    mode.y = settings.offsetY;
    mode.width = settings.width;
    mode.height = settings.height;
    mode.framesPerSecond = 0; // TODO: implement
    mode.videoFormat = vidFormat;
    
    std::shared_ptr<VideoModeDetails> details(new VideoModeDetails);
    details->f7Info = info;
    details->settings = settings;
    details->packetInfo = packetInfo;

    mode.data = details;

    return mode;
}

CustomVideoModeConstraints PointGreyCamera::impl::getCustomVideoModeConstraints() const
{
    pgr::Camera* nonconstCamera = const_cast<pgr::Camera*>(&_camera);

    pgr::Format7Info f7Info;
    bool f7IsSupported;
    nonconstCamera->GetFormat7Info(&f7Info, &f7IsSupported);

    CustomVideoModeConstraints c = {};
    c.supported = f7IsSupported;
    if (!f7IsSupported)
    {
        return c;
    }

    c.maxWidth = f7Info.maxWidth;
    c.maxHeight = f7Info.maxHeight;
    c.maxFramesPerSecond = 0; // TODO: implement
    
    unsigned int vidFormats = 0;
    if (BITMASK_CHECK(f7Info.pixelFormatBitField, pgr::PIXEL_FORMAT_RGB8))
    {
        vidFormats |= VID_RGB24;
    }

    if (BITMASK_CHECK(f7Info.pixelFormatBitField, pgr::PIXEL_FORMAT_MONO8))
    {
        vidFormats |= VID_GRAY8;
    }

    if (BITMASK_CHECK(f7Info.pixelFormatBitField, pgr::PIXEL_FORMAT_RAW8))
    {
        vidFormats |= VID_RAW8;
        vidFormats |= VID_RGB24;
        vidFormats |= VID_GRAY8;
    }

    c.supportedVideoFormats = vidFormats;
    return c;
}

bool PointGreyCamera::impl::setVideoMode(VideoMode mode)
{
    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    if (!mode.data)
    {
        // Unsupported
        return false;
    }

    VideoModeDetails* details = reinterpret_cast<VideoModeDetails*>(mode.data.get());
    pgr::Error e = _camera.SetFormat7Configuration(&details->settings,
                                                   (float)90);

    if (e == pgr::PGRERROR_OK)
    {
        _vidMode = mode;
        return true;
    }

    return false;
}

bool PointGreyCamera::impl::getCameraSetting(CameraSetting setting, double* value, CameraSettingFlags* flags)
{
    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    pgr::PropertyType type = cameraSettingToPgrPropertyType(setting);

    if (type == pgr::UNSPECIFIED_PROPERTY_TYPE)
    {
        *flags = NOT_SUPPORTED;
        return false;
    }

    pgr::Property prop;
    prop.type = type;
    if (_camera.GetProperty(&prop) != pgr::PGRERROR_OK)
    {
        return false;
    }

    *flags = prop.autoManualMode ? AUTOSET : MANUALSET;
    *value = prop.absValue;

    return true;
}

bool PointGreyCamera::impl::setCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags)
{
    std::lock_guard<tbb::recursive_mutex> lock(_lock);
    
    pgr::PropertyType type = cameraSettingToPgrPropertyType(setting);

    if (type == pgr::UNSPECIFIED_PROPERTY_TYPE)
    {
        return false;
    }

    pgr::Property prop;
    prop.type = type;
    _camera.GetProperty(&prop);

    prop.onOff = true;
    prop.autoManualMode = (flags & AUTOSET) != 0;
    prop.absValue = (float)value;
    prop.absControl = true;

    return _camera.SetProperty(&prop) == pgr::PGRERROR_OK;
}

bool PointGreyCamera::impl::getCameraSettingRange(CameraSetting setting, double* lower, double* delta, double* upper, double* def)
{
    std::lock_guard<tbb::recursive_mutex> lock(_lock);
    
    pgr::PropertyType type = cameraSettingToPgrPropertyType(setting);
    if (type == pgr::UNSPECIFIED_PROPERTY_TYPE)
    {
        return false;
    }

    pgr::PropertyInfo info;
    info.type = type;
    if (_camera.GetPropertyInfo(&info) != pgr::PGRERROR_OK)
    {
        return false;
    }

    *lower = info.absMin;
    *upper = info.absMax;
    *delta = 1;
    *def = 0;

    return true;
}

bool PointGreyCamera::impl::connect()
{
    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    bool success = _camera.StartCapture() == pgr::PGRERROR_OK;

    if (success)
    {
        // Power up the camera (0x610 = CAMERA_POWER register)
        // See: http://www.ptgrey.com/support/downloads/documents/Point%20Grey%20Digital%20Camera%20Register%20Reference.pdf
        //_camera.WriteRegister(0x610, 1 << 31);
        
        //unsigned int val;
        //do
        //{
        //    _camera.ReadRegister(0x610, &val);
        //} while (val == 0);

        // Capture an image to warm up the camera
        //pgr::Image img;
        //_camera.FireSoftwareTrigger();
        //_camera.RetrieveBuffer(&img);

        _pgrCam->setIsConnected(success);
    }

    return success;
}

bool PointGreyCamera::impl::disconnect()
{
    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    bool success = _camera.StopCapture() == pgr::PGRERROR_OK;
    if (success)
    {
        // Power down the camera (0x610 = CAMERA_POWER register)
        // See connect() (above)
        //_camera.WriteRegister(0x610, 0);
        //_pgrCam->setIsConnected(false);
    }

    return success;
}

bool PointGreyCamera::impl::capture()
{
    // Ensure the trigger is armed
    unsigned int val;
    do
    {
        // 0x62C = SOFTWARE_TRIGGER
        // Returns 0 if trigger is ready, > 0 if busy
        _camera.ReadRegister(0x62C, &val);
    } while (val != 0);

    auto err = _camera.FireSoftwareTrigger();
    return err == pgr::PGRERROR_OK;
}

bool PointGreyCamera::impl::retrieve(cv::Mat &image)
{
    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    pgr::Error err;
    if ((err = _camera.RetrieveBuffer(&_buffer)) != pgr::PGRERROR_OK)
    {
        err.PrintErrorTrace();
        return false;
    }

    VideoFormat vidFormat = getVideoMode().videoFormat;
    int openCvFormat = videoFormatToOpenCvType(vidFormat);

    if (image.rows != _buffer.GetRows() || image.cols != _buffer.GetCols() ||
        image.type() != openCvFormat)
    {
        // Reallocate to the correct format
        image = cv::Mat(_buffer.GetRows(), _buffer.GetCols(), videoFormatToOpenCvType(vidFormat));
    }

    pgr::Image pgrImg(image.ptr(), image.step1() * image.rows);
    _buffer.SetColorProcessing(pgr::IPP);
    _buffer.Convert(videoFormatToPgrPixelFormat(getVideoMode().videoFormat), &pgrImg);

    return true;
}

//
// PointGreyCamera stubs
//
#pragma warning(push)
#pragma warning(disable: 4355) // Passing "this" before the object is fully constructed. This
                               // is fine because all impl() does is initialize the pointer
PointGreyCamera::PointGreyCamera(pgr::PGRGuid guid) : Camera(CAM_POINT_GREY), _m(new impl(this, guid)) { }
#pragma warning(pop)

PointGreyCamera::~PointGreyCamera() { }
std::vector<VideoMode> PointGreyCamera::enumerateVideoModes() { return _m->enumerateVideoModes(); }
VideoMode PointGreyCamera::createCustomVideoMode(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int f, VideoFormat v) const { return _m->createCustomVideoMode(x, y, w, h, f, v); }
std::string PointGreyCamera::getModelName() const { return _m->getModelName(); }
std::string PointGreyCamera::getSerialNumber() const { return _m->getSerialNumber(); }
VideoMode PointGreyCamera::getVideoMode() const { return _m->getVideoMode(); }
CustomVideoModeConstraints PointGreyCamera::getCustomVideoModeConstraints() const { return _m->getCustomVideoModeConstraints(); }
bool PointGreyCamera::getCameraSetting(CameraSetting setting, double* value, CameraSettingFlags* flags) { return _m->getCameraSetting(setting, value, flags); }
bool PointGreyCamera::getCameraSettingRange(CameraSetting setting, double* lower, double* delta, double* upper, double* def) { return _m->getCameraSettingRange(setting, lower, delta, upper, def); }
bool PointGreyCamera::setCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags) { return _m->setCameraSetting(setting, value, flags); }
bool PointGreyCamera::setVideoMode(VideoMode mode) { return _m->setVideoMode(mode); }
bool PointGreyCamera::connect() { return _m->connect(); }
bool PointGreyCamera::disconnect() { return _m->disconnect(); }
bool PointGreyCamera::capture() { return _m->capture(); }
bool PointGreyCamera::retrieve(cv::Mat &image) { return _m->retrieve(image); }

}