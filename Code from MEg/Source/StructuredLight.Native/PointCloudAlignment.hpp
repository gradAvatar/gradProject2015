#pragma once

#include <opencv2/opencv.hpp>
#include <pcl/point_types.h>
#include <fstream>
#include <iostream>
#include <sstream>

namespace sl {
	
class PointCloudAlignment
{
public:
	static pcl::PointCloud<pcl::PointNormal> alignPointCloud(pcl::PointCloud<pcl::PointNormal>::Ptr baseCloud,
					 pcl::PointCloud<pcl::PointNormal>::Ptr inputCloud);
	static void Testing();
private:
	static pcl::PointCloud<pcl::PointNormal> transformPointCloud(
		pcl::PointCloud<pcl::PointNormal>& pc, cv::Mat_<float> transform);
};
}