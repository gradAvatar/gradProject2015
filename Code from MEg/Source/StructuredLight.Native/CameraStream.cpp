#include "CameraStream.hpp"
#include "Camera.hpp"
#include "Event.hpp"
#include "Exceptions.hpp"
#include <tbb/mutex.h>
#include <tbb/recursive_mutex.h>
#include <tbb/compat/thread>
#include <tbb/compat/condition_variable>

namespace sl {

/// \internal
/// Implementation.
class CameraStream::impl
{
public:
    // Threading stuff
    tbb::recursive_mutex _lock;
    std::thread _processingThread;
    ManualResetEvent _captureFrameEvent;
    AutoResetEvent _frameCapturedEvent;
    volatile bool _stopRequested;

    // Stream state
    Camera* const _cam;
    std::unique_ptr<MatPool> _pool;
    CallbackManager<ImageCapturedCallbackArgs> _cbPool;
    STREAM_MODE _currentMode;
    PooledMat _frame;
    volatile bool _started, _lastWasSuccessful;
    bool _modeLocked;

    impl(Camera* cam);
    ~impl();

    void processingThread(AutoResetEvent* camConnectedEv, bool* camConnected);
    bool lockStreamMode(STREAM_MODE mode);
    void unlockStreamMode(STREAM_MODE originalMode);
    bool isStopRequested() { return _stopRequested; }

    // Public members
    bool start();
    bool stop();
    void trigger();
    bool waitForFrame(unsigned int milliseconds = 0xFFFFFFFF);
    bool retrieveLastCapture(cv::Mat &image);
    bool triggerWaitRetrieve(cv::Mat &image);

    CallbackToken addCallback(ImageCapturedCallback cb) { return _cbPool.add(cb); }
    void removeCallback(CallbackToken token) { _cbPool.remove(token); }

    bool isStarted() const { return _started; }
    bool isStreamModeLocked() const { return _modeLocked; }
    STREAM_MODE getStreamMode() const { return _currentMode; }

    bool setStreamMode(STREAM_MODE mode);
};

CameraStream::impl::impl(Camera* cam_)
    : _cam(cam_), _started(false), _stopRequested(false), _currentMode(AUTO), _modeLocked(false),
      _captureFrameEvent(true), _frameCapturedEvent(false)
{
    _cbPool.setPreInvokeFunc([](ImageCapturedCallbackArgs p) { p._pooled.addRef(); });
    _cbPool.setPostInvokeFunc([](ImageCapturedCallbackArgs p) { p._pooled.removeRef(); });
}

CameraStream::impl::~impl()
{
    if (_started)
    {
        stop();
    }
}

void CameraStream::impl::processingThread(AutoResetEvent* camConnectedEv, bool* camConnected)
{
    *camConnected = _cam->connect();
    camConnectedEv->set();

    // camConnectedEv should be considered invalid after this point

    if (!*camConnected)
    {
        // Just to be safe
        _cam->disconnect();
        return;
    }

    while (true)
    {
        // Wait for someone to ask us to capture a frame
        _captureFrameEvent.wait();

        if (isStopRequested())
        {
            break;
        }

        PooledMat buffer = _pool->acquireMat();
        _cam->capture();
        _lastWasSuccessful = _cam->retrieve(*buffer);

        if (_frame.isValid())
        {
            _frame.removeRef();
        }

        _frame = buffer;

        _frameCapturedEvent.set();

        ImageCapturedCallbackArgs data;
        data._pooled = _frame;
        _cbPool.invoke(data);

        if (_currentMode == MANUAL)
        {
            _captureFrameEvent.reset();
        }
    }

    _cam->disconnect();
}

bool CameraStream::impl::lockStreamMode(STREAM_MODE mode)
{
    if (_modeLocked)
    {
        return false;
    }

    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    setStreamMode(mode);
    _modeLocked = true;

    return true;
}

void CameraStream::impl::unlockStreamMode(STREAM_MODE originalMode)
{
    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    _modeLocked = false;
    setStreamMode(originalMode);
}

bool CameraStream::impl::start()
{
    if (isStarted())
    {
        return true;
    }

    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    _pool = std::unique_ptr<MatPool>(new MatPool(_cam->getVideoMode().getResolution(), CV_8UC3));
    
    if (_currentMode == AUTO)
    {
        _captureFrameEvent.set();
    }
    else
    {
        _captureFrameEvent.reset();
    }

    _started = true;

    AutoResetEvent ev(false);
    bool camConnected = false;
    _processingThread = std::thread(std::bind(&impl::processingThread, this, &ev, &camConnected));
    
    // See if the camera connected successfully
    ev.wait();
    if (!camConnected)
    {
        _processingThread.join();
        return false;
    }

    return true;
}

bool CameraStream::impl::stop()
{
    if (!isStarted())
    {
        return true;
    }

    // Tell the retreival thread to capture a frame, but instead, it should
    // stop immediately (it checks if a stop has been requested before capturing)
    _stopRequested = true;
    _captureFrameEvent.set();

    // Wait for the retrieval thread to exit
    _processingThread.join();

    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    // Wait for any callback threads to finish
    _cbPool.waitForAll();

    _started = false;
    _stopRequested = false;
    _processingThread = std::thread();
    _frame.forceReset();
    _pool.reset();

    return true;
}

void CameraStream::impl::trigger()
{
    if (_currentMode == MANUAL)
    {
        _captureFrameEvent.set();
    }
}

bool CameraStream::impl::waitForFrame(unsigned int milliseconds)
{
    return _frameCapturedEvent.wait(milliseconds) == EV_OK;
}

bool CameraStream::impl::retrieveLastCapture(cv::Mat &image)
{
    if (!_lastWasSuccessful)
    {
        return false;
    }

    _frame->copyTo(image);
    return true;
}

bool CameraStream::impl::triggerWaitRetrieve(cv::Mat &image)
{
    trigger();
    waitForFrame();
    return retrieveLastCapture(image);
}

bool CameraStream::impl::setStreamMode(STREAM_MODE mode)
{
    if (_modeLocked)
    {
        return false;
    }

    std::lock_guard<tbb::recursive_mutex> lock(_lock);

    STREAM_MODE oldMode = _currentMode;
    _currentMode = mode;
    if (oldMode == MANUAL && mode == AUTO)
    {
        _captureFrameEvent.set();
    }

    return true;
}

//
// CameraStream stubs
//
CameraStream::~CameraStream() { }
bool CameraStream::start() { return _m->start(); }
bool CameraStream::stop() { return _m->stop(); }
void CameraStream::trigger() { _m->trigger(); }
bool CameraStream::waitForFrame(unsigned int m) { return _m->waitForFrame(m); }
bool CameraStream::retrieveLastCapture(cv::Mat &i) { return _m->retrieveLastCapture(i); }
bool CameraStream::triggerWaitRetrieve(cv::Mat &i) { return _m->triggerWaitRetrieve(i); }
CallbackToken CameraStream::addCallback(ImageCapturedCallback cb) { return _m->addCallback(cb); }
void CameraStream::removeCallback(CallbackToken token) { return _m->removeCallback(token); }
bool CameraStream::isStarted() const { return _m->isStarted(); }
bool CameraStream::isStreamModeLocked() const { return _m->isStreamModeLocked(); }
STREAM_MODE CameraStream::getStreamMode() const { return _m->getStreamMode(); }
bool CameraStream::setStreamMode(STREAM_MODE mode) { return _m->setStreamMode(mode); }
CameraStream::CameraStream(Camera* cam) : _m(new impl(cam)) { }

//
// StreamLock implementation
//
StreamLock::StreamLock(CameraStream* stream, STREAM_MODE modeToLockTo)
    : _stream(nullptr)
{
    STREAM_MODE originalMode = stream->getStreamMode();
    if (stream->_m->lockStreamMode(modeToLockTo))
    {
        _stream = stream;
        _originalMode = originalMode;
    }
}

StreamLock::~StreamLock()
{
    unlock();
}

void StreamLock::unlock()
{
    if (_stream != nullptr)
    {
        _stream->_m->unlockStreamMode(_originalMode);
    }

    _stream = nullptr;
}

bool StreamLock::isLockActive() const { return _stream != nullptr; }

}