#pragma once

#include "Exceptions.hpp"
#include "Utils.hpp"
#include <list>
#include <functional>
#include <opencv2/opencv.hpp>

namespace sl {

class PooledMat
{
public:
    PooledMat()
        : _mat(0), _id(0), _returnCallback(0), _refCount(0)
    {
    }

    void addRef()
    {
        if (*_refCount == 0)
        {
            // Adding a reference to a freed Mat
            THROW_EX(useOfFreedMatEx);
        }

        (*_refCount)++;
    }

    void removeRef()
    {
        if (*_refCount <= 0)
        {
            // Removing a reference to a freed Mat
            THROW_EX(useOfFreedMatEx);
        }

        if (--(*_refCount) == 0)
        {
            int id = *_id;
            cv::Mat* mat = _mat;

            // Return this Mat to the pool
            _returnCallback(this);
			
            _mat = nullptr;
            _refCount.reset();
            _id.reset();
        }
    }

    void forceReset()
    {
        if (*_refCount <= 0)
        {
            THROW_EX(useOfFreedMatEx);
        }

        *_refCount = 1;
        removeRef();
    }

    // Accessors
    unsigned int getId() const { return *_id; }
    bool isValid() const { return _mat != 0; }

    // Operators
    cv::Mat& operator*() const { return *_mat; }
    cv::Mat* operator->() const { return _mat; }

private:
    cv::Mat* _mat;
    std::shared_ptr<unsigned int> _refCount;
    std::shared_ptr<unsigned int> _id;
    std::function<void (PooledMat* const)> _returnCallback;

    PooledMat(cv::Mat* m, unsigned int id, std::function<void (PooledMat* const)> returnCallback)
        : _mat(m), _id(new unsigned int(id)), _returnCallback(returnCallback),
          _refCount(new unsigned int(1))
    {
    }

    friend class MatPool;
};

class MatPool
{
public:
    MatPool(cv::Size resolution, int type)
        : _resolution(resolution), _type(type)
    {
    }

    ~MatPool()
    {
        reset();
    }

    PooledMat acquireMat()
    {
        static int nextId = 0;

        cv::Mat* mat;
        int id;

        if (_pooledMats.size() == 0)
        {
            // Allocate a new Mat
            id = nextId++;
            mat = new cv::Mat(cv::Mat::zeros(_resolution, _type));

            //std::cout << "Allocating a new pooled cv::Mat (id = " << id << ")" << std::endl;
        }
        else
        {
            std::pair<cv::Mat*, int> pair = _pooledMats.front();
            _pooledMats.pop_front();
        
            mat = pair.first;
            id = pair.second;
        }

        _borrowedMats.insert(std::pair<int, cv::Mat*>(id, mat));

        mat->setTo(0);
        return PooledMat(mat, id, [this](PooledMat* const mat) { _returnToPool(mat); });
    }

    void reset()
    {
        for (auto it = _pooledMats.cbegin(); it != _pooledMats.cend(); it++)
        {
            delete it->first;
        }

        _pooledMats.clear();

        for (auto it = _borrowedMats.cbegin(); it != _borrowedMats.cend(); it++)
        {
            delete it->second;
        }

        _borrowedMats.clear();
    }

private:
    const cv::Size _resolution;
    const int _type;
    std::list<std::pair<cv::Mat*, int>> _pooledMats;
    std::map<int, cv::Mat*> _borrowedMats;

    void _returnToPool(PooledMat* const pooledMat)
    {
        cv::Mat* cvMat = pooledMat->operator->();
        int matId = pooledMat->getId();

        //std::cout << "Returning pooled cv::Mat to a pool (id = " << id << ")" << std::endl;

        if (*(cvMat->refcount) != 1)
        {
            PRINT_WARN("Returned cv::Mat has more than one reference!");
        }

        _borrowedMats.erase(matId);
        _pooledMats.push_back(std::pair<cv::Mat*, int>(cvMat, matId));
    }

    DISALLOW_COPY_AND_ASSIGN(MatPool);
};

}