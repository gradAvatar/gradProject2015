#pragma once

#include "Projector.hpp"
#include "Utils.hpp"
#include <opencv2/opencv.hpp>

namespace sl {

class GuiWindow
{
public:
    GuiWindow(std::string const& title, cv::Size resolution, cv::Point2i position);
    GuiWindow(const Projector* const proj);
    ~GuiWindow();

    void close();
    void setImage(cv::Mat mat);
    void clear();

private:
    class impl;
    const std::unique_ptr<impl> _m;

    DISALLOW_COPY_AND_ASSIGN(GuiWindow);
};

}
