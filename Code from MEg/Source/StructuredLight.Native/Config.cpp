#include "Config.hpp"
#include "Scanner.hpp"
#include "Exceptions.hpp"
#include "Serialization.hpp"

namespace sl {
 
std::unique_ptr<Config> Config::_obj = 0;
 
void Config::loadConfig(std::string fileName)
{
    if (_obj)
    {
        // Config already loaded
        THROW_EX(configLoadedEx);
    }

    _obj = std::unique_ptr<Config>(new Config);
    cv::FileStorage fs(fileName, cv::FileStorage::READ);

    int i = 0;

    _obj->_fileName = fileName;
    setCalibrationFolder(fs["cameras"]["calibDir"]);
    setCircleDiameter(fs["calibration"]["circle_diameter"]);
    setCircleSpacing(fs["calibration"]["circle_spacing"]);
    setProjectedCircleDiameter(static_cast<int>(fs["calibration"]["proj_circle_diameter"]));
    setProjectedCircleSpacing(static_cast<int>(fs["calibration"]["proj_circle_spacing"]));
    setCircleRows(static_cast<int>(fs["calibration"]["circle_rows"]));
    setCircleColumns(static_cast<int>(fs["calibration"]["circle_columns"]));
    setMotionThreshold(fs["calibration"]["motion_threshold"]);
    setContrastThreshold(static_cast<int>(fs["scanning"]["contrast_threshold"]));
    setFrameDelay(static_cast<int>(fs["scanning"]["frame_delay"]));
    setScanType(static_cast<sl::ScanType>(static_cast<int>(fs["scanning"]["mode"])));
 
    cv::FileNode userSettings = fs["user_settings"];
    for (auto it = userSettings.begin(); it != userSettings.end(); i++)
    {
        std::string setting = (*it).name(), value;
        it >> value;
        _obj->setUserSetting(setting, value);
    }
}

void Config::saveConfig()
{
    if (!_obj)
    {
        THROW_EX(configNotLoadedEx);
    }
 
    cv::FileStorage fs(_obj->_fileName, cv::FileStorage::WRITE);
     
    fs << "cameras" <<
        "{" <<
            "calibDir" << getCalibrationFolder() <<
        "}";
 
    fs << "calibration" <<
        "{" <<
            "circle_diameter" << getCircleDiameter() <<
            "circle_spacing" << getCircleSpacing() <<
            "proj_circle_diameter" << (int)getProjectedCircleDiameter() <<
            "proj_circle_spacing" << (int)getProjectedCircleSpacing() <<
            "motion_threshold" << getMotionThreshold() <<
            "circle_rows" << (int)getCircleRows() <<
            "circle_columns" << (int)getCircleColumns() <<
        "}";
 
    fs << "scanning" <<
        "{" <<
            "mode" << (int)getScanType() <<
            "frame_delay" << (int)getFrameDelay() <<
            "contrast_threshold" << (int)getContrastThreshold() <<
        "}";
 
    fs << "user_settings" << "{";
 
    for (auto it = _obj->_userSettings.begin(); it != _obj->_userSettings.end(); it++)
    {
        fs << it->first << it ->second;
    }
 
    fs << "}";
}
 
}