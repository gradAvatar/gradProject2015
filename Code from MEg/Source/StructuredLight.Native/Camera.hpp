#pragma once

#include "Utils.hpp"
#include <opencv2/opencv.hpp>

namespace sl {

// Forward declarations
class CameraStream;
struct CameraCalibration;
struct IntrinsicCalibration;

/// \brief Values that represent different output formats that a Camera can support.
enum VideoFormat
{
    /// An unknown video format; default value.
    VID_UNKNOWN = 0,

    /// A format that consists of three channels, red, green, and blue (in that order), with each
    /// channel comprising 8 bits for a total of 24 bits per pixel.
    VID_RGB24 = 1,

    /// A format that consists of a single, grayscale channel, with that channel comprising 8 bits
    /// for a total of 8 bits per pixel.
    VID_GRAY8 = 2,

    VID_RAW8 = 4
};

/// \brief Values that represent different properties of a Camera supported by this library.
enum CameraSetting : long
{
    BRIGHTNESS,
    CONTRAST,
    HUE,
    SATURATION,
    SHARPNESS,
    GAMMA,
    WHITE_BALANCE,
    GAIN,
    EXPOSURE,
    FOCUS,
    SHUTTER
};

/// \brief Values that represent different properties of a Camera supported by this library.
enum CameraSettingFlags : long
{
    /// The value is automatically set by the camera
    AUTOSET = 1,
    /// The value is being manually set by a program
    MANUALSET = 2,
    /// The value is not supported
    NOT_SUPPORTED = 4
};

/// \brief Converts the specified VideoFormat to an equivalent OpenCV matrix type.
/// \param format The VideoFormat to convert.
/// \return An integer that can be used during the initialization of an OpenCV matrix.
static int videoFormatToOpenCvType(VideoFormat format)
{
    switch (format)
    {
    case VID_RGB24: return CV_8UC3;
    case VID_GRAY8:
    case VID_RAW8:
        return CV_8UC1;
    default: return -1;
    }
}

/// \brief A structure containing information about a video mode supported by a Camera instance.
/// \see Camera::enumerateVideoModes()
/// \see Camera::setVideoMode()
struct VideoMode
{
    /// \brief The X component of the origin of the image for this video mode.
    unsigned int x;

    /// \brief The Y component of the origin of the image for this video mode.
    unsigned int y;

    /// \brief The width of the image dimension for this video mode.
    unsigned int width;

    /// \brief The height of the image dimension for this video mode.
    unsigned int height;

    /// \brief The maximum (but not necessarily only) frame rate supported by this video mode.
    unsigned int framesPerSecond;

    /// \brief The video format that this video mode outputs.
    VideoFormat videoFormat;

    /// \brief Arbitrary data specified by each individual Camera implementation. Do not modify.
    std::shared_ptr<void> data;

    /// \brief Gets an instance of cv::Size initialized with \ref width and \ref height.
    /// \return An instance of cv::Size initialized with \ref width and \ref height.
    cv::Size getResolution() const { return cv::Size(width, height); }
};

/// \brief A structure containing information about the maximum settings supported by a Camera.
/// \see Camera::getCustomVideoModeConstraints()
struct CustomVideoModeConstraints
{
    /// \brief Whether or not custom video modes are supported by the Camera.
    bool supported;

    /// \brief The maximum horizontal resolution supported by the Camera.
    unsigned int maxWidth;

    /// \brief The maximum vertical resolution supported by the Camera.
    unsigned int maxHeight;

    /// \brief The maximum framerate supported by the Camera.
    unsigned int maxFramesPerSecond;

    /// \brief A bitfield of \link sl::VideoFormat VideoFormats\endlink containing all video
    /// formats supported by the Camera.
    /// \see VideoFormat
    unsigned int supportedVideoFormats;
};

/// \brief Values that represent different types of \link sl::Camera Cameras\endlink supported by
///        this library.
enum CameraType
{
    /// An unknown type of Camera; default value.
    CAM_UNKNOWN = 0,

    /// A Camera that uses DirectShow in its implementation. Windows only.
    CAM_DIRECTSHOW,

    /// A Camera that uses Point Grey's FlyCapture SDK in its implementation.
    CAM_POINT_GREY
};

/// \brief A class that represents a piece of video hardware available on the local system.
class Camera
{
public:
    /// \brief Enumerates all available Cameras available on the local system.
    /// \return A vector containing pointers to every available Camera on the local system.
    static std::vector<std::shared_ptr<Camera>> enumerateCameras();

    /// \brief Finalizes an instance of the Camera class.
    virtual ~Camera() { }

    /// \brief Enumerates all available video modes supported by this Camera.
    /// \return A vector containing information about all available video modes supported by this
    ///         Camera.
    virtual std::vector<VideoMode> enumerateVideoModes() = 0;

    /// \brief Opens a CameraStream tied to this Camera, used for accessing images from the
    ///        underlying hardware.
    /// \return A shared pointer to a CameraStream tied to this Camera. Note that if another client
    ///         opened a stream previously, and it is still open, a pointer to that instance will
    ///         be returned.
    std::shared_ptr<CameraStream> openStream();

    /// \brief If supported, creates a custom, camera-specific video mode instance that can be used
    ///        with setVideoMode.
    ///        
    /// To determine the limits of what the camera can support (e.g. its maximum resolution),
    /// including whether or not custom video modes are supported at all, refer to the
    /// information returned by getCustomVideoModeConstraints().
    /// 
    /// Note that values specified as parameters may be rounded (e.g. to the nearest 8th or 16th
    /// integer) as dictated by each individual camera's capabilities. You are not guaranteed to
    /// get the exact resolution or framerate you requested.
    /// \param width The horizontal resolution of the video mode you wish to create.
    /// \param height The vertical resolution of the video mode you wish to create.
    /// \param fps The framerate of the video mode you wish to create.
    /// \param vidFormat The video format of the video mode you wish to create.
    /// \return The custom VideoMode that can be used with setVideoMode().
    /// \see getCustomVideoModeConstraints()
    /// \see CustomVideoModeConstraints.
    virtual VideoMode createCustomVideoMode(unsigned int x, unsigned int y, unsigned int width,
                                            unsigned int height, unsigned int fps,
                                            VideoFormat vidFormat) const = 0;

    /// \brief Gets the model name of the Camera.
    /// \return The model name of the Camera.
    virtual std::string getModelName() const = 0;

    /// \brief Gets the serial number of the Camera.
    /// \return The serial number of the Camera.
    virtual std::string getSerialNumber() const = 0;

    /// \brief Gets the current video mode of the Camera.
    /// \return The current video mode of the Camera.
    virtual VideoMode getVideoMode() const = 0;

    /// \brief Gets the current resolution of the Camera. This is equivalent to 
    ///        getVideoMode().\link sl::VideoMode::getResolution getResolution()\endlink.
    /// \return The current resolution of the Camera.
    cv::Size getResolution() const { return getVideoMode().getResolution(); }

    /// \brief Determines whether or not a CameraStream tied to this Camera is currently open.
    /// \return \c true if a stream is open; false otherwise;
    bool isStreamOpen() const { return !_currentStream.expired(); }

    /// \brief Gets the type of interface the underlying Camera implementation uses to access the
    ///        camera hardware.
    /// \return The type of interface the underlying Camera implementation uses.
    CameraType getType() const { return _type; }

    /// \brief Returns information detailing the maximum possible values for custom video modes, if
    ///        supported.
    /// \return Information detailing the capabilities of the camera in terms of creating custom
    ///         video modes.
    virtual CustomVideoModeConstraints getCustomVideoModeConstraints() const = 0;

    /// \brief Attempts to change the video mode to the specified instance.
    /// \param mode The mode to change to.
    /// \return \c true if the mode is successfully changed; \c false if a CameraStream is
    ///         currently open (i.e. isStreamOpen() == true) or an error occurred in the underlying
    ///         implementation.
    virtual bool setVideoMode(VideoMode mode) = 0;

    /// \brief Attempts to get a property of the camera
    /// \param setting The setting to find.
    /// \param value The output value
    /// \param flags The camera flags output value
    /// \return \c true if the setting was successfully found and retrieved; \c false if the property
    ///         isn't supported or the camera was not able to retrieve the property.
    virtual bool getCameraSetting(CameraSetting setting, double* value, CameraSettingFlags* flags) = 0;

    /// \brief Attempts to set a property of the camera
    /// \param setting The setting to change.
    /// \param value The value
    /// \param flags The camera flags value
    /// \return \c true if the setting was successfully found and set; \c false if the property
    ///         isn't supported or the camera was not able to set the property.
    virtual bool setCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags) = 0;

    /// \brief Attempts to get the valid range of a property of the camera
    /// \param setting The setting to find.
    /// \param lower The lower bound of the setting
    /// \param delta The smallest amount the setting can change
    /// \param upper The upper bound of the setting
    /// \param def The default value of the setting
    /// \return \c true if the setting was successfully found and retrieved; \c false if the property
    ///         isn't supported or the camera was not able to retrieve the property.
    virtual bool getCameraSettingRange(CameraSetting setting, double* lower, double* delta, double* upper, double* def) = 0;

protected:
    /// \brief Creates a shared pointer for a bare Camera pointer.
    /// 
    /// This should be used whenever creating an instance of a Camera-derived class. This method
    /// attaches a custom destructor which allows for proper destruction of any open CameraStreams
    /// (in the event that a Camera is destructed before its CameraStream is).
    /// \param[in] cam A pointer to an instance of a Camera-derived class.
    /// \return The new shared pointer with an attached custom destructor.
    static std::shared_ptr<Camera> createSharedPtr(Camera* cam);

    /// \brief Attempts to connect to the underlying camera hardware.
    ///   
    /// If this method succeeds, subsequent calls to capture() and retrieve() should also succeed
    /// under normal circumstances. The connection to the hardware should persist until
    /// disconnect() is called, or until an unrecoverable error (e.g. the camera is disconnected)
    /// occurs.
    /// \return \c true if the connection to the hardware succeeds; \c false otherwise.
    virtual bool connect() = 0;

    /// \brief Attempts to disconnect from the underlying camera hardware.
    /// 
    /// This method should always return true unless an unrecoverable error occurs during the
    /// disconnection process. Implementations are discouraged from returning false, and instead
    /// should attempt to gracefully handle errors such that a disconnection is still possible.
    /// \return \c true if the connection to the hardware is successfully destroyed; \c false
    ///         otherwise.
    virtual bool disconnect() = 0;

    /// \brief Instructs the underlying Camera hardware to capture an image as soon as possible.
    /// \return \c true if the trigger is successfully sent, \c false otherwise.
    virtual bool capture() = 0;

    /// \brief Retrieves the image that was last captured by the underlying Camera hardware.
    /// \param[out] image The image to be filled.
    /// \return \c true if \a image was successfully filled; \c false otherwise.
    virtual bool retrieve(cv::Mat &image) = 0;

    /// \brief Initializes a new instance of the Camera class.
    /// \param type The type of interface the implementation uses to access camera hardware.
    Camera(CameraType type)
        : _type(type)
    {
    }

    /// \brief Combines calls to capture() and retrieve() into a single method call.
    /// \param[out] image The image to be filled.
    /// \return \c true if \a image was successfully filled; \c false otherwise.
    bool captureRetrieve(cv::Mat& image) { return capture() && retrieve(image); }

    /// \brief Determines if a connection to the underlying camera hardware is currently open.
    /// \return \c true if connected; \c false otherwise.
    bool isConnected() { return _connected; }

    /// \brief Sets whether or not a connection to the underlying camera hardware is currently
    ///        open.
    /// \param isConnected whether or not a connection is currently open.
    void setIsConnected(bool isConnected) { _connected = isConnected; }

private:
    const CameraType _type;
    bool _connected;
    std::weak_ptr<CameraStream> _currentStream;

    friend class CameraStream;

    DISALLOW_COPY_AND_ASSIGN(Camera);
};

}