#include "GrayCodeDetector.hpp"
#include "Event.hpp"
#include "Config.hpp"
#include <tbb/concurrent_queue.h>
#include <tbb/compat/thread>

namespace sl {

class GrayCodeDetector::impl
{
public:
    const Camera* const _cam;
    const Projector* const _proj;
    const GrayCodeColor _color;
    tbb::concurrent_queue<PooledMat> _queue;
    AutoResetEvent _wakeEv;
    bool _running, _cancelRequested;

    cv::Mat_<bool> _pixelMask;
    cv::Mat_<short> _columns, _rows;

    std::thread _processingThread;
    void processing();

    impl(const Camera* const cam, const Projector* const proj, const GrayCodeColor color);
    ~impl() { }
};

void GrayCodeDetector::impl::processing()
{
    cv::Size camRes = _cam->getResolution();

    // Results
    cv::Mat_<bool> mask(camRes, 0);
    cv::Mat_<ushort> columns(camRes, 0), rows(camRes, 0);

    // Working variables
    cv::Mat_<uchar> gray1(camRes, 0), gray2(camRes, 0), columnsBitPlane(camRes, 0),
                    rowsBitPlane(camRes, 0), temp(camRes, 0);
    GrayCodeParameters gcParams = _proj->getGrayCodeParameters();

    Orientation orient = ORIENT_COLUMNS;
    bool nextIsInverted = false;
    unsigned int pos = 0;
    while (true)
    {
        _wakeEv.wait();
        if (_cancelRequested)
        {
            return;
        }

        if (!_running)
        {
            break;
        }

        PooledMat mat;
        while (_queue.try_pop(mat))
        {
            cv::Mat target = nextIsInverted ? gray2 : gray1;
            if (_cam->getVideoMode().videoFormat == VID_GRAY8)
            {
                mat->copyTo(target);
            }
            else
            {
                if (_color == WHITE)
                {
                    cv::cvtColor(*mat, target, CV_RGB2GRAY);
                }
                else
                {
                    double redShare = 0.299 * _color.red / 255;
                    double greenShare = 0.587 * _color.green / 255;
                    double blueShare = 0.114 * _color.blue / 255;

                    for (int r = 0; r < target.rows; r++)
                    {
                        for (int c = 0; c < target.cols; c++)
                        {
                            cv::Vec3b srcPx = mat->at<cv::Vec3b>(r, c);
                            target.at<uchar>(r, c) = 
                                (uchar)floor((redShare * srcPx[2] + greenShare * srcPx[1] +
                                    blueShare * srcPx[0]) / (redShare + greenShare + blueShare) + 0.5);
                        }
                    }
                }
            }

            mat.removeRef();

            if (!nextIsInverted)
            {
                // Wait for the next image
                nextIsInverted = true;
                continue;
            }
            else
            {
                nextIsInverted = false;
            }

            // Decode bit-plane and update mask
            cv::absdiff(gray1, gray2, temp);
            mask |= temp >= Config::getContrastThreshold();
            temp = gray1 >= gray2;

            // Convert from Gray code to decimal value
            if (orient == ORIENT_COLUMNS)
            {
                columnsBitPlane ^= temp;
                cv::add(columns, 1 << (gcParams.numColumns - pos++ - 1), columns, columnsBitPlane);

                if (pos == gcParams.numColumns)
                {
                    // Switch to rows
                    orient = ORIENT_ROWS;
                    pos = 0;
                }
            }
            else
            {
                rowsBitPlane ^= temp;
                cv::add(rows, 1 << (gcParams.numRows - pos++ - 1), rows, rowsBitPlane);
            }
        }
    }

    columns -= gcParams.columnShift;
    rows -= gcParams.rowShift;

    // Eliminate invalid column/row estimates
    mask &= columns <= _proj->getResolution().width - 1;
    mask &= rows <= _proj->getResolution().height - 1;

    temp = ~mask;
    columns.setTo(0, temp);
    rows.setTo(0, temp);

    _pixelMask = mask;
    _columns = columns;
    _rows = rows;
}

GrayCodeDetector::impl::impl(const Camera* const cam, const Projector* const proj, const GrayCodeColor color)
    : _cam(cam), _proj(proj), _color(color), _running(true), _cancelRequested(false), _wakeEv(false)
{
    _wakeEv.reset();
    _processingThread = std::thread(std::bind(&GrayCodeDetector::impl::processing, this));
}

void GrayCodeDetector::processImage(PooledMat mat)
{
    _m->_queue.push(mat);
    _m->_wakeEv.set();
}

void GrayCodeDetector::cancel()
{
    if (_m->_processingThread.joinable())
    {
        _m->_cancelRequested = true;
        _m->_wakeEv.set();
        _m->_processingThread.join();

        _m->_queue.clear();
        _m->_pixelMask.release();
        _m->_rows.release();
        _m->_columns.release();
    }

    _m->_running = false;
    _m->_cancelRequested = false;
}

ProcessedGrayCodes GrayCodeDetector::finish()
{
    if (!_m->_processingThread.joinable())
    {
        THROW_EX(detectorNotRunningEx);
    }

    _m->_running = false;
    _m->_cancelRequested = false;
    _m->_wakeEv.set();
    _m->_processingThread.join();

    ProcessedGrayCodes processed = {};
    processed.mask = _m->_pixelMask;
    processed.columns = _m->_columns;
    processed.rows = _m->_rows;

    return processed;
}

GrayCodeDetector::GrayCodeDetector(const Camera* const cam, const Projector* const proj, const GrayCodeColor color)
    : _m(new impl(cam, proj, color))
{
}

GrayCodeDetector::~GrayCodeDetector()
{
    cancel();
}

}
