#include "Calibration.hpp"
#include "CameraStream.hpp"
#include "Config.hpp"
#include "Exceptions.hpp"
#include "Timer.hpp"
#include <boost/chrono.hpp>

namespace sl {

bool CameraCalibrator::calibrationImpl(IntrinsicCalibration &results)
{
    std::shared_ptr<CameraStream> stream = getCamera()->openStream();

    StreamLock streamLock(stream.get(), MANUAL);
    if (!streamLock.isLockActive())
    {
        throw streamModeLockFailedEx;
    }

    stream->start();

    cv::Size patternSize = cv::Size(Config::getCircleRows(), Config::getCircleColumns());
 
    // Various collected calibration points
    std::vector<std::vector<cv::Point2f>> imagePts; // Points that lie on the image itself
    std::vector<std::vector<cv::Point3f>> objectPts; // Points that lie in the calibration board's coordinate system

    Timer timer;

    int matType = videoFormatToOpenCvType(getCamera()->getVideoMode().videoFormat);
    cv::Mat img(getCamera()->getResolution(), matType);
    cv::Mat lastImg(getCamera()->getResolution(), matType);
    unsigned int i = 0;
    unsigned int consecutiveFinds = 0;
    double lastNorm;
    while (i < getTargetAcceptedCaptures() && !isCancelRequested())
    {
        stream->triggerWaitRetrieve(img);

        double norm = cv::norm(img, lastImg, CV_RELATIVE_L2);

        // Search for a circle grid and draw any detected circles
        std::vector<cv::Point2f> corners;
        bool found = cv::findCirclesGrid(img, patternSize, corners, cv::CALIB_CB_ASYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING);
        cv::drawChessboardCorners(img, patternSize, corners, found);
        consecutiveFinds = found ? consecutiveFinds + 1 : 0;
 
        bool acceptImage = consecutiveFinds > 1 && timer.elapsed() > boost::chrono::seconds(3) && 
            abs(lastNorm - norm) < Config::getMotionThreshold();

        if (acceptImage)
        {
            i++;

            // Invert image to tell the user an image was accepted
            cv::bitwise_not(img, img);
 
            // Save the detected circles
            imagePts.push_back(corners);

            float circleDiameter = Config::getCircleDiameter();
            float circleSpacing = Config::getCircleSpacing();
            float boardWidth = 4 * circleDiameter + 3 * circleSpacing + circleSpacing / 2 + circleDiameter / 2;
 
            // Construct the object coordinates that correspond to each image coordinate
            std::vector<cv::Point3f> object;
            for (int y = 0; y < patternSize.height; y++)
                 for (int x = 0; x < patternSize.width; x++)
                    object.push_back(cv::Point3f(boardWidth - float((y % 2 == 0 ? (circleDiameter / 2) : (circleDiameter + circleSpacing / 2)) + ((circleDiameter + circleSpacing) * x)),
                                                 float(circleDiameter / 2 + (circleDiameter + circleSpacing) / 2 * y),
                                                 0));
 
            objectPts.push_back(object);
            timer.reset();
        }

        CalibrationPreviewData status = {};
        status.currentPreview = img;
        status.numAcceptedCaptures = i;
        status.currentPreviewWasAccepted = acceptImage;

        invokePreviewCallbacks(status);

        img.copyTo(lastImg);
        lastNorm = norm;
    }

    if (isCancelRequested())
    {
        return false;
    }

    cv::Mat camera, distortion;
    double error = cv::calibrateCamera(objectPts,
                                       imagePts,
                                       getCamera()->getResolution(),
                                       camera,
                                       distortion,
                                       cv::noArray(),
                                       cv::noArray(),
                                       CV_CALIB_FIX_K3);

    results.camera = camera;
    results.distortion = distortion;
    results.reprojectionError = error;
    return true;
}

//
// CameraCalibrator stubs
//
CameraCalibrator::CameraCalibrator(Camera* const cam) : Calibrator(cam) { }
CameraCalibrator::~CameraCalibrator() { }

}