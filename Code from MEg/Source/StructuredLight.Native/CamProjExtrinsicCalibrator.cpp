#include "Calibration.hpp"
#include "CameraStream.hpp"
#include "Timer.hpp"
#include "Exceptions.hpp"
#include "Config.hpp"
#include "Serialization.hpp"
#include "GuiWindow.hpp"

namespace sl {

bool CamProjExtrinsicCalibrator::calibrationImpl(CamProjExtrinsicCalibration &results)
{
    std::shared_ptr<CameraStream> stream = getCamera()->openStream();

    StreamLock streamLock(stream.get(), MANUAL);
    if (!streamLock.isLockActive())
    {
        throw streamModeLockFailedEx;
    }
    stream->start();

    cv::Size patternSize(Config::getCircleRows(), Config::getCircleColumns());
    cv::Size projRes = getProjector()->getResolution();

    std::vector<cv::Point2f> projectedCirclePoints;
    cv::Mat grid = generateAsymmGrid(projRes, patternSize, Config::getProjectedCircleSpacing(),
                                     Config::getProjectedCircleDiameter(), projectedCirclePoints);
    cv::Mat white(projRes, CV_8UC1, cv::Scalar(255));
    cv::Mat img, grayImg, grayProjImg;

    GuiWindow window(_device);
    window.setImage(cv::Mat(projRes, CV_8UC1, cv::Scalar(255)));
 
    std::vector<cv::Point2f> camImagePoints, projImagePoints;
 
    Timer t;
    t.start();

    bool captured = false;
    while (!captured && !isCancelRequested())
    {
        window.setImage(white);
        Sleep(Config::getFrameDelay());

        cv::Mat img;
        while (!stream->triggerWaitRetrieve(img))
        {
        }

        bool found = cv::findCirclesGrid(img, patternSize, camImagePoints, cv::CALIB_CB_ASYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING);
        if (found)
        {
            // Display the projected circle grid and give the camera some time to refresh
            window.setImage(grid);
            Sleep(Config::getFrameDelay());

            cv::Mat projImg;
            while (!stream->triggerWaitRetrieve(projImg))
            {
            }

            // Isolate the projected circle grid by subtracting the
            // white calibration board from the black projected circle grid
            if (getCamera()->getVideoMode().videoFormat == VID_RGB24)
            {
                cv::cvtColor(img, grayImg, CV_RGB2GRAY);
                cv::cvtColor(projImg, grayProjImg, CV_RGB2GRAY);
            }
            else
            {
                grayImg = img;
                grayProjImg = projImg;
            }

            grayImg -= grayProjImg;

            // Set the image to only black or white (i.e. only 0 or 255) to
            // prevent the calibration board from interfering with the projected grid
            grayImg = grayImg >= Config::getContrastThreshold();
            grayImg ^= 255; // Invert

            // Find the projected circle grid
            bool foundProj = cv::findCirclesGrid(grayImg, patternSize, projImagePoints, cv::CALIB_CB_ASYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING);
            cv::drawChessboardCorners(projImg, patternSize, projImagePoints, foundProj);

            if (foundProj && t.elapsed() > boost::chrono::seconds(3))
            {
                cv::bitwise_not(projImg, projImg);
                t.stop();
                break;
            }

            img = projImg;

            // Clear the projected image
            window.setImage(white);
            cv::waitKey(Config::getFrameDelay());
        }

        CalibrationPreviewData data = {};
        data.currentPreview = img;
        data.currentPreviewWasAccepted = captured;
        data.numAcceptedCaptures = captured ? 1 : 0;

        invokePreviewCallbacks(data);
    }
	
	streamLock.unlock();
    if (isCancelRequested())
    {
        return false;
    }

    std::vector<cv::Point2f> objectPoints2D = generateCircleGridObjectPoints(patternSize, Config::getCircleDiameter(), Config::getCircleSpacing());
    std::vector<cv::Point3f> objectPoints = generateCircleGridObjectPoints3D(patternSize, Config::getCircleDiameter(), Config::getCircleSpacing());

    // Find the homography
    std::vector<cv::Point2f> undistCamImagePoints, undistProjImagePoints;

    IntrinsicCalibration camCalib;
    if (!loadIntrinsicCalibration(getCamera(), &camCalib))
    {
        std::cout << "ERROR (" << __FUNCTION__ << "): intrinsic camera calibration not found!" << std::endl;
        return false;
    }

    IntrinsicCalibration projCalib;
    if (!loadIntrinsicCalibration(_device, &projCalib))
    {
        std::cout << "ERROR (" << __FUNCTION__ << "): intrinsic projector calibration not found!" << std::endl;
        return false;
    }

    cv::undistortPoints(camImagePoints, undistCamImagePoints, camCalib.camera, camCalib.distortion);
    cv::undistortPoints(projImagePoints, undistProjImagePoints, camCalib.camera, camCalib.distortion);

    cv::Mat homography = cv::findHomography(undistCamImagePoints, objectPoints2D);

    std::vector<cv::Point2f> projDst;
    cv::perspectiveTransform(undistProjImagePoints, projDst, homography);

    std::vector<cv::Point3f> projUndistObjectPoints;
    for (unsigned int i = 0; i < projDst.size(); i++)
    {
        projUndistObjectPoints.push_back(cv::Point3f(projDst[i].x, projDst[i].y, 0));
    }

    cv::Mat camRvec, camTvec;
    cv::solvePnP(objectPoints, camImagePoints, camCalib.camera, camCalib.distortion, camRvec, camTvec);

    cv::Mat projRvec, projTvec;
    cv::solvePnP(projUndistObjectPoints, projectedCirclePoints, projCalib.camera, projCalib.distortion, projRvec, projTvec);

    cv::Matx23d camExtrinsic, projExtrinsic;
    for (int i = 0; i < 3; i++)
    {
        camExtrinsic(0, i) = (float)camRvec.at<double>(i, 0);
        camExtrinsic(1, i) = (float)camTvec.at<double>(i, 0);

        projExtrinsic(0, i) = (float)projRvec.at<double>(i, 0);
        projExtrinsic(1, i) = (float)projTvec.at<double>(i, 0);
    }

    CamProjExtrinsicCalibration extCalib = {};
    extCalib.camExtrinsic = camExtrinsic;
    extCalib.projExtrinsic = projExtrinsic;
    results = extCalib;

    saveCamProjExtrinsicCalibration(getCamera(), _device, results);

    return true;
}

CamProjExtrinsicCalibrator::CamProjExtrinsicCalibrator(Camera* const cam, Projector* const device)
    : Calibrator(cam), _device(device)
{
}

CamProjExtrinsicCalibrator::~CamProjExtrinsicCalibrator() { }

}
