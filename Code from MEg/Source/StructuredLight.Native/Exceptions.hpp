#pragma once

#include <exception>

//! \cond

#define PRINT_WARN(msg) std::cout << "WARNING: (" << __FILE__ << ":" << __LINE__ << "): " \
    << msg << std::endl;
#define THROW_EX(ex) std::cout << "ERROR: (" << __FILE__ << ":" << __LINE__ << "): " \
    << ex.what() << std::endl; throw ex;

#define CUSTOM_EXCEPTION(_name_, _msg_, _varName_) \
    const class _name_ : public std::exception { public: \
    virtual const char* what() const throw() { return _msg_; } } _varName_;

namespace sl {

CUSTOM_EXCEPTION(CameraConnectFailedException,
                 "Connecting to the camera failed.",
                 connectFailedEx)

CUSTOM_EXCEPTION(CameraDisconnectFailedException,
                 "Disconnecting from the camera failed.",
                 disconnectFailedEx)

CUSTOM_EXCEPTION(StreamAlreadyOpenException,
                 "A stream of a different type has already been opened.",
                 streamAlreadyOpenEx)

CUSTOM_EXCEPTION(StreamModeLockFailedException,
                 "An attempt to lock the stream mode failed.",
                 streamModeLockFailedEx)

CUSTOM_EXCEPTION(ConfigAlreadyLoadedException,
                 "The configuration for this instance of the library has already been loaded.",
                 configLoadedEx)
 
CUSTOM_EXCEPTION(ConfigNotLoadedException,
                 "The configuration for this instance of the library has not been loaded yet.",
                 configNotLoadedEx)

CUSTOM_EXCEPTION(CalibrationNotStartedException,
                 "The attempt to cancel calibration failed because a calibration has not been "
                 "started.", calibNotStartedEx)

CUSTOM_EXCEPTION(CalibrationAlreadyStartedException,
                 "The attempt to calibrate failed because a calibration has already been started.",
                 calibAlreadyStartedEx)

CUSTOM_EXCEPTION(CalibrationStartedException,
                 "This value may not be changed because a calibration has already been started.",
                 calibStartedEx);

CUSTOM_EXCEPTION(CameraInitializationFailed,
                 "An error occured while attempting to initialize the camera.",
                 cameraInitFailedEx);

CUSTOM_EXCEPTION(NotIntrinsiclyCalibrationException,
                 "At least one specified device is not intrinsicly calibrated",
                 notIntrinsiclyCalibEx);

CUSTOM_EXCEPTION(NoMorePatternsException,
                 "There are no more patterns to pop.",
                 noMorePatternsEx);

CUSTOM_EXCEPTION(ScanInProgressException,
                 "An error occurred because a scan is in progress.",
                 scanInProgressEx);

CUSTOM_EXCEPTION(GrayCodeDetectorNotRunningException,
                 "The detector is not running.",
                 detectorNotRunningEx);

CUSTOM_EXCEPTION(UseOfFreedMatException,
                 "Attempted to use a freed cv::Mat.",
                 useOfFreedMatEx);

CUSTOM_EXCEPTION(ScanningImageCaptureFailedException,
                 "An error occurred while attempting to capture images for scanning.",
                 scanImageCaptureFailedEx);
}

#undef CUSTOM_EXCEPTION

//! \endcond
