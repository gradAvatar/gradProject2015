#include "GuiWindow.hpp"
#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

namespace
{
    // Constants
    const CHAR* className = "Structured Light";

    void draw(const cv::Mat image, const HDC hDC, const HGLRC hRC);
    void handleResize(unsigned int width, unsigned int height);

    // http://blogs.msdn.com/b/oldnewthing/archive/2004/10/25/247180.aspx
    EXTERN_C IMAGE_DOS_HEADER __ImageBase;
    #define HINST_THISCOMPONENT ((HINSTANCE)&__ImageBase)
}

namespace sl {

class GuiWindow::impl
{
public:
    impl(std::string const& title_, cv::Size resolution_)
        : title(title_), resolution(resolution_), black(resolution_, CV_8UC3, cv::Scalar(0))
    {
    }

    // Common constructor
    void init(cv::Point2i position);

    std::string title;
    cv::Size resolution;
    cv::Mat black;

    // Windows stuff
    HDC hDC;
    HGLRC hRC;
    HWND hWnd;
};

void GuiWindow::impl::init(cv::Point2i position)
{
    WNDCLASS wc = {};
    wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wc.lpfnWndProc = DefWindowProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = HINST_THISCOMPONENT;
    wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = NULL;
    wc.lpszMenuName = NULL;
    wc.lpszClassName = className;

    RegisterClass(&wc);

    // Create the window
    hWnd = CreateWindowEx(WS_EX_APPWINDOW,
                          className,
                          title.c_str(),
                          WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
                          position.x, position.y,
                          resolution.width,
                          resolution.height,
                          NULL,
                          NULL,
                          HINST_THISCOMPONENT,
                          NULL);

    PIXELFORMATDESCRIPTOR pfd = {};
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 32;
    pfd.iLayerType = PFD_MAIN_PLANE;

    hDC = GetDC(hWnd);

    int pixelFormat = ChoosePixelFormat(hDC, &pfd);
    SetPixelFormat(hDC, pixelFormat, &pfd);
    hRC = wglCreateContext(hDC);

    wglMakeCurrent(hDC, hRC);

    ShowWindow(hWnd, SW_SHOW);
    SetForegroundWindow(hWnd);
    SetFocus(hWnd);

    // Initialize OpenGL
    handleResize(resolution.width, resolution.height);

    glShadeModel(GL_FLAT);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);

    // Draw the initial image
    draw(black, hDC, hRC);
}

GuiWindow::GuiWindow(const Projector* const proj)
    : _m(new impl(proj->getSerialNumber(), proj->getResolution()))
{
    _m->init(proj->getOrigin());
}

GuiWindow::~GuiWindow()
{
    close();
}

void GuiWindow::close()
{
    if (_m->hRC)
    {
        wglMakeCurrent(NULL, NULL);
        wglDeleteContext(_m->hRC);

        _m->hRC = NULL;
    }

    ReleaseDC(_m->hWnd, _m->hDC);
    DestroyWindow(_m->hWnd);
    UnregisterClass(className, HINST_THISCOMPONENT);
}

void GuiWindow::setImage(cv::Mat mat)
{
    draw(mat, _m->hDC, _m->hRC);
}

void GuiWindow::clear()
{
    setImage(_m->black);
}

}

namespace {

void draw(const cv::Mat image, const HDC hDC, const HGLRC hRC)
{
    wglMakeCurrent(hDC, hRC);

    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glPixelStorei(GL_UNPACK_SKIP_PIXELS, 0);
    glPixelStorei(GL_UNPACK_SKIP_ROWS, 0);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.cols, image.rows, 0, 
                 image.type() == CV_8UC1 ? GL_LUMINANCE : GL_BGR_EXT, GL_UNSIGNED_BYTE, image.data);

    glBegin(GL_QUADS);
        glTexCoord2i(0, 0); glVertex2i(0, 0);
        glTexCoord2i(0, 1); glVertex2i(0, image.rows);
        glTexCoord2i(1, 1); glVertex2i(image.cols, image.rows);
        glTexCoord2i(1, 0); glVertex2i(image.cols, 0);
    glEnd();

    glDisable(GL_TEXTURE_2D);
    SwapBuffers(hDC);
}

void handleResize(unsigned int width, unsigned int height)
{
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // Create a flat, 2D projection matrix
    gluOrtho2D(0, width, height, 0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

}
