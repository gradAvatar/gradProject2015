#pragma once

#include "Config.hpp"
#include "Scanner.hpp"
#include <string>
#include <unordered_map>
#include <opencv2/opencv.hpp>

#define SETTING(type, name) \
    private: type _cfg##name; \
    public: static inline type get##name() { return _obj->_cfg##name; } \
    public: static inline void set##name(type value) { _obj->_cfg####name = value; }

namespace sl {

class Config
{
public:
    static void loadConfig(std::string fileName);
    static void saveConfig();

    // Arbitrary user settings
    static inline std::string getUserSetting(std::string setting, std::string defaultValue = "")
    {
        auto i = _obj->_userSettings.find(setting);
        return i != _obj->_userSettings.cend() ? i->second : defaultValue;
    }

    static inline void setUserSetting(std::string setting, std::string value)
    {
        _obj->_userSettings[setting] = value;
    }

    SETTING(unsigned int, ContrastThreshold);
    SETTING(unsigned int, FrameDelay);
    SETTING(unsigned int, CircleRows);
    SETTING(unsigned int, CircleColumns);
    SETTING(float,        CircleDiameter);
    SETTING(float,        CircleSpacing);
    SETTING(unsigned int, ProjectedCircleDiameter);
    SETTING(unsigned int, ProjectedCircleSpacing);
    SETTING(float,        MotionThreshold);
    SETTING(std::string,  CalibrationFolder);
    SETTING(ScanType,     ScanType);

private:
    static std::unique_ptr<Config> _obj;

    // Class related state
    bool _loaded;
    std::string _fileName;
    std::unordered_map<std::string, std::string> _userSettings;

    Config() { }
};

#undef SETTING

}