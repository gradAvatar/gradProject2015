#pragma once

#include "Camera.hpp"
#include "CallbackManager.hpp"
#include "MatPool.hpp"
#include "Scanner.hpp"
#include "GrayCodeGenerator.hpp"
#include "PointCloudAlignment.hpp"
#include <memory>

namespace sl {

// Forward declarations
class Projector;

enum ScanType
{
    // Traditional black and white structured light scanning
    BW,

    // Takes two structured light patterns and combines them into a single RGB
    // image by placing one pattern into the green channel and another into both
    // the red and blue channels. This, in theory, works well with cameras that
    // utilize a Bayer-arrangement where, for each pixel, there are two green
    // sensors, one red, and one blue.
    BAYER,

    // Takes three structured light patterns and combines them into a single RGB
    // image simply by converting each pattern into an intensity map and
    // assigning each to the red, blue, and green channels in the image.
    RGB
};

struct ScanPreviewData
{
public:
    cv::Mat getPreview(unsigned int i) const { return *(_previews[i]); }
    unsigned int getPreviewCount() const { return _previewCount; }
    unsigned int getImageIndex() const { return _imageIndex; }
    unsigned int getTotalImages() const { return _totalImages; }

private:
    ScanPreviewData(std::vector<PooledMat> previews, unsigned int imgI, unsigned int totalImgs)
        : _previews(previews), _previewCount(previews.size()), _imageIndex(imgI),
          _totalImages(totalImgs)
    {
    }

    std::vector<PooledMat> _previews;
    const unsigned int _previewCount, _imageIndex, _totalImages;

    friend class Scanner;
};

class Scanner
{
public:
    typedef CallbackManager<ScanPreviewData>::Callback ScanPreviewCallback;
    typedef CallbackManager<void>::Callback ScanCompleteCallback;

    Scanner(Camera* const cam, Projector* const projector);
    Scanner(std::vector<Camera*> cams, std::vector<Projector*> projs,
            std::vector<GrayCodeColor> colors = std::vector<GrayCodeColor>());
    ~Scanner();

    void scan();
    void cancel();

    CallbackToken addPreviewCallback(ScanPreviewCallback cb);
    void removePreviewCallback(CallbackToken token);
    CallbackToken addFinishedCallback(ScanCompleteCallback cb);
    void removeFinishedCallback(CallbackToken token);

    std::string getOutputFileName();
    bool isScanning();

    void setOutputFileName(std::string const& fileName);

private:
    class impl;
    const std::unique_ptr<impl> _m;

    DISALLOW_COPY_AND_ASSIGN(Scanner);
};
 
}