#include <atlbase.h> // Include this before DirectShowCamera.hpp because
                     // of an issue with including strsafe.h before tchar.h
#include "DirectShowCamera.hpp"
#include "Exceptions.hpp"
#include <string>
#include <vector>

//
// Local declarations
//
namespace {

#define SAFE_RELEASE(p) { if (p) { (p)->Release(); p = NULL; } }

static bool _coinitializeCalled = false;

std::string getDeviceProperty(IMoniker* device, std::string propertyName);
std::vector<std::shared_ptr<IMoniker>> enumerateDevices();
void initializeGraph();
void initializeFilters();
void setupCamera(IMoniker* device, IBaseFilter** captureFilter, IAMVideoProcAmp** vidProcAmp, IAMCameraControl** camControl);
std::vector<std::shared_ptr<IPin>> enumeratePins(IBaseFilter* filter);
cv::Size setCameraResolution(ISampleGrabber* grabber, IPin* capturePin);
sl::VideoFormat getVideoFormat(AM_MEDIA_TYPE* amt, VIDEOINFOHEADER* vih);

void FreeMediaType(AM_MEDIA_TYPE& mt);
void DeleteMediaType(AM_MEDIA_TYPE *pmt);

}

//
// Library implementations
//
namespace sl {

void DirectShowCamera::enumerateCameras(std::vector<std::shared_ptr<Camera>>& cams)
{
    std::vector<std::shared_ptr<IMoniker>> devices = enumerateDevices();
    for (unsigned int i = 0; i < devices.size(); i++)
    {
        // Because DirectShow is awful
        //cams.push_back(createSharedPtr(new DirectShowCamera(devices[i])));
    }
}

DirectShowCamera::DirectShowCamera(std::shared_ptr<IMoniker> device)
    : Camera(CAM_DIRECTSHOW)
{
    std::lock_guard<tbb::recursive_mutex> lock(_ctrlLock);

    IMoniker* devPtr = device.get();

    _devicePath = getDeviceProperty(devPtr, "DevicePath");
    _modelName = getDeviceProperty(devPtr, "FriendlyName");

    HRESULT hr;

    // Create the DirectShow graph and its filters
    initializeGraph();
    initializeFilters(devPtr);

    auto capturePins = enumeratePins(_captureFilter);
    auto grabberPins = enumeratePins(_grabberFilter);
    auto nullPins = enumeratePins(_nullRenderFilter);

    _capturePin = capturePins[0];

    // Camera -> Sample Grabber -> Null Renderer
    hr = _graph->Connect(capturePins[0].get(), grabberPins[0].get());
    hr = _graph->Connect(grabberPins[1].get(), nullPins[0].get());

    // Only capture a single frame each time
    hr = _grabber->SetOneShot(FALSE);
    hr = _grabber->SetBufferSamples(FALSE);
    
    _callback = new SampleGrabberCallback(this);
    hr = _grabber->SetCallback(_callback, 0);
    hr = _graph->QueryInterface(IID_PPV_ARGS(&_mediaController));

    // Set the highest resolution setting as the default
    auto modes = enumerateVideoModes();
    if (modes.size() > 0)
    {
        VideoMode largest = *std::max_element(begin(modes), end(modes),
            [](VideoMode a, VideoMode b)
            {
                return a.getResolution().area() < b.getResolution().area();
            });

        setVideoMode(largest);
    }
}

DirectShowCamera::~DirectShowCamera()
{
    disconnect();

    if (_coinitializeCalled)
    {
        CoUninitialize();
    }
}

std::vector<VideoMode> DirectShowCamera::enumerateVideoModes()
{
    IAMStreamConfig* streamControl;
    HRESULT hr = _capturePin->QueryInterface(IID_PPV_ARGS(&streamControl));

    int numCaps, rawFormatSize;
    hr = streamControl->GetNumberOfCapabilities(&numCaps, &rawFormatSize);

    std::vector<VideoMode> modes;

    // VIDEO_STREAM_CONFIG_CAPS is deprecated, use AM_MEDIA_TYPE instead (except for FPS)
    BYTE* rawFormat = new BYTE[rawFormatSize];
    VIDEO_STREAM_CONFIG_CAPS* format = reinterpret_cast<VIDEO_STREAM_CONFIG_CAPS*>(rawFormat);
    for (int i = 0; i < numCaps; i++)
    {
        AM_MEDIA_TYPE* amt;
        hr = streamControl->GetStreamCaps(i, &amt, rawFormat);
        
        auto deleter = [](void* ptr) { DeleteMediaType(reinterpret_cast<AM_MEDIA_TYPE*>(ptr)); };
        std::shared_ptr<void> amtPtr(amt, deleter);

        VIDEOINFOHEADER* vih = reinterpret_cast<VIDEOINFOHEADER*>(amt->pbFormat);
        VideoFormat vidFormat = getVideoFormat(amt, vih);
        if (vidFormat == VID_UNKNOWN)
        {
            continue;
        }

        vih->AvgTimePerFrame = 10000000 / format->MinFrameInterval;

        VideoMode mode;
        mode.width = vih->bmiHeader.biWidth;
        mode.height = vih->bmiHeader.biHeight;
        mode.framesPerSecond = static_cast<unsigned int>(10000000 / format->MinFrameInterval);
        mode.videoFormat = vidFormat;
        mode.data = amtPtr;

        modes.push_back(mode);
    }

    delete[] rawFormat;
    SAFE_RELEASE(streamControl);

    return modes;
}

VideoMode DirectShowCamera::createCustomVideoMode(unsigned int width, unsigned int height,
                                                  unsigned int fps, VideoFormat vidFormat) const
{
    VideoMode mode = {};
    mode.data = 0; // NULL pointer indicates unsupported mode
    return mode;
}

std::string DirectShowCamera::getModelName() const { return _modelName; }
std::string DirectShowCamera::getSerialNumber() const { return _devicePath; }
VideoMode DirectShowCamera::getVideoMode() const { return _vidMode; }
CustomVideoModeConstraints DirectShowCamera::getCustomVideoModeConstraints() const
{
    CustomVideoModeConstraints constraints = {};
    constraints.supported = false;
    return constraints;
}

bool DirectShowCamera::setVideoMode(VideoMode mode)
{
    if (isStreamOpen())
    {
        return false;
    }

    if (!mode.data)
    {
        return false;
    }

    std::lock_guard<tbb::recursive_mutex> lock(_ctrlLock);

    IAMStreamConfig* streamControl;
    HRESULT hr = _capturePin->QueryInterface(IID_PPV_ARGS(&streamControl));

    AM_MEDIA_TYPE* amt = reinterpret_cast<AM_MEDIA_TYPE*>(mode.data.get());
    streamControl->SetFormat(amt);
    _grabber->SetMediaType(amt);

    SAFE_RELEASE(streamControl);

    _vidMode = mode;

    return true;
}

bool DirectShowCamera::connect()
{
    if (isConnected())
    {
        return false;
    }

    std::lock_guard<tbb::recursive_mutex> lock(_ctrlLock);

    _captureFrame = false;
    _bufferFilled = false;
    _buffer = cv::Mat(_vidMode.getResolution(), CV_8UC3);

    // Start the camera
    HRESULT hr = _mediaController->Run();
    if (FAILED(hr))
    {
        return false;
    }

    setIsConnected(true);
    return true;
}

bool DirectShowCamera::disconnect()
{
    if (!isConnected())
    {
        return false;
    }

    std::lock_guard<tbb::recursive_mutex> lock(_ctrlLock);

    _captureFrame = false;
    _bufferFilled = false;

    HRESULT hr = _mediaController->Stop();
    _buffer.release();
    setIsConnected(false);

    return true;
}

bool DirectShowCamera::capture()
{
    if (!isConnected())
    {
        return false;
    }

    // Signal the callback to save the next frame
    _captureFrame = true;
    return true;
}

bool DirectShowCamera::retrieve(cv::Mat& image)
{
    if (!isConnected())
    {
        return false;
    }

    // Wait until the buffer is filled
    std::unique_lock<tbb::mutex> lock(_bufferLock);
    while (!_bufferFilled)
    {
        _bufferReady.wait(lock);
    }

    bool result;
    if (isConnected() && _bufferFilled)
    {
        cv::flip(_buffer, _buffer, 0);
        _buffer.copyTo(image);
        _bufferFilled = false;

        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}

void DirectShowCamera::initializeGraph()
{
    HRESULT hr;

    // Initialize COM library
    if (!_coinitializeCalled)
    {
        hr = CoInitialize(NULL);
        _coinitializeCalled = true;
    }

    // Initialize the various Graph objects
    hr = CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&_graph));
}

void DirectShowCamera::initializeFilters(IMoniker* device)
{
    HRESULT hr;

    // Create the camera filter
    hr = device->BindToObject(0, 0, IID_PPV_ARGS(&_captureFilter));
    hr = (_captureFilter)->QueryInterface(IID_PPV_ARGS(&_vidProcAmp));
    hr = (_captureFilter)->QueryInterface(IID_PPV_ARGS(&_camCtrl));
    hr = _graph->AddFilter(_captureFilter, L"Capture Filter");

    // Create the SampleGrabber
    hr = CoCreateInstance(CLSID_SampleGrabber, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&_grabberFilter));
    hr = _graph->AddFilter(_grabberFilter, L"Sample Grabber");
    hr = _grabberFilter->QueryInterface(IID_ISampleGrabber, (void**)&_grabber);

    // Create the null renderer to serve as the final filter in the graph
    hr = CoCreateInstance(CLSID_NullRenderer, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&_nullRenderFilter));
    hr = _graph->AddFilter(_nullRenderFilter, L"Null Renderer");
}

bool DirectShowCamera::getCameraSetting(CameraSetting setting, double* value, CameraSettingFlags* flags)
{
    std::unique_lock<tbb::mutex> lock(_bufferLock);

    long prop = 0;

    switch (setting)
    {
    case BRIGHTNESS: prop = VideoProcAmp_Brightness; break;
    case CONTRAST: prop = VideoProcAmp_Contrast; break;
    case HUE: prop = VideoProcAmp_Hue; break;
    case SATURATION: prop = VideoProcAmp_Saturation; break;
    case SHARPNESS: prop = VideoProcAmp_Sharpness; break;
    case GAMMA: prop = VideoProcAmp_Gamma; break;
    case WHITE_BALANCE: prop = VideoProcAmp_WhiteBalance; break;
    case GAIN: prop = VideoProcAmp_Gain; break;
    case EXPOSURE: prop = CameraControl_Exposure; break;
    case FOCUS: prop = CameraControl_Focus; break;
    default:
        *flags = NOT_SUPPORTED;
        return false;
    }

    long valueL;

    HRESULT hr;
    long rawFlags;
    if (setting == EXPOSURE || setting == FOCUS)
    {
        hr = _camCtrl->Get(prop, &valueL, &rawFlags);
    }
    else
    {
        hr = _vidProcAmp->Get(prop, &valueL, &rawFlags);
    }

    *value = (double)valueL;
    *flags = (CameraSettingFlags)rawFlags;

    return SUCCEEDED(hr);
}

bool DirectShowCamera::setCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags)
{
    std::unique_lock<tbb::mutex> lock(_bufferLock);

    long prop = 0;

    switch (setting)
    {
    case BRIGHTNESS: prop = VideoProcAmp_Brightness; break;
    case CONTRAST: prop = VideoProcAmp_Contrast; break;
    case HUE: prop = VideoProcAmp_Hue; break;
    case SATURATION: prop = VideoProcAmp_Saturation; break;
    case SHARPNESS: prop = VideoProcAmp_Sharpness; break;
    case GAMMA: prop = VideoProcAmp_Gamma; break;
    case WHITE_BALANCE: prop = VideoProcAmp_WhiteBalance; break;
    case GAIN: prop = VideoProcAmp_Gain; break;
    case EXPOSURE: prop = CameraControl_Exposure; break;
    case FOCUS: prop = CameraControl_Focus; break;
    default:
        return false;
    }

    long valueL = (long)value;

    HRESULT hr;
    if (setting == EXPOSURE || setting == FOCUS)
    {
        hr = _camCtrl->Set(prop, valueL, flags);
    }
    else
    {
        hr = _vidProcAmp->Set(prop, valueL, flags);
    }

    return SUCCEEDED(hr);
}

bool DirectShowCamera::getCameraSettingRange(CameraSetting setting, double* lower, double* delta, double* upper, double* def)
{
    std::unique_lock<tbb::mutex> lock(_bufferLock);

    long prop = 0;

    switch (setting)
    {
    case BRIGHTNESS: prop = VideoProcAmp_Brightness; break;
    case CONTRAST: prop = VideoProcAmp_Contrast; break;
    case HUE: prop = VideoProcAmp_Hue; break;
    case SATURATION: prop = VideoProcAmp_Saturation; break;
    case SHARPNESS: prop = VideoProcAmp_Sharpness; break;
    case GAMMA: prop = VideoProcAmp_Gamma; break;
    case WHITE_BALANCE: prop = VideoProcAmp_WhiteBalance; break;
    case GAIN: prop = VideoProcAmp_Gain; break;
    case EXPOSURE: prop = CameraControl_Exposure; break;
    case FOCUS: prop = CameraControl_Focus; break;
    default:
        return false;
    }

    HRESULT hr;
    long dummy;

    long lowerL, deltaL, upperL, defL;

    if (setting == EXPOSURE || setting == FOCUS)
    {
        hr = _camCtrl->GetRange(prop, &lowerL, &upperL, &deltaL, &defL, &dummy);
    }
    else
    {
        hr = _vidProcAmp->GetRange(prop, &lowerL, &upperL, &deltaL, &defL, &dummy);
    }

    if (!SUCCEEDED(hr))
    {
        return false;
    }

    *lower = (long)lowerL;
    *delta = (long)deltaL;
    *upper = (long)upperL;
    *def = (long)defL;

    return true;
}

STDMETHODIMP DirectShowCamera::SampleGrabberCallback::QueryInterface(REFIID riid, void** ppvObject)
{
    if (NULL == ppvObject)
    {
        return E_POINTER;
    }

    if (riid == __uuidof(IUnknown))
    {
        *ppvObject = static_cast<IUnknown*>(this);
        return S_OK;
    }

    if (riid == __uuidof(ISampleGrabberCB))
    {
        *ppvObject = static_cast<ISampleGrabberCB*>(this);
        return S_OK;
    }

    return E_NOTIMPL;
}

STDMETHODIMP DirectShowCamera::SampleGrabberCallback::SampleCB(double Time, IMediaSample* pSample)
{
    if (!_cam->_captureFrame)
    {
        return S_OK;
    }

    BYTE* pointer;
    pSample->GetPointer(&pointer);

    std::lock_guard<tbb::mutex> lock(_cam->_bufferLock);

    cv::Mat temp(_cam->_vidMode.getResolution(), CV_8UC3, (uchar*)pointer);
    temp.copyTo(_cam->_buffer);
        
    _cam->_captureFrame = false;
    _cam->_bufferFilled = true;
    _cam->_bufferReady.notify_all();

    return S_OK;
}

}

//
// Local implementations
//
namespace {

std::string getDeviceProperty(IMoniker* device, std::string propertyName)
{
    IPropertyBag* properties;
    device->BindToStorage(0, 0, IID_IPropertyBag, (void**)&properties);

    VARIANT name;
    VariantInit(&name);

    HRESULT hr = properties->Read((LPCOLESTR)CA2W(propertyName.c_str()), &name, 0);
    std::wstring str(name.bstrVal);

    VariantClear(&name);
    SAFE_RELEASE(properties);

    return static_cast<std::string>(CW2A(str.c_str()));
}

std::vector<std::shared_ptr<IMoniker>> enumerateDevices()
{
    HRESULT hr;

    if (!_coinitializeCalled)
    {
        hr = CoInitialize(NULL);
    }

    ICreateDevEnum* dev;
    hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&dev));

    IEnumMoniker* enumer;
    hr = dev->CreateClassEnumerator(CLSID_VideoInputDeviceCategory, &enumer, 0);

    std::vector<std::shared_ptr<IMoniker>> devices;
    while (enumer)
    {
        IMoniker* device;
        ULONG fetched;

        HRESULT foundDev = enumer->Next(1, &device, &fetched);

        if (foundDev == S_OK)
        {
            auto ptr = std::shared_ptr<IMoniker>(device, [](IMoniker* m) { SAFE_RELEASE(m); });
            devices.push_back(ptr);
        }
        else
        {
            break;
        }
    }

    SAFE_RELEASE(dev);
    SAFE_RELEASE(enumer);

    return devices;
}

std::vector<std::shared_ptr<IPin>> enumeratePins(IBaseFilter* filter)
{
    IEnumPins* pinEnum;
    HRESULT hr = filter->EnumPins(&pinEnum);

    std::vector<std::shared_ptr<IPin>> pins;
    while (true)
    {
        IPin* pin;
        ULONG fetched;

        HRESULT foundDev = pinEnum->Next(1, &pin, &fetched);

        if (foundDev == S_OK)
        {
            std::shared_ptr<IPin> ptr(pin, [](IPin* p) { SAFE_RELEASE(p); });
            pins.push_back(ptr);
        }
        else
        {
            break;
        }
    }

    SAFE_RELEASE(pinEnum);

    return pins;
}

sl::VideoFormat getVideoFormat(AM_MEDIA_TYPE* amt, VIDEOINFOHEADER* vih)
{
    sl::VideoFormat vidFormat;

    // MEDIASUBTYPE_RGB8/24/32/etc are all the
    // same GUIDs, so we have to check the bitrate too
    if (amt->subtype == MEDIASUBTYPE_RGB24 && vih->bmiHeader.biBitCount == 24)
    {
        vidFormat = sl::VID_RGB24;
    }
    else
    {
        vidFormat = sl::VID_UNKNOWN;
    }

    return vidFormat;
}

// Release the format block for a media type.
// See remarks: http://msdn.microsoft.com/en-us/library/windows/desktop/dd375432%28v=vs.85%29.aspx
void FreeMediaType(AM_MEDIA_TYPE& mt)
{
    if (mt.cbFormat != 0)
    {
        CoTaskMemFree((PVOID)mt.pbFormat);
        mt.cbFormat = 0;
        mt.pbFormat = NULL;
    }
    if (mt.pUnk != NULL)
    {
        // pUnk should not be used.
        mt.pUnk->Release();
        mt.pUnk = NULL;
    }
}

// Delete a media type structure that was allocated on the heap.
// See remarks: http://msdn.microsoft.com/en-us/library/windows/desktop/dd375432%28v=vs.85%29.aspx
void DeleteMediaType(AM_MEDIA_TYPE *pmt)
{
    if (pmt != NULL)
    {
        FreeMediaType(*pmt); 
        CoTaskMemFree(pmt);
    }
}

}