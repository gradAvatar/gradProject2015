#include "AlignPair.hpp"
#include "OccupancyGrid.hpp"
#include "AlignGlobal.hpp"
#include "VCGPointCloudAlignment.hpp"

#undef __cdecl
#include <vcg/simplex/vertex/component_ocf.h>
#include <vcg/simplex/face/component_ocf.h>
#include <wrap/io_trimesh/export_ply.h>
#include <wrap/io_trimesh/import_ply.h>
#include <wrap/ply/plylib.cpp>

// Forward declarations needed for creating the used types
class CVertexO;
class CEdgeO;
class CFaceO;

// Declaration of the semantic of the used types
class CUsedTypesO: public vcg::UsedTypes < vcg::Use<CVertexO>::AsVertexType,
                                           vcg::Use<CEdgeO   >::AsEdgeType,
                                           vcg::Use<CFaceO  >::AsFaceType >{};


// The Main Vertex Class
// Most of the attributes are optional and must be enabled before use.
// Each vertex needs 40 byte, on 32bit arch. and 44 byte on 64bit arch.

class CVertexO  : public vcg::Vertex< CUsedTypesO,
  vcg::vertex::InfoOcf,           /*  4b */
  vcg::vertex::Coord3f,           /* 12b */
  vcg::vertex::BitFlags,          /*  4b */
  vcg::vertex::Normal3f,          /* 12b */
  vcg::vertex::Qualityf,          /*  4b */
  vcg::vertex::Color4b,           /*  4b */
  vcg::vertex::VFAdjOcf,          /*  0b */
  vcg::vertex::MarkOcf,           /*  0b */
  vcg::vertex::TexCoordfOcf,      /*  0b */
  vcg::vertex::CurvaturefOcf,     /*  0b */
  vcg::vertex::CurvatureDirfOcf,  /*  0b */
  vcg::vertex::RadiusfOcf         /*  0b */
  >{
};


// The Main Edge Class
// Currently it does not contains anything.
class CEdgeO : public vcg::Edge<CUsedTypesO,
    vcg::edge::BitFlags,          /*  4b */
    vcg::edge::EVAdj,
    vcg::edge::EEAdj
    >{
};

// Each face needs 32 byte, on 32bit arch. and 48 byte on 64bit arch.
class CFaceO    : public vcg::Face<  CUsedTypesO,
      vcg::face::InfoOcf,              /* 4b */
      vcg::face::VertexRef,            /*12b */
      vcg::face::BitFlags,             /* 4b */
      vcg::face::Normal3f,             /*12b */
      vcg::face::QualityfOcf,          /* 0b */
      vcg::face::MarkOcf,              /* 0b */
      vcg::face::Color4bOcf,           /* 0b */
      vcg::face::FFAdjOcf,             /* 0b */
      vcg::face::VFAdjOcf,             /* 0b */
      vcg::face::WedgeTexCoordfOcf     /* 0b */
    > {};

class CMeshO    : public vcg::tri::TriMesh< vcg::vertex::vector_ocf<CVertexO>, vcg::face::vector_ocf<CFaceO> >
{
public:
	int sfn; //The number of selected faces.
	int svn; //The number of selected faces.
	vcg::Matrix44d Tr; // Usually it is the identity. It is applied in rendering and filters can or cannot use it. (most of the filter will ignore this)

	const vcg::Box3f &trBB()
	{
		vcg::Box3f bb;
		bb.Add(vcg::Matrix44f::Construct(Tr),bbox);
		return bb;
	}
};


class MeshNode
{
public:
	MeshNode(CMeshO *_m, int _id)
	{
		m=_m;
		id = _id;
		glued=true;
	}
	MeshNode() { m=0;id=-1;}
	bool glued;
	int id;
	CMeshO *m;
	vcg::Matrix44d &tr() {return m->Tr;}
	const vcg::Box3f &bbox() const {return m->bbox;}
};

void ProcessArc(CMeshO &fix, CMeshO &mov, vcg::AlignPair::Result &result, vcg::AlignPair::Param ap)
{	
	vcg::Matrix44d FixM=vcg::Matrix44d::Construct(fix.Tr);		
	vcg::Matrix44d MovM=vcg::Matrix44d::Construct(mov.Tr);		
	vcg::Matrix44d MovToFix = Inverse(FixM) * MovM;

	vcg::AlignPair::A2Mesh Fix, Mov;
	vcg::AlignPair aa;

	// 1) Convert fixed mesh and put it into the grid.
	aa.ConvertMesh<CMeshO>(fix, Fix);
	vcg::AlignPair::A2Grid UG;
	vcg::AlignPair::A2GridVert VG;

	Fix.InitVert(vcg::Matrix44d::Identity(),false);
	vcg::AlignPair::InitFixVert(&Fix,ap,VG);

	// 2) Convert the second mesh and sample a <ap.SampleNum> points on it.
	aa.ConvertMesh<CMeshO>(mov,Mov);

	std::vector<vcg::AlignPair::A2Vertex> tmpmv;     
	aa.ConvertVertex(Mov.vert,tmpmv);
	aa.SampleMovVert(tmpmv, ap.SampleNum, ap.SampleMode);

	aa.mov=&tmpmv;
	aa.fix=&Fix;
	aa.ap = ap;

	vcg::Matrix44d In=MovM;
	// Perform the ICP algorithm
	aa.Align(In,UG,VG,result);
	result.FixName = 0;
	result.MovName = 1;
	result.as.Dump(stdout);
}

void ProcessGlobal(std::vector<MeshNode*> nodeList, std::vector<vcg::AlignPair::Result *> ResVecPtr, vcg::AlignPair::Param &ap)
{
	vcg::Matrix44d Zero44; Zero44.SetZero();
	std::vector<vcg::Matrix44d> PaddedTrVec(nodeList.size(),Zero44);
	// matrix trv[i] is relative to mesh with id IdVec[i]
	// if all the mesh are glued GluedIdVec=={1,2,3,4...}
	std::vector<int> GluedIdVec;
	std::vector<vcg::Matrix44d> GluedTrVec;
		
	std::vector<std::string> names(nodeList.size());
	
	for (int i = 0; i < nodeList.size(); i++)
	{
		MeshNode* mn = nodeList[i];
		if(mn->glued)
		{
			GluedIdVec.push_back(mn->id);							
			GluedTrVec.push_back(vcg::Matrix44d::Construct(mn->tr()));				
			PaddedTrVec[mn->id]=GluedTrVec.back();
		}
	}		
		
	vcg::AlignGlobal AG;
	AG.BuildGraph(ResVecPtr, GluedTrVec, GluedIdVec);
   
	int maxiter = 1000;
	float StartGlobErr = 0.001f;
	while(!AG.GlobalAlign(names, 	StartGlobErr, 100, true, stdout)){
		StartGlobErr*=2;
		AG.BuildGraph(ResVecPtr,GluedTrVec, GluedIdVec);
	}

	std::vector<vcg::Matrix44d> GluedTrVecOut(GluedTrVec.size());
	AG.GetMatrixVector(GluedTrVecOut,GluedIdVec);

	//Now get back the results!
	for(int ii=0;ii<GluedTrVecOut.size();++ii)
		nodeList[ii]->m->Tr.Import(GluedTrVecOut[ii]);	

}

vcg::Matrix44d vcgalignPointClouds(CMeshO &fixed, CMeshO &movable, vcg::Matrix44d transform)
{
	std::vector<MeshNode*> nodeList;
	nodeList.push_back(new MeshNode(&fixed, 0));
	nodeList.push_back(new MeshNode(&movable, 1));

	vcg::OccupancyGrid grid;
	std::vector<vcg::AlignPair::Result> ResVec;
	std::vector<vcg::AlignPair::Result *> ResVecPtr;

	// Find the total bounding box of both meshes
	vcg::Box3d totalBBox;
	totalBBox.Add(fixed.Tr, vcg::Box3d::Construct(fixed.bbox));
	totalBBox.Add(movable.Tr, vcg::Box3d::Construct(movable.bbox));

	fixed.Tr = vcg::Matrix44d::Identity();
	movable.Tr = transform;

	grid.Init(2, totalBBox, 10000);
	grid.AddMesh(fixed, vcg::Matrix44d::Identity(), 0);
	grid.AddMesh(movable, transform, 1);
	grid.Compute();
	grid.Dump(stdout);

	ResVec.clear();
	ResVec.resize(grid.SVA.size());
	ResVecPtr.clear();

	int i;
	for(i=0;i<grid.SVA.size() && grid.SVA[i].norm_area > .1; ++i)
	{
		ProcessArc(fixed, movable, ResVec[i], vcg::AlignPair::Param());
		ResVec[i].area = grid.SVA[i].norm_area;
		if (ResVec[i].IsValid())
			ResVecPtr.push_back(&ResVec[i]);

		std::pair<double,double> dd=ResVec[i].ComputeAvgErr();
		if( ResVec[i].IsValid() ) 
			printf("(%2i/%i) %2i -> %2i Aligned AvgErr dd=%f -> dd=%f \n",int(i+1),int(grid.SVA.size()),grid.SVA[i].s,grid.SVA[i].t,dd.first,dd.second);
	}

	ResVec.resize(i);
	ProcessGlobal(nodeList, ResVecPtr, vcg::AlignPair::Param());

	return ResVecPtr[0]->Tr;
}

namespace {

void Point3fVectorToCMeshO(std::vector<cv::Vec6f> &vec, CMeshO &converted)
{
	CMeshO::VertexIterator vi;

	converted.vert.resize(vec.size());
	int i = 0;
	for (vi = converted.vert.begin(); vi != converted.vert.end(); vi++)
	{
		(*vi).P().Import(vcg::Point3f(vec[i][0], vec[i][1], vec[i][2]));
		(*vi).N().Import(vcg::Point3f(vec[i][3], vec[i][4], vec[i][5]));
		i++;
	}

	converted.vn = vec.size();
	converted.Tr = vcg::Matrix44d::Identity();
	vcg::tri::UpdateBounding<CMeshO>::Box(converted);
}

}

namespace sl {

cv::Mat_<float> VCGPointCloudAlignment::alignPointClouds(std::vector<cv::Vec6f> fixed, std::vector<cv::Vec6f> movable)
{
	CMeshO fixedMesh, movableMesh;
	Point3fVectorToCMeshO(fixed, fixedMesh);
	Point3fVectorToCMeshO(movable, movableMesh);

	vcg::tri::UpdateBounding<CMeshO>::Box(fixedMesh);
	vcg::tri::UpdateBounding<CMeshO>::Box(movableMesh);

	// TODO: intelligent transform picking (pick the one with the lowest error?)
	vcg::Matrix44d transform = vcg::Matrix44d::Identity();
	for (int i = 0; i < 4; i++)
	{
		transform = vcgalignPointClouds(fixedMesh, movableMesh, transform);
	}
	
	cv::Mat_<float> cvTr(4, 4);
	for (int r = 0; r < 4; r++)
	{
		for (int c = 0; c < 4; c++)
		{
			cvTr(r, c) = transform.ElementAt(r, c);
		}
	}

	return cvTr;
}

/*
int main()
{
	vcg::tri::io::ImporterPLY<CMeshO> f;
	CMeshO fixed, movable;
	f.Open(fixed, "C:\\left.ply");
	f.Open(movable, "C:\\right.ply");

	fixed.Tr = vcg::Matrix44d::Identity();
	movable.Tr = vcg::Matrix44d::Identity();

	vcg::tri::UpdateBounding<CMeshO>::Box(fixed);
	vcg::tri::UpdateBounding<CMeshO>::Box(movable);

	vcg::Matrix44d transform = alignPointClouds(fixed, movable);
	transform.print();
	transform = alignPointClouds(fixed, movable);
	transform.print();
	transform = alignPointClouds(fixed, movable);
	transform.print();
	transform = alignPointClouds(fixed, movable);
	transform.print();
	transform = alignPointClouds(fixed, movable);
	transform.print();
	transform = alignPointClouds(fixed, movable);
	transform.print();
	transform = alignPointClouds(fixed, movable);
	transform.print();
}
*/
}