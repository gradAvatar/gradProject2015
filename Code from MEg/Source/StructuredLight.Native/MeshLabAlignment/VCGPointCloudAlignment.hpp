#pragma once

#include <opencv2/opencv.hpp>
#include <fstream>
#include <iostream>
#include <sstream>

namespace sl {
	
class VCGPointCloudAlignment
{
public:
	static cv::Mat_<float> alignPointClouds(std::vector<cv::Vec6f> fixed, std::vector<cv::Vec6f> movable);
};

}