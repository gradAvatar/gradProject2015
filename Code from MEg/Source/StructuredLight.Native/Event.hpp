#pragma once

#include <memory>
#include "Utils.hpp"

namespace sl {

enum EVENT_STATUS
{
    EV_UNKNOWN,
    EV_OK,
    EV_TIMED_OUT
};

template <bool BManualReset>
class Event
{
public:
    Event(bool isInitiallySet);
    ~Event();

    bool isSet();

    void set();
    void reset();
    EVENT_STATUS wait(unsigned int timeout = 0xFFFFFFFF);

private:
    struct impl;
    const std::unique_ptr<impl> _m;

    DISALLOW_COPY_AND_ASSIGN(Event);
};

typedef Event<true> ManualResetEvent;
typedef Event<false> AutoResetEvent;

}