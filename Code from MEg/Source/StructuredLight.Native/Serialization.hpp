#pragma once

#include <string>

namespace sl {

class Camera;
class Projector;
struct IntrinsicCalibration;
struct CamProjExtrinsicCalibration;
struct CamCamExtrinsicCalibration;

bool loadIntrinsicCalibration(const Camera* const cam, IntrinsicCalibration* const calib = nullptr);
bool loadIntrinsicCalibration(const Projector* const proj, IntrinsicCalibration* const calib = nullptr);
void saveIntrinsicCalibration(const Camera* const cam, const IntrinsicCalibration& calib);
void saveIntrinsicCalibration(const Projector* const proj, const IntrinsicCalibration& calib);

bool loadCamProjExtrinsicCalibration(const Camera* const cam, const Projector* const proj,
                                     CamProjExtrinsicCalibration* const calib = nullptr);
void saveCamProjExtrinsicCalibration(const Camera* const cam, const Projector* const proj,
                                     const CamProjExtrinsicCalibration& calib);

bool loadCamCamExtrinsicCalibration(const Camera* const source, const Camera* const target,
                                    CamCamExtrinsicCalibration* const calib = nullptr);
void saveCamCamExtrinsicCalibration(const Camera* const source, const Camera* const target,
                                    const CamCamExtrinsicCalibration& calib);

bool getFriendlyName(const Camera* const cam, std::string* const name = nullptr);
bool getFriendlyName(const Projector* const proj, std::string* const name = nullptr);
void setFriendlyName(const Camera* const cam, const std::string& name);
void setFriendlyName(const Projector* const proj, const std::string& name);

}
