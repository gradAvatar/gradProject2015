#include "Projector.hpp"
#include "Serialization.hpp"
#include <boost/timer/timer.hpp>
#include <Windows.h>
#include <SetupAPI.h>
#include <tbb/mutex.h>
#include <tbb/compat/thread>
#include <tbb/compat/condition_variable>

// http://msdn.microsoft.com/en-us/library/windows/hardware/ff545901%28v=vs.85%29.aspx
DEFINE_GUID(GUID_DEVINTERFACE_MONITOR, 0xE6F07B5F, 0xEE97, 0x4A90, 0xB0, 0x76, 0x33, 0xF5, 0x7B,
                                       0xF4, 0xEA, 0xA7);

namespace {

double log2(double x) { return log(x) / 0.6931471805599453; } // ln(x) / ln(2)
bool getMonitorRegistryKey(std::string const& deviceId, HKEY& key);
bool getDisplayInformation(std::string const& deviceId, std::string& friendlyName, std::string& serial);
    
}

namespace sl {

std::vector<std::shared_ptr<Projector>> Projector::enumerateProjectors()
{
    DWORD devNum = 0;
    DISPLAY_DEVICE device;
    device.cb = sizeof(DISPLAY_DEVICE);

    std::vector<std::shared_ptr<Projector>> devs;

    char deviceName[32] = "";
    while (EnumDisplayDevices(NULL, devNum, &device, EDD_GET_DEVICE_INTERFACE_NAME))
    {
        strcpy_s(deviceName, device.DeviceName);
        EnumDisplayDevices(deviceName, 0, &device, EDD_GET_DEVICE_INTERFACE_NAME);

        if (device.StateFlags & DISPLAY_DEVICE_ATTACHED)
        {
            DEVMODE devMode = {};
            devMode.dmSize = sizeof(devMode);
            EnumDisplaySettings(deviceName, -1, &devMode);

            std::string friendlyName, serial;
            getDisplayInformation(device.DeviceID, friendlyName, serial);

            cv::Point2i origin(devMode.dmPosition.x, devMode.dmPosition.y);
            cv::Size resolution(devMode.dmPelsWidth, devMode.dmPelsHeight);

            if (serial == "")
            {
                serial = device.DeviceID;
            }

            auto dev = new Projector(device.DeviceID, friendlyName, serial, origin, resolution);
            devs.push_back(std::shared_ptr<Projector>(dev));
        }

        devNum++;
    }

    return devs;
}

Projector::Projector(std::string devId, std::string name, std::string serial, cv::Point2i origin,
                     cv::Size res)
    : _devId(devId), _name(name), _serial(serial), _origin(origin), _res(res)
{
}

GrayCodeParameters Projector::getGrayCodeParameters() const
{
    GrayCodeParameters params;
    cv::Size res = getResolution();

    // Calculate the number of required horizontal and vertical patterns
    params.numColumns = (unsigned int)ceil(log2(res.width));
    params.numRows = (unsigned int)ceil(log2(res.height));

    // If the width or height are not powers of two, the generated Gray codes will
    // be too large to create images from without scaling. To avoid that, we center
    // the generated code by shifting it (e.g. if the width is 1280px, we shift it
    // by 384 pixels to center the Gray code, which would be 2048 pixels wide).
    params.columnShift = ((1 << params.numColumns) - res.width) / 2; // 1 << x = 2^x
    params.rowShift = ((1 << params.numRows) - res.height) / 2;

    params.resolution = res;
    return params;
};

}

namespace {

bool getMonitorRegistryKey(std::string const& deviceId, HKEY& key)
{
    HKEY tempKey = reinterpret_cast<HKEY>(INVALID_HANDLE_VALUE);

    // Create an empty device information set
    HDEVINFO devInfo = SetupDiCreateDeviceInfoList(NULL, NULL);
    if (devInfo != INVALID_HANDLE_VALUE)
    {
        // Add the specified device to devInfo
        SP_DEVICE_INTERFACE_DATA dia = {};
        dia.cbSize = sizeof(dia);
        if (SetupDiOpenDeviceInterface(devInfo, deviceId.c_str(), 0, &dia))
        {
            // Load the device's information from devInfo
            SP_DEVINFO_DATA devData = {};
            devData.cbSize = sizeof(devData);
            if (SetupDiEnumDeviceInfo(devInfo, 0, &devData))
            {
                // Open the device's registry key
                tempKey = SetupDiOpenDevRegKey(devInfo, &devData, DICS_FLAG_GLOBAL, 0, DIREG_DEV, KEY_READ);
            }

            SetupDiDeleteDeviceInterfaceData(devInfo, &dia);
        }

        SetupDiDestroyDeviceInfoList(devInfo);
    }

    if (tempKey == INVALID_HANDLE_VALUE)
    {
        return false;
    }

    key = tempKey;
    return true;
}

bool matchDescriptor(BYTE* descriptorStart, BYTE match[5], std::string &output)
{
    if (!std::equal(descriptorStart, descriptorStart + 5, match))
    {
        // Keep looking
        return false;
    }

    // Found the descriptor
    BYTE* name = descriptorStart + 5;
    unsigned int length = 0;
    for (; length < 13; length++)
    {
        // Strings are terminated with 0x0A if their length is under 13 bytes
        if (name[length] == 0x0A)
        {
            break;
        }
    }

    output = std::string(name, name + length);
    return true;
}

// Parses the VESA EDID (extended display identification data) to find the monitor's model name
// See: VESA Enhanced EDID Standard (http://read.pudn.com/downloads110/ebook/456020/E-EDID%20Standard.pdf)
// Specifically, page 9 lists offsets into the binary blob (monitor descriptors start at 0x48, end
// at 0x7D), page 19 describes the format of monitor descriptors
bool getDisplayInformation(std::string const& deviceId, std::string& friendlyName, std::string& serial)
{
    HKEY key;
    if (!getMonitorRegistryKey(deviceId, key))
    {
        return false;
    }

    BYTE data[128];
    DWORD valueType, dataSize = sizeof(data);
    RegQueryValueEx(key, "EDID", NULL, &valueType, data, &dataSize);
    if (valueType != REG_BINARY)
    {
        // Wrong type
        return false;
    }

    BYTE modelNameId[5] = { 0x00, 0x00, 0x00, 0xFC, 0x00 };
    BYTE serialId[5] = { 0x00, 0x00, 0x00, 0xFF, 0x00 };

    bool foundName = false, foundSerial = false;
    for (unsigned int i = 0x48; i < 0x7E; i += 18)
    {
        BYTE* descriptor = data + i;

        if (!foundName && matchDescriptor(descriptor, modelNameId, friendlyName))
        {
            foundName = true;
        }

        if (!foundSerial && matchDescriptor(descriptor, serialId, serial))
        {
            foundSerial = true;
        }
    }

    RegCloseKey(key);
    return foundName && foundSerial;
}

}
