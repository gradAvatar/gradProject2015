#include "Camera.hpp"
#include "CameraStream.hpp"
#include "Exceptions.hpp"
#include "DirectShowCamera.hpp"
#include "PointGreyCamera.hpp"

//
// Library implementations
//
namespace sl {

std::shared_ptr<Camera> Camera::createSharedPtr(Camera* cam)
{
    return std::shared_ptr<Camera>(cam, [](Camera* const camPtr)
    {
        if (auto stream = camPtr->_currentStream.lock())
        {
            // Invalidate any open CameraStreams
            if (stream->isStarted())
            {
                PRINT_WARN("Camera destroyed before its CameraStream was!");
                stream->stop();
            }

            stream.reset();
        }

        delete camPtr;
    });
}

std::vector<std::shared_ptr<Camera>> Camera::enumerateCameras()
{
    std::vector<std::shared_ptr<Camera>> cams;
    DirectShowCamera::enumerateCameras(cams);
    PointGreyCamera::enumerateCameras(cams);

    return cams;
}

std::shared_ptr<CameraStream> Camera::openStream()
{
    std::shared_ptr<CameraStream> sharedPtr;
    if (_currentStream.expired())
    {
        // Opening a new stream
        sharedPtr = std::shared_ptr<CameraStream>(new CameraStream(this));
        _currentStream = std::weak_ptr<CameraStream>(sharedPtr);
    }
    else
    {
        sharedPtr = _currentStream.lock();
    }

    return sharedPtr;
}

}
