#include "GrayCodeGenerator.hpp"
#include "GrayCodeDetector.hpp"
#include "Exceptions.hpp"
#include "Event.hpp"
#include <queue>
#include <tbb/compat/thread>
#include <tbb/compat/condition_variable>
#include <tbb/recursive_mutex.h>

namespace {

inline unsigned int getGrayCodePixel(unsigned int pixelPos, unsigned int patternPos)
{
    // Derived from the binaryToGray function as defined in:
    // https://en.wikipedia.org/wiki/Gray_code#Converting_to_and_from_Gray_code
    // (Converts a binary number to a Gray code; basic formula is: (number >> 1) ^ number)
    unsigned int grayCode = (pixelPos >> 1) ^ pixelPos;
    bool isWhite = (grayCode >> (patternPos - 1)) & 1;
    return isWhite ? 255 : 0;
}

}

namespace sl {

struct GrayCodeGenerator::impl
{
    impl(const GrayCodeParameters params_, const GrayCodeColor gcColor_)
        : params(params_), gcColor(gcColor_), finished(false), pool(params.resolution, matType),
          matType(gcColor_ == WHITE ? CV_8UC1 : CV_8UC3), cancelRequested(false)
    {
    }

    void generationThread();

    const GrayCodeParameters params;
    const GrayCodeColor gcColor;

    int matType;
    bool finished, cancelRequested;
    std::thread thread;
    std::condition_variable queueModified;
    tbb::mutex queueLock;
    std::queue<PooledMat> patternQueue;
    MatPool pool;
};

GrayCodeGenerator::GrayCodeGenerator(const GrayCodeParameters params, const GrayCodeColor gcColor)
    : _m(new impl(params, gcColor))
{
    _m->thread = std::thread(std::bind(&GrayCodeGenerator::impl::generationThread, _m.get()));    
}

GrayCodeGenerator::~GrayCodeGenerator()
{
    cancel();
}

PooledMat GrayCodeGenerator::nextPattern()
{
    if (_m->finished && _m->patternQueue.empty())
    {
        THROW_EX(noMorePatternsEx);
    }

    std::unique_lock<tbb::mutex> lock(_m->queueLock);
    while (_m->patternQueue.size() == 0)
    {
        _m->queueModified.wait(lock);
    }

    PooledMat mat = _m->patternQueue.front();
    _m->patternQueue.pop();

    _m->queueModified.notify_all();

    return mat;
}

void GrayCodeGenerator::cancel()
{
    if (_m->thread.joinable())
    {
        {
            std::lock_guard<tbb::mutex> lock(_m->queueLock);
            _m->cancelRequested = true;
        }

        _m->queueModified.notify_all();
        _m->thread.join();
    }
}

void GrayCodeGenerator::impl::generationThread()
{
    cv::Size resolution = params.resolution;

    bool invert = false;
    Orientation orientation = ORIENT_COLUMNS;
    unsigned int pos = 0;

    cv::Mat_<uchar> colsMaskSingle(1, resolution.width, (uchar)0),
                    rowsMaskSingle(resolution.height, 1, (uchar)0),
                    fullMask(resolution);

    for (unsigned int i = 0; i < 2 * (params.numRows + params.numColumns); i++)
    {
        if (invert)
        {
            cv::bitwise_not(fullMask, fullMask);

            if (++pos == params.numColumns && orientation == ORIENT_COLUMNS)
            {
                // Switch orientations
                orientation = ORIENT_ROWS;
                pos = 0;
            }

            invert = false;
        }
        else 
        {
            if (orientation == ORIENT_COLUMNS)
            {
                colsMaskSingle.setTo(0);
                for (int c = 0; c < resolution.width; c++)
                {
                    unsigned int color = getGrayCodePixel(c + params.columnShift,
                                                          params.numColumns - pos);

                    colsMaskSingle(0, c) = color == 0 ? 0 : 255;
                }

                cv::repeat(colsMaskSingle, resolution.height, 1, fullMask);
            }
            else
            {
                for (int r = 0; r < resolution.height; r++)
                {
                    unsigned int color = getGrayCodePixel(r + params.rowShift,
                                                          params.numRows - pos);

                    rowsMaskSingle(r, 0) = color == 0 ? 0 : 255;
                }

                cv::repeat(rowsMaskSingle, 1, resolution.width, fullMask);
            }

            invert = true;
        }

        PooledMat output = pool.acquireMat();
        output->setTo(toScalar(gcColor), fullMask);

        std::unique_lock<tbb::mutex> lock(queueLock);

        patternQueue.push(output);
        queueModified.notify_all();

        // Only keep 3 patterns cached
        while (patternQueue.size() >= 3 && !cancelRequested)
        {
            queueModified.wait(lock);
        }
    }

    finished = true;
}

bool GrayCodeGenerator::isFinished() const
{
    return _m->finished;
}

}
