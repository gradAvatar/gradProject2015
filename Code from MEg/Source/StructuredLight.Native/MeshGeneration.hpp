#pragma once

#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/mls_omp.h>
#include <pcl/surface/reconstruction.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/poisson.h>
#include <pcl/surface/concave_hull.h>
#include <pcl/surface/gp3.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>
#include <pcl/io/ply_io.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/elch.h>

namespace sl {
    
/// \brief A single 3D Point (double format)
struct Point3d
{
public:
    float X;
    float Y;
    float Z;
    Point3d(float x, float y, float z) : X(x), Y(y), Z(z)
    {
    };
};

/// \brief A triangle polygon, holds 3 3D point values
struct Polygon
{
public:
    Point3d A;
    Point3d B;
    Point3d C;
    Polygon(Point3d x, Point3d y, Point3d z) : A(x), B(y), C(z)
    {
    };
};

class MeshGeneration
{
public:
	/// \brief: Assist method for the scanner class to save a cloud as a PLY
    static bool savePointCloudPLY(std::string filename, std::vector<cv::Point3d>& pts);
	/// \brief: Assist method for the scanner class to combine all aligned clouds, adds normals, and save them to a file as a PLY
	static void combineAndSavePLY(std::string filename, std::vector<std::vector<cv::Point3d>>& points);
	/// \brief: Assist method for the scanner class to combine all aligned clouds, adds normals, meshes it, and save them to a file as a PLY
	static void combineAndSaveMeshPLY(std::string alignedNormalizedFilename, std::string meshFileName, std::vector<pcl::PointCloud<pcl::PointNormal>::Ptr>& points);
	
	/// \brief: Loads an OBJ file and converts it into a poisson mesh
	static void convertPointCloudToMesh(std::string filename, std::string outputFilename);

    /// \brief Converts an OpenCV point cloud into a PCL point cloud
    static pcl::PointCloud<pcl::PointXYZ> convertCloudToPCL(std::vector<cv::Point3d> pts);
    /// \brief Converts a PCL point cloud into an OpenCV point cloud
    static std::vector<cv::Point3d> convertCloudToCV(pcl::PointCloud<pcl::PointXYZ>& pts);
	static std::vector<cv::Point3f> convertCloudToCVFloat(pcl::PointCloud<pcl::PointXYZ>& pts);
    /// \brief Converts a PCL point cloud into an OpenCV point cloud
	static std::vector<cv::Vec6f> convertNormalCloudToCV(pcl::PointCloud<pcl::PointNormal>::Ptr pts);
    
    /// \brief Converts a managed-ready mesh into a PCL mesh
    static std::vector<Polygon> convertMeshToManagedMesh(pcl::PolygonMesh mesh);
    /// \brief Converts a PCL mesh into a managed-ready mesh
    static std::vector<Point3d> convertPointCloudToManagedPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);

    /// \brief Converts a managed point cloud into a PCL point cloud
    static pcl::PointCloud<pcl::PointXYZ>::Ptr convertManagedPointCloudToPointCloud(std::vector<Point3d>& pointCloud);
    
    /// \brief Saves a point cloud to an Wavefront OBJ (.obj) file
    static bool savePointCloudOBJ(std::string filename, pcl::PointCloud<pcl::PointXYZ>& points);
    /// \brief Saves a point cloud with normals to an Wavefront OBJ (.obj) file
    static bool savePointCloudOBJ(std::string filename, pcl::PointCloud<pcl::PointNormal>& points);
    /// \brief Saves a mesh to an Wavefront OBJ (.obj) file
    static bool savePointCloudOBJ(std::string filename, pcl::PolygonMesh& mesh);

    /// \brief Loads a point cloud from an Wavefront OBJ (.obj) file
    static pcl::PointCloud<pcl::PointXYZ>::Ptr loadPointCloudObj(std::string filename);
    /// \brief Loads a point cloud from an Wavefront OBJ (.obj) file
    static pcl::PointCloud<pcl::PointNormal> loadPointCloudNormalsObj(std::string filename);

    /// \brief Generates a mesh using the poisson surface reconstruction algorithm after 
    ///          smoothening and calculating normals
    static pcl::PolygonMesh generateMesh(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
    
    /// \brief Generates a concave hull with the given alpha value for the points.
    /// \see pcl::ConcaveHull
    static std::vector<Polygon> generateConcaveHull(double alpha, pcl::PointCloud<pcl::PointXYZ>::Ptr pts);
	
    /// \brief Attempts to align two point clouds together
    static pcl::PointCloud<pcl::PointNormal>::Ptr alignPointClouds(pcl::PointCloud<pcl::PointNormal>::Ptr a, 
        pcl::PointCloud<pcl::PointNormal>::Ptr b);

    /// \brief Generates normals from the given point cloud with pcl::NormalEstimation after smoothening the 
    ///          point cloud using MLS
    static pcl::PointCloud<pcl::PointNormal>::Ptr calculateSmoothNormals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);

    /// \brief Generates a water-tight mesh using the poisson surface reconstruction algorithm
    static pcl::PolygonMesh poissonReconstruction(pcl::PointCloud<pcl::PointNormal>::Ptr cloud);

    /// \brief Flips/inverts the normals of the given cloud
    static void flipNormals(pcl::PointCloud<pcl::PointNormal>::Ptr cloud_normals);
};
 
}