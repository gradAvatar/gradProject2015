#include "Calibration.hpp"
#include "Camera.hpp"
#include "CameraStream.hpp"
#include "Config.hpp"
#include "Timer.hpp"
#include "Serialization.hpp"

namespace {

void openCameraStream(sl::Camera* cam, std::shared_ptr<sl::CameraStream>* stream,
                      std::unique_ptr<sl::StreamLock>* lock)
{
    *stream = cam->openStream();

    *lock = std::unique_ptr<sl::StreamLock>(new sl::StreamLock(stream->get(), sl::MANUAL));
    if (!lock->get()->isLockActive())
    {
        throw sl::streamModeLockFailedEx;
    }

    stream->get()->start();
}

}

namespace sl {

bool CamCamExtrinsicCalibrator::calibrationImpl(CamCamExtrinsicCalibration& results)
{
    std::shared_ptr<sl::CameraStream> stream1, stream2;
    std::unique_ptr<sl::StreamLock> lock1, lock2;

    openCameraStream(getCamera(), &stream1, &lock1);
    openCameraStream(getTarget(), &stream2, &lock2);

    cv::Size patternSize = cv::Size(sl::Config::getCircleRows(), sl::Config::getCircleColumns());

    // Points that lie on the image itself
    std::vector<std::vector<cv::Point2f>> imagePts1, imagePts2;

    // Points that lie in the calibration board's coordinate system
    std::vector<std::vector<cv::Point3f>> objectPtsVec;
    std::vector<cv::Point3f> objectPts = generateCircleGridObjectPoints3D(
        patternSize, sl::Config::getCircleDiameter(), sl::Config::getCircleSpacing());

    sl::Timer timer;

    int matType1 = sl::videoFormatToOpenCvType(getCamera()->getVideoMode().videoFormat);
    int matType2 = sl::videoFormatToOpenCvType(getTarget()->getVideoMode().videoFormat);
    cv::Mat img1(getCamera()->getResolution(), matType1);
    cv::Mat img2(getTarget()->getResolution(), matType1);

    unsigned int i = 0;
    double lastNorm;
    while (i < getTargetAcceptedCaptures() && !isCancelRequested())
    {
        stream1->trigger();
        stream2->trigger();

        stream1->waitForFrame();
        bool s1Success = stream1->retrieveLastCapture(img1);

        stream2->waitForFrame();
        if (!stream2->retrieveLastCapture(img2) || !s1Success)
        {
            continue;
        }

        // Search for a circle grid and draw any detected circles
        std::vector<cv::Point2f> corners1, corners2;
        bool found1 = cv::findCirclesGrid(img1, patternSize, corners1, cv::CALIB_CB_ASYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING);
        bool found2 = cv::findCirclesGrid(img2, patternSize, corners2, cv::CALIB_CB_ASYMMETRIC_GRID | cv::CALIB_CB_CLUSTERING);
        cv::drawChessboardCorners(img1, patternSize, corners1, found1);
        cv::drawChessboardCorners(img2, patternSize, corners2, found1);
 
        bool acceptImage = found1 && found2 && timer.elapsed() > boost::chrono::seconds(6);

        if (acceptImage)
        {
            i++;

            // Invert image to tell the user an image was accepted
            cv::bitwise_not(img1, img1);
            cv::bitwise_not(img2, img2);
 
            // Save the detected circles
            imagePts1.push_back(corners1);
            imagePts2.push_back(corners2);
            objectPtsVec.push_back(objectPts);
            timer.reset();
        }

        CamCamExtrinsicCalibrationPreviewData data;
        data.sourceCamPreview = img1;
        data.targetCamPreview = img2;
        data.currentPreviewWasAccepted = acceptImage;
        data.numAcceptedCaptures = i;

        invokePreviewCallbacks(data);
    }

    if (isCancelRequested())
    {
        return false;
    }

    sl::IntrinsicCalibration calib1, calib2;
    sl::loadIntrinsicCalibration(getCamera(), &calib1);
    sl::loadIntrinsicCalibration(getTarget(), &calib2);

    cv::Mat_<double> R;
    cv::Mat_<double> T;
    cv::stereoCalibrate(objectPtsVec, imagePts1, imagePts2, calib1.camera, calib1.distortion, calib2.camera,
        calib2.distortion, getCamera()->getResolution(), R, T, cv::noArray(), cv::noArray(),
        cv::TermCriteria(cv::TermCriteria::COUNT+cv::TermCriteria::EPS, 30, 1e-6),
        CV_CALIB_FIX_INTRINSIC);

    results.rotation = (cv::Mat)R;
    results.translation = (cv::Mat)T;

    saveCamCamExtrinsicCalibration(getCamera(), getTarget(), results);

    return true;
}

CamCamExtrinsicCalibrator::CamCamExtrinsicCalibrator(Camera* const source, Camera* const target)
    : Calibrator(source), _target(target)
{
}

CamCamExtrinsicCalibrator::~CamCamExtrinsicCalibrator() { }

}
