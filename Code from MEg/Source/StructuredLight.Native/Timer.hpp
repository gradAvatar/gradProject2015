#pragma once

#include <boost/chrono.hpp>

namespace sl {

class Timer
{
public:
    Timer()
        : _started(true), _start(_clock.now())
    {
    }

    void start()
    {
        if (!_started)
        {
            _started = true;
            _start = _clock.now();
        }
    }

    void stop()
    {
        if (_started)
        {
            _started = false;
            _end = _clock.now();
        }
    }

    void reset()
    {
        if (_started)
        {
            _start = _clock.now();
        }
        else
        {
            _start = _clock.now();
            _end = _start;
        }
    }

    boost::chrono::system_clock::duration elapsed()
    {
        if (_started)
        {
            return _clock.now() - _start;
        }
        else
        {
            return _end - _start;
        }
    }

private:
    bool _started;
    boost::chrono::system_clock _clock;
    boost::chrono::system_clock::time_point _start, _end;
};

}
