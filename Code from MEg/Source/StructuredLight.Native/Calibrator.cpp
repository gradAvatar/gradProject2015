#include "Calibration.hpp"
#include "Exceptions.hpp"
#include <tbb/compat/thread>

namespace sl {

template <typename TResult, typename TPreviewData>
class Calibrator<TResult, TPreviewData>::impl
{
public:
    Camera* const _cam;
    unsigned int _targetCaptures;
    bool _calibrating, _success, _cancelRequested;
    TResult _results;
    CallbackManager<TPreviewData> _prevCbs;
    CallbackManager<void> _finishCbs;

    std::thread _processingThread;

    impl(Camera* const cam)
        : _cam(cam), _targetCaptures(10), _calibrating(false), _success(false),
          _cancelRequested(false)
    {
    }

    ~impl() { }

    void start(Calibrator* const calib);
    void cancel();

    CallbackToken addPreviewCallback(CalibrationPreviewCallback cb) { return _prevCbs.add(cb); }
    void removePreviewCallback(CallbackToken token) { _prevCbs.remove(token); }
    CallbackToken addFinishedCallback(CalibrationFinished cb) { return _finishCbs.add(cb); }
    void removeFinishedCallback(CallbackToken token) { _finishCbs.remove(token); }

    Camera* const getCamera() const { return _cam; }
    unsigned int getTargetAcceptedCaptures() const { return _targetCaptures; }
    bool isCalibrating() const { return _calibrating; }
    bool getResults(TResult &calib) const;

    void setTargetAcceptedCaptures(unsigned int val);

    void invokePreviewCallbacks(TPreviewData data) { _prevCbs.invoke(data); }
    void invokeFinishedCallbacks() { _finishCbs.invoke(); }
    bool isCancelRequested() { return _cancelRequested; }
};

#define CALIBRATOR_CTOR template <typename TResult, typename TPreviewData> Calibrator<TResult, TPreviewData>
#define CALIBRATOR(_retType_) template <typename TResult, typename TPreviewData> _retType_ Calibrator<TResult, TPreviewData>

CALIBRATOR(void)::impl::start(Calibrator* const calib)
{
    _calibrating = true;

    _processingThread = std::thread([=]()
    {
        TResult results;
        bool success = calib->calibrationImpl(results);
        _calibrating = false;

        _results = results;
        _success = success;

        if (success)
        {
            invokeFinishedCallbacks();
        }
    });
}

CALIBRATOR(void)::impl::cancel()
{
    _cancelRequested = true;
    _processingThread.join();
    _cancelRequested = false;
}

CALIBRATOR(bool)::impl::getResults(TResult &calib) const
{
    if (_success)
    {
        calib = _results;
        return true;
    }

    return false;
}

CALIBRATOR(void)::impl::setTargetAcceptedCaptures(unsigned int val)
{
    if (_calibrating)
    {
        THROW_EX(calibStartedEx);
    }

    _targetCaptures = val;
}

CALIBRATOR_CTOR::Calibrator(Camera* const cam) : _m(new impl(cam)) { }
CALIBRATOR_CTOR::~Calibrator() { }
CALIBRATOR(void)::start() { _m->start(this); }
CALIBRATOR(void)::cancel() { _m->cancel(); }
CALIBRATOR(CallbackToken)::addPreviewCallback(CalibrationPreviewCallback cb) { return _m->addPreviewCallback(cb); }
CALIBRATOR(void)::removePreviewCallback(CallbackToken t) { _m->removePreviewCallback(t); }
CALIBRATOR(CallbackToken)::addFinishedCallback(CalibrationFinished cb) { return _m->addFinishedCallback(cb); }
CALIBRATOR(void)::removeFinishedCallback(CallbackToken t) { _m->removeFinishedCallback(t); }
CALIBRATOR(Camera* const)::getCamera() const { return _m->getCamera(); }
CALIBRATOR(unsigned int)::getTargetAcceptedCaptures() const { return _m->getTargetAcceptedCaptures(); }
CALIBRATOR(bool)::isCalibrating() const { return _m->isCalibrating(); }
CALIBRATOR(bool)::getResults(TResult &calib) const { return _m->getResults(calib); }
CALIBRATOR(void)::setTargetAcceptedCaptures(unsigned int val) { _m->setTargetAcceptedCaptures(val); }
CALIBRATOR(void)::invokePreviewCallbacks(TPreviewData data) { _m->invokePreviewCallbacks(data); }
CALIBRATOR(void)::invokeFinishedCallbacks() { _m->invokeFinishedCallbacks(); }
CALIBRATOR(bool)::isCancelRequested() { return _m->isCancelRequested(); }

//
// Template instantiations
//
template class Calibrator<IntrinsicCalibration>;
template class Calibrator<CamProjExtrinsicCalibration>;
template class Calibrator<CamCamExtrinsicCalibration, CamCamExtrinsicCalibrationPreviewData>;

//
// Utilities
//

// Generates an asymmetrical circle grid (sideways, as most resolutions have a larger width than height)
cv::Mat generateAsymmGrid(cv::Size projRes, cv::Size patternSize, unsigned int spacing, unsigned int radius, std::vector<cv::Point2f>& circlePoints)
{
    cv::Mat grid(projRes, CV_8UC1, cv::Scalar(255));

    unsigned int diameter = radius * 2;
    unsigned int diamAndSpace = diameter + spacing;

    int centerX = (projRes.width - (patternSize.height - 1) * (diamAndSpace / 2)) / 2;
    int centerY = (projRes.height - (diameter + (patternSize.width - 1) * diamAndSpace + diamAndSpace / 2)) / 2;
    for (int i = 0; i < patternSize.height; i++)
    {
        for (int j = 0; j < patternSize.width; j++)
        {
            unsigned int x = i * diamAndSpace / 2;
            unsigned int y = radius + j * diamAndSpace + ((i % 2) * diamAndSpace / 2);

            cv::Point pt = cv::Point(centerX + x, projRes.height - (centerY + y));
            cv::circle(grid, pt, radius, cv::Scalar(0), -1);

            circlePoints.push_back(pt);
        }
    }

    return grid;
}

std::vector<cv::Point2f> generateCircleGridObjectPoints(cv::Size patternSize, float circDiameter, float circSpacing)
{
    float boardWidth = 4 * circDiameter + 3 * circSpacing + circSpacing / 2 + circDiameter / 2;

    std::vector<cv::Point2f> objectPoints;
    for (int r = 0; r < patternSize.height; r++)
    {
        for (int c = 0; c < patternSize.width; c++)
        {
            float x = boardWidth - float((r % 2 == 0 ? (circDiameter / 2) : (circDiameter + circSpacing / 2)) + ((circDiameter + circSpacing) * c));
            float y = float(circDiameter / 2 + (circDiameter + circSpacing) / 2 * r);
            objectPoints.push_back(cv::Point2f(x, y));
        }
    }

    return objectPoints;
}

std::vector<cv::Point3f> generateCircleGridObjectPoints3D(cv::Size patternSize, float circDiameter, float circSpacing)
{
    std::vector<cv::Point2f> objectPoints2D = generateCircleGridObjectPoints(patternSize, circDiameter, circSpacing);
    std::vector<cv::Point3f> objectPoints3D(objectPoints2D.size());

    for (unsigned int i = 0; i < objectPoints2D.size(); i++)
    {
        objectPoints3D[i] = cv::Point3f(objectPoints2D[i].x, objectPoints2D[i].y, 0);
    }

    return objectPoints3D;
}

}
