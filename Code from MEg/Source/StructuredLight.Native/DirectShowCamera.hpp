#pragma once

#include "Camera.hpp"
#include "Event.hpp"
#include "Utils.hpp"
#include <tbb/compat/condition_variable> // This rash of dependency-related includes are fine since
#include <tbb/mutex.h>                   // clients shouldn't be including DirectShowCamera.hpp
#include <tbb/recursive_mutex.h>         // anyways
#include <DShow.h>
#include <Windows.h>
#include "qedit.h"

namespace sl {

class DirectShowCamera : public Camera
{
public:
    std::vector<VideoMode> enumerateVideoModes();
    VideoMode createCustomVideoMode(unsigned int width, unsigned int height, unsigned int fps,
                                    VideoFormat vidFormat) const;

    ~DirectShowCamera();

    // Accessors
    std::string getModelName() const;
    std::string getSerialNumber() const;
    VideoMode getVideoMode() const;
    CustomVideoModeConstraints getCustomVideoModeConstraints() const;
    bool getCameraSetting(CameraSetting setting, double* value, CameraSettingFlags* flags);
    bool getCameraSettingRange(CameraSetting setting, double* lower, double* delta, double* upper, double* def);
    bool setCameraSetting(CameraSetting setting, double value, CameraSettingFlags flags);

    // Mutators
    bool setVideoMode(VideoMode mode);

protected:
    bool connect();
    bool disconnect();
    bool capture();
    bool retrieve(cv::Mat& image);

private:
    friend class Camera;

    // http://stackoverflow.com/questions/3019416
    class SampleGrabberCallback : public ISampleGrabberCB
    {
    private:
        DirectShowCamera* _cam;

    public:
        SampleGrabberCallback(DirectShowCamera* cam) : _cam(cam) { }

        STDMETHODIMP_(ULONG) AddRef() { return 1; }
        STDMETHODIMP_(ULONG) Release() { return 0; }
        STDMETHODIMP QueryInterface(REFIID riid, void** ppvObject);
        STDMETHODIMP SampleCB(double Time, IMediaSample* pSample);
        STDMETHODIMP BufferCB(double Time, BYTE* pBuffer, long BufferLen) { return E_NOTIMPL; }
    };

    IGraphBuilder* _graph;
    IBaseFilter* _captureFilter;
    ISampleGrabber* _grabber;
    IBaseFilter* _grabberFilter;
    IBaseFilter* _nullRenderFilter;
    IMediaControl* _mediaController;
    IAMVideoProcAmp* _vidProcAmp;
    IAMCameraControl* _camCtrl;
    std::shared_ptr<IPin> _capturePin;

    std::string _devicePath;
    std::string _modelName;

    SampleGrabberCallback* _callback;
    VideoMode _vidMode;
    tbb::recursive_mutex _ctrlLock;
    tbb::mutex _bufferLock;
    std::condition_variable _bufferReady;
    bool _captureFrame, _bufferFilled;
    cv::Mat _buffer;

    static void enumerateCameras(std::vector<std::shared_ptr<Camera>> &cams);

    DirectShowCamera(std::shared_ptr<IMoniker> device);

    void initializeGraph();
    void initializeFilters(IMoniker* device);

    DISALLOW_COPY_AND_ASSIGN(DirectShowCamera);
};

}