#include "CallbackManager.hpp"
#include "Event.hpp"
#include "CameraStream.hpp"
#include "Calibration.hpp"
#include "Scanner.hpp"
#include "Utils.hpp"
#include <vector>
#include <tbb/tbb.h>
#include <tbb/compat/condition_variable>

namespace sl {

template <typename TData>
struct CallbackManager<TData>::impl
{
    struct CallbackState
    {
        CallbackState(CallbackToken t, Callback cb)
            : token(t), userCallback(cb), finishedEv(true)
        {
        }

        const CallbackToken token;
        const Callback userCallback;
        ManualResetEvent finishedEv;
    };

    void genericInvoke(std::function<void (Callback preFunc)> preFuncInvoker,
                       std::function<void (CallbackState*, Callback postFunc)> invoked);

    tbb::task_group taskGroup;
    std::vector<std::unique_ptr<CallbackState>> callbacks;
    Callback preFunc, postFunc;
    unsigned int nextId;
};

//
// Implementation
//
template<typename TData>
CallbackManager<TData>::CallbackManager()
    : _m(new impl)
{
}

template<typename TData>
CallbackManager<TData>::~CallbackManager()
{
    removeAll();
}

template <typename TData>
CallbackToken CallbackManager<TData>::add(Callback cb)
{
    CallbackToken token = _m->nextId++;
    _m->callbacks.push_back(std::unique_ptr<impl::CallbackState>(new impl::CallbackState(token, cb)));

    return token;
}

template <typename TData>
void CallbackManager<TData>::remove(CallbackToken t)
{
    for (auto st = begin(_m->callbacks); st != end(_m->callbacks); st++)
    {
        if ((*st)->token == t)
        {
            // Wait for task, then remove it
            (*st)->finishedEv.wait();
            _m->callbacks.erase(st);
            break;
        }
    }
}

template <typename TData>
void CallbackManager<TData>::removeAll()
{
    waitForAll();
    _m->callbacks.clear();
}

template <typename TData>
void CallbackManager<TData>::waitForAll()
{
    _m->taskGroup.wait();
}

template <typename TData>
void CallbackManager<TData>::impl::genericInvoke(
    std::function<void (Callback preFunc)> preFuncInvoker,
    std::function<void (CallbackState*, Callback postFunc)> invoked)
{
    Callback localPreFunc = preFunc, localPostFunc = postFunc;

    std::for_each(begin(callbacks), end(callbacks),
        [=](const std::unique_ptr<CallbackState>& state)
        {
            if (!state->finishedEv.isSet())
            {
                // Callback is still running, don't invoke it again
                return;
            }

            state->finishedEv.reset();

            preFuncInvoker(localPreFunc);

            CallbackState* statePtr = state.get();
            taskGroup.run([=]
            {
                invoked(statePtr, localPostFunc);
                statePtr->finishedEv.set();
            });
        });
}

// Empty argument specialization
template <>
void CallbackManager<void>::invoke()
{
    _m->genericInvoke(
        [=](Callback preFunc)
        {
            if (preFunc)
            {
                preFunc();
            }
        },
        [=](impl::CallbackState* state, Callback postFunc)
        {
            state->userCallback();

            if (postFunc)
            {
                postFunc();
            }
        });
}

template <typename TData>
void CallbackManager<TData>::invoke(TData data)
{
    _m->genericInvoke(
        [=](Callback preFunc)
        {
            if (preFunc)
            {
                preFunc(data);
            }
        },
        [=](impl::CallbackState* state, Callback postFunc)
        {
            state->userCallback(data);

            if (postFunc)
            {
                postFunc(data);
            }
        });
}

template <typename TData>
void CallbackManager<TData>::setPreInvokeFunc(Callback f)
{
    _m->preFunc = f;
}

template <typename TData>
void CallbackManager<TData>::setPostInvokeFunc(Callback f)
{
    _m->postFunc = f;
}

//
// Template instantiations
//
template class CallbackManager<ImageCapturedCallbackArgs>;
template class CallbackManager<CalibrationPreviewData>;
template class CallbackManager<CamCamExtrinsicCalibrationPreviewData>;
template class CallbackManager<ScanPreviewData>;
template class CallbackManager<void>;

}