#pragma once

#include "Utils.hpp"
#include <functional>
#include <memory>

namespace sl {

typedef unsigned int CallbackToken;

template<typename TData>
class CallbackManager
{
public:
    typedef std::function<void (TData)> Callback;

    CallbackManager();
    ~CallbackManager();

    CallbackToken add(Callback cb);
    void remove(CallbackToken t);
    void removeAll();
    void waitForAll();
    void invoke(TData);
    void setPreInvokeFunc(Callback f);
    void setPostInvokeFunc(Callback f);

private:
    struct impl;
    const std::unique_ptr<impl> _m;

    DISALLOW_COPY_AND_ASSIGN(CallbackManager);
};

}