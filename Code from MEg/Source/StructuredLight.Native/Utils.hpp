#pragma once

// Intended to be used to prevent the copy constructor and assignment operator
// from begin used. Place in the private section of the class as the very last item.
// Sourced from the Google C++ Style Guide:
// http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&); \
    void operator=(const TypeName&)
