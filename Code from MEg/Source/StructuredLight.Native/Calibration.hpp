#pragma once

#include "CallbackManager.hpp"
#include <opencv2/opencv.hpp>

// Convenience typedef
namespace cv { typedef Matx<double, 1, 5> Matx15d; }

namespace sl {

// Forward declarations
class Camera;
class Projector;

cv::Mat generateAsymmGrid(cv::Size projRes, cv::Size patternSize, unsigned int spacing, unsigned int radius, std::vector<cv::Point2f>& circlePoints);
std::vector<cv::Point2f> generateCircleGridObjectPoints(cv::Size patternSize, float circDiameter, float circSpacing);
std::vector<cv::Point3f> generateCircleGridObjectPoints3D(cv::Size patternSize, float circDiameter, float circSpacing);

struct IntrinsicCalibration
{
    /// \brief The 3x3 intrinsic calibration matrix of the Camera.
    /// \see <a href="http://opencv.itseez.com/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#calibratecamera">
    ///      OpenCV: cv::calibrateCamera</a> (see \a cameraMatrix parameter)
    cv::Matx33d camera;

    /// \brief The 1x5 distortion matrix of the Camera.
    /// \see <a href="http://opencv.itseez.com/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#calibratecamera">
    ///      OpenCV: cv::calibrateCamera</a> (see \a distCoeffs parameter)
    cv::Matx15d distortion;

    double reprojectionError;
};

#define ROTATION(_var_) cv::Matx33d rot; cv::Rodrigues(_var_.row(0), rot); return rot
#define TRANSLATION(_var_) return _var_.t().col(1)

struct CamProjExtrinsicCalibration
{
    cv::Matx23d camExtrinsic;
    cv::Matx23d projExtrinsic;

    cv::Matx33d getCamRotation() { ROTATION(camExtrinsic); }
    cv::Matx31d getCamTranslation() { TRANSLATION(camExtrinsic); }
    cv::Matx33d getProjRotation() { ROTATION(projExtrinsic); }
    cv::Matx31d getProjTranslation() { TRANSLATION(projExtrinsic); }
};

#undef ROTATION
#undef TRANSLATION

struct CamCamExtrinsicCalibration
{
    cv::Matx33d rotation;
    cv::Matx31d translation;
};

struct CalibrationPreviewData
{
    cv::Mat currentPreview;
    unsigned int numAcceptedCaptures;
    bool currentPreviewWasAccepted;
};

struct CamCamExtrinsicCalibrationPreviewData
{
    cv::Mat sourceCamPreview;
    cv::Mat targetCamPreview;
    unsigned int numAcceptedCaptures;
    bool currentPreviewWasAccepted;
};

typedef CallbackManager<void>::Callback CalibrationFinished;

template <typename TResult, typename TPreviewData = CalibrationPreviewData>
class Calibrator
{
public:
    typedef typename CallbackManager<TPreviewData>::Callback CalibrationPreviewCallback;

    virtual ~Calibrator();

    void start();
    void cancel();

    CallbackToken addPreviewCallback(CalibrationPreviewCallback cb);
    void removePreviewCallback(CallbackToken token);
    CallbackToken addFinishedCallback(CalibrationFinished cb);
    void removeFinishedCallback(CallbackToken token);

    Camera* const getCamera() const;
    unsigned int getTargetAcceptedCaptures() const;
    bool isCalibrating() const;
    bool getResults(TResult &calib) const;

    void setTargetAcceptedCaptures(unsigned int val);

protected:
    virtual bool calibrationImpl(TResult &results) = 0;

    void invokePreviewCallbacks(TPreviewData data);
    void invokeFinishedCallbacks();

    bool isCancelRequested();

private:
    Calibrator(Camera* const cam);

    class impl;
    const std::unique_ptr<impl> _m;

    friend class CameraCalibrator;
    friend class ProjectorCalibrator;
    friend class CamProjExtrinsicCalibrator;
    friend class CamCamExtrinsicCalibrator;

    DISALLOW_COPY_AND_ASSIGN(Calibrator);
};

class CameraCalibrator : public Calibrator<IntrinsicCalibration>
{
public:
    CameraCalibrator(Camera* const cam);
    ~CameraCalibrator();

protected:
    bool calibrationImpl(IntrinsicCalibration &results);

private:
    DISALLOW_COPY_AND_ASSIGN(CameraCalibrator);
};

class ProjectorCalibrator : public Calibrator<IntrinsicCalibration>
{
public:
    ProjectorCalibrator(Camera* const cam, Projector* const device);
    ~ProjectorCalibrator();

    Projector* getProjector() const { return _device; }

protected:
    bool calibrationImpl(IntrinsicCalibration &results);

private:
    Projector* const _device;

    DISALLOW_COPY_AND_ASSIGN(ProjectorCalibrator);
};

class CamProjExtrinsicCalibrator : public Calibrator<CamProjExtrinsicCalibration>
{
public:
    CamProjExtrinsicCalibrator(Camera* const cam, Projector* const device);
    ~CamProjExtrinsicCalibrator();

    Projector* getProjector() const { return _device; }

protected:
    bool calibrationImpl(CamProjExtrinsicCalibration &results);

private:
    Projector* const _device;

    DISALLOW_COPY_AND_ASSIGN(CamProjExtrinsicCalibrator);
};

class CamCamExtrinsicCalibrator : public Calibrator<CamCamExtrinsicCalibration,
                                                    CamCamExtrinsicCalibrationPreviewData>
{
public:
    CamCamExtrinsicCalibrator(Camera* const source, Camera* const target);
    ~CamCamExtrinsicCalibrator();

    Camera* getTarget() const { return _target; }

protected:
    bool calibrationImpl(CamCamExtrinsicCalibration &results);

private:
    Camera* const _target;

    DISALLOW_COPY_AND_ASSIGN(CamCamExtrinsicCalibrator);
};

}
