#include "PointCloudAlignment.hpp"
#include "MeshGeneration.hpp"
#include "MeshLabAlignment/VCGPointCloudAlignment.hpp"

namespace sl {

pcl::PointCloud<pcl::PointNormal> PointCloudAlignment::alignPointCloud(pcl::PointCloud<pcl::PointNormal>::Ptr baseCloud,
					 pcl::PointCloud<pcl::PointNormal>::Ptr inputCloud)
{
	MeshGeneration::savePointCloudOBJ("asdfasdfiasofgijaoisgjaosi.obj", *baseCloud);
	pcl::PointCloud<pcl::PointNormal> base = MeshGeneration::loadPointCloudNormalsObj("asdfasdfiasofgijaoisgjaosi.obj");
	MeshGeneration::savePointCloudOBJ("asdfasdfiasofgijaoisgjaosj.obj", *inputCloud);
	pcl::PointCloud<pcl::PointNormal> input = MeshGeneration::loadPointCloudNormalsObj("asdfasdfiasofgijaoisgjaosj.obj");
	cv::Mat_<float> transform = VCGPointCloudAlignment::alignPointClouds(MeshGeneration::convertNormalCloudToCV(base.makeShared()), 
		MeshGeneration::convertNormalCloudToCV(input.makeShared()));
	return transformPointCloud(input, transform);
}

/*pcl::PointCloud<pcl::PointNormal> PointCloudAlignment::alignPointCloud(pcl::PointCloud<pcl::PointNormal>::Ptr baseCloud,
					 pcl::PointCloud<pcl::PointNormal>::Ptr inputCloud)
{
	cv::Mat_<float> transform = VCGPointCloudAlignment::alignPointClouds(MeshGeneration::convertNormalCloudToCV(baseCloud), 
		MeshGeneration::convertNormalCloudToCV(inputCloud));
	return transformPointCloud(inputCloud, transform);
}*/

void PointCloudAlignment::Testing()
{
	pcl::PointCloud<pcl::PointNormal>::Ptr first = MeshGeneration::loadPointCloudNormalsObj("0_output.obj").makeShared();
	pcl::PointCloud<pcl::PointNormal>::Ptr second = MeshGeneration::loadPointCloudNormalsObj("1_aligned_output.obj").makeShared();
	pcl::io::savePLYFile("test_alignment.ply", alignPointCloud(first, second));
}
	
pcl::PointCloud<pcl::PointNormal> PointCloudAlignment::transformPointCloud(
	pcl::PointCloud<pcl::PointNormal>& pc, cv::Mat_<float> transform)
{
	Eigen::Matrix4f eigenTr;
	for (int r = 0; r < 4; r++)
	{
		for (int c = 0; c < 4; c++)
		{
			eigenTr(r, c) = transform(r, c);
		}
	}

	pcl::PointCloud<pcl::PointNormal> transformed;
	pcl::transformPointCloud(pc, transformed, eigenTr);

	return transformed;
}
}