#include "Event.hpp"
#include <Windows.h>

namespace {

sl::EVENT_STATUS win32ToEventStatus(DWORD waitStatus)
{
    switch (waitStatus)
    {
    case WAIT_OBJECT_0: return sl::EV_OK;
    case WAIT_TIMEOUT: return sl::EV_TIMED_OUT;
    default: return sl::EV_UNKNOWN;
    }
}

}

namespace sl {

template <bool BManualReset>
struct Event<BManualReset>::impl
{
public:
    HANDLE ev;
};

template <bool BManualReset>
Event<BManualReset>::Event(bool initialState)
    : _m(new impl)
{
    _m->ev = CreateEvent(NULL, BManualReset, initialState, NULL);
}

template <bool BManualReset>
Event<BManualReset>::~Event()
{
    CloseHandle(_m->ev);
}

template <bool BManualReset>
bool Event<BManualReset>::isSet() { return WaitForSingleObject(_m->ev, 0) != WAIT_TIMEOUT; }
template <bool BManualReset>
void Event<BManualReset>::set() { SetEvent(_m->ev); }
template <bool BManualReset>
void Event<BManualReset>::reset() { ResetEvent(_m->ev); }
template <bool BManualReset>
EVENT_STATUS Event<BManualReset>::wait(unsigned int timeout)
{
    return win32ToEventStatus(WaitForSingleObject(_m->ev, timeout));
}

// Template instantiations
template class Event<true>;
template class Event<false>;

}