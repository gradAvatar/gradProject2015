#pragma once

#include "MatPool.hpp"
#include "CallbackManager.hpp"
#include <functional>
#include <opencv2/opencv.hpp>

namespace sl {

// Forward declarations
class Camera;
class CameraStream;
struct ImageCapturedCallbackArgs;
class StreamLock;

/// \brief Values that represent the two types of stream modes that a CameraStream supports.
enum STREAM_MODE
{
    /// Represents a CameraStream mode that causes the stream to continuously capture images from
    /// the underlying Camera.
    AUTO,

    /// Represents a CameraStream mode that causes the stream to only capture images when
    /// CameraStream::trigger() is called.
    MANUAL
};

/// \brief Defines the signature of the callback that is invoked when a new image has been captured
///        by a CameraStream.
///
/// Signature should be: void (ImageCapturedCallbackArgs).
typedef CallbackManager<const ImageCapturedCallbackArgs>::Callback ImageCapturedCallback;

/// \brief A class that allows a client to obtain a stream of images from a Camera. This class is
///        not copyable.
/// \see Camera::openStream
class CameraStream
{
public:
    /// \brief Finalizes an instance of the CameraStream class.
    ~CameraStream();

    /// \brief Attempts to connect to the CameraStream's underlying Camera and begin streaming.
    /// \return \c true if the connection to the camera hardware succeeds, or if the stream has
    ///         already been started; \c false otherwise.
    bool start();

    /// \brief Attempts to disconnect from the underlying Camera and stop streaming.
    /// \return \c true if the connection to the camera hardware was successfully destroyed, or if
    ///         no stream is currently open; \c false otherwise.
    bool stop();

    /// \brief Requests that an image should be captured as soon as possible. Only works when the
    ///        stream mode is manual (i.e. getStreamMode() == STREAM_MODE::MANUAL).
    void trigger();

    /// \brief Waits for the next image from the underlying Camera to arrive.
    /// \param timeout (optional) How long to wait for the image to arrive.
    /// \return \c true if an image arrived within the specified timeout; \c false otherwise.
    bool waitForFrame(unsigned int timeout = 0xFFFFFFFF);

    /// \brief Retrieves the image that was last captured from the underlying Camera.
    /// \param[out] image The cv::Mat that will be filled.
    /// \return \c true if \a image was successfully filled; \c false otherwise.
    bool retrieveLastCapture(cv::Mat &image);

    /// \brief Combines calls to trigger(), waitForFrame(), and retrieveLastCapture() into a single
    ///        method call.
    /// \param[out] image The cv::Mat that will be filled.
    /// \return \c true if \a image was successfully filled; \c false otherwise.
    bool triggerWaitRetrieve(cv::Mat &image);

    /// \brief Subscribes the specified callback to this stream's new image signal. The specified 
    /// callback is invoked whenever a new image arrives from the underlying Camera.
    /// \param cb The callback to subscribe with.
    /// \return A token that should be saved for use with removeCallback.
    CallbackToken addCallback(ImageCapturedCallback cb);

    /// \brief Unsubscribes the callback that is represented by the specified CallbackToken.
    /// \param token The token that represents the callback that is to be unsubscribed.
    void removeCallback(CallbackToken token);

    /// \brief Determines whether or not the stream is currently started.
    /// \return \c true if the stream is started; \c false if not.
    bool isStarted() const;

    /// \brief Determines if the stream mode is currently locked.
    /// \return \c true if the stream mode is locked; \c false if not.
    /// \see StreamLock
    bool isStreamModeLocked() const;

    /// \brief Gets the current stream mode.
    /// \return The current stream mode.
    STREAM_MODE getStreamMode() const;

    /// \brief Attempts to set the stream mode to the specified value.
    /// \param mode The mode to change to.
    /// \return \c true if the stream is unlocked (i.e. isStreamLocked() == false); \c false
    ///         otherwise.
    /// \see StreamLock
    bool setStreamMode(STREAM_MODE mode);

private:
    explicit CameraStream(Camera* cam);

    class impl;
    const std::shared_ptr<impl> _m;

    friend class Camera;
    friend class StreamLock;

    DISALLOW_COPY_AND_ASSIGN(CameraStream);
};

/// \brief A class that locks the stream mode of a CameraStream to prevent others from changing it.
///        This class is not copyable.
/// \see CameraStream
class StreamLock
{
public:
    /// \brief Initializes a new instance of the StreamLock class, locking the stream mode of
    ///        \a stream to \a modeToLockTo (only if the mode is not already locked).
    /// \param[in] stream The stream whose stream mode is to be locked.
    /// \param modeToLockTo    The mode to lock to.
    StreamLock(CameraStream* stream, STREAM_MODE modeToLockTo);

    /// \brief Calls \ref unlock and finalizes an instance of the StreamLock class.
    ~StreamLock();

    /// \brief Unlocks the stream mode of the stream specified during construction.
    void unlock();

    /// \brief Determines if this instance is actively locking the stream mode of the stream
    ///        specified during construction.
    /// \return \c true if the lock is active; \c false if not.
    bool isLockActive() const;

private:
    CameraStream* _stream;
    STREAM_MODE _originalMode;

    friend class CameraStream;

    DISALLOW_COPY_AND_ASSIGN(StreamLock);
};

/// \brief Arguments specified when an ImageCapturedCallback is invoked.
struct ImageCapturedCallbackArgs
{
public:
    /// \brief Gets the image from the CameraStream.
    /// \return The image from the CameraStream.
    const cv::Mat getImage() const { return *_pooled; }

private:
    PooledMat _pooled;

    friend class CameraStream;
};

}