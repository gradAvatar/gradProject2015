#include "MeshGeneration.hpp"

using namespace std;

namespace sl {

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while(std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split(s, delim, elems);
}

pcl::PointCloud<pcl::PointXYZ> MeshGeneration::convertCloudToPCL(std::vector<cv::Point3d> pts)
{
    pcl::PointCloud<pcl::PointXYZ> pc;
	pc.resize(pts.size());
	for (unsigned int i = 0; i < pts.size(); i++)
	{
		pc[i] = pcl::PointXYZ((float)pts[i].x, (float)pts[i].y, (float)pts[i].z);
	}
	return pc;
}

std::vector<cv::Point3d> MeshGeneration::convertCloudToCV(pcl::PointCloud<pcl::PointXYZ>& pts)
{
    std::vector<cv::Point3d> pc;
	for (unsigned int i = 0; i < pts.size(); i++)
	{
        pc.push_back(cv::Point3d(pts[i].x, pts[i].y, pts[i].z));
	}
	return pc;
}

std::vector<cv::Point3f> MeshGeneration::convertCloudToCVFloat(pcl::PointCloud<pcl::PointXYZ>& pts)
{
    std::vector<cv::Point3f> pc;
	for (unsigned int i = 0; i < pts.size(); i++)
	{
        pc.push_back(cv::Point3f(pts[i].x, pts[i].y, pts[i].z));
	}
	return pc;
}

std::vector<cv::Vec6f> MeshGeneration::convertNormalCloudToCV(pcl::PointCloud<pcl::PointNormal>::Ptr pts)
{
    std::vector<cv::Vec6f> pc;
	for (unsigned int i = 0; i < pts->size(); i++)
	{
        pc.push_back(cv::Vec6f(pts->at(i).x, pts->at(i).y, pts->at(i).z, 
			pts->at(i).normal_x, pts->at(i).normal_y, pts->at(i).normal_z));
	}
	return pc;
}

std::vector<Polygon> MeshGeneration::convertMeshToManagedMesh(pcl::PolygonMesh mesh)
{
    std::vector<Polygon> polys = std::vector<Polygon>();
    pcl::PointCloud<pcl::PointXYZ> rebuiltCloud; 
    pcl::fromROSMsg(mesh.cloud, rebuiltCloud); 
    for(unsigned int i = 0; i < mesh.polygons.size(); i++)
    {
        pcl::PointXYZ a = rebuiltCloud[mesh.polygons[i].vertices[0]];
        pcl::PointXYZ b = rebuiltCloud[mesh.polygons[i].vertices[1]];
        pcl::PointXYZ c = rebuiltCloud[mesh.polygons[i].vertices[2]];
        polys.push_back(Polygon(Point3d(a.x, a.y, a.z),
            Point3d(b.x, b.y, b.z),
            Point3d(c.x, c.y, c.z)));
    }
    return polys;
}

std::vector<Point3d> MeshGeneration::convertPointCloudToManagedPointCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    std::vector<Point3d> polys = std::vector<Point3d>();
    for(unsigned int i = 0; i < cloud->size(); i++)
    {
        polys.push_back(*(Point3d*)(&cloud->at(i)));
    }
    return polys;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr MeshGeneration::convertManagedPointCloudToPointCloud(std::vector<Point3d>& pointCloud)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ> ());
    for(unsigned int i = 0; i < pointCloud.size(); i++)
        cloud->push_back(pcl::PointXYZ((float)pointCloud[i].X, (float)pointCloud[i].Y, (float)pointCloud[i].Z));
    return cloud;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr MeshGeneration::loadPointCloudObj(std::string filename)
{
    FILE * pFile;
    long lSize;
    char * buffer;
    size_t result;
    pcl::PointCloud<pcl::PointXYZ> cloud;
    auto file = filename.c_str();
    pFile = fopen ( file , "rb" );
    if (pFile==NULL) {fputs ("File error",stderr); exit (1);}

    // obtain file size:
    fseek (pFile , 0 , SEEK_END);
    lSize = ftell (pFile);
    rewind (pFile);

    // allocate memory to contain the whole file:
    buffer = (char*) malloc (sizeof(char)*lSize);
    if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

    // copy the file into the buffer:
    result = fread (buffer,1,lSize,pFile);
    if (result != lSize) {fputs ("Reading error",stderr); exit (3);}

    /* the whole file is now loaded in the memory buffer. */

    char * pch;
    pch = strtok (buffer,"\n");
    while (pch != NULL)
    {
        std::string line = std::string(pch);
        if(line.size() > 1 && (line[0]) == 'v' && (line[1]) == ' ')
        {
            std::vector<std::string> vec = split(line, ' ');
            cloud.push_back(pcl::PointXYZ((float)::atof(vec[1].c_str()), 
                (float)::atof(vec[2].c_str()),
                (float)::atof(vec[3].c_str())));
        }
        pch = strtok (NULL,"\n");
    }

    // terminate
    fclose (pFile);
    free (buffer);
    return cloud.makeShared();
}

pcl::PointCloud<pcl::PointNormal> MeshGeneration::loadPointCloudNormalsObj(std::string filename)
{
    FILE * pFile;
    long lSize;
    char * buffer;
    size_t result;
    pcl::PointCloud<pcl::PointNormal> cloud;
    auto file = filename.c_str();
    pFile = fopen ( file , "rb" );
    if (pFile==NULL) {fputs ("File error",stderr); exit (1);}

    // obtain file size:
    fseek (pFile , 0 , SEEK_END);
    lSize = ftell (pFile);
    rewind (pFile);

    // allocate memory to contain the whole file:
    buffer = (char*) malloc (sizeof(char)*lSize);
    if (buffer == NULL) {fputs ("Memory error",stderr); exit (2);}

    // copy the file into the buffer:
    result = fread (buffer,1,lSize,pFile);
    if (result != lSize) {fputs ("Reading error",stderr); exit (3);}

    /* the whole file is now loaded in the memory buffer. */

    char * pch;
    pch = strtok (buffer,"\n");
	pcl::PointNormal normal;
    while (pch != NULL)
    {
        std::string line = std::string(pch);
        if(line.size() > 1 && (line[0]) == 'v' && (line[1]) == 'n' && (line[2]) == ' ')
        {
			normal = pcl::PointNormal();
            std::vector<std::string> vec = split(line, ' ');
			normal.normal_x = (float)::atof(vec[1].c_str());
			normal.normal_y = (float)::atof(vec[2].c_str());
			normal.normal_z = (float)::atof(vec[3].c_str());
        }
        if(line.size() > 1 && (line[0]) == 'v' && (line[1]) == ' ')
        {
            std::vector<std::string> vec = split(line, ' ');
			normal.x = (float)::atof(vec[1].c_str());
			normal.y = (float)::atof(vec[2].c_str());
			normal.z = (float)::atof(vec[3].c_str());
            cloud.push_back(normal);
        }
        pch = strtok (NULL,"\n");
    }

    // terminate
    fclose (pFile);
    free (buffer);
    return cloud;
}

bool MeshGeneration::savePointCloudPLY(std::string filename, std::vector<cv::Point3d>& pts)
{
	pcl::io::savePLYFile(filename, convertCloudToPCL(pts));
	return true;
}

bool MeshGeneration::savePointCloudOBJ(std::string filename, pcl::PointCloud<pcl::PointXYZ>& points)
{
    // Open output file and create header.

    FILE* fout = fopen(filename.c_str(), "w");
    fprintf(fout, "#OBJ\n");
    fprintf(fout, "#Vertices: %i\n", points.size());
    
    for (unsigned int i = 0; i < points.size(); i++)
    {
        pcl::PointXYZ point = points[i];
        fprintf(fout, "v %f %f %f\n", point.x, point.y, point.z);
    }

    fclose(fout);

    // Return without errors.
    return true;
}

bool MeshGeneration::savePointCloudOBJ(std::string filename, pcl::PointCloud<pcl::PointNormal>& points)
{
    // Open output file and create header.

    FILE* fout = fopen(filename.c_str(), "w");
    fprintf(fout, "#OBJ\n");
    fprintf(fout, "#Vertices: %i\n", points.size());
    
    for (unsigned int i = 0; i < points.size(); i++)
    {
        pcl::PointNormal point = points[i];
        fprintf(fout, "vn %f %f %f\n", point.normal_x, point.normal_y, point.normal_z);
        fprintf(fout, "v %f %f %f\n", point.x, point.y, point.z);
    }

    fclose(fout);

    // Return without errors.
    return true;
}

bool MeshGeneration::savePointCloudOBJ(std::string filename, pcl::PolygonMesh& mesh)
{
    // Open output file and create header.

    pcl::PointCloud<pcl::PointXYZ> cloud; 
    pcl::fromROSMsg(mesh.cloud, cloud); 

    FILE* fout = fopen(filename.c_str(), "w");
    fprintf(fout, "#OBJ\n");
    fprintf(fout, "#Vertices: %i\n", cloud.size());
    
    for (unsigned int i = 0; i < cloud.size(); i++)
    {
        pcl::PointXYZ point = cloud[i];
        fprintf(fout, "v %f %f %f\n", point.x, point.y, point.z);
    }
    /*for (unsigned int i = 0; i < cloud.size(); i++)
    {
        pcl::PointNormal point = cloud[i];
        fprintf(fout, "vn %f %f %f\n", point.normal_x, point.normal_y, point.normal_z);
    }*/
    for(unsigned int i = 0; i < mesh.polygons.size(); i++)
    {
        fprintf(fout, "f %i %i %i\n", 
            mesh.polygons[i].vertices[0],
            mesh.polygons[i].vertices[1],
            mesh.polygons[i].vertices[2]);
    }

    fclose(fout);

    // Return without errors.
    return true;
}

void MeshGeneration::convertPointCloudToMesh(std::string filename, std::string outputFilename)
{
	pcl::io::savePLYFile(outputFilename, generateMesh(loadPointCloudObj(filename)));
}

pcl::PolygonMesh MeshGeneration::generateMesh(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{   
    return poissonReconstruction(calculateSmoothNormals(cloud));
}

void MeshGeneration::combineAndSavePLY(std::string filename, std::vector<std::vector<cv::Point3d>>& points)
{
	pcl::PointCloud<pcl::PointNormal> cloud;
    for (std::vector<std::vector<cv::Point3d>>::iterator pair = begin(points); pair != end(points); pair++)
	{
		std::vector<cv::Point3d> pts = *pair;
		pcl::PointCloud<pcl::PointXYZ> pclPoints = convertCloudToPCL(pts);
		cloud += *calculateSmoothNormals(pclPoints.makeShared());
	}
	pcl::io::savePLYFile(filename, cloud);
}

pcl::PolygonMesh MeshGeneration::poissonReconstruction(pcl::PointCloud<pcl::PointNormal>::Ptr cloud)
{
    cout << "Generating Poisson Mesh" << endl;
    pcl::PolygonMesh mesh;
    pcl::Poisson<pcl::PointNormal> poisson;
    poisson.setInputCloud(cloud);
	/*
	Unsafe parameters
	*/
    /*poisson.setDepth(12);
    poisson.setSolverDivide(8);
    poisson.setIsoDivide(8);
    poisson.setSamplesPerNode(2);*/
    /*
    Safe parameters
	*/
    poisson.setDepth(10);
    poisson.setSolverDivide(7);
    poisson.setIsoDivide(8);
    poisson.setSamplesPerNode(2);
    poisson.reconstruct(mesh);
    return mesh;
}

pcl::PointCloud<pcl::PointNormal>::Ptr MeshGeneration::calculateSmoothNormals(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

    pcl::PointCloud<pcl::PointXYZ>::Ptr mls_points (new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal> ());
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud_smoothed_normals (new pcl::PointCloud<pcl::PointNormal> ());

    // Init object (second point type is for the normals, even if unused)
    pcl::MovingLeastSquaresOMP<pcl::PointXYZ, pcl::PointXYZ> mls;
    pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> ne;

    // Set parameters
    mls.setSearchRadius(3);
    mls.setInputCloud (cloud);
    mls.setSearchMethod (tree);
    mls.setPolynomialFit (true);
    mls.setPolynomialOrder (3);
    //mls.setUpsamplingMethod (pcl::MovingLeastSquares<pcl::PointXYZ, pcl::PointXYZ>::SAMPLE_LOCAL_PLANE);
    //mls.setUpsamplingRadius (1);
    //mls.setUpsamplingStepSize (0.5);
    mls.setNumberOfThreads(8);
    ne.setRadiusSearch (3);
    ne.setNumberOfThreads (8);
	ne.setViewPoint(0,0,-1000);

    // Reconstruct
    mls.process (*mls_points);

    ne.setInputCloud (mls_points);
    ne.compute (*cloud_normals);

    pcl::concatenateFields (*mls_points, *cloud_normals, *cloud_smoothed_normals);

	return cloud_smoothed_normals;
}

void MeshGeneration::flipNormals(pcl::PointCloud<pcl::PointNormal>::Ptr cloud_normals)
{
    for (size_t i = 0; i < cloud_normals->size (); ++i)
    {
        cloud_normals->points[i].normal_x *= -1;
        cloud_normals->points[i].normal_y *= -1;
        cloud_normals->points[i].normal_z *= -1;
    }
}

std::vector<Polygon> MeshGeneration::generateConcaveHull(double alpha, pcl::PointCloud<pcl::PointXYZ>::Ptr pts)
{
    pcl::ConcaveHull<pcl::PointXYZ> concaveHull;
    
    concaveHull.setInputCloud(pts);

    pcl::PolygonMesh mesh;
	concaveHull.setAlpha(alpha);
    concaveHull.setDimension(3);
    concaveHull.reconstruct(mesh);
    
    return convertMeshToManagedMesh(mesh);
}

pcl::PointCloud<pcl::PointNormal>::Ptr MeshGeneration::alignPointClouds(pcl::PointCloud<pcl::PointNormal>::Ptr a, 
        pcl::PointCloud<pcl::PointNormal>::Ptr b)
{
	pcl::IterativeClosestPointNonLinear<pcl::PointNormal, pcl::PointNormal> icp;

    boost::shared_ptr<pcl::WarpPointRigid6D<pcl::PointNormal, pcl::PointNormal> > warp_fcn 
                (new pcl::WarpPointRigid6D<pcl::PointNormal, pcl::PointNormal>); 

    // Create a TransformationEstimationLM object, and set the warp to it 
    boost::shared_ptr<pcl::registration::TransformationEstimationLM<pcl::PointNormal, pcl::PointNormal> > te (new pcl::registration::TransformationEstimationLM<pcl::PointNormal, pcl::PointNormal>); 
    te->setWarpFunction (warp_fcn); 

    // Pass the TransformationEstimation objec to the ICP algorithm 
    icp.setTransformationEstimation (te); 
	
    icp.setInputTarget(a);
    icp.setInputCloud(b);
    icp.setMaxCorrespondenceDistance(100);
    // Set the maximum number of iterations (criterion 1)
    icp.setMaximumIterations (2);
    // Set the transformation epsilon (criterion 2)
    icp.setTransformationEpsilon (10);
    icp.setRANSACIterations(2);
    icp.setRANSACOutlierRejectionThreshold(100);
    // Set the euclidean distance difference epsilon (criterion 3)
    icp.setEuclideanFitnessEpsilon (0.01);
    pcl::PointCloud<pcl::PointNormal> Final;
    icp.align(Final);
    std::cout << "has converged:" << icp.hasConverged() << " score: " << icp.getFitnessScore() << std::endl;
    std::cout << icp.getFinalTransformation() << std::endl;
    
    pcl::transformPointCloud(*b, Final, icp.getFinalTransformation());

    return Final.makeShared();
}
}