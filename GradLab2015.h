﻿template <class  Interface>
inline  void  safeRelease(Interface * & pInterfaceToRelease)
{
	if (pInterfaceToRelease != NULL) {
		pInterfaceToRelease->Release();
		pInterfaceToRelease = NULL;
	}
}

int initializeSensor(void);
int livePointCloud(int);
int shutDownSensor(void);
int printMenu(void);
int mergeClouds();


IKinectSensor * pSensor; //Sensor Instance
ICoordinateMapper * pCoordinateMapper;
IColorFrameSource * pColorSource;
IDepthFrameSource * pDepthSource;
IColorFrameReader * pColorReader;
IDepthFrameReader * pDepthReader;
IFrameDescription * pColorDescription;
IFrameDescription * pDepthDescription;
pcl::visualization::CloudViewer viewer("Point Cloud Viewer");


HRESULT  hResult = S_OK;

int  colorWidth = 0;
int  colorHeight = 0;
int  depthWidth = 0;
int  depthHeight = 0;

//These values will be fine tuned to pull out the background.
float zMin = 0.0;
float zMax = 3.0;

pcl::PointCloud <pcl::PointXYZRGB> ::Ptr pointCloud(new  pcl::PointCloud <pcl::PointXYZRGB>());

